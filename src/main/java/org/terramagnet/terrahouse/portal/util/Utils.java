package org.terramagnet.terrahouse.portal.util;

import java.io.UnsupportedEncodingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.commons.codec.binary.Base64;
import org.terramagnet.terrahouse.portal.module.UnauthenticationException;
import org.terramagnet.terrahouse.portal.module.user.UserInfo;
import org.terramagnet.terrahouse.portal.web.interceptor.SaveRequestInterceptor;

/**
 *
 * @author lip
 */
public class Utils {

    public static final String SESSIONKEY_SMSCODE = "org.terramagnet.terrahouse.portal.smscode";
    public static final String SESSIONKEY_PRINCIPAL = "principal";
    private static final Base64 base64 = new Base64(true);

    public static Base64 getBase64() {
        return base64;
    }

    private static String hashRequestPage(HttpServletRequest request) {
        String reqUri = request.getRequestURI();
        String query = request.getQueryString();
        if (query != null) {
            reqUri += "?" + query;
        }
        String targetPage = null;
        try {
            targetPage = base64.encodeAsString(reqUri.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException ex) {
            //this does not happen
        }
        return targetPage;
    }

    public static String retrieve(String targetPage) {
        byte[] decode = base64.decode(targetPage);
        try {
            String requestUri = new String(decode, "UTF-8");
            int i = requestUri.indexOf("/", 1);
            return requestUri.substring(i);
        } catch (UnsupportedEncodingException ex) {
            //this does not happen
            return null;
        }
    }

    public static void saveRequest(HttpServletRequest request) {
        request.getSession().setAttribute(SaveRequestInterceptor.LAST_PAGE, Utils.hashRequestPage(request));
    }

    public static String retrieveSavedRequest(HttpSession session) {
        String HashedlastPage = (String) session.getAttribute(SaveRequestInterceptor.LAST_PAGE);
        if (HashedlastPage == null) {
            return "/home";
        } else {
            return retrieve(HashedlastPage);
        }
    }

    public static boolean clearSmscode(int smscode, HttpSession session) {
        Integer code = (Integer) session.getAttribute(SESSIONKEY_SMSCODE);
        if (code != null && code == smscode) {
            session.removeAttribute(SESSIONKEY_SMSCODE);
            return true;
        } else {
            return false;
        }
    }

    public static UserInfo getPrincipal(HttpSession session) {
        return (UserInfo) session.getAttribute(SESSIONKEY_PRINCIPAL);
    }

    /**
     * 字符是否是汉字字符.
     *
     * @param c 待判断的字符
     * @return true-汉字 false-不是汉字
     */
    public static boolean isChinese(char c) {
        Character.UnicodeBlock ub = Character.UnicodeBlock.of(c);
        return ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS
                || ub == Character.UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS
                || ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A
                || ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_B
                || ub == Character.UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION
                || ub == Character.UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS
                || ub == Character.UnicodeBlock.GENERAL_PUNCTUATION;
    }

    /**
     * 判断字符串中是否包含汉字.
     *
     * @param strName 待判断的字符串
     * @return true-包含汉字 false-不包含汉字
     */
    public static boolean isChinese(String strName) {
        char[] ch = strName.toCharArray();
        for (int i = 0; i < ch.length; i++) {
            char c = ch[i];
            if (isChinese(c)) {
                return true;
            }
        }
        return false;
    }
}
