package org.terramagnet.terrahouse.portal.web;

/**
 * 封装页面ajax调用的返回值.
 * @author lip
 */
public class AjaxResponse {
    private int code;
    private String message;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    
}
