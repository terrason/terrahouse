/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.terramagnet.terrahouse.portal.web;

import java.util.Collection;
import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.terramagnet.terrahouse.portal.module.block.BlockInfo;
import org.terramagnet.terrahouse.portal.module.block.BlockInfoService;
import org.terramagnet.terrahouse.portal.module.block.Polity;
import org.terramagnet.terrahouse.portal.module.user.UserInfo;
import org.terramagnet.terrahouse.portal.util.Utils;

/**
 * 街区.
 *
 * @author lip
 */
@Controller
public class BlockController {

    @Resource
    private BlockInfoService blockInfoService;

    @RequestMapping(value = "/block", method = RequestMethod.GET)
    public String index(Model model) {
        Collection<Polity> polities = blockInfoService.findAllPolities();
        model.addAttribute("polities", polities);
        return "block/home";
    }

    @RequestMapping(value = "/block/{id}", method = RequestMethod.GET)
    public String block(@PathVariable long id, Model model) {
        Collection<Polity> polities = blockInfoService.findAllPolities();
        model.addAttribute("polities", polities);
        BlockInfo block = blockInfoService.findEntity(id);
        model.addAttribute("entity", block);
        return "block/block";
    }

    @RequestMapping(value = "/block/{id}/describe", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResponse describe(@PathVariable long id,@RequestParam String description,@RequestParam String email,HttpSession session) {
        AjaxResponse response = new AjaxResponse();
        UserInfo principal = Utils.getPrincipal(session);
        Long principalId=null;
	if (principal != null) {
	    principalId=principal.getId();
	}
	blockInfoService.createDescription(description,email,id,principalId);
        return response;
    }
}
