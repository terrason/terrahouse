/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.terramagnet.terrahouse.portal.web;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import javax.annotation.Resource;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.terramagnet.terrahouse.portal.module.attachment.AttachmentService;
import org.terramagnet.terrahouse.portal.module.attachment.PathInfo;

/**
 * 文件上传.
 *
 * @author lip
 */
@Controller
public class UpfileController {

    private final Logger logger = LoggerFactory.getLogger(UpfileController.class);
    @Resource
    private AttachmentService attachmentService;

    @RequestMapping(value = "/upfile", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResponse upfile(@RequestParam("file") MultipartFile file) {
        AjaxResponse response = new AjaxResponse();
        if (file.isEmpty()) {
            response.setCode(HttpStatus.NO_CONTENT.value());
            response.setMessage("没有上传任何文件");
            return response;
        }
        PathInfo pathInfo = attachmentService.createPath(file.getOriginalFilename());
        File directory = pathInfo.getFile();
        String webPath = pathInfo.getUrl();
        if (!directory.exists()) {
            directory.mkdirs();
        }
        
        InputStream inputStream = null;
        try {
            inputStream = file.getInputStream();
            File repositoryFile = new File(directory, file.getOriginalFilename());
            repositoryFile.createNewFile();
            FileUtils.copyInputStreamToFile(inputStream, repositoryFile);
            response.setMessage(webPath+"/"+file.getOriginalFilename());
            return response;
        } catch (IOException e) {
            response.setCode(HttpStatus.SERVICE_UNAVAILABLE.value());
            response.setMessage(e.getMessage());
            return response;
        } finally {
            try {
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (IOException ex) {
                logger.warn("未知的错误", ex);
            }
        }
    }
}
