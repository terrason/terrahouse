/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.terramagnet.terrahouse.portal.web;

import java.io.IOException;
import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.terramagnet.terrahouse.portal.module.sms.SmsService;
import org.terramagnet.terrahouse.portal.util.Utils;

/**
 * 短信验证码.
 *
 * @author lip
 */
@Controller
public class SmscodeConntroller {

    private final Logger logger = LoggerFactory.getLogger(SmscodeConntroller.class);
    @Resource
    private SmsService smsService;

    @RequestMapping(value = "/smscode/{mobile}", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResponse signIn(@PathVariable String mobile,@RequestParam String name, HttpSession session) {
            AjaxResponse response  = new AjaxResponse();
	try {
	    int smscode = smsService.sendSms(mobile);
	    session.setAttribute(Utils.SESSIONKEY_SMSCODE, smscode);
	    response.setMessage(String.valueOf(smscode));
	} catch (IOException ex) {
            logger.error("发送短信验证码失败",ex);
	    response.setCode(HttpStatus.BAD_GATEWAY.value());
	    response.setMessage("网络异常");
	}
	return response;
    }
}
