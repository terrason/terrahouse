/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.terramagnet.terrahouse.portal.web;

import javax.annotation.Resource;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 释放缓存接口.
 *
 * @author lip
 */
@Controller
public class CacheEvictionController {

    @Resource
    private CacheManager cacheManager;

    @RequestMapping(value = "/cache/evicte/{cache}")
    @ResponseBody
    public AjaxResponse evicteCache(@PathVariable String cache, @RequestParam(required = false) String key) {
        AjaxResponse response = new AjaxResponse();
        Cache c = cacheManager.getCache(cache);
        if (c == null) {
            response.setCode(HttpStatus.NOT_FOUND.value());
            response.setMessage("当前不存在缓存：" + cache);
            return response;
        }
        if (StringUtils.hasText(key)) {
            c.evict(key);
        } else {
            c.clear();
        }
        return response;
    }

}
