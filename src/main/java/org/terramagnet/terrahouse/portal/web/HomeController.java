package org.terramagnet.terrahouse.portal.web;


import java.util.Collection;
import javax.annotation.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.terramagnet.terrahouse.portal.module.block.BlockInfo;
import org.terramagnet.terrahouse.portal.module.block.BlockInfoService;
import org.terramagnet.terrahouse.portal.module.block.Polity;

/**
 * 首页.
 *
 * @author lip
 */
@Controller
public class HomeController {
    @Resource
    private BlockInfoService blockInfoService;

    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public String home(Model model) {
        Collection<Polity> polities = blockInfoService.findAllPolities();
        model.addAttribute("polities", polities);
        BlockInfo[] blocks = blockInfoService.findTopBlocks();
        model.addAttribute("blocks", blocks);
        return "home/home";
    }
}
