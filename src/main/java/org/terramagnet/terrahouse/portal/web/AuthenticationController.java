package org.terramagnet.terrahouse.portal.web;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.terramagnet.terrahouse.portal.module.mail.MailFacade;
import org.terramagnet.terrahouse.portal.module.user.UserInfo;
import org.terramagnet.terrahouse.portal.module.user.UserInfoService;
import org.terramagnet.terrahouse.portal.util.Utils;

/**
 * 登陆认证&注册.
 *
 * @author lip
 */
@Controller
public class AuthenticationController {

    @Resource
    private UserInfoService userInfoService;
    @Resource
    private MailFacade mailFacade;

    @RequestMapping(value = "/authc/signIn/validate/email", method = RequestMethod.GET)
    @ResponseBody
    public boolean validateSignInEmail(@RequestParam String email) {
	return userInfoService.findEnityByEmail(email) != null;
    }

    @RequestMapping(value = "/authc/signUp/validate/email", method = RequestMethod.GET)
    @ResponseBody
    public boolean validateSignUpEmail(@RequestParam String email) {
	return userInfoService.findEnityByEmail(email) == null;
    }

    @RequestMapping(value = "/authc/signIn", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResponse signIn(@RequestParam String email, @RequestParam String password, HttpSession session) {
	AjaxResponse response = new AjaxResponse();
	UserInfo entity = userInfoService.findEnityByEmail(email);
	if (entity == null) {
	    response.setCode(HttpStatus.NOT_FOUND.value());
	    response.setMessage("用户名或密码输入错误");
	    return response;
	}
	String hashedPassword = DigestUtils.md5Hex(password).toUpperCase();
	if (!hashedPassword.equals(entity.getPassword().toUpperCase())) {
	    response.setCode(HttpStatus.BAD_REQUEST.value());
	    response.setMessage("用户名或密码输入错误");
	    return response;
	}
	if (entity.getStatus() == 1) {
	    response.setCode(HttpStatus.PRECONDITION_FAILED.value());
	    response.setMessage(email);
	    return response;
	}
	doSignIn(session, entity);
	return response;
    }

    @RequestMapping(value = "/authc/signUp", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResponse signUp(@RequestParam String email, @RequestParam String password, HttpSession session) throws Exception {
	AjaxResponse response = new AjaxResponse();
	UserInfo entity = userInfoService.signUp(email, password);
//        注册不发送激活邮件，直接登录。
	doSignIn(session, entity);
	return response;
    }

    @RequestMapping(value = "/authc/signUp/mail", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResponse resendSignUpMail(@RequestParam String email) throws Exception {
	AjaxResponse response = new AjaxResponse();
	mailFacade.sendWellcomeMail(email);
	return response;
    }

    @RequestMapping(value = "/authc/activate/{key}", method = RequestMethod.GET)
    public String activate(@PathVariable String key, HttpSession session) throws Exception {
	String email = Utils.retrieve(key);
	userInfoService.activate(email);
	String lastPage = Utils.retrieveSavedRequest(session);
	doSignIn(session, userInfoService.findEnityByEmail(email));
	return "redirect:" + lastPage;
    }

    @RequestMapping(value = "/authc/passwd/mail", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResponse sendPasswordMail(@RequestParam String email) throws Exception {
	AjaxResponse response = new AjaxResponse();
	UserInfo entity = userInfoService.findEnityByEmail(email);
	if (entity == null) {
	    response.setCode(HttpStatus.NOT_FOUND.value());
	    response.setMessage("注册邮箱不正确");
	    return response;
	}
	mailFacade.sendPasswordMail(email);
	return response;
    }

    @RequestMapping(value = "/authc/passwd/{key}", method = RequestMethod.GET)
    public String showPasswd(@PathVariable String key, Model model) throws Exception {
	String email = Utils.retrieve(key);
	model.addAttribute("email", email);
	return "home/passwd";
    }

    @RequestMapping(value = "/authc/passwd", method = RequestMethod.POST)
    public String passwd(@RequestParam String email, @RequestParam String password, HttpSession session) throws Exception {
	userInfoService.passwd(email, password);
	UserInfo entity = (UserInfo) session.getAttribute(Utils.SESSIONKEY_PRINCIPAL);
	if (entity != null) {
	    entity.setPassword(DigestUtils.md2Hex(password).toUpperCase());
	} else {
	    doSignIn(session, userInfoService.findEnityByEmail(email));
	}
	String lastPage = Utils.retrieveSavedRequest(session);
	return "redirect:" + lastPage;
    }

    @RequestMapping(value = "/authc/signOut", method = RequestMethod.POST)
    public String signOut(HttpSession session) {
	session.invalidate();
	return "redirect:/home";
    }

    @RequestMapping(value = "/authc/principal", method = RequestMethod.GET)
    public String pricipal(HttpSession session) {
	return "home/principal";
    }

    protected void doSignIn(HttpSession session, UserInfo principal) {
	session.setAttribute(Utils.SESSIONKEY_PRINCIPAL, principal);
    }
}
