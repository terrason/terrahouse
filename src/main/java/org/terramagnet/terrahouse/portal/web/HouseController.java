package org.terramagnet.terrahouse.portal.web;

import java.util.Arrays;
import java.util.List;
import javax.annotation.Resource;
import javax.mail.MessagingException;
import javax.servlet.http.HttpSession;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.terramagnet.terrahouse.portal.module.Pager;
import org.terramagnet.terrahouse.portal.module.UnauthenticationException;
import org.terramagnet.terrahouse.portal.module.house.Comment;
import org.terramagnet.terrahouse.portal.module.house.CustomerPreorder;
import org.terramagnet.terrahouse.portal.module.house.HouseInfo;
import org.terramagnet.terrahouse.portal.module.house.HouseInfoService;
import org.terramagnet.terrahouse.portal.module.mail.MailFacade;
import org.terramagnet.terrahouse.portal.module.user.UserInfo;
import org.terramagnet.terrahouse.portal.module.user.UserInfoService;
import org.terramagnet.terrahouse.portal.util.Utils;

/**
 *
 * @author lip
 */
@Controller
public class HouseController {

    @Resource
    private HouseInfoService houseInfoService;
    @Resource
    private UserInfoService userInfoService;
    @Resource
    private MailFacade mailFacade;

    @RequestMapping(value = "/house/pulish/home", method = RequestMethod.GET)
    public String publish() {
	return "house/publish";
    }

    @RequestMapping(value = "/house/pulish", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResponse publish(HouseInfo house, @RequestParam int smscode, HttpSession session) {
	AjaxResponse response = new AjaxResponse();
	if (!Utils.clearSmscode(smscode, session)) {
	    response.setCode(HttpStatus.PRECONDITION_FAILED.value());
	    response.setMessage("短信验证码不正确");
	    return response;
	}
	String mobile = house.getUserPhone();
	UserInfo principal = Utils.getPrincipal(session);
	if (principal == null) {
	    throw new UnauthenticationException();
	}
	boolean principalNeedUpdate = false;
	if (!StringUtils.hasText(principal.getMobile())) {
	    principal.setMobile(mobile);
	    principalNeedUpdate = true;
	}
	if (!StringUtils.hasText(principal.getUsername()) || principal.getUsername().equalsIgnoreCase(principal.getEmail())) {
	    principal.setUsername(house.getUserName());
	    principalNeedUpdate = true;
	}
	if (principalNeedUpdate) {
	    userInfoService.updateAuthzInfo(principal);
	}
	String[] snapshots = house.getSnapshots();
	String[] pics = snapshots == null ? new String[20] : Arrays.copyOf(snapshots, 20);
	house.setSnapshots(pics);
	houseInfoService.createHouse(house, principal);
	return response;
    }

    @RequestMapping(value = "/house/{id}", method = RequestMethod.GET)
    public String house(@PathVariable long id, Model model) {
	HouseInfo entity = houseInfoService.findEntityById(id);
	model.addAttribute("house", entity);
	return "house/house";
    }

    @RequestMapping(value = "/house/{id}/fork", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResponse fork(@PathVariable long id, @RequestParam boolean like) {
	houseInfoService.fork(id, like);
	return new AjaxResponse();
    }

    @RequestMapping(value = "/house/subscribe", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResponse subscribe(CustomerPreorder subscription, @RequestParam int smscode, HttpSession session) {
	AjaxResponse response = new AjaxResponse();
	if (!Utils.clearSmscode(smscode, session)) {
	    response.setCode(HttpStatus.PRECONDITION_FAILED.value());
	    response.setMessage("短信验证码不正确");
	    return response;
	}
	String mobile = subscription.getCustomerPhone();
	UserInfo principal = Utils.getPrincipal(session);
	if (principal == null) {
	    throw new UnauthenticationException();
	}
	boolean principalNeedUpdate = false;
	if (!StringUtils.hasText(principal.getMobile())) {
	    principal.setMobile(mobile);
	    principalNeedUpdate = true;
	}
	if (!StringUtils.hasText(principal.getUsername()) || principal.getUsername().equalsIgnoreCase(principal.getEmail())) {
	    principal.setUsername(subscription.getCustomerName());
	    principalNeedUpdate = true;
	}
	if (principalNeedUpdate) {
	    userInfoService.updateAuthzInfo(principal);
	}
	subscription.setCustomerId(principal.getId());
	houseInfoService.subscribe(subscription);
	return response;
    }

    @RequestMapping(value = "/house/hothouse", method = RequestMethod.GET)
    @ResponseBody
    public List<HouseInfo> hotHouse(Pager pager,@RequestParam(required = false) String polity) {
	return houseInfoService.findEntities(polity,pager.getFirstIndex(), pager.getPageSize());
    }

    @RequestMapping(value = "/house/{house}/comment", method = RequestMethod.GET)
    public String comment(@PathVariable int house, Pager pager, Model model) {
	List<Comment> comments = houseInfoService.findComments(house, pager.getFirstIndex(), pager.getPageSize());
	model.addAttribute("comments", comments);
	return "house/comment";
    }

    @RequestMapping(value = "/house/{house}/comment", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResponse comment(Comment comment, HttpSession session) {
	AjaxResponse response = new AjaxResponse();
	UserInfo principal = Utils.getPrincipal(session);
	if (principal != null) {
	    comment.setCustomerId(principal.getId());
	    comment.setCustomerName(principal.getUsername());
	}
	houseInfoService.comment(comment);
	return response;
    }

    @RequestMapping(value = "/house/{houseId}/share", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResponse share(@PathVariable long houseId, @RequestParam String[] email) throws MessagingException {
	AjaxResponse response = new AjaxResponse();
	mailFacade.sendShareMail(houseId, email);
	return response;
    }

}
