package org.terramagnet.terrahouse.portal.web;

import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.terramagnet.terrahouse.portal.module.axis.Axis;
import org.terramagnet.terrahouse.portal.module.axis.AxisService;

/**
 *
 * @author lip
 */
@Controller
public class AxisController {

    @Resource
    private AxisService axisService;

    @RequestMapping(value = "/axis", method = RequestMethod.GET)
    @ResponseBody
    public List<Axis> axis() {
        return axisService.findEntities();
    }
}
