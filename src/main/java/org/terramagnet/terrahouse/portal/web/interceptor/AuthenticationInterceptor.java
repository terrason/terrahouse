package org.terramagnet.terrahouse.portal.web.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.util.WebUtils;
import org.terramagnet.terrahouse.portal.module.authorization.Principal;
import org.terramagnet.terrahouse.portal.util.Utils;

/**
 * @deprecated 
 * @author lip
 */
public class AuthenticationInterceptor extends HandlerInterceptorAdapter {

    public static final String USER_SESSION_KEY = "principal";

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
//        Principal principal = (Principal) WebUtils.getSessionAttribute(request, USER_SESSION_KEY);
//        if (principal != null) {
            return true;
//        } else {
//            response.setCharacterEncoding("UTF-8");
//            response.setStatus(401);
//            String targetPage = Utils.hashRequestPage(request);
//            response.sendRedirect(request.getContextPath() + "/common/sessionout.jsp" + "?targetPage=" + targetPage);
//            return false;
//        }
    }

}
