/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.terramagnet.terrahouse.portal.web;

import java.security.Principal;
import javax.naming.AuthenticationException;
import javax.servlet.http.HttpServletRequest;
import org.springframework.web.util.WebUtils;

public abstract class ControllerAdapter {

    public static final String KEY_PRINCIPAL = "principal";

    protected Principal getCurrentPrincipal(HttpServletRequest request) throws AuthenticationException{
        Principal principal=(Principal)WebUtils.getSessionAttribute(request, KEY_PRINCIPAL);
        if(principal==null){
            throw new AuthenticationException("尚未登录");
        }
        return principal;
    }
    
    
}
