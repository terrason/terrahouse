package org.terramagnet.terrahouse.portal.web;

import java.util.Collection;
import java.util.List;
import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.terramagnet.terrahouse.portal.module.Pager;
import org.terramagnet.terrahouse.portal.module.block.BlockInfoService;
import org.terramagnet.terrahouse.portal.module.block.Polity;
import org.terramagnet.terrahouse.portal.module.house.HouseInfo;
import org.terramagnet.terrahouse.portal.module.house.HouseInfoService;
import org.terramagnet.terrahouse.portal.module.house.HouseRecommend;
import org.terramagnet.terrahouse.portal.module.house.HouseRecommendService;
import org.terramagnet.terrahouse.portal.module.user.UserInfo;
import org.terramagnet.terrahouse.portal.util.Utils;

/**
 *
 * @author lip
 */
@Controller
public class SearchController {

    @Resource
    private HouseInfoService houseInfoService;
    @Resource
    private BlockInfoService blockInfoService;
    @Resource
    private HouseRecommendService houseRecommendService;

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public String search(Model model) {
        Collection<Polity> polities = blockInfoService.findAllPolities();
        model.addAttribute("polities", polities);
        return "house/search";
    }

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public String search(@RequestParam(required = false) String keyword,
            @RequestParam(required = false) Integer priceFrom,
            @RequestParam(required = false) Integer priceTo,
            @RequestParam(required = false) Integer areaFrom,
            @RequestParam(required = false) Integer areaTo,
            @RequestParam(required = false) Integer ageFrom,
            @RequestParam(required = false) Integer ageTo,
            @RequestParam(required = false) String polity,
            @RequestParam(required = false) Integer bedroom,
            @RequestParam(required = false) String decoration,
            @RequestParam(required = false) Double minX,
            @RequestParam(required = false) Double minY,
            @RequestParam(required = false) Double maxX,
            @RequestParam(required = false) Double maxY,
            @RequestParam(defaultValue = "false") boolean fiveYearsOlder,
            @RequestParam(defaultValue = "false") boolean unique,
            @RequestParam(defaultValue = "0") int orderby,
            @RequestParam(defaultValue = "false") boolean needRecord,
            Pager pager, Model model, HttpSession session) {

        List<HouseInfo> houses = houseInfoService.findEntities(keyword, priceFrom, priceTo, areaFrom, areaTo, ageFrom, ageTo, polity, bedroom, decoration,
                minX, minY, maxX, maxY,
                fiveYearsOlder, unique,
                orderby,
                pager);
        if (houses.isEmpty()) {
            pager.setCurrentPage(pager.getCurrentPage() - 1);
        }
        if (!houses.isEmpty() && needRecord && StringUtils.isNotBlank(polity)) {
            HouseRecommend recommendation = new HouseRecommend();
            UserInfo principal = Utils.getPrincipal(session);
            if (principal != null) {
                recommendation.setUserId(principal.getId());
            }

            recommendation.setPolity(polity);
            recommendation.setPriceFrom(priceFrom);
            recommendation.setPriceTo(priceTo);
            houseRecommendService.saveSearchRecord(recommendation);
        }
        
        model.addAttribute("houses", houses);
        model.addAttribute("pager", pager);
        return "house/widgets";
    }
    
    @RequestMapping(value="/search/suggest")
    @ResponseBody
    public List<String> suggest(@RequestParam String query){
        return houseInfoService.suggest(query);
    }
}
