/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.terramagnet.terrahouse.portal.web.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.terramagnet.terrahouse.portal.util.Utils;

/**
 * 保存最后一次GET的请求.
 *
 * @author terrason
 */
public class SaveRequestInterceptor extends HandlerInterceptorAdapter {

    public static final String LAST_PAGE = "org.terramagnet.terrahouse.portal.web.lastPage";

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
	Utils.saveRequest(request);
	return true;
    }

}
