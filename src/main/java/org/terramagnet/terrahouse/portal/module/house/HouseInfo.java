package org.terramagnet.terrahouse.portal.module.house;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

/**
 * 房源信息
 *
 * @author Administrator
 *
 */
public class HouseInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    private long id;
    private long userId;
    private String userName;
    private String userPhone;
    private String name;
    /**
     * 小区名称.
     */
    private String residenceName;
    private String price;
    private String address;
    private String preview;
    /**
     * 被收藏次数.
     */
    private Integer forks;
    /**
     * 面积.
     */
    private String area;
    /**
     * 行政区域名称.
     */
    private String politicalAreaName;
    /**
     * 社区名称.
     */
    private String communityName;
    /**
     * 社区ID.
     */
    private Long communityId;

    private String introduce;
    /**
     * 房屋照片.
     */
    private String[] snapshots;
    /**
     * 户型图.
     */
    private String[] layouts;
    /**
     * 居室. 室
     */
    private Integer bedroom;
    /**
     * 居室. 厅
     */
    private Integer parlor;
    /**
     * 居室. 卫
     */
    private Integer bathroom;
    /**
     * 房屋概况.
     */
    private HouseOverview overview;
    /**
     * 周边环境.
     */
    private HouseSurround surround;
    /**
     * 预约.
     */
    private Map<Date, HouseSubscription> subscription;

    /**
     * 其他信息.
     */
    private HouseExtension ext;

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the userId
     */
    public long getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(long userId) {
        this.userId = userId;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 小区名称.
     *
     * @return the residenceName
     */
    public String getResidenceName() {
        return residenceName;
    }

    /**
     * 小区名称.
     *
     * @param residenceName the residenceName to set
     */
    public void setResidenceName(String residenceName) {
        this.residenceName = residenceName;
    }

    /**
     * @return the price
     */
    public String getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(String price) {
        this.price = price;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return the preview
     */
    public String getPreview() {
        return preview;
    }

    /**
     * @param preview the preview to set
     */
    public void setPreview(String preview) {
        this.preview = preview;
    }

    /**
     * 面积.
     *
     * @return the area
     */
    public String getArea() {
        return area;
    }

    /**
     * 面积.
     *
     * @param area the area to set
     */
    public void setArea(String area) {
        this.area = area;
    }

    /**
     * 行政区域名称.
     *
     * @return the politicalAreaName
     */
    public String getPoliticalAreaName() {
        return politicalAreaName;
    }

    /**
     * 行政区域名称.
     *
     * @param politicalAreaName the politicalAreaName to set
     */
    public void setPoliticalAreaName(String politicalAreaName) {
        this.politicalAreaName = politicalAreaName;
    }

    /**
     * 社区名称.
     *
     * @return the communityName
     */
    public String getCommunityName() {
        return communityName;
    }

    /**
     * 社区名称.
     *
     * @param communityName the communityName to set
     */
    public void setCommunityName(String communityName) {
        this.communityName = communityName;
    }

    /**
     * @return the introduce
     */
    public String getIntroduce() {
        return introduce;
    }

    /**
     * @param introduce the introduce to set
     */
    public void setIntroduce(String introduce) {
        this.introduce = introduce;
    }

    /**
     * 居室. 室
     *
     * @return the bedroom
     */
    public Integer getBedroom() {
        return bedroom;
    }

    /**
     * 居室. 室
     *
     * @param bedroom the bedroom to set
     */
    public void setBedroom(Integer bedroom) {
        this.bedroom = bedroom;
    }

    /**
     * 居室. 厅
     *
     * @return the parlor
     */
    public Integer getParlor() {
        return parlor;
    }

    /**
     * 居室. 厅
     *
     * @param parlor the parlor to set
     */
    public void setParlor(Integer parlor) {
        this.parlor = parlor;
    }

    /**
     * 居室. 卫
     *
     * @return the bathroom
     */
    public Integer getBathroom() {
        return bathroom;
    }

    /**
     * 居室. 卫
     *
     * @param bathroom the bathroom to set
     */
    public void setBathroom(Integer bathroom) {
        this.bathroom = bathroom;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * 社区ID.
     *
     * @return the communityId
     */
    public Long getCommunityId() {
        return communityId;
    }

    /**
     * 社区ID.
     *
     * @param communityId the communityId to set
     */
    public void setCommunityId(Long communityId) {
        this.communityId = communityId;
    }

    /**
     * 被收藏次数.
     *
     * @return the forks
     */
    public Integer getForks() {
        return forks;
    }

    /**
     * 被收藏次数.
     *
     * @param forks the forks to set
     */
    public void setForks(Integer forks) {
        this.forks = forks;
    }

    /**
     * 房屋照片.
     *
     * @return the snapshots
     */
    public String[] getSnapshots() {
        return snapshots;
    }

    /**
     * 房屋照片.
     *
     * @param snapshots the snapshots to set
     */
    public void setSnapshots(String[] snapshots) {
        this.snapshots = snapshots;
    }

    /**
     * 户型图.
     *
     * @return the layouts
     */
    public String[] getLayouts() {
        return layouts;
    }

    /**
     * 户型图.
     *
     * @param layouts the layouts to set
     */
    public void setLayouts(String[] layouts) {
        this.layouts = layouts;
    }

    /**
     * 房屋概况.
     *
     * @return the overview
     */
    public HouseOverview getOverview() {
        return overview;
    }

    /**
     * 房屋概况.
     *
     * @param overview the overview to set
     */
    public void setOverview(HouseOverview overview) {
        this.overview = overview;
    }

    /**
     * 周边环境.
     *
     * @return the surround
     */
    public HouseSurround getSurround() {
        return surround;
    }

    /**
     * 周边环境.
     *
     * @param surround the surround to set
     */
    public void setSurround(HouseSurround surround) {
        this.surround = surround;
    }

    /**
     * 预约.
     *
     * @return the subscription
     */
    public Map<Date, HouseSubscription> getSubscription() {
        return subscription;
    }

    /**
     * 预约.
     *
     * @param subscription the subscription to set
     */
    public void setSubscription(Map<Date, HouseSubscription> subscription) {
        this.subscription = subscription;
    }

    /**
     * 其他信息.
     *
     * @return the ext
     */
    public HouseExtension getExt() {
        return ext;
    }

    /**
     * 其他信息.
     *
     * @param ext the ext to set
     */
    public void setExt(HouseExtension ext) {
        this.ext = ext;
    }
}
