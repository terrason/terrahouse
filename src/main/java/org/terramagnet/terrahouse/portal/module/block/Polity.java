package org.terramagnet.terrahouse.portal.module.block;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 行政区域.
 */
public class Polity implements Serializable {

    private long id;
    private String name;
    private List<BlockInfo> communities;

    public long getId() {
	return id;
    }

    public void setId(long id) {
	this.id = id;
    }

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public List<BlockInfo> getCommunities() {
	return communities;
    }

    public void setCommunities(List<BlockInfo> communities) {
	this.communities = communities;
    }
    
    public void addCommunity(BlockInfo block){
	if(communities==null){
	    communities=new ArrayList<BlockInfo>();
	}
	communities.add(block);
    }

}
