/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.terramagnet.terrahouse.portal.module.sms;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.Random;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * 手机短信.
 *
 * @author lip
 */
@Service
public class SmsService {

    private static final Logger logger = LoggerFactory.getLogger(SmsService.class);
    
    @Value("#{settings['sms.url']}")
            private String url;
    @Value("#{settings['sms.action']}")
            private String action;
    @Value("#{settings['sms.ac']}")
            private String ac;
    @Value("#{settings['sms.authkey']}")
            private String authkey;
    @Value("#{settings['sms.cgid']}")
            private String cgid;
    
    public int sendSms(String mobile) throws IOException {
        Random random = new Random();
        int code = random.nextInt(900000) + 100000;
        logger.debug("发送短信验证码：{}", code);
        Document document = Jsoup.connect(url)
                .ignoreContentType(true)
		.ignoreHttpErrors(true)
                .data("url",url)
                .data("action",action)
                .data("ac",ac)
                .data("authkey",authkey)
                .data("cgid",cgid)
                .data("c",URLEncoder.encode("```联系方式确认验证码："+code+"。【窝噻网Wellsite】```","UTF-8"))
                .data("m",mobile)
                .get();
        return code;
    }
}
