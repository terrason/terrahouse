package org.terramagnet.terrahouse.portal.module.house;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.annotation.Resource;
import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.terramagnet.terrahouse.portal.module.DuplicatedEntityException;
import org.terramagnet.terrahouse.portal.module.Pager;
import org.terramagnet.terrahouse.portal.module.axis.Axis;
import org.terramagnet.terrahouse.portal.module.user.UserInfo;

@Service
public class HouseInfoService {

    @Resource
    private JdbcTemplate jdbcTemplate;
    private final RowMapper<Comment> commentRowMapper = new RowMapper<Comment>() {

        @Override
        public Comment mapRow(ResultSet rs, int rowNum) throws SQLException {
            Comment entity = new Comment();
            entity.setId(rs.getLong("id"));
            entity.setHouseId(rs.getLong("h_id"));
            entity.setCustomerId(rs.getLong("from_id"));
            entity.setTarget(rs.getLong("to_id"));
            entity.setCustomerName(rs.getString("u_name"));
            entity.setText(rs.getString("comment"));
            entity.setCreateTime(rs.getTimestamp("ptime"));
            entity.setAvatar(rs.getString("u_pic"));
            return entity;
        }
    };

    public List<HouseInfo> findEntities(String polity,int index, int size) {
	StringBuilder sql=new StringBuilder();
	ArrayList params=new ArrayList();
	sql.append("select id,h_name,h_community,h_area,h_pic,h_total_money,h_mj from house_info where h_state=1 ");
	if(StringUtils.isNotBlank(polity)){
	    sql.append(" and h_area=?");
	    params.add(polity);
	}
	sql.append(" order by h_total_value desc,h_desc desc limit ?,?");
	params.add(index);
	params.add(size);
        List<HouseInfo> list = jdbcTemplate.query(sql.toString(), new RowMapper<HouseInfo>() {
            @Override
            public HouseInfo mapRow(ResultSet rs, int rowNum) throws SQLException {
                HouseInfo entity = new HouseInfo();
                entity.setId(rs.getLong("id"));
                entity.setName(rs.getString("h_name"));
                entity.setCommunityName(rs.getString("h_community"));
                entity.setPoliticalAreaName(rs.getString("h_area"));
                entity.setPreview(rs.getString("h_pic"));
                entity.setPrice(rs.getString("h_total_money"));
                entity.setArea(rs.getString("h_mj"));
                return entity;
            }
        }, params.toArray());
        return list;
    }

    public List<HouseInfo> findEntities(String keyword, Integer priceFrom, Integer priceTo, Integer areaFrom, Integer areaTo, Integer ageFrom, Integer ageTo, String polity, Integer bedroom, String decorationint,
            Double minX, Double minY, Double maxX, Double maxY,
            boolean fiveYearsOlder, boolean unique,
            int orderby,
            Pager pager) {
        ArrayList<Object> params = new ArrayList<Object>();
        String sql = "select id,h_name,h_area,h_community,h_total_money,h_mj,seat_xy\n"
                + ",h_pic,h_pic1,h_pic2,h_pic3,h_pic4,h_pic5,h_pic6,h_pic7,h_pic8,h_pic9,h_pic10,h_pic11,h_pic12,h_pic13,h_pic14,h_pic15,h_pic16,h_pic17,h_pic18,h_pic19\n"
                + "from house_info where h_state=1";
        StringBuilder builder = new StringBuilder();
        if (StringUtils.isNotBlank(keyword)) {
            builder.append(" and (h_name like ? or h_area like ? or h_address like ?)");
            String keywordLookup = "%" + keyword + "%";
            params.add(keywordLookup);
            params.add(keywordLookup);
            params.add(keywordLookup);
        }
        if (priceFrom != null) {
            builder.append(" and h_total_money >= ?");
            params.add(priceFrom);
        }
        if (priceTo != null) {
            builder.append(" and h_total_money <= ?");
            params.add(priceTo);
        }
        if (areaFrom != null) {
            builder.append(" and h_mj >= ?");
            params.add(areaFrom);
        }
        if (areaTo != null) {
            builder.append(" and h_mj <= ?");
            params.add(areaTo);
        }
        int curyear = Calendar.getInstance().get(Calendar.YEAR);
        if (ageFrom != null) {
            builder.append(" and h_age >= ?");
            params.add(curyear - ageFrom);
        }
        if (ageTo != null) {
            builder.append(" and h_age <= ?");
            params.add(curyear - ageTo);
        }
        if (StringUtils.isNotBlank(polity)) {
            builder.append(" and h_area = ?");
            params.add(polity);
        }
        if (bedroom != null) {
            if (bedroom > 0) {
                builder.append(" and h_sh = ?");
                params.add(bedroom);
            } else {
                builder.append(" and h_sh > ?");
                params.add(bedroom * -1);
            }
        }
        if (StringUtils.isNotBlank(decorationint)) {
            builder.append(" and h_decorate = ?");
            params.add(decorationint);
        }
        if (minX != null && minY != null && maxX != null && maxY != null) {
            //TODO. 性能问题
            builder.append(" and cast(substring(seat_xy,2,position(',' in seat_xy)-2) as decimal(12,8)) between ? and ?");
            builder.append(" and cast(substring(seat_xy,position(',' in seat_xy)+1,(length(seat_xy)-position(',' in seat_xy)-2)) as decimal(12,8)) between ? and ?");
            params.add(minX);
            params.add(maxX);
            params.add(minY);
            params.add(maxY);
        }
        if (fiveYearsOlder) {
            builder.append(" and is_rate=?");
            params.add("是");
        }
        if (unique) {
            builder.append(" and in_weiyi=?");
            params.add("是");
        }

        switch (orderby) {
            case 0:
                builder.append(" order by h_total_value desc,h_desc desc");
                break;
            case 1:
                builder.append(" order by h_total_money desc");
                break;
            case 2:
                builder.append(" order by h_total_money asc");
                break;
            case 3:
                builder.append(" order by h_location_value desc");
                break;
            case 4:
                builder.append(" order by h_age desc");
                break;
            case 5:
                builder.append(" order by h_likes desc");
                break;
            default:
                builder.append(" order by h_total_value desc,h_desc desc");
        }
        int total = jdbcTemplate.queryForObject("select count(1) from house_info where 1=1 " + builder, Integer.class, params.toArray());
        pager.setTotalSize(total);
        builder.append(" limit ?,?");
        params.add(pager.getFirstIndex());
        params.add(pager.getPageSize());
        List<HouseInfo> list = jdbcTemplate.query(sql + builder.toString(), new RowMapper<HouseInfo>() {
            @Override
            public HouseInfo mapRow(ResultSet rs, int rowNum) throws SQLException {
                HouseInfo entity = new HouseInfo();
                entity.setId(rs.getLong("id"));
                entity.setName(rs.getString("h_name"));
                entity.setCommunityName(rs.getString("h_community"));
                entity.setPoliticalAreaName(rs.getString("h_area"));
                entity.setPreview(rs.getString("h_pic"));
                entity.setPrice(rs.getString("h_total_money"));
                entity.setArea(rs.getString("h_mj"));
                String xy = rs.getString("seat_xy");
                HouseSurround surround = new HouseSurround();
                entity.setSurround(surround);
                if (StringUtils.isNotBlank(xy)) {
                    Axis axis = new Axis(xy);
                    surround.setAxis(axis);
                }
                readSnapshotsFromResultSet(rs, entity);
                return entity;
            }
        }, params.toArray());
        return list;
    }

    public HouseInfo findEntityById(long id) {
        List<HouseInfo> houses = jdbcTemplate.query("select h_name,h_area,h_community_id,h_community,h_address,h_likes\n"
                + ",h_pic,h_pic1,h_pic2,h_pic3,h_pic4,h_pic5,h_pic6,h_pic7,h_pic8,h_pic9,h_pic10,h_pic11,h_pic12,h_pic13,h_pic14,h_pic15,h_pic16,h_pic17,h_pic18,h_pic19\n"
                + ",h_hxt,h_hxt1,h_hxt2,h_hxt3,h_hxt4,h_hxt5,h_hxt6,h_hxt7,h_hxt8,h_hxt9,h_hxt10,h_hxt11,h_hxt12,h_hxt13,h_hxt14,h_hxt15,h_hxt16,h_hxt17,h_hxt18,h_hxt19\n"
                + ",h_detail,comm_detail,h_mj,h_decorate,h_sh,h_th,h_wh,h_type,h_floor,h_total_money,h_price,h_age\n"
                + ",h_age_value,h_decorate_value,h_type_value,h_location_value,h_floor_value,h_area_value\n"
                + ",arround_detail,h_traffic,seat_xy,to_rmgc_gj,to_rmgc_ds,to_ljz_gj,to_ljz_ds,to_xjh_gj,to_xjh_ds,to_jas_gj,to_jas_ds,to_wjc_gj,to_wjc_ds\n"
                + ",is_rate,h_gm_money,in_seat,in_weiyi\n"
                + "from house_info where id=?", new RowMapper<HouseInfo>() {

                    @Override
                    public HouseInfo mapRow(ResultSet rs, int rowNum) throws SQLException {
                        HouseInfo entity = new HouseInfo();
                        entity.setName(rs.getString("H_NAME"));
                        entity.setPoliticalAreaName(rs.getString("H_AREA"));
                        entity.setCommunityId(rs.getLong("H_COMMUNITY_ID"));
                        entity.setCommunityName(rs.getString("H_COMMUNITY"));
                        entity.setAddress(rs.getString("H_ADDRESS"));
                        entity.setForks(rs.getInt("H_LIKES"));
                        entity.setPreview(rs.getString("H_PIC"));
                        readSnapshotsAndLayoutsFromResultSet(rs, entity);
                        readOverviewFromResultSet(rs, entity);
                        readSurroundFromResultSet(rs, entity);
                        readExtensionFromResultSet(rs, entity);
                        return entity;
                    }

                }, id);
        if (houses.isEmpty()) {
            return null;
        } else if (houses.size() > 1) {
            throw new DuplicatedEntityException("房源信息", houses);
        } else {
            HouseInfo house = houses.get(0);
            house.setId(id);
            final Map<Date, HouseSubscription> subscriptions = new HashMap<Date, HouseSubscription>();
            house.setSubscription(subscriptions);
            jdbcTemplate.query("select days,m_person,a_person,n_person from meet_cale where h_id=? and `state`=0", new RowCallbackHandler() {

                @Override
                public void processRow(ResultSet rs) throws SQLException {
                    HouseSubscription subscription = new HouseSubscription();
                    Date day = rs.getDate("days");
                    subscription.setDay(day);
                    HouseSubscriptionRangeItem morning = new HouseSubscriptionRangeItem();
                    morning.setTitle("上午");
                    morning.setLimit(rs.getInt("m_person"));
                    HouseSubscriptionRangeItem afternoon = new HouseSubscriptionRangeItem();
                    afternoon.setTitle("下午");
                    afternoon.setLimit(rs.getInt("a_person"));
                    HouseSubscriptionRangeItem night = new HouseSubscriptionRangeItem();
                    night.setTitle("晚上");
                    night.setLimit(rs.getInt("n_person"));
                    subscription.setItems(new HouseSubscriptionRangeItem[]{morning, afternoon, night});
                    subscriptions.put(day, subscription);
                }
            }, id);
            jdbcTemplate.query("select u_id,h_date,h_time from custom_order where h_id=?", new RowCallbackHandler() {

                @Override
                public void processRow(ResultSet rs) throws SQLException {
                    Date day = rs.getDate("h_date");
                    HouseSubscription subscription = subscriptions.get(day);
                    if (subscription == null) {
                        return;
                    }
                    int time = rs.getInt("h_time");
                    subscription.getItems()[time].putCustomer(rs.getLong("u_id"));
                }
            }, id);
            return house;
        }
    }

    public List<Comment> findComments(int house, int firstIndex, int pageSize) {
        List<Comment> entities = jdbcTemplate.query("select c.id,c.h_id,c.from_id,c.to_id,c.u_name,c.comment,c.ptime,u.u_pic from pinglun c left join user_info u on c.from_id=u.id where c.h_id=? and c.to_id=0 limit ?,?", commentRowMapper, house, firstIndex, pageSize);
        for (Comment comment : entities) {
            List<Comment> reply = jdbcTemplate.query("select id,h_id,from_id,to_id,u_name,comment,ptime,null as u_pic from pinglun where to_id=?", commentRowMapper, comment.getId());
            comment.setReply(reply);
        }
        return entities;
    }

    public void subscribe(CustomerPreorder subscription) {
        int id = UUID.randomUUID().hashCode();
        jdbcTemplate.update("insert into custom_order(id,U_ID,custom_name,custom_phone,h_id,h_date,h_time,`state`)values(?,?,?,?,?,?,?,0)",
                id,
                subscription.getCustomerId(),
                subscription.getCustomerName(),
                subscription.getCustomerPhone(),
                subscription.getHouseId(),
                subscription.getDay(),
                subscription.getTime());
    }

    public void comment(Comment comment) {
        int id = UUID.randomUUID().hashCode();
        jdbcTemplate.update("insert into pinglun(id,h_id,from_id,to_id,u_name,comment,ptime,`state`,is_read)values(?,?,?,0,?,?,now(),0,0)",
                id,
                comment.getHouseId(),
                comment.getCustomerId(),
                comment.getCustomerName(),
                comment.getText());
    }

    public void fork(long id, boolean like) {
        jdbcTemplate.update("update house_info set h_likes=h_likes+? where id=?", like ? 1 : -1, id);
    }

    @Transactional
    public void createHouse(HouseInfo house, UserInfo principal) {
        String sql = "insert into house_info (id,user_id,h_user_name,h_user_phone,h_ploat,h_address,h_mj,h_sh,h_th,h_wh,h_insid_detail\n"
                + ",h_pic,h_pic1,h_pic2,h_pic3,h_pic4,h_pic5,h_pic6,h_pic7,h_pic8,h_pic9,h_pic10,h_pic11,h_pic12,h_pic13,h_pic14,h_pic15,h_pic16,h_pic17,h_pic18,h_pic19\n"
                + ",h_state,h_sale_state,h_desc,h_tj,h_create_time)values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,0,0,0,0,now())";
        jdbcTemplate.update(sql,
                Math.abs(UUID.randomUUID().hashCode()),
                principal.getId(),
                house.getUserName(),
                house.getUserPhone(),
                house.getResidenceName(),
                house.getAddress(),
                house.getArea(),
                house.getBedroom(),
                house.getParlor(),
                house.getBathroom(),
                house.getIntroduce(),
                house.getSnapshots()[0],
                house.getSnapshots()[1],
                house.getSnapshots()[2],
                house.getSnapshots()[3],
                house.getSnapshots()[4],
                house.getSnapshots()[5],
                house.getSnapshots()[6],
                house.getSnapshots()[7],
                house.getSnapshots()[8],
                house.getSnapshots()[9],
                house.getSnapshots()[10],
                house.getSnapshots()[11],
                house.getSnapshots()[12],
                house.getSnapshots()[13],
                house.getSnapshots()[14],
                house.getSnapshots()[15],
                house.getSnapshots()[16],
                house.getSnapshots()[17],
                house.getSnapshots()[18],
                house.getSnapshots()[19]);
    }

    public List<String> suggest(String query) {
        return jdbcTemplate.query("select h_name from house_info where h_code like concat('%',?,'%') limit 15",new RowMapper<String>() {

            @Override
            public String mapRow(ResultSet rs, int i) throws SQLException {
                return rs.getString(1);
            }
        },query);
    }
    private void readSnapshotsAndLayoutsFromResultSet(ResultSet rs, HouseInfo entity) throws SQLException {
        ArrayList<String> snapshotList = new ArrayList<String>();
        ArrayList<String> layoutList = new ArrayList<String>();
        String snapshot;
        String layout;
        //<editor-fold defaultstate="collapsed" desc="recommend to collepse this section">

        snapshot = rs.getString("h_pic");
        layout = rs.getString("h_hxt");
        if (StringUtils.isNotBlank(snapshot)) {
            snapshotList.add(snapshot);
            layoutList.add(layout);
        }
        snapshot = rs.getString("h_pic1");
        layout = rs.getString("h_hxt1");
        if (StringUtils.isNotBlank(snapshot)) {
            snapshotList.add(snapshot);
            layoutList.add(layout);
        }
        snapshot = rs.getString("h_pic2");
        layout = rs.getString("h_hxt2");
        if (StringUtils.isNotBlank(snapshot)) {
            snapshotList.add(snapshot);
            layoutList.add(layout);
        }
        snapshot = rs.getString("h_pic3");
        layout = rs.getString("h_hxt3");
        if (StringUtils.isNotBlank(snapshot)) {
            snapshotList.add(snapshot);
            layoutList.add(layout);
        }
        snapshot = rs.getString("h_pic4");
        layout = rs.getString("h_hxt4");
        if (StringUtils.isNotBlank(snapshot)) {
            snapshotList.add(snapshot);
            layoutList.add(layout);
        }
        snapshot = rs.getString("h_pic5");
        layout = rs.getString("h_hxt5");
        if (StringUtils.isNotBlank(snapshot)) {
            snapshotList.add(snapshot);
            layoutList.add(layout);
        }
        snapshot = rs.getString("h_pic6");
        layout = rs.getString("h_hxt6");
        if (StringUtils.isNotBlank(snapshot)) {
            snapshotList.add(snapshot);
            layoutList.add(layout);
        }
        snapshot = rs.getString("h_pic7");
        layout = rs.getString("h_hxt7");
        if (StringUtils.isNotBlank(snapshot)) {
            snapshotList.add(snapshot);
            layoutList.add(layout);
        }
        snapshot = rs.getString("h_pic8");
        layout = rs.getString("h_hxt8");
        if (StringUtils.isNotBlank(snapshot)) {
            snapshotList.add(snapshot);
            layoutList.add(layout);
        }
        snapshot = rs.getString("h_pic9");
        layout = rs.getString("h_hxt9");
        if (StringUtils.isNotBlank(snapshot)) {
            snapshotList.add(snapshot);
            layoutList.add(layout);
        }
        snapshot = rs.getString("h_pic10");
        layout = rs.getString("h_hxt10");
        if (StringUtils.isNotBlank(snapshot)) {
            snapshotList.add(snapshot);
            layoutList.add(layout);
        }
        snapshot = rs.getString("h_pic11");
        layout = rs.getString("h_hxt11");
        if (StringUtils.isNotBlank(snapshot)) {
            snapshotList.add(snapshot);
            layoutList.add(layout);
        }
        snapshot = rs.getString("h_pic12");
        layout = rs.getString("h_hxt12");
        if (StringUtils.isNotBlank(snapshot)) {
            snapshotList.add(snapshot);
            layoutList.add(layout);
        }
        snapshot = rs.getString("h_pic13");
        layout = rs.getString("h_hxt13");
        if (StringUtils.isNotBlank(snapshot)) {
            snapshotList.add(snapshot);
            layoutList.add(layout);
        }
        snapshot = rs.getString("h_pic14");
        layout = rs.getString("h_hxt14");
        if (StringUtils.isNotBlank(snapshot)) {
            snapshotList.add(snapshot);
            layoutList.add(layout);
        }
        snapshot = rs.getString("h_pic15");
        layout = rs.getString("h_hxt15");
        if (StringUtils.isNotBlank(snapshot)) {
            snapshotList.add(snapshot);
            layoutList.add(layout);
        }
        snapshot = rs.getString("h_pic16");
        layout = rs.getString("h_hxt16");
        if (StringUtils.isNotBlank(snapshot)) {
            snapshotList.add(snapshot);
            layoutList.add(layout);
        }
        snapshot = rs.getString("h_pic17");
        layout = rs.getString("h_hxt17");
        if (StringUtils.isNotBlank(snapshot)) {
            snapshotList.add(snapshot);
            layoutList.add(layout);
        }
        snapshot = rs.getString("h_pic18");
        layout = rs.getString("h_hxt18");
        if (StringUtils.isNotBlank(snapshot)) {
            snapshotList.add(snapshot);
            layoutList.add(layout);
        }
        snapshot = rs.getString("h_pic19");
        layout = rs.getString("h_hxt19");
        if (StringUtils.isNotBlank(snapshot)) {
            snapshotList.add(snapshot);
            layoutList.add(layout);
        }
//</editor-fold>
        entity.setSnapshots(snapshotList.toArray(new String[0]));
        entity.setLayouts(layoutList.toArray(new String[0]));
    }

    private void readSnapshotsFromResultSet(ResultSet rs, HouseInfo entity) throws SQLException {
        ArrayList<String> snapshotList = new ArrayList<String>();
        String snapshot;
        //<editor-fold defaultstate="collapsed" desc="recommend to collepse this section">

        snapshot = rs.getString("h_pic");
        if (StringUtils.isNotBlank(snapshot)) {
            snapshotList.add(snapshot);
        }
        snapshot = rs.getString("h_pic1");
        if (StringUtils.isNotBlank(snapshot)) {
            snapshotList.add(snapshot);
        }
        snapshot = rs.getString("h_pic2");
        if (StringUtils.isNotBlank(snapshot)) {
            snapshotList.add(snapshot);
        }
        snapshot = rs.getString("h_pic3");
        if (StringUtils.isNotBlank(snapshot)) {
            snapshotList.add(snapshot);
        }
        snapshot = rs.getString("h_pic4");
        if (StringUtils.isNotBlank(snapshot)) {
            snapshotList.add(snapshot);
        }
        snapshot = rs.getString("h_pic5");
        if (StringUtils.isNotBlank(snapshot)) {
            snapshotList.add(snapshot);
        }
        snapshot = rs.getString("h_pic6");
        if (StringUtils.isNotBlank(snapshot)) {
            snapshotList.add(snapshot);
        }
        snapshot = rs.getString("h_pic7");
        if (StringUtils.isNotBlank(snapshot)) {
            snapshotList.add(snapshot);
        }
        snapshot = rs.getString("h_pic8");
        if (StringUtils.isNotBlank(snapshot)) {
            snapshotList.add(snapshot);
        }
        snapshot = rs.getString("h_pic9");
        if (StringUtils.isNotBlank(snapshot)) {
            snapshotList.add(snapshot);
        }
        snapshot = rs.getString("h_pic10");
        if (StringUtils.isNotBlank(snapshot)) {
            snapshotList.add(snapshot);
        }
        snapshot = rs.getString("h_pic11");
        if (StringUtils.isNotBlank(snapshot)) {
            snapshotList.add(snapshot);
        }
        snapshot = rs.getString("h_pic12");
        if (StringUtils.isNotBlank(snapshot)) {
            snapshotList.add(snapshot);
        }
        snapshot = rs.getString("h_pic13");
        if (StringUtils.isNotBlank(snapshot)) {
            snapshotList.add(snapshot);
        }
        snapshot = rs.getString("h_pic14");
        if (StringUtils.isNotBlank(snapshot)) {
            snapshotList.add(snapshot);
        }
        snapshot = rs.getString("h_pic15");
        if (StringUtils.isNotBlank(snapshot)) {
            snapshotList.add(snapshot);
        }
        snapshot = rs.getString("h_pic16");
        if (StringUtils.isNotBlank(snapshot)) {
            snapshotList.add(snapshot);
        }
        snapshot = rs.getString("h_pic17");
        if (StringUtils.isNotBlank(snapshot)) {
            snapshotList.add(snapshot);
        }
        snapshot = rs.getString("h_pic18");
        if (StringUtils.isNotBlank(snapshot)) {
            snapshotList.add(snapshot);
        }
        snapshot = rs.getString("h_pic19");
        if (StringUtils.isNotBlank(snapshot)) {
            snapshotList.add(snapshot);
        }
        //</editor-fold>
        entity.setSnapshots(snapshotList.toArray(new String[0]));
    }

    private void readOverviewFromResultSet(ResultSet rs, HouseInfo entity) throws SQLException {
        HouseOverview overview = new HouseOverview();
        entity.setOverview(overview);
//        overview.setHouseIntroduction(rs.getString("h_detail"));
//        overview.setCommunityIntroduction(rs.getString("comm_detail"));
        overview.setHouseIntroductionBlob(rs.getBytes("h_detail"));
        overview.setCommunityIntroductionBlob(rs.getBytes("comm_detail"));

        HouseOverviewEssential essential = new HouseOverviewEssential();
        overview.setEssential(essential);
        essential.setAcreage(rs.getString("h_mj"));
        entity.setArea(essential.getAcreage());
        essential.setDecoration(rs.getString("h_decorate"));
        entity.setBedroom(rs.getInt("h_sh"));
        entity.setParlor(rs.getInt("h_th"));
        entity.setBathroom(rs.getInt("h_wh"));
        essential.setLayout(formatLayout(entity));
        essential.setSchema(rs.getString("h_type"));
        essential.setStorey(rs.getString("h_floor"));
        essential.setTotalPrice(rs.getDouble("h_total_money"));
        entity.setPrice(String.valueOf(essential.getTotalPrice()));
        essential.setUnitPrice(rs.getDouble("h_price"));
        essential.setYears(rs.getString("h_age"));

        HouseOverviewEssentialScore score = new HouseOverviewEssentialScore();
        essential.setCycle(score);
        score.setAge(rs.getString("h_age_value"));
        score.setDecoration(rs.getString("h_decorate_value"));
        score.setLayout(rs.getString("h_type_value"));
        score.setLocation(rs.getString("h_location_value"));
        score.setStorey(rs.getString("h_floor_value"));
        score.setSurround(rs.getString("h_area_value"));
    }

    private void readSurroundFromResultSet(ResultSet rs, HouseInfo entity) throws SQLException {
        HouseSurround surround = new HouseSurround();
        entity.setSurround(surround);
        surround.setLivingFacilities(rs.getBytes("arround_detail"));

        String xy = rs.getString("seat_xy");
        if (StringUtils.isNotBlank(xy)) {
            Axis axis = new Axis(xy);
            surround.setAxis(axis);
        }
        HouseSurroundTraffic traffic = new HouseSurroundTraffic();
        surround.setTraffic(traffic);
        traffic.setOverview(rs.getBytes("h_traffic"));

        ArrayList<Guidepost> guideposts = new ArrayList<Guidepost>();
        traffic.setGuidepost(guideposts);

        Guidepost guidepost = new Guidepost();
        guideposts.add(guidepost);
        guidepost.setLocation("人民广场");
        guidepost.setBus(rs.getInt("to_rmgc_gj"));
        guidepost.setTaxi(rs.getInt("to_rmgc_ds"));
        guidepost = new Guidepost();
        guideposts.add(guidepost);
        guidepost.setLocation("陆家嘴");
        guidepost.setBus(rs.getInt("to_ljz_gj"));
        guidepost.setTaxi(rs.getInt("to_ljz_ds"));
        guidepost = new Guidepost();
        guideposts.add(guidepost);
        guidepost.setLocation("徐家汇");
        guidepost.setBus(rs.getInt("to_xjh_gj"));
        guidepost.setTaxi(rs.getInt("to_xjh_ds"));
        guidepost = new Guidepost();
        guideposts.add(guidepost);
        guidepost.setLocation("静安寺");
        guidepost.setBus(rs.getInt("to_jas_gj"));
        guidepost.setTaxi(rs.getInt("to_jas_ds"));
        guidepost = new Guidepost();
        guideposts.add(guidepost);
        guidepost.setLocation("五角场");
        guidepost.setBus(rs.getInt("to_wjc_gj"));
        guidepost.setTaxi(rs.getInt("to_wjc_ds"));
    }

    private void readExtensionFromResultSet(ResultSet rs, HouseInfo entity) throws SQLException {
        HouseExtension ext = new HouseExtension();
        entity.setExt(ext);
        ext.setFiveYearsOlder(rs.getString("is_rate"));
        ext.setLandlordCost(rs.getString("h_gm_money"));
        ext.setPosition(rs.getString("in_seat"));
        ext.setUnique(rs.getString("in_weiyi"));
    }

    private final MessageFormat layoutFormat = new MessageFormat("{0}室{1}厅{2}卫");
    private final String[] numberNames = {"", "一", "二", "三", "四", "五", "六", "七", "八", "九"};

    private String formatLayout(HouseInfo entity) {
        return layoutFormat.format(new Object[]{
            numberToChinese(entity.getBedroom()),
            numberToChinese(entity.getParlor()),
            numberToChinese(entity.getBathroom())
        });
    }

    private String numberToChinese(int number) {
        if (number <= 0 || number > 99) {
            return "零";
        }
        int t = number % 10;
        number /= 10;
        int ten = number % 10;
        if (ten == 0) {
            return numberNames[t];
        } else {
            return numberNames[ten] + "十" + numberNames[t];
        }
    }

}
