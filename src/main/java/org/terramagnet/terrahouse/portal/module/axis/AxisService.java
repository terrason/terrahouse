package org.terramagnet.terrahouse.portal.module.axis;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

/**
 * 处理用户相关服务.
 *
 * @author lip
 */
@Service
public class AxisService {

    @Resource
    private JdbcTemplate jdbcTemplate;

    @Cacheable(value = "axis")
    public List<Axis> findEntities() {
        return jdbcTemplate.query("select id,s_xy,s_name,s_type from axis", new RowMapper<Axis>() {

            @Override
            public Axis mapRow(ResultSet rs, int i) throws SQLException {
                String xy = rs.getString("s_xy");
                Axis entity = new Axis(xy);
                entity.setId(rs.getLong("id"));
                entity.setName(rs.getString("s_name"));
                entity.setType(rs.getInt("s_type"));
                return entity;
            }
        }, (Object[]) null);
    }
}
