/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.terramagnet.terrahouse.portal.module.attachment;

import java.io.File;

public class PathInfo {

    private final File file;
    private final String url;

    public PathInfo(File file, String url) {
        this.file = file;
        this.url = url;
    }

    public File getFile() {
        return file;
    }

    public String getUrl() {
        return url;
    }
    }
