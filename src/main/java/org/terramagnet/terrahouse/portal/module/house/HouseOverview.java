/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.terramagnet.terrahouse.portal.module.house;

import java.io.Serializable;

public class HouseOverview implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 基本信息.
     */
    private HouseOverviewEssential essential;
    /**
     * 房屋概况.
     */
    private String houseIntroduction;
    /**
     * 小区环境.
     */
    private String communityIntroduction;
    /**
     * 房屋概况blob.
     */
    private byte[] houseIntroductionBlob;
    /**
     * 小区环境blob.
     */
    private byte[] communityIntroductionBlob;

    /**
     * 基本信息.
     *
     * @return the essential
     */
    public HouseOverviewEssential getEssential() {
        return essential;
    }

    /**
     * 基本信息.
     *
     * @param essential the essential to set
     */
    public void setEssential(HouseOverviewEssential essential) {
        this.essential = essential;
    }

    /**
     * 房屋概况.
     *
     * @return the houseIntroduction
     */
    public String getHouseIntroduction() {
        return houseIntroduction;
    }

    /**
     * 房屋概况.
     *
     * @param houseIntroduction the houseIntroduction to set
     */
    public void setHouseIntroduction(String houseIntroduction) {
        this.houseIntroduction = houseIntroduction;
    }

    /**
     * 小区环境.
     *
     * @return the communityIntroduction
     */
    public String getCommunityIntroduction() {
        return communityIntroduction;
    }

    /**
     * 小区环境.
     *
     * @param communityIntroduction the communityIntroduction to set
     */
    public void setCommunityIntroduction(String communityIntroduction) {
        this.communityIntroduction = communityIntroduction;
    }

    /**
     * 房屋概况blob.
     *
     * @return the houseIntroductionBlob
     */
    public byte[] getHouseIntroductionBlob() {
        return houseIntroductionBlob;
    }

    
    /**
     * 房屋概况blob.
     *
     * @param houseIntroductionBlob the houseIntroductionBlob to set
     */
    public void setHouseIntroductionBlob(byte[] houseIntroductionBlob) {
        this.houseIntroductionBlob = houseIntroductionBlob;
        if (houseIntroductionBlob != null) {
            houseIntroduction = new String(houseIntroductionBlob);
            houseIntroduction=houseIntroduction.replaceAll("\0"," ");
        } else {
            houseIntroduction = "";
        }
    }

    /**
     * 小区环境blob.
     *
     * @return the communityIntroductionBlob
     */
    public byte[] getCommunityIntroductionBlob() {
        return communityIntroductionBlob;
    }

    /**
     * 小区环境blob.
     *
     * @param communityIntroductionBlob the communityIntroductionBlob to set
     */
    public void setCommunityIntroductionBlob(byte[] communityIntroductionBlob) {
        this.communityIntroductionBlob = communityIntroductionBlob;
        if (communityIntroductionBlob != null) {
            communityIntroduction = new String(communityIntroductionBlob);
            communityIntroduction=communityIntroduction.replaceAll("\0"," ");
        } else {
            communityIntroduction = "";
        }
    }

}
