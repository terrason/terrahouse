/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.terramagnet.terrahouse.portal.module.house;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 房源预约信息.
 *
 * @author lip
 */
public class HouseSubscription implements Serializable {

    private static final long serialVersionUID = 1L;
    private Date day;
    private HouseSubscriptionRangeItem[] items;

    public Date getDay() {
        return day;
    }

    public void setDay(Date day) {
        this.day = day;
    }

    public HouseSubscriptionRangeItem[] getItems() {
        return items;
    }

    public void setItems(HouseSubscriptionRangeItem[] items) {
        this.items = items;
    }

    private final SimpleDateFormat dayFormatter=new SimpleDateFormat("yyyy年MM月dd日，E");
    public String getDayInfo(){
        return dayFormatter.format(day);
    }
}
