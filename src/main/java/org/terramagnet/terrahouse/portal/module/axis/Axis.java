/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.terramagnet.terrahouse.portal.module.axis;

import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author lip
 */
public class Axis implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Pattern regex = Pattern.compile("\\(([\\d\\.]+), *([\\d\\.]+)\\)");
    private long id;
    private double x;
    private double y;
    private String name;
    private int type;

    public Axis() {
    }

    /**
     * 使用坐标字符串构建坐标. 不合法的字符串会抛出{@code IllegalArgumentException}异常。
     * <p>
     * 坐标字符串例：(121.395611,31.111321) </p>
     *
     * @param xy 坐标字符串
     */
    public Axis(String xy) throws IllegalArgumentException {
        Matcher matcher = regex.matcher(xy);
        if (matcher.find()) {
            try {
                String xaxis = matcher.group(1);
                String yaxis = matcher.group(2);
                x = Double.parseDouble(xaxis);
                y = Double.parseDouble(yaxis);
            } catch (NumberFormatException ex) {
                throw new IllegalArgumentException(xy + " does not acceptable!");
            }
        } else {
            throw new IllegalArgumentException(xy + " does not acceptable!");
        }
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return x + "," + y;
    }
}
