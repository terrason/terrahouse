/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.terramagnet.terrahouse.portal.module.house;

import java.io.Serializable;
import org.terramagnet.terrahouse.portal.module.axis.Axis;

public class HouseSurround implements Serializable {

    private static final long serialVersionUID = 1L;
    private String livingFacilities;
    private HouseSurroundTraffic traffic;
    private Axis axis;

    /**
     * @return the livingFacilities
     */
    public String getLivingFacilities() {
        return livingFacilities;
    }

    /**
     * @param livingFacilities the livingFacilities to set
     */
    public void setLivingFacilities(String livingFacilities) {
        this.livingFacilities = livingFacilities;
    }

    /**
     * @param livingFacilitiesBlob the livingFacilities to set
     */
    public void setLivingFacilities(byte[] livingFacilitiesBlob) {
        if (livingFacilitiesBlob != null) {
            livingFacilities = new String(livingFacilitiesBlob);
            livingFacilities = livingFacilities.replaceAll("\0", " ");
        } else {
            livingFacilities = "";
        }
    }

    /**
     * @return the traffic
     */
    public HouseSurroundTraffic getTraffic() {
        return traffic;
    }

    /**
     * @param traffic the traffic to set
     */
    public void setTraffic(HouseSurroundTraffic traffic) {
        this.traffic = traffic;
    }

    public Axis getAxis() {
        return axis;
    }

    public void setAxis(Axis axis) {
        this.axis = axis;
    }
}
