package org.terramagnet.terrahouse.portal.module.block;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.terramagnet.terrahouse.portal.module.axis.Axis;

/**
 * 社区信息.
 *
 * @author lip
 */
public class BlockInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    private long id;
    /**
     * 行政区域ID.
     */
    private long areaId;
    /**
     * 行政区域名称.
     */
    private String areaName;
    /**
     * 社区名称.
     */
    private String name;
    /**
     * logo图片.
     */
    private String logo;
    /**
     * 主图片.
     */
    private String preview;
    /**
     * 社区概述.
     */
    private String description;
    /**
     * 社区坐标.
     */
    private Axis axis;
    /**
     * 社区多边形区域坐标.
     */
    private Collection<Axis> region;

    /**
     * 社区地理信息.
     */
    private String position;
    /**
     * 轨道交通.
     */
    private int[] metro;
    /**
     * 交通路标.
     */
    private List<Guidepost> guidepost;
    /**
     * 社区剪影.
     */
    private String gallery;

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * 社区名称.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * 社区名称.
     *
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 主图片.
     *
     * @return the preview
     */
    public String getPreview() {
        return preview;
    }

    /**
     * 主图片.
     *
     * @param preview the preview to set
     */
    public void setPreview(String preview) {
        this.preview = preview;
    }

    /**
     * 社区概述.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * 社区概述.
     *
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 设置社区概述.
     *
     * @param descriptionBlob 社区概述字节码
     */
    public void setDescription(byte[] descriptionBlob) {
        if (descriptionBlob != null) {
            description = new String(descriptionBlob);
            description = description.replaceAll("\0", " ");
        } else {
            description = "";
        }
    }

    /**
     * 社区地理信息.
     *
     * @return the position
     */
    public String getPosition() {
        return position;
    }

    /**
     * 社区地理信息.
     *
     * @param position the position to set
     */
    public void setPosition(String position) {
        this.position = position;
    }

    /**
     * 设置地理信息.
     *
     * @param positionBlob 地理信息节码
     */
    public void setPosition(byte[] positionBlob) {
        if (positionBlob != null) {
            position = new String(positionBlob);
            position = position.replaceAll("\0", " ");
        } else {
            position = "";
        }
    }

    /**
     * 轨道交通.
     *
     * @return the metro
     */
    public int[] getMetro() {
        return metro;
    }

    /**
     * 轨道交通.
     *
     * @param metro the metro to set
     */
    public void setMetro(int[] metro) {
        this.metro = metro;
    }

    /**
     * 交通路标.
     *
     * @return the guidepost
     */
    public List<Guidepost> getGuidepost() {
        return guidepost;
    }

    /**
     * 交通路标.
     *
     * @param guidepost the guidepost to set
     */
    public void setGuidepost(List<Guidepost> guidepost) {
        this.guidepost = guidepost;
    }

    /**
     * 社区剪影.
     *
     * @return the gallery
     */
    public String getGallery() {
        return gallery;
    }

    /**
     * 社区剪影.
     *
     * @param gallery the gallery to set
     */
    public void setGallery(String gallery) {
        this.gallery = gallery;
    }

    /**
     * 设置社区剪影.
     *
     * @param galleryBlob 社区剪影blob
     */
    public void setGallery(byte[] galleryBlob) {
        if (galleryBlob != null) {
            gallery = new String(galleryBlob);
            gallery = gallery.replaceAll("\0", " ");
        } else {
            gallery = "";
        }
    }

    /**
     * 社区坐标.
     *
     * @return the axis
     */
    public Axis getAxis() {
        return axis;
    }

    /**
     * 社区坐标.
     *
     * @param axis the axis to set
     */
    public void setAxis(Axis axis) {
        this.axis = axis;
    }

    /**
     * 社区多边形区域坐标.
     *
     * @return the region
     */
    public Collection<Axis> getRegion() {
        return region;
    }

    /**
     * 社区多边形区域坐标.
     *
     * @param region the region to set
     */
    public void setRegion(Collection<Axis> region) {
        this.region = region;
        _cachedRegionPeaks = null;
    }

    /**
     * 添加社区多边形区域顶点.
     *
     * @param axis 区域顶点
     */
    public void addRegionPeak(Axis axis) {
        if (region == null) {
            region = new ArrayList<Axis>();
        }
        region.add(axis);
        _cachedRegionPeaks = null;
    }

    private String _cachedRegionPeaks;

    public String getRegionPeaks() {
        if (_cachedRegionPeaks == null) {
            if (region.isEmpty()) {
                _cachedRegionPeaks = "";
            } else {
                StringBuilder builder = new StringBuilder();
                for (Axis point : region) {
                    builder.append(point).append(";");
                }
                _cachedRegionPeaks = builder.substring(0, builder.length() - 1);
            }
        }
        return _cachedRegionPeaks;
    }

    /**
     * 行政区域ID.
     *
     * @return the areaId
     */
    public long getAreaId() {
        return areaId;
    }

    /**
     * 行政区域ID.
     *
     * @param areaId the areaId to set
     */
    public void setAreaId(long areaId) {
        this.areaId = areaId;
    }

    /**
     * logo图片.
     *
     * @return the logo
     */
    public String getLogo() {
        return logo;
    }

    /**
     * logo图片.
     *
     * @param logo the logo to set
     */
    public void setLogo(String logo) {
        this.logo = logo;
    }

    /**
     * 行政区域名称.
     *
     * @return the areaName
     */
    public String getAreaName() {
        return areaName;
    }

    /**
     * 行政区域名称.
     *
     * @param areaName the areaName to set
     */
    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }
}
