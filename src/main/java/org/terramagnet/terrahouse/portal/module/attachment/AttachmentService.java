/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.terramagnet.terrahouse.portal.module.attachment;

import java.io.File;
import java.util.Calendar;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.ServletContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service("fileManager")
public class AttachmentService {

    private final Pattern imgsrc = Pattern.compile("(src=\")([a-zA-Z0-9/\\._-]+)(\")");
    @Value("#{settings['file.host']}")
    private String host;
    @Value("#{settings['file.upload']}")
    private String _uploadRoot;
    private String root;
    @Value("#{settings['file.download']}")
    private String _download;
    private String namespace;

    @Autowired
    private ServletContext context;

    /**
     * 根据当前时间创建文件路径和网络路径.
     * <p>
     * 返回的文件是按照系统日期和UUID组织的目录，但是并不保证该目录实际存在。</p>
     * <p>
     * 返回的网络路径是上述文件的相对URL写法,总是以“{@code /}”开始但不以“{@code /}”结束。</p>
     *
     * @return
     */
    public PathInfo createPath(String fileName) {
        String uuid = UUID.randomUUID().toString();
        Calendar now = Calendar.getInstance();
        int year = now.get(Calendar.YEAR);
        int month = now.get(Calendar.MONTH) + 1;
        int day = now.get(Calendar.DATE);
        StringBuilder filepath = new StringBuilder();
        filepath.append(getRoot());
        filepath.append(File.separatorChar).append(year);
        filepath.append(File.separatorChar).append(month);
        filepath.append(File.separatorChar).append(day);
        filepath.append(File.separatorChar).append(fileName).append("-").append(uuid);
        StringBuilder webpath = new StringBuilder();
        webpath.append(getNamespace());
        webpath.append("/").append(year);
        webpath.append("/").append(month);
        webpath.append("/").append(day);
        webpath.append("/").append(fileName).append("-").append(uuid);
        return new PathInfo(new File(filepath.toString()), webpath.toString());
    }

    public PathInfo createFile(String filePathAndName) {
        StringBuilder filePath = new StringBuilder();
        StringBuilder webpath = new StringBuilder();
        filePathAndName = filePathAndName.replace('/', File.separatorChar);
        filePath.append(getRoot());
        webpath.append(getNamespace());
        if (!filePathAndName.startsWith(File.separator)) {
            filePath.append(File.separatorChar);
            webpath.append("/");
        }
        filePath.append(filePathAndName);
        webpath.append(filePathAndName.replace(File.separatorChar, '/'));
        return new PathInfo(new File(filePath.toString()), webpath.toString());
    }

    private String getRoot() {
        if (root == null) {
            if (_uploadRoot.charAt(0) == '.') {
                root = context.getRealPath("/") + _uploadRoot;
            } else {
                root = _uploadRoot;
            }
        }
        return root;
    }

    private String getNamespace() {
        if (namespace == null) {
            namespace = _download;
        }
        return namespace;
    }
}
