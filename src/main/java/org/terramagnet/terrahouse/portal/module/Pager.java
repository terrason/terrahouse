package org.terramagnet.terrahouse.portal.module;

/**
 * JavaBean实现的分页器. 用来封装分页信息。
 *
 * @author LEE
 */
public class Pager {

    public static final int DEFAULT_PAGESIZE = 8;
    private int currentPage = 1;
    private int pageSize = DEFAULT_PAGESIZE;
    private int totalSize;

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        if (currentPage > 1) {
            this.currentPage = currentPage;
        }
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        if (pageSize > 1) {
            this.pageSize = pageSize;
        }
    }

    public int getTotalSize() {
        return totalSize;
    }

    public int getFirstIndex() {
        return (currentPage - 1) * pageSize;
    }

    public void setTotalSize(int totalSize) {
        this.totalSize = totalSize;
    }

    public String toJson() {
        return "{totalSize:" + totalSize + ",pageSize:" + pageSize + ",currentPage:" + currentPage + "}";
    }

    /**
     * {@inheritDoc }. 与{@link #toJson() }等价
     *
     * @return JSON字符串
     */
    @Override
    public String toString() {
        return toJson();
    }
}
