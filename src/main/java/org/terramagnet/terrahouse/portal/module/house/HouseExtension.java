/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.terramagnet.terrahouse.portal.module.house;

import java.io.Serializable;

/**
 *
 * @author lip
 */
public class HouseExtension implements Serializable {

    /**
     * 是否为房东唯一住宅.
     */
    private String unique;
    /**
     * 是否满5年.
     */
    private String fiveYearsOlder;
    /**
     * 房屋所处区域.
     */
    private String position;
    /**
     * 房东购买价.
     */
    private String landlordCost;

    /**
     * 是否为房东唯一住宅.
     *
     * @return the unique
     */
    public String getUnique() {
        return unique;
    }

    /**
     * 是否为房东唯一住宅.
     *
     * @param unique the unique to set
     */
    public void setUnique(String unique) {
        this.unique = unique;
    }

    /**
     * 是否满5年.
     *
     * @return the fiveYearsOlder
     */
    public String getFiveYearsOlder() {
        return fiveYearsOlder;
    }

    /**
     * 是否满5年.
     *
     * @param fiveYearsOlder the fiveYearsOlder to set
     */
    public void setFiveYearsOlder(String fiveYearsOlder) {
        this.fiveYearsOlder = fiveYearsOlder;
    }

    /**
     * 房屋所处区域.
     *
     * @return the position
     */
    public String getPosition() {
        return position;
    }

    /**
     * 房屋所处区域.
     *
     * @param position the position to set
     */
    public void setPosition(String position) {
        this.position = position;
    }

    /**
     * 房东购买价.
     *
     * @return the landlordCost
     */
    public String getLandlordCost() {
        return landlordCost;
    }

    /**
     * 房东购买价.
     *
     * @param landlordCost the landlordCost to set
     */
    public void setLandlordCost(String landlordCost) {
        this.landlordCost = landlordCost;
    }
}
