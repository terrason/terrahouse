/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.terramagnet.terrahouse.portal.module.house;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author lip
 */
public class HouseSubscriptionRangeItem implements Serializable {

    private static final long serialVersionUID = 1L;
    private String title;
    private Set<Long> customers=new HashSet<Long>();
    private int limit;

    public boolean putCustomer(long customerId){
        return customers.add(customerId);
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Set<Long> getCustomers() {
        return customers;
    }

    public void setCustomers(Set<Long> customers) {
        this.customers = customers;
    }
}
