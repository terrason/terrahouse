/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.terramagnet.terrahouse.portal.module.mail;

import java.util.Date;
import java.util.Properties;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * 发送邮件服务.
 *
 * @author lip
 */
@Service
public class MailService {

    private final Logger logger = LoggerFactory.getLogger(MailService.class);
    @Value("#{settings['system.mail.host']}")
    private String host;
    @Value("#{settings['system.mail.auth']}")
    private String auth;
    @Value("#{settings['system.mail.debug']}")
    private boolean debug;
    @Value("#{settings['system.mail.sender']}")
    private String sender;
    @Value("#{settings['system.mail.password']}")
    private String password;
    private String charset = "UTF-8";

    /**
     * 发送邮件. 不支持附件.
     *
     * @param title 邮件标题
     * @param mailContent 邮件内容
     * @param mimetype 邮件mimetype
     * @param receivers 接收人
     * @throws MessagingException 发送邮件失败时抛出此异常
     */
    public void sendEmail(String title, String mailContent, String mimetype, String... receivers) throws MessagingException {
        if(receivers==null || receivers.length==0){
            throw new MessagingException("无邮件接收人");
        }
        Properties props = new Properties();
        //设置smtp服务器地址
        //这里使用QQ邮箱，记得关闭独立密码保护功能和在邮箱中设置POP3/IMAP/SMTP服务
        props.put("mail.smtp.host", host);
        //需要验证
        props.put("mail.smtp.auth", auth);
        //创建验证器
        Authenticator authenticator = new Authenticator() {

            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(sender, password);
            }
        };
        //使用Properties创建Session
        Session session = Session.getDefaultInstance(props, authenticator);
        //Set the debug setting for this Session
        session.setDebug(debug);
        //使用session创建MIME类型的消息
        MimeMessage mimeMessage = new MimeMessage(session);
        //设置发件人邮件
        mimeMessage.setFrom(new InternetAddress(sender));
        //获取所有收件人邮箱地址
        InternetAddress[] receiver = new InternetAddress[receivers.length];
        for (int i = 0; i < receivers.length; i++) {
            receiver[i] = new InternetAddress(receivers[i]);
        }
        //设置收件人邮件
        mimeMessage.setRecipients(Message.RecipientType.TO, receiver);
        //设置标题
        mimeMessage.setSubject(title, charset);
        //设置邮件发送时间
        //SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        //mimeMessage.setSentDate(format.parse("2011-12-1"));
        mimeMessage.setSentDate(new Date());
        //创建附件
        Multipart multipart = new MimeMultipart();
        //创建邮件内容
        MimeBodyPart body = new MimeBodyPart();
        //设置邮件内容
        body.setContent(mailContent, (mimetype != null && !"".equals(mimetype) ? mimetype : "text/plain") + ";charset=" + charset);
        multipart.addBodyPart(body);//发件内容
        //设置邮件内容（使用Multipart方式）
        mimeMessage.setContent(multipart);
        //发送邮件
        Transport.send(mimeMessage);
    }

    public void setHost(String host) {
        this.host = host;
    }

    public void setAuth(String auth) {
        this.auth = auth;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setCharset(String charset) {
        this.charset = charset;
    }
}
