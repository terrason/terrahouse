package org.terramagnet.terrahouse.portal.module.block;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.annotation.Resource;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;
import org.terramagnet.terrahouse.portal.module.DuplicatedEntityException;
import org.terramagnet.terrahouse.portal.module.axis.Axis;

@Service
public class BlockInfoService {

    private final Logger logger = LoggerFactory.getLogger(BlockInfoService.class);
    @Resource
    private JdbcTemplate jdbcTemplate;

    /**
     * 查询所有行政和社区基本名称.
     *
     * @return 行政和社区
     */
    @Cacheable(value = "polities")
    public Collection<Polity> findAllPolities() {
        final Map<Long, Polity> polities = new LinkedHashMap<Long, Polity>();
        jdbcTemplate.query("select id,area_name from area", new RowCallbackHandler() {

            @Override
            public void processRow(ResultSet rs) throws SQLException {
                Polity polity = new Polity();
                polity.setId(rs.getLong("id"));
                polity.setName(rs.getString("area_name"));
                polities.put(polity.getId(), polity);
            }
        }, (Object[]) null);

        jdbcTemplate.query("select id,area_id,community_name from community", new RowCallbackHandler() {

            @Override
            public void processRow(ResultSet rs) throws SQLException {
                BlockInfo block = new BlockInfo();
                long areaId = rs.getLong("area_id");
                Polity polity = polities.get(areaId);
                if (polity == null) {
                    logger.warn("社区所在行政不存在 {}", block);
                    return;
                }

                block.setId(rs.getLong("id"));
                block.setName(rs.getString("community_name"));
                block.setAreaName(polity.getName());
                polity.addCommunity(block);
            }
        }, (Object[]) null);
        return polities.values();
    }

    @Cacheable(value = "topBlocks")
    public BlockInfo[] findTopBlocks() {
        List<BlockInfo> blocks = jdbcTemplate.query("select id,community_name,picture from community where is_invode=1 limit 4", new RowMapper<BlockInfo>() {

            @Override
            public BlockInfo mapRow(ResultSet rs, int rowNum) throws SQLException {
                BlockInfo entity = new BlockInfo();
                entity.setId(rs.getLong("id"));
                entity.setName(rs.getString("community_name"));
                entity.setPreview(rs.getString("picture"));
                return entity;
            }
        }, (Object[]) null);
        return blocks.toArray(new BlockInfo[4]);
    }

    /**
     * 根据ID查询社区信息.
     *
     * @param id 社区ID
     * @return 社区详细信息
     */
    public BlockInfo findEntity(long id) {
        List<BlockInfo> blocks = jdbcTemplate.query("select c.id,c.`area_id`,c.`community_name`,c.`community_desc`,c.`communtiy_dl_info`,c.`communtiy_xy`,c.`communtiy_bjxy`,c.`communtiy_gds`\n"
                + ",c.`dt_rmgc`,c.`gj_rmgc`,c.`dt_ljz`,c.`gj_ljz`,c.`dt_xjh`,c.`gj_xjh`,c.`dt_jas`,c.`gj_jas`,c.`dt_wjc`,c.`gj_wjc`\n"
                + ",c.`community_comment`,c.`picture`,area.area_name\n"
                + "from community c left join area on c.area_id=area.id where c.id=?", new RowMapper<BlockInfo>() {

                    @Override
                    public BlockInfo mapRow(ResultSet rs, int i) throws SQLException {
                        BlockInfo block = new BlockInfo();
                        long areaId = rs.getLong("area_id");
                        block.setId(rs.getLong("id"));
                        block.setAreaId(areaId);
                        block.setAreaName(rs.getString("area_name"));
                        block.setName(rs.getString("community_name"));
                        block.setLogo(rs.getString("picture"));
                        block.setPreview(block.getLogo());
                        block.setDescription(rs.getBytes("community_desc"));
                        block.setPosition(rs.getBytes("communtiy_dl_info"));
                        String xy = rs.getString("communtiy_xy");
                        if (StringUtils.isNotBlank(xy)) {
                            Axis axis = new Axis(xy);
                            block.setAxis(axis);
                        }
                        String bjxy = rs.getString("communtiy_bjxy");
                        if (StringUtils.isNotBlank(bjxy)) {
                            for (String point : bjxy.split(";")) {
                                Axis axis = new Axis(point);
                                block.addRegionPeak(axis);
                            }
                        }
                        String metros = rs.getString("communtiy_gds");
                        if (StringUtils.isNotBlank(metros)) {
                            String[] s = metros.split(",");
                            int[] metroNums = new int[s.length];
                            for (int j = 0; j < s.length; j++) {
                                String str = s[j];
                                metroNums[j] = NumberUtils.toInt(str);
                            }
                            block.setMetro(metroNums);
                        } else {
                            block.setMetro(new int[0]);
                        }
                        readGuidpostFromResultSet(rs, block);

                        block.setGallery(rs.getBytes("community_comment"));
                        return block;
                    }
                }, id);

        if (blocks.isEmpty()) {
            return null;
        } else if (blocks.size() > 1) {
            throw new DuplicatedEntityException("社区信息", blocks);
        } else {
            return blocks.get(0);
        }
    }

    private void readGuidpostFromResultSet(ResultSet rs, BlockInfo block) throws SQLException {
        ArrayList<Guidepost> guideposts = new ArrayList<Guidepost>();
        block.setGuidepost(guideposts);

        Guidepost guidepost = new Guidepost();
        guideposts.add(guidepost);
        guidepost.setLocation("人民广场");
        guidepost.setBus(rs.getInt("gj_rmgc"));
        guidepost.setMetro(rs.getInt("dt_rmgc"));
        guidepost = new Guidepost();
        guideposts.add(guidepost);
        guidepost.setLocation("陆家嘴");
        guidepost.setBus(rs.getInt("gj_ljz"));
        guidepost.setMetro(rs.getInt("dt_ljz"));
        guidepost = new Guidepost();
        guideposts.add(guidepost);
        guidepost.setLocation("徐家汇");
        guidepost.setBus(rs.getInt("gj_xjh"));
        guidepost.setMetro(rs.getInt("dt_xjh"));
        guidepost = new Guidepost();
        guideposts.add(guidepost);
        guidepost.setLocation("静安寺");
        guidepost.setBus(rs.getInt("gj_jas"));
        guidepost.setMetro(rs.getInt("dt_jas"));
        guidepost = new Guidepost();
        guideposts.add(guidepost);
        guidepost.setLocation("五角场");
        guidepost.setBus(rs.getInt("gj_wjc"));
        guidepost.setMetro(rs.getInt("dt_wjc"));
    }

    public void createDescription(String description, String email, long communityId, Long principalId) {
        jdbcTemplate.update("insert into community_pinglun (id,community,comment,user_id,email)values(?,?,?,?,?)",
                Math.abs(UUID.randomUUID().hashCode()),
                communityId,
                description,
                principalId,
                email);
    }
}
