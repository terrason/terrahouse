/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.terramagnet.terrahouse.portal.module.house;

import java.io.Serializable;

/**
 * 房源基本信息-圆圈分值信息.
 *
 * @author lip
 */
public class HouseOverviewEssentialScore implements Serializable {

    /**
     * 地段.
     */
    private String location;
    /**
     * 房型.
     */
    private String layout;
    /**
     * 装修.
     */
    private String decoration;
    /**
     * 周边.
     */
    private String surround;
    /**
     * 楼层.
     */
    private String storey;
    /**
     * 房龄.
     */
    private String age;

    /**
     * 地段.
     *
     * @return the location
     */
    public String getLocation() {
        return location;
    }

    /**
     * 地段.
     *
     * @param location the location to set
     */
    public void setLocation(String location) {
        this.location = location;
    }

    /**
     * 房型.
     *
     * @return the layout
     */
    public String getLayout() {
        return layout;
    }

    /**
     * 房型.
     *
     * @param layout the layout to set
     */
    public void setLayout(String layout) {
        this.layout = layout;
    }

    /**
     * 装修.
     *
     * @return the decoration
     */
    public String getDecoration() {
        return decoration;
    }

    /**
     * 装修.
     *
     * @param decoration the decoration to set
     */
    public void setDecoration(String decoration) {
        this.decoration = decoration;
    }

    /**
     * 周边.
     *
     * @return the surround
     */
    public String getSurround() {
        return surround;
    }

    /**
     * 周边.
     *
     * @param surround the surround to set
     */
    public void setSurround(String surround) {
        this.surround = surround;
    }

    /**
     * 楼层.
     *
     * @return the storey
     */
    public String getStorey() {
        return storey;
    }

    /**
     * 楼层.
     *
     * @param storey the storey to set
     */
    public void setStorey(String storey) {
        this.storey = storey;
    }

    /**
     * 房龄.
     *
     * @return the age
     */
    public String getAge() {
        return age;
    }

    /**
     * 房龄.
     *
     * @param age the age to set
     */
    public void setAge(String age) {
        this.age = age;
    }
}
