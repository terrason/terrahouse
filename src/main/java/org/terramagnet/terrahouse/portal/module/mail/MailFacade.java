/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.terramagnet.terrahouse.portal.module.mail;

import freemarker.template.TemplateException;
import java.io.IOException;
import javax.annotation.Resource;
import javax.mail.MessagingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.terramagnet.terrahouse.portal.template.FreemarkerConfiguration;
import org.terramagnet.terrahouse.portal.util.Utils;

/**
 *
 * @author lip
 */
@Service
public class MailFacade {
    private final Logger logger=LoggerFactory.getLogger(MailFacade.class);
    @Resource
    private FreemarkerConfiguration freemarkerConfiguration;
    @Resource
    private MailService mailService;
    @Value("#{settings['system.mailback.activate']}")
    private String activateUrl;
    @Value("#{settings['system.mailback.password']}")
    private String resetPasswordUrl;
    @Value("#{settings['system.mailback.share']}")
    private String shareUrl;
    /**
     * 向某邮箱发送欢迎邮件.
     *
     * @param email 邮箱
     * @throws MessagingException 发送邮件失败
     */
    public void sendWellcomeMail(String email) throws MessagingException {
        try {
            String uid = email.toLowerCase();
            String url = activateUrl + Utils.getBase64().encodeAsString(uid.getBytes());
            logger.debug("发送激活邮件，账号激活链接：{}", url);
            String html = freemarkerConfiguration.instanceWellcomeMailContent(url);
            mailService.sendEmail("欢迎您注册窝噻网", html, "text/html", uid);
        } catch (TemplateException ex) {
            throw new RuntimeException("实例化邮件模版失败", ex);
        } catch (IOException ex) {
            throw new RuntimeException("实例化邮件模版失败", ex);
        }
    }

    /**
     * 向多个邮箱发送房源分享邮件.
     *
     * @param email 邮箱
     * @param houseId 房源ID
     * @throws MessagingException 发送邮件失败
     */
    public void sendShareMail(long houseId,String... email) throws MessagingException {
        try {
            String url = activateUrl + houseId;
            logger.debug("发送分享邮件，分享链接：{}", url);
            String html = freemarkerConfiguration.instanceWellcomeMailContent(url);
            mailService.sendEmail("窝噻网-房源分享", html, "text/html", email);
        } catch (TemplateException ex) {
            throw new RuntimeException("实例化邮件模版失败", ex);
        } catch (IOException ex) {
            throw new RuntimeException("实例化邮件模版失败", ex);
        }
    }

    /**
     * 发送密码重置邮件.
     *
     * @param email 注册邮箱
     *
     * @throws javax.mail.MessagingException 发送邮件失败
     */
    public void sendPasswordMail(String email) throws MessagingException {
        try {
            String uid = email.toLowerCase();
            String url = resetPasswordUrl + Utils.getBase64().encodeAsString(uid.getBytes());
            logger.debug("发送密码重置邮件，密码重置链接：{}", url);
            String html = freemarkerConfiguration.instanceResetPasswordContent(url);
            mailService.sendEmail("窝噻网-密码重置", html, "text/html", uid);
        } catch (TemplateException ex) {
            throw new RuntimeException("实例化邮件模版失败", ex);
        } catch (IOException ex) {
            throw new RuntimeException("实例化邮件模版失败", ex);
        }
    }

}
