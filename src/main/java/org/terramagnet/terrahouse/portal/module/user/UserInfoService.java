package org.terramagnet.terrahouse.portal.module.user;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import javax.annotation.Resource;
import javax.mail.MessagingException;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.terramagnet.terrahouse.portal.module.DuplicatedEntityException;
import org.terramagnet.terrahouse.portal.module.mail.MailFacade;

/**
 * 处理用户相关服务.
 *
 * @author lip
 */
@Service
public class UserInfoService {

    private final Logger logger = LoggerFactory.getLogger(UserInfoService.class);
    @Resource
    private JdbcTemplate jdbcTemplate;
    @Resource
    private MailFacade mailFacade;

    /**
     * 通过email查找注册用户.
     *
     * @param email 用户email.
     * @return 用户详细信息
     */
    @Cacheable(value = "userInfo", key = "#email")
    public UserInfo findEnityByEmail(String email) {
        List<UserInfo> entities = jdbcTemplate.query("select * from user_info where email_union=?", new RowMapper<UserInfo>() {

            @Override
            public UserInfo mapRow(ResultSet rs, int i) throws SQLException {
                UserInfo entity = new UserInfo();
                entity.setId(rs.getLong("id"));
                entity.setAddress(rs.getString("addr"));
                entity.setAvatar(rs.getString("u_pic"));
                entity.setEmail(rs.getString("email"));
                entity.setMobile(rs.getString("mobile"));
                entity.setPassword(rs.getString("passwords"));
                entity.setSex(rs.getInt("sex"));
                entity.setUsername(rs.getString("user_name"));
                entity.setStatus(rs.getInt("state"));

                entity.setAppointment(3);
                entity.setSubscription(23);
                entity.setFavorite(442);

                return entity;
            }
        }, email.toLowerCase());
        if (entities.isEmpty()) {
            return null;
        } else if (entities.size() > 1) {
            throw new DuplicatedEntityException("用户", entities);
        } else {
            return entities.get(0);
        }
    }

    /**
     * 注册.
     *
     * @param email 注册邮箱
     * @param password 注册密码
     * @return 初始化的用户
     * @throws javax.mail.MessagingException 发送邮件失败
     */
    @Transactional(rollbackFor = Exception.class)
    @CacheEvict(value = "userInfo", key = "#email")
    public UserInfo signUp(String email, String password) throws MessagingException {
        String uid = email.toLowerCase();
        UserInfo entity = new UserInfo();
        entity.setId(Math.abs(uid.hashCode()));
        entity.setUsername("");
        entity.setPassword(DigestUtils.md5Hex(password).toUpperCase());
        entity.setEmail(email);
        entity.setStatus(0);

        jdbcTemplate.update("insert into user_info(id,user_name,passwords,email,email_union,`type`,is_invode,c_cion,f_cion,`state`)values(?,?,?,?,?,'用户',0,3,5,0)",
                entity.getId(),
                entity.getUsername(),
                entity.getPassword(),
                entity.getEmail(),
                entity.getEmail().toLowerCase());
        mailFacade.sendWellcomeMail(email);
        return entity;
    }

    /**
     * 激活账号.
     *
     * @param email 电子邮箱
     */
    @CacheEvict(value = "userInfo", key = "#email")
    public void activate(String email) {
        jdbcTemplate.update("update user_info set state=0 where email_union=?", email.toLowerCase());
    }

    /**
     * 设置密码.
     *
     * @param email 账号邮箱.
     * @param password 新的密码
     */
    @CacheEvict(value = "userInfo", key = "#email")
    public void passwd(String email, String password) {
        jdbcTemplate.update("update user_info set passwords=? where email_union=?", DigestUtils.md5Hex(password), email.toLowerCase());
    }

    @CachePut(value = "userInfo", key = "#entity.email.toLowerCase()")
    public UserInfo updateAuthzInfo(UserInfo entity) {
        jdbcTemplate.update("update user_info set mobile=?,user_name=? where email_union=?;", entity.getMobile(), entity.getUsername(), entity.getEmail().toLowerCase());
        return entity;
    }

    /**
     * 判断某个电子邮箱是否被用户注册过.
     *
     * @deprecated
     * 使用{@link #findEnityByEmail(java.lang.String) findEnityByEmail}代替，findEnityByEmail会自动用户信息。
     *
     * @param email 待测试的电子邮箱地址
     * @return 该邮件已被注册过返回{@code true}.
     */
    public boolean isEmailRegist(String email) {
        int exist = jdbcTemplate.queryForObject("select count(1) from user_info where email_union=?", Integer.class, email.toLowerCase());
        return exist > 0;
    }

    /**
     * 判断用户密码是否正确.
     *
     * @deprecated
     * 使用{@link #findEnityByEmail(java.lang.String) findEnityByEmail}代替，findEnityByEmail会自动用户信息。
     *
     * @param email 用户email.
     * @param password 用户密码（明文）
     * @return 用户密码正确返回{@code true}.
     */
    public boolean isPasswordValid(String email, String password) {
        int exist = jdbcTemplate.queryForObject("select count(1) from user_info where email_union=? and passwords=?", Integer.class, email.toLowerCase(), password);
        return exist > 0;
    }
}
