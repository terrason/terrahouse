/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.terramagnet.terrahouse.portal.module.authorization;

/**
 * 实体接口. 需要权限控制的用户需实现此接口接口.
 * @author terrason
 */
public interface Principal {
    /**
     * 当前实体是否已授权.
     * 在用户手动输入用户名密码后，该方法返回{@code true}.
     * @return 
     */
    public boolean isAuthenticated();
    
}
