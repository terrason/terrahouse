package org.terramagnet.terrahouse.portal.module.house;

import java.io.Serializable;
import java.util.List;

public class HouseSurroundTraffic implements Serializable {

    private static final long serialVersionUID = 1L;
    private String overview;
    private List<Guidepost> guidepost;

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }
    
    public void setOverview(byte[] overviewBlob) {
        if (overviewBlob != null) {
            overview = new String(overviewBlob);
            overview = overview.replaceAll("\0", " ");
        } else {
            overview = "";
        }
    }

    public List<Guidepost> getGuidepost() {
        return guidepost;
    }

    public void setGuidepost(List<Guidepost> guidepost) {
        this.guidepost = guidepost;
    }
}
