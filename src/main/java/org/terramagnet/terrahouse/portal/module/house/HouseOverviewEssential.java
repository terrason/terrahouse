/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.terramagnet.terrahouse.portal.module.house;

import java.io.Serializable;
/**
 * 房源信息. 基本信息.
 * @author lip
 */
public class HouseOverviewEssential implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 总价.
     */
    private double totalPrice;
    /**
     * 单价.
     */
    private double unitPrice;
    /**
     * 房型描述.
     */
    private String schema;
    /**
     * 装修.
     */
    private String decoration;
    /**
     * 面积.
     */
    private String acreage;
    /**
     * 楼层.
     */
    private String storey;
    /**
     * 居室.
     */
    private String layout;
    /**
     * 年代.
     */
    private String years;
    /**
     * 圆圈分值信息.
     */
    private HouseOverviewEssentialScore cycle;

    /**
     * 总价.
     * @return the totalPrice
     */
    public double getTotalPrice() {
        return totalPrice;
    }

    /**
     * 总价.
     * @param totalPrice the totalPrice to set
     */
    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    /**
     * 单价.
     * @return the unitPrice
     */
    public double getUnitPrice() {
        return unitPrice;
    }

    /**
     * 单价.
     * @param unitPrice the unitPrice to set
     */
    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    /**
     * 房型描述.
     * @return the schema
     */
    public String getSchema() {
        return schema;
    }

    /**
     * 房型描述.
     * @param schema the schema to set
     */
    public void setSchema(String schema) {
        this.schema = schema;
    }

    /**
     * 装修.
     * @return the decoration
     */
    public String getDecoration() {
        return decoration;
    }

    /**
     * 装修.
     * @param decoration the decoration to set
     */
    public void setDecoration(String decoration) {
        this.decoration = decoration;
    }

    /**
     * 面积.
     * @return the acreage
     */
    public String getAcreage() {
        return acreage;
    }

    /**
     * 面积.
     * @param acreage the acreage to set
     */
    public void setAcreage(String acreage) {
        this.acreage = acreage;
    }

    /**
     * 楼层.
     * @return the storey
     */
    public String getStorey() {
        return storey;
    }

    /**
     * 楼层.
     * @param storey the storey to set
     */
    public void setStorey(String storey) {
        this.storey = storey;
    }

    /**
     * 居室.
     * @return the layout
     */
    public String getLayout() {
        return layout;
    }

    /**
     * 居室.
     * @param layout the layout to set
     */
    public void setLayout(String layout) {
        this.layout = layout;
    }

    /**
     * 年代.
     * @return the years
     */
    public String getYears() {
        return years;
    }

    /**
     * 年代.
     * @param years the years to set
     */
    public void setYears(String years) {
        this.years = years;
    }

    /**
     * 圆圈分值信息.
     * @return the cycle
     */
    public HouseOverviewEssentialScore getCycle() {
        return cycle;
    }

    /**
     * 圆圈分值信息.
     * @param cycle the cycle to set
     */
    public void setCycle(HouseOverviewEssentialScore cycle) {
        this.cycle = cycle;
    }

}
