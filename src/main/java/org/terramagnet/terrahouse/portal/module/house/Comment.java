/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.terramagnet.terrahouse.portal.module.house;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 房源评论.
 *
 * @author lip
 */
public class Comment implements Serializable {

    private static final long serialVersionUID = 1L;
    private long id;
    /**
     * 所评论的房源ID.
     */
    private long houseId;
    /**
     * 发布评论的用户ID.
     */
    private long customerId;
    /**
     * 对评论进行评论时的目标评论ID. 默认0.
     */
    private long target;
    /**
     * 评论者姓名.
     */
    private String customerName;
    /**
     * 评论者图像.
     */
    private String avatar;
    /**
     * 评论内容.
     */
    private String text;
    /**
     * 创建时间.
     */
    private Date createTime;
    /**
     * 管理员回复.
     */
    private List<Comment> reply;

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * 所评论的房源ID.
     * @return the houseId
     */
    public long getHouseId() {
        return houseId;
    }

    /**
     * 所评论的房源ID.
     * @param houseId the houseId to set
     */
    public void setHouseId(long houseId) {
        this.houseId = houseId;
    }

    /**
     * 发布评论的用户ID.
     * @return the customerId
     */
    public long getCustomerId() {
        return customerId;
    }

    /**
     * 发布评论的用户ID.
     * @param customerId the customerId to set
     */
    public void setCustomerId(long customerId) {
        this.customerId = customerId;
    }

    /**
     * 对评论进行评论时的目标评论ID. 默认0.
     * @return the target
     */
    public long getTarget() {
        return target;
    }

    /**
     * 对评论进行评论时的目标评论ID. 默认0.
     * @param target the target to set
     */
    public void setTarget(long target) {
        this.target = target;
    }

    /**
     * 评论者姓名.
     * @return the customerName
     */
    public String getCustomerName() {
        return customerName;
    }

    /**
     * 评论者姓名.
     * @param customerName the customerName to set
     */
    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    /**
     * 评论者图像.
     * @return the avatar
     */
    public String getAvatar() {
        return avatar;
    }

    /**
     * 评论者图像.
     * @param avatar the avatar to set
     */
    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    /**
     * 评论内容.
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     * 评论内容.
     * @param text the text to set
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * 创建时间.
     * @return the createTime
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 创建时间.
     * @param createTime the createTime to set
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 管理员回复.
     * @return the reply
     */
    public List<Comment> getReply() {
        return reply;
    }

    /**
     * 管理员回复.
     * @param reply the reply to set
     */
    public void setReply(List<Comment> reply) {
        this.reply = reply;
    }


}
