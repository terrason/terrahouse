package org.terramagnet.terrahouse.portal.module.house;

import java.util.UUID;
import javax.annotation.Resource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class HouseRecommendService {

    @Resource
    private JdbcTemplate jdbcTemplate;

    public void saveSearchRecord(HouseRecommend recommendation) {
        jdbcTemplate.update("insert into recommend(ID,U_ID,AREA_NAME,PRICE_LOWER,PRICE_UPER,JTIME)values(?,?,?,?,?,now())",
                Math.abs(UUID.randomUUID().hashCode()),
                recommendation.getUserId(),
                recommendation.getPolity(),
                recommendation.getPriceTo(),
                recommendation.getPriceFrom());

    }
}
