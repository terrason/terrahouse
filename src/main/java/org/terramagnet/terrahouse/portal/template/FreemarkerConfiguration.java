package org.terramagnet.terrahouse.portal.template;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import java.io.IOException;
import java.io.StringWriter;
import org.springframework.stereotype.Service;

@Service
public class FreemarkerConfiguration {

    private final Configuration configuration;
    private Template activate;
    private Template passwd;

    public FreemarkerConfiguration() {
        Configuration cfg = new Configuration();
        cfg.setDefaultEncoding("UTF-8");
        cfg.setClassForTemplateLoading(getClass(), "");
        configuration = cfg;
    }

    public String instanceWellcomeMailContent(String url) throws TemplateException, IOException{
        if (activate == null) {
            activate = configuration.getTemplate("email-welcome.html");
        }
        MailRoot root=new MailRoot();
        root.setUrl(url);
        StringWriter htmlBuffer = new StringWriter();
        activate.process(root, htmlBuffer);
        return htmlBuffer.toString();
    }
    
    public String instanceResetPasswordContent(String url) throws TemplateException, IOException{
        if (passwd == null) {
            passwd = configuration.getTemplate("email-reset-password.html");
        }
        MailRoot root=new MailRoot();
        root.setUrl(url);
        StringWriter htmlBuffer = new StringWriter();
        passwd.process(root, htmlBuffer);
        return htmlBuffer.toString();
    }
}
