package org.terramagnet.terrahouse.portal.template;

public class MailRoot {

    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
