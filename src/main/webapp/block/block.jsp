<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jspf/prepare.jspf" %>
<!DOCTYPE html>
<html>
    <head>
        <title>街区-<spring:message code="application.title"/></title>
        <%@include file="/WEB-INF/jspf/common.jspf" %>
	<script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=sZrTH9UHFf5i9T5KDx122dUB"></script>
	<link href="${ctx}/css/house-item.css" rel="stylesheet"/>
	<link href="${ctx}/css/blocks-menu.css" rel="stylesheet"/>
        <link href="${ctx}/css/house-suggestions.css" rel="stylesheet"/>
	<link href="${ctx}/css/block.css" rel="stylesheet"/>
        <script src="${ctx}/js/terrahouse-bmap.js"></script>
	<script src="${ctx}/js/blocks-menu.js"></script>
	<script src="${ctx}/js/block.js"></script>
        <script type="text/javascript" src="${ctx}/js/house-suggestions.js"></script>
    </head>
    <body>
	<div class="sticky-container">
	    <%@include file="/WEB-INF/jspf/navigation.jspf" %>
	    <div id="quick-nav">
		<div class="content-width">
		    <ul class="sf-menu clearfix">
			<li><a href="javascript:;">${entity.name}</a>
			    <ul>
				<c:forEach items="${polities}" var="cur">
				    <li>
					<a href="javascript:;">${cur.name}</a>
					<c:if test="${not empty cur.communities}">
					    <ul>
						<c:forEach items="${cur.communities}" var="block">
						    <li>
							<a href="${ctx}/block/${block.id}">${block.name}</a>
						    </li>
						</c:forEach>
					    </ul>
					</c:if>
				    </li>
				</c:forEach>
			    </ul>
			</li>
		    </ul>
		    <div id="quick-actions">
			<ul>
			    <li><a href="#descriptions">街区概述</a></li>
			    <li><a href="#geographic-information">地理信息</a></li>
			    <li><a href="#block-images">街区剪影</a></li>
			    <li><a href="#house-suggestions">街区房源</a></li>
			</ul>
		    </div>
		    <div class="clear"></div>
		</div>
		<div class="quick-nav-shadow"></div>
	    </div>
	</div>
	<div id="top-image-container" class="clearfix">
	    <img src="${empty entity.logo ? ctx : srx}${empty entity.logo ? "/images/block-cover.jpg" : entity.logo}" alt="">
	    <div class="block-name">${entity.name}</div>
	    <div id="button-more-houses-container">
		<div class="content-width">
		    <a id="button-more-houses" class="btn btn-primary btn-default" href="${ctx}/search?polity=${entity.areaName}">查看该街区更多房源</a>
		</div>
	    </div>
	</div>
	<div id="descriptions" class="content-width clearfix">
	    <div class="block-decription-container">
		<div class="title">街区概述</div>
		<div class="content">
		    ${entity.description}
		</div>
		<div class="full-text-switcher">
		    <a href="javascript:;">
			<span class="icon-less-more-toggle-blue"></span>
			<span class="text">显示全文</span>
		    </a>
		</div>
	    </div>
	    <div class="add-decription-container">
		<div class="title">比窝噻网了解更多？</div>
		<div class="content">
		    <p>我们很乐意看到您将自己对于这个街区的心得和感悟积极地分享给大家。窝噻尚未发现地精彩等您去挖掘。</p>
		</div>
		<button id="i-wanna-say-something-button" class="btn btn-sm btn-light btn-with-left-icon" type="button">
		    <span class="icon-plus"></span> 我要发言
		</button>
		<div id="i-wanna-say-something-popup">
		    <div class="title">比窝噻了解更多？</div>
		    <form id="form-describe" class="validate" role="form" action="${ctx}/block/${id}/describe" method="post">
			<div class="form-group input-email">
                            <input class="form-control" type="email" placeholder="email@example.com" name="email" value="${principal.email}"
				   data-rule-required="true"
				   data-rule-email="true"
				   data-msg-email="请输入格式正确的电子邮箱"
				   data-rule-maxlength="60"/>
			    <span class="icon-email"></span>
			</div>
			<div class="form-group">
			    <input type="hidden" name="blockId" value="${id}"/>
			    <span>我们鼓励大家一起分享更多有用的街区信息，帮助更多的人选择好未来的住处。向窝噻说说你生活在这个街区里的各种发现和体会吧！</span>
			</div>
			<div class="form-group">
			    <textarea class="form-control" rows="3" name="description"
				      data-rule-maxlength="300"></textarea>
			</div>
			<div class="form-group" style="text-align: right;">
			    <button id="close-describe" class="btn btn-gray btn-default" type="button">关&nbsp&nbsp闭</button>
			    <button class="btn btn-primary btn-default" type="submit">提&nbsp&nbsp交</button>
			</div>
		    </form>
		</div>

	    </div>
	</div>
	<div id="geographic-information" class="content-width">
	    <div class="text">
		<div class="title">地理信息</div>
		<div class="content">
		    ${entity.position}
		</div>
	    </div>
	    <div id="map" class="bmap-polygon"
                 data-x="${entity.axis.x}"
                 data-y="${entity.axis.y}"
                 data-polygon="${entity.regionPeaks}"></div>
	    <div id="traffic" class="clearfix">
		<div class="metro">
		    <div class="title">附近的轨道交通</div>
		    <ul class="clearfix">
			<c:forEach items="${entity.metro}" var="cur">
			    <li class="icon-metro-${cur}">
				<img src="${ctx}/images/icons-metro-lines.png" />
			    </li>
			</c:forEach>
		    </ul>
		</div>
		<table>
		    <c:forEach items="${entity.guidepost}" var="cur" varStatus="status">
			<tr>
			    <td class="commercial-area">${cur.location}</td>
			    <td class="bus">
				<span class="icon-bus"></span><span class="time-cost">${cur.bus}</span>min
			    </td>
			    <td class="texi">
				<span class="icon-texi"></span><span class="time-cost">${cur.metro}</span>min
			    </td>
			</tr>
		    </c:forEach>
		</table>
	    </div>
	</div>
	<div id="block-images">
	    <div class="content-width">
		<div class="title">街区剪影</div>
		<div class="section">
		    ${entity.gallery}
		</div>
	    </div>
	</div>

	<div id="house-suggestions" class="house-suggestions">
            <div class="content-width"><div class="title">街区推荐房源</div></div>
            <div class="items content-width clearfix" data-page="0" data-page-size="2" data-polity="${entity.areaName}"></div>
            <div class="content-width"><a id="button-more-houses" class="btn btn-primary btn-default btn-light" href="${ctx}/search?polity=${entity.areaName}">查看该街区更多房源</a></div>
            <div class="house-item-template">
                <div class="house-item">
                    <a class="house-anchor" href="javascript:;">
                        <img class="preview" src="${ctx}/images/house-item-pic-default.png" alt="" style="width:238px;height:140px;"/>
                        <table>
                            <tr>
                                <td class='name'>丝庐花语</td>
                                <td class='price'><span class='number'>580</span>万</td>
                            </tr>
                            <tr>
                                <td class='address'>徐汇，斜土路大木桥</td>
                                <td class='area'><span class='number'>125</span>m<sup>2</sup></td>
                            </tr>
                        </table>
                    </a>
                </div>
            </div>
        </div>
	<%@include file="/WEB-INF/jspf/copyright.jspf" %>
    </body>
</html>
