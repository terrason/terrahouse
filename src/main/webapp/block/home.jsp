<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jspf/prepare.jspf" %>
<!DOCTYPE html>
<html>
    <head>
	<title>街区-<spring:message code="application.title"/></title>
        <%@include file="/WEB-INF/jspf/common.jspf" %>
	<link href="${ctx}/css/blocks-menu.css" rel="stylesheet">
	<link href="${ctx}/css/blocks.css" rel="stylesheet">
	<script src="${ctx}/js/blocks-menu.js"></script>
    </head>
    <body>
	<%@include file="/WEB-INF/jspf/navigation.jspf" %>
	<div id="quick-nav">
	    <div class="content-width clearfix">
		<ul class="sf-menu clearfix">
		    <li><a href="#">街区</a>
			<ul>
			    <c:forEach items="${polities}" var="cur">
				<li>
				    <a href="javascript:;">${cur.name}</a>
				    <c:if test="${not empty cur.communities}">
					<ul>
					    <c:forEach items="${cur.communities}" var="block">
						<li>
						    <a href="${ctx}/block/${block.id}">${block.name}</a>
						</li>
					    </c:forEach>
					</ul>
				    </c:if>
				</li>
			    </c:forEach>
			</ul>
		    </li>
		</ul>
	    </div>
	    <div class="quick-nav-shadow"></div>
	</div>
	<div id="top-image-container">
	    <img src="${ctx}/images/blocks-top-img.jpg" alt="">
	    <div class="city-description-container">
		<div class="city-name">上海</div>
		<div class="city-spell">Shanghai</div>
		<div class="description">中式的地道风华与西欧的情调生活精妙融合，扑面的清新而奢靡气息，彰显着属于这座魔幻之都的时尚魅力。</div>
	    </div>
	</div>
	<div class="content-width clearfix">
	    <div class="all-block-title">所有街区</div>
	    <dl>
		<c:forEach items="${polities}" var="cur">
		    <dt>${cur.name}</dt>
		    <dd>
			<ul class="clearfix">
			    <c:forEach items="${cur.communities}" var="block">
				<li>
				    <a href="${ctx}/block/${block.id}">${block.name}</a>
				</li>
			    </c:forEach>
			</ul>
		    </dd>
		</c:forEach>
	    </dl>
	</div>
        <%@include file="/WEB-INF/jspf/copyright.jspf" %>
    </body>
</html>
