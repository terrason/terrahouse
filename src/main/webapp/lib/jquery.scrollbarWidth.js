// - See more at: http://
// chris-spittles.co.uk/jquery-calculate-scrollbar-width/#sthash.pVtbiET0.dpuf

(function($, undefined) {
	$.scrollbarWidth = function() {
		var $inner = $('<div style="width: 100%; height:200px;">test</div>');
		var $outer = $(
				'<div style="width:200px;height:150px; position: absolute; top: 0; left: 0; visibility: hidden; overflow:hidden;"></div>')
				.append($inner);
		var inner = $inner[0];
		var outer = $outer[0];
		$('body').append(outer);
		var width1 = inner.offsetWidth;
		$outer.css('overflow', 'scroll');
		var width2 = outer.clientWidth;
		$outer.remove();
		var result = width1 - width2;
		$.scrollbarWidth = function() {
			return result;
		};
		return result;
	};
})(jQuery);