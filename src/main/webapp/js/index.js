$(function() {
    var wall = null;
    var timerCounter = 0;
    $('.carousel').carousel({
	interval: 8000
    });
    $('.carousel .carousel-control').hover(function() {
	$('.carousel .carousel-control').addClass('hilight');
    }, function() {
	$('.carousel .carousel-control').removeClass('hilight');
    });
    $('input, textarea').placeholder();

    $(document).mouseup(function(e) {
	var container = $('#search-price-popover');
	var containerTrigger = $('#search-price-popover-trigger');

	if ((!container.is(e.target) && container.has(e.target).length === 0)
		&& (!containerTrigger.is(e.target) && containerTrigger.has(e.target).length === 0)) {
	    container.hide();
	    refreshPriseArea();
	}
    });

    $('#search-price-popover-trigger').click(function() {
	$('#search-price-popover').toggle();
	refreshPriseArea();
    });

    $(".numeric").numeric();

    $('#how-to .tab-head#tab-head-sold').click(function() {
	if (!$(this).hasClass('current')) {
	    timer.stop();
	    $("#how-to ul.tab-contant#tab-sold").each(function(i, element) {
		$lis = $(this).find('li');
		$lis.removeClass('hovered');
		$(this).children().eq(0).addClass('hovered');
		timerCounter = 1;
		timer.play();
	    });
	    $('#how-to .tab-head#tab-head-buy').toggleClass('current');
	    $('#how-to .tab-head#tab-head-sold').toggleClass('current');
	    $('#how-to .tab-content-container').switchClass('left', 'right', 500);
	}
    });
    $('#how-to .tab-head#tab-head-buy').click(function() {
	if (!$(this).hasClass('current')) {
	    timer.stop();
	    $("#how-to ul.tab-contant#tab-buy").each(function(i, element) {
		$lis = $(this).find('li');
		$lis.removeClass('hovered');
		$(this).children().eq(0).addClass('hovered');
		timerCounter = 1;
		timer.play();
	    });
	    $('#how-to .tab-head#tab-head-buy').toggleClass('current');
	    $('#how-to .tab-head#tab-head-sold').toggleClass('current');
	    $('#how-to .tab-content-container').switchClass('right', 'left', 500);
	}
    });

    wall = new freewall("#suggestion .items");
    wall.reset({
	selector: '.house-item',
	animate: false,
	cellW: 230,
	cellH: 188,
	onResize: function() {
	    wall.refresh();
	}
    });
    wall.fitWidth();
    // for scroll bar appear;
    $(window).trigger("resize");

    loadMoreSuggestion();
    $('#suggestion .loading img').hide();
    // $('#load-more').click(loadMoreSuggestion);
    $('#load-more').click(function() {
	$('#suggestion .loading img').show();
	$.timer(loadMoreSuggestion).once(2000);
    });

    var timer = $.timer(function() {
	$("#how-to ul.tab-contant").each(function(i, element) {
	    $lis = $(this).find('li');
	    $lis.removeClass('hovered');
	    $(this).children().eq(timerCounter % 4).addClass('hovered');
	});
	timerCounter = timerCounter + 1;
    });

    timer.set({
	time: 2500,
	autostart: true
    });

    function addSuggestions(items) {
	var $itemsContainer = $('#suggestion div.items');
	$.each(items, function(index, value) {
	    var $item = $('.load-more-container .house-item').clone();
	    $item.find('.preview').attr("src", value.preview ? srx + value.preview : ctx + "/images/house-item-pic-default.png");
	    $item.find('.name').text(value.name);
	    $item.find('.price .number').text(value.price);
	    $item.find('.address').text(value.politicalAreaName + "，" + value.communityName);
	    $item.find('.area span.number').text(value.area);
            $item.find(".house-anchor").attr("href",ctx+"/house/"+value.id);

	    $item.appendTo($itemsContainer);
	});

	wall.refresh();
    }

    function loadMoreSuggestion() {
	var $houseSuggestionContainer = $('#suggestion div.items');
	var currentPage = $houseSuggestionContainer.data("page");
	$.get(ctx + "/house/hothouse", {
	    currentPage: currentPage + 1,
	    pageSize: 8
	}).done(function(data) {
	    addSuggestions(data);
	    $houseSuggestionContainer.data("page", currentPage + 1);
	    $('#suggestion .loading img').hide();
	});
    }

    function refreshPriseArea() {
	var from = $('[name=priceFrom]').val();
	var to = $('[name=priceTo]').val();
	if (from != '' && to != '') {
	    $('#search-price').html(
		    from + '&nbsp;至&nbsp;' + to
		    + '万&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');
	} else if (from == '' && to != '') {
	    $('#search-price').html(to + '万以下&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');
	} else if (from != '' && to == '') {
	    $('#search-price').html(from + '万以上&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');
	} else {
	    $('#search-price').html('价格');

	}
    }
});

