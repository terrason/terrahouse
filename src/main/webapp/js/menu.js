$(function() {
	$('#navigation #actions>ul>li>ul>li>a').click(function() {
		$(this).find('span.bubble-number').remove();
		var $userInfoLi = $('li.user-info-li');
		var count = $userInfoLi.find('span.bubble-number').length;
		if (count == 0) {
			$userInfoLi.find('span.icon-unread-flag').hide();
		}
	});
});
