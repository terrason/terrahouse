(function($) {
    var FacilityNameOverlay = function(point, text, offset) {
        this._point = point;
        this._text = text;
        this._offset = offset;
    };
    FacilityNameOverlay.prototype = new BMap.Overlay();
    FacilityNameOverlay.prototype.initialize = function(map) {
        this._map = map;
        var div = this._div = document.createElement("div");
        div.style.position = "absolute";
        div.style.zIndex = BMap.Overlay.getZIndex(this._point.lat);
        div.style.padding = "0px";
        var span = this._span = document.createElement("div");
        span.style.padding = "4px";
        span.style.color = "#6e6e6e";
        span.style.backgroundColor = "#ffffff";
        span.style.border = "1px solid #ffffff";
        span.style.whiteSpace = "nowrap";
        span.style.MozUserSelect = "none";
        span.style.fontSize = "12px";
        span.style.borderRadius = "4px";
        div.appendChild(span);
        span.appendChild(document.createTextNode(this._text));
        var that = this;

        var arrow = this._arrow = document.createElement("div");
        arrow.style.width = "18px";
        arrow.style.height = "18px";
        arrow.style.borderTop = "9px solid #ffffff";
        arrow.style.borderLeft = "9px solid transparent";
        arrow.style.borderRight = "9px solid transparent";
        arrow.style.margin = "0px auto";
        div.appendChild(arrow);

        map.getPanes().labelPane.appendChild(div);

        return div;
    };
    FacilityNameOverlay.prototype.draw = function() {
        var map = this._map;
        var pixel = map.pointToOverlayPixel(this._point);
        var left = pixel.x - $(this._div).width() / 2;
        var top = pixel.y - 64;
        if (this._offset) {
            left = left + this._offset.width;
            top = top + this._offset.height;
        }
        this._div.style.left = left + 'px';
        this._div.style.top = top + 'px';
    };
    var icons = [{
            normal: ctx + "/images/icon-block-facility-park-default.png",
            hover: ctx + "/images/icon-block-facility-park-hover.png",
	    small:ctx+"/images/icon-marker-small-default.png",
	    smallHover:ctx+"/images/icon-marker-small-hover.png"
        }, {
            normal: ctx + "/images/icon-block-facility-gas-station-default.png",
            hover: ctx + "/images/icon-block-facility-gas-station-hover.png",
	    small:ctx+"/images/icon-marker-small-default.png",
	    smallHover:ctx+"/images/icon-marker-small-hover.png"
        }, {
            normal: ctx + "/images/icon-block-facility-hospital-default.png",
            hover: ctx + "/images/icon-block-facility-hospital-hover.png",
	    small:ctx+"/images/icon-marker-small-default.png",
	    smallHover:ctx+"/images/icon-marker-small-hover.png"
        }, {
            normal: ctx + "/images/icon-block-facility-mall-default.png",
            hover: ctx + "/images/icon-block-facility-mall-hover.png",
	    small:ctx+"/images/icon-marker-small-default.png",
	    smallHover:ctx+"/images/icon-marker-small-hover.png"
        }, {
            normal: ctx + "/images/icon-block-facility-super-market-default.png",
            hover: ctx + "/images/icon-block-facility-super-market-hover.png",
	    small:ctx+"/images/icon-marker-small-default.png",
	    smallHover:ctx+"/images/icon-marker-small-hover.png"
        }, {
            normal: ctx + "/images/icon-block-facility-library-default.png",
            hover: ctx + "/images/icon-block-facility-library-hover.png",
	    small:ctx+"/images/icon-marker-small-default.png",
	    smallHover:ctx+"/images/icon-marker-small-hover.png"
        }, {
            normal: ctx + "/images/icon-block-facility-car-park-default.png",
            hover: ctx + "/images/icon-block-facility-car-park-hover.png",
	    small:ctx+"/images/icon-marker-small-default.png",
	    smallHover:ctx+"/images/icon-marker-small-hover.png"
        }, {
            normal: ctx + "/images/icon-marker-red.png",
            hover: ctx + "/images/icon-marker-red.png",
	    small:ctx+"/images/icon-marker-small-default.png",
	    smallHover:ctx+"/images/icon-marker-small-hover.png"
        }];

    $.widget("org-terramagnet-terrahouse-bmap.facility", {
        options: {
            index: 0,
            icon: icons,
            markSelf: true,
            ajaxFacility: ctx + "/axis",
            x: 121.387835,
            y: 31.102757,
            type: 7,
            name: '',
            level: 15
        },
        bmap: null,
        _create: function() {
            var widget = this;
            var $element = this.element;
            var options = this.options;
            $.extend(true, options, $element.data());
            var id = $element.attr("id");
            if (!id) {
                id = "bmap-facility-" + this.options.index++;
                this.element.attr("id", id);
            }
            var map = new BMap.Map(id); // 创建Map实例
            this.bmap = map;
            var point = new BMap.Point(this.options.x, this.options.y); // 创建点坐标
            map.centerAndZoom(point, this.options.level); // 初始化地图,设置中心点坐标和地图级别。
            map.enableScrollWheelZoom();
            map.addControl(new BMap.NavigationControl()); // 添加默认缩放平移控件
            map.addControl(new BMap.ScaleControl());// 添加默认比例尺控件

            map.addEventListener("resize", function() {
                map.centerAndZoom(point, options.level); // 重新设置中心点坐标和地图级别。
            });
	    map.addEventListener("zoomend",function(data){
		var level=data.target.qa;
		if(level<=13){
		    var marks=map.getOverlays();
		    for(var i=0;i<marks.length;i++){
			if(marks[i].tag){
			    marks[i].tag.status="small";
			    var size = new BMap.Size(17,17);
			    var anchor=new BMap.Size(-9,-9);
			    marks[i].setIcon(new BMap.Icon(widget.options.icon[marks[i].tag.type].small, size));
			}
		    }
		}else{
		    var marks=map.getOverlays();
		    for(var i=0;i<marks.length;i++){
			if(marks[i].tag){
			    marks[i].tag.status="normal";
			    var size = new BMap.Size(42,57);
			    var anchor=new BMap.Size(-21,-29);
			    marks[i].setIcon(new BMap.Icon(widget.options.icon[marks[i].tag.type].normal, size));
			}
		    }
		}
	    });

            this.addFacility({
                x: options.x,
                y: options.y,
                type: options.type,
                name: options.name
            });
            $.get(this.options.ajaxFacility).done(function(facilities) {
                $.each(facilities, function() {
                    widget.addFacility(this);
                });
            });
        },
        addFacility: function(facility) {
            var widget = this;
            var map = this.bmap;
            var point = new BMap.Point(facility.x, facility.y);
            var icon = new BMap.Icon(widget.options.icon[facility.type].normal, new BMap.Size(43, 57));
            var marker = new BMap.Marker(point, {
                icon: icon
            });
            marker.tag = facility;
            map.addOverlay(marker);
            marker.addEventListener("mouseover", function(type, target, point, pixel) {
		var facility=this.tag;
		var icon = widget.options.icon[this.tag.type];
		var size = facility.status==="small" ? new BMap.Size(17, 17) : new BMap.Size(42,57);
		var anchor = facility.status==="small" ? new BMap.Size(-9, -9) :new BMap.Size(-22,-29);
                this.setIcon(new BMap.Icon(facility.status==="small" ? icon.smallHover:icon.hover, size));
                if (!marker.nameOverlay) {
                    var nameOverlay = new FacilityNameOverlay(marker.getPosition(), facility.name);
                    marker.nameOverlay = nameOverlay;
                    map.addOverlay(nameOverlay);
                } else {
                    marker.nameOverlay.show();
                }
            });
            marker.addEventListener("mouseout", function(type, target, point, pixel) {
		var facility=this.tag;
		var icon = widget.options.icon[this.tag.type];
		var size = facility.status==="small" ? new BMap.Size(17, 17) : new BMap.Size(42,57);
		var anchor = facility.status==="small" ? new BMap.Size(-9, -9) :new BMap.Size(-22,-29);
                this.setIcon(new BMap.Icon(facility.status==="small" ? icon.small:icon.normal, size));
                if (marker.nameOverlay && marker.nameOverlay.isVisible()) {
                    marker.nameOverlay.hide();
                }
            });
        }
    });
    $.widget("org-terramagnet-terrahouse-bmap.panorama", {
        options: {
            index: 0,
            x: 121.387835,
            y: 31.102757,
            type: 7,
            name: '',
            level: 14
        },
        _create: function() {
            var $element = this.element;
            var options = this.options;
            $.extend(true, options, $element.data());
            this._setup();
        },
        _setup: function() {
            var id = this.element.attr("id");
            if (!id) {
                id = "bmap-panorama-" + this.options.index++;
                this.element.attr("id", id);
            }
	    var options=this.options;
            var mapStreatView = new BMap.Map(id); // 创建Map实例
            var point = new BMap.Point(this.options.x, this.options.y); // 创建点坐标
            mapStreatView.centerAndZoom(point, this.options.level); // 初始化地图,设置中心点坐标和地图级别。
            mapStreatView.addTileLayer(new BMap.PanoramaCoverageLayer());// 添加街景

            var panorama = new BMap.Panorama(id);
            var panoramaService = new BMap.PanoramaService();
            panoramaService.getPanoramaByLocation(new BMap.Point(this.options.x, this.options.y), 10000000, function(data) {
		if(!data){
		    console.warn("cannot find panorama by location: "+options.x+","+options.y+" !");
		    return;
		}
                panorama.setPosition(new BMap.Point(data.position.lng, data.position.lat));
            });
            panorama.setPov({
                heading: -40,
                pitch: 6
            });
            // mapStreatView.addControl(new BMap.OverviewMapControl()); //添加默认缩略地图控件
            mapStreatView.addControl(new BMap.OverviewMapControl({
                isOpen: true,
                anchor: BMAP_ANCHOR_TOP_RIGHT
            })); // 右上角，打开
        }
    });
    $.widget("org-terramagnet-terrahouse-bmap.polygon", {
        options: {
            index: 0,
            ajaxFacility: ctx + "/axis",
            icon: icons,
            x: 121.387835,
            y: 31.102757,
            polygon: "",
            level: 14
        },
        bmap: null,
        _create: function() {
            var widget = this;
            var $element = this.element;
            var options = this.options;
            $.extend(true, options, $element.data());
            var id = $element.attr("id");
            if (!id) {
                id = "bmap-polygon-" + this.options.index++;
                this.element.attr("id", id);
            }
            var map = new BMap.Map(id); // 创建Map实例
            this.bmap = map;
            var point = new BMap.Point(this.options.x, this.options.y); // 创建点坐标
            map.centerAndZoom(point, this.options.level); // 初始化地图,设置中心点坐标和地图级别。
            map.enableScrollWheelZoom();
            map.addControl(new BMap.NavigationControl()); // 添加默认缩放平移控件
            map.addControl(new BMap.ScaleControl());// 添加默认比例尺控件

            map.addEventListener("resize", function() {
                map.centerAndZoom(point, this.options.level); // 重新设置中心点坐标和地图级别。
            });

            var ply = new BMap.Polygon(this.options.polygon, {
                strokeWeight: 2,
                strokeColor: "#227dda",
                strokeOpacity: 1,
                fillColor: "#227dda",
                fillOpacity: 0.2
            }); // 建立多边形覆盖物
            map.addOverlay(ply); // 添加覆盖物

            $.get(this.options.ajaxFacility).done(function(facilities) {
                $.each(facilities, function() {
                    widget.addFacility(this);
                });
            });
        },
        addFacility: function(facility) {
            var widget = this;
            var map = this.bmap;
            var point = new BMap.Point(facility.x, facility.y);
            var icon = new BMap.Icon(widget.options.icon[facility.type].normal, new BMap.Size(43, 57));
            var marker = new BMap.Marker(point, {
                icon: icon
            });
            marker.tag = facility;
            map.addOverlay(marker);
            marker.addEventListener("mouseover", function(type, target, point, pixel) {
                this.setIcon(new BMap.Icon(widget.options.icon[this.tag.type].hover, new BMap.Size(43, 57)));
                if (!marker.nameOverlay) {
                    var nameOverlay = new FacilityNameOverlay(marker.getPosition(), facility.name, new BMap.Size(-22, -29));
                    marker.nameOverlay = nameOverlay;
                    map.addOverlay(nameOverlay);
                } else {
                    marker.nameOverlay.show();
                }
            });
            marker.addEventListener("mouseout", function(type, target, point, pixel) {
                this.setIcon(new BMap.Icon(widget.options.icon[this.tag.type].normal, new BMap.Size(43, 57)));
                if (marker.nameOverlay && marker.nameOverlay.isVisible()) {
                    marker.nameOverlay.hide();
                }
            });
        }
    });


    $.widget("org-terramagnet-terrahouse-bmap.bmap", {
        options: {
            index: 0,
            icon: icons,
            x: 121.478305,
            y: 31.228908,
            level: 14,
            //events
            mapReady: null,
            controlReady: null,
            facilityReady:null,
            polygonReady:null
        },
        bmap: null,
        controls: {
            search: {
                widget: null,
                $button: $('<button type="button" class="btn btn-white-blue btn-sm">按照地图显示区域重新搜索</button>'),
                defaultAnchor: BMAP_ANCHOR_TOP_LEFT,
                defaultOffset: new BMap.Size(60, 15),
                initialize: function(bmap) {
                    var widget=this.widget;
                    var $btn = this.$button.clone();
                    bmap.getContainer().appendChild($btn[0]);
                    $btn.click(function(event) {
                        widget._trigger("search",event,widget);
                    });
                    return $btn[0];
                }
            }
        },
        _create: function() {
            var widget = this;
            var $element = this.element;
            var options = this.options;
            $.extend(true, options, $element.data());
            var id = $element.attr("id");
            if (!id) {
                id = "bmap-search-" + this.options.index++;
                this.element.attr("id", id);
            }
            var map = new BMap.Map(id); // 创建Map实例
            this.bmap = map;
            var point = new BMap.Point(this.options.x, this.options.y); // 创建点坐标
            map.centerAndZoom(point, this.options.level); // 初始化地图,设置中心点坐标和地图级别。
            map.enableScrollWheelZoom();
            map.addControl(new BMap.NavigationControl()); // 添加默认缩放平移控件
            map.addControl(new BMap.ScaleControl());// 添加默认比例尺控件

            map.addEventListener("resize", function() {
                map.centerAndZoom(new BMap.Point(widget.options.x, widget.options.y), widget.options.level); // 重新设置中心点坐标和地图级别。
            });
            widget._trigger("mapReady",null,widget);

            for (var name in this.controls) {
                var controlTemplate = this.controls[name];
                if (controlTemplate) {
                    var control = $.extend(true, new BMap.Control(), controlTemplate);
                    control.widget = widget;
                    map.addControl(control);
                }
            }
            widget._trigger("controlReady",null,widget);

            if (options.ajaxFacility) {
                $.get(options.ajaxFacility).done(function(facilities) {
                    $.each(facilities, function() {
                        widget.addFacility(this);
                    });
                });
                widget._trigger("facilityReady",null,widget);
            }

            if (options.polygon) {
                var polygonOption = {
                    strokeWeight: $element.data("polygonStrokeWeight") || 2,
                    strokeColor: $element.data("polygonStrokeColor") || "#227dda",
                    strokeOpacity: $element.data("polygonStrokeOpacity") || 1,
                    fillColor: $element.data("polygonFillColor") || "#227dda",
                    fillOpacity: $element.data("polygonFillOpacity") || 0.2
                };
                var polygon = new BMap.Polygon(options.polygon, polygonOption);
                map.addOverlay(polygon);
                widget._trigger("polygonReady",null,widget);
            }
        },
        addFacility: function(facility) {
            var widget = this;
            var map = this.bmap;
            var point = new BMap.Point(facility.x, facility.y);
            var icon = new BMap.Icon(widget.options.icon[facility.type].normal, new BMap.Size(43, 57));
            var marker = new BMap.Marker(point, {
                icon: icon
            });
            marker.tag = facility;
            map.addOverlay(marker);
            marker.addEventListener("mouseover", function(type, target, point, pixel) {
                this.setIcon(new BMap.Icon(widget.options.icon[this.tag.type].hover, new BMap.Size(43, 57)));
                if (!marker.nameOverlay) {
                    var nameOverlay = new FacilityNameOverlay(marker.getPosition(), facility.name);
                    marker.nameOverlay = nameOverlay;
                    map.addOverlay(nameOverlay);
                } else {
                    marker.nameOverlay.show();
                }
            });
            marker.addEventListener("mouseout", function(type, target, point, pixel) {
                this.setIcon(new BMap.Icon(widget.options.icon[this.tag.type].normal, new BMap.Size(43, 57)));
                if (marker.nameOverlay && marker.nameOverlay.isVisible()) {
                    marker.nameOverlay.hide();
                }
            });
        },
        getMap:function(){
            return this.bmap;
        }
    });
})(jQuery);