(function($) {
    var timer;
    var validateCodeInfoFlag = -1;
    var $smsBtn, $form;
    var datePicker = null;
// 可预约的日期和时间段数据
    var availableDays = [];

// 显示的时间段
    var timeScope = ["09:00 - 12:00", "12:00 - 18:00", "18:00 - 21:00"];
// 待开放的日期
    var tobeAvailableDays = [];
// 选择的日期和时间段
    var selectedDay = {
        day: null,
        availableIndex: [], // 可选的时间段的索引
        index: -1          // 选择了的时间段的索引
    };

    $(function() {
        $smsBtn = $('#receive-validate-number');
        $form = $("#form-preorder");
        // 初始化可预约的日期数据
        $(".data-preorder-day").each(function() {
            var preorderDay = {
                day: $(this).data("day"),
                index: []
            };
            $(this).children().each(function(i, item) {
                if ($(item).data("available") === true) {
                    preorderDay.index.push(i);
                }
            });
            availableDays.push(preorderDay);
        });

        // 初始化可预约的日期初始化待开放的日期数据
        // tobeAvailableDays = [ "2013-11-28", "2013-11-29", "2013-11-30" ];

        datePicker = $(".datepicker").datepicker({
            inline: true,
            showOtherMonths: false,
            dateFormat: "yy-mm-dd",
            onSelect: function(date, picker) {
                doOnDayClicked(date, picker);
            },
            beforeShowDay: hilightDays
        });
//        $(".datepicker").datepicker("setDate", "2013-11-12");
        $('#book-it').click(bookIt);
        $('.book-container .select-time .time-item').each(function(i, element) {
            $this = $(this);
            $this.click({
                index: i,
                element: $this
            }, function(event) {
                selectTimeIndex(event);
            });
        });

        timer = $.timer(function() {
            if (validateCodeInfoFlag > 0) {
                $smsBtn.text(validateCodeInfoFlag + '秒后重新获取');
                validateCodeInfoFlag = validateCodeInfoFlag - 1;
            } else {
                $smsBtn.text('重新获取验证码');
                timer.stop();
            }
        }, 1000);
        $smsBtn.click(function() {
            if (validateCodeInfoFlag <= 0
                    && $form.data("validator").element("#phone-num")
                    && $form.data("validator").element("#real-name")) {
                $.post(ctx + "/smscode/" + $("#phone-num").val(), {name: $("#real-name").val()}).done(function(data) {
                    if (debug.sms) {
                        $alertPopup.showAlert("success", "模拟短信验证码", data.message);
                    }
                });
                validateCodeInfoFlag = 15;
                timer.action();
                timer.play();
            }
        });

        $('.popup.book-confirm .checkbox input[type="checkbox"]').change(function() {
            var $btn = $('#book-confirmed-action');
            if (this.checked) {
                $btn.addClass('btn-primary');
                $btn.prop('disabled', false);
            } else {
                $btn.removeClass('btn-primary');
                $btn.prop('disabled', true);
            }
        });
        $form.ajaxForm({
            cache: false,
            beforeSubmit: function(arr, $form, options) {
                return $form.data("validator").form();
            },
            success: function(data, status, xhr, $form) {
                if (data.code === 0) {
                    popupBookConfirmResult();
                } else {
                    $form.data("validator").showErrors({"smscode": data.message});
                }
            },
            error: function(xhr, status, ex, $form) {
                $form.data("validator").showErrors({"smscode": "预约失败"});
            }
        });
        $('.popup.book-confirm-result #book-confirmed-result-action').click(function() {
            $('.popup.popup.book-confirm-result').bPopup().close();
            return true;
        });
    });

// 点击日历某一天的事件
    function doOnDayClicked(date, picker) {
        $.each(availableDays, function(i, element) {
            if (element.day === date) {
                hiddenError();
                selectedDay.index = -1;

                if (selectedDay.day !== date) {
                    selectedDay.day = date;
                    selectedDay.availableIndex = element.index;

                    $('.book-container .select-time').show('blind', 300);
                    resetTimeIndex();
                } else {
                    selectedDay.day = null;
                    selectedDay.availableIndex = [];

                    $('.book-container .select-time').hide('blind', 300);
                    resetTimeIndex(true);
                }
            }
        });

    }

// 自定义要高亮的日期单元格的class
    function hilightDays(date) {

        var formatDay = $.format.date(date, "yyyy-MM-dd");

        if (formatDay === selectedDay.day) {
            return [true, 'selected-days'];
        }

        for (var i = 0; i < tobeAvailableDays.length; i++) {
            if (formatDay === tobeAvailableDays[i]) {
                return [true, 'tobe-available-days'];
            }
        }
        for (var i = 0; i < availableDays.length; i++) {
            if (formatDay === availableDays[i].day) {
                return [true, 'available-days'];
            }
        }
        return [true, ''];
    }

// 点击预定后触发的事件。提交数据在本方法写。
    function bookIt() {
        if (!selectedDay.day) {
            displayCalendarError('请选择日期和具体时段！');
        } else if (selectedDay.index < 0) {
            displayCalendarError('请选择具体时段！');
        } else {
            hiddenError();
            // testDisplayMessage('预约：' + selectedDay.day + ' '
            // + timeScope[selectedDay.index]);

            var date = new Date(selectedDay.day);
            var formatDay = $.format.date(date, "yyyy年MM月dd日，ddd");
            $('.popup.book-confirm form .step .text.right .content').text(formatDay + ' ' + timeScope[selectedDay.index]);
            var dayPre = date.setDate(date.getDate() - 1);
            var formatDayPre = $.format.date(dayPre, "yyyy年MM月dd日ddd");
            $('.popup.book-confirm-result .description span.day-pre').text(formatDayPre);
            $("input[name=day]", $form).val(selectedDay.day);
            $("input[name=time]", $form).val(selectedDay.index);
            popupBookConfirm();
        }
    }

// 点击时间段触发的事件
    function selectTimeIndex(event) {
        var element = event.data.element;
        if (element.hasClass('disabled')) {
            displayCalendarError('抱歉，此时间段不可预约！');
        } else if (!element.hasClass('selected')) {
            hiddenError();
            resetTimeIndex();
            element.addClass('selected');
            selectedDay.index = event.data.index;
            element.find('.checkbox .checkbox-bg').stop().scrollTo('0%', 300);

            element.find('.checkbox .blockContainer').switchClass('left', 'right', 300);
        } else {
            selectedDay.index = -1;
            element.removeClass('selected');
            element.find('.checkbox .blockContainer').switchClass('right', 'left', 300);
            element.find('.checkbox .checkbox-bg').stop().scrollTo('100%', 300);
        }
    }

// 重置时间段样式
    function resetTimeIndex(isNoAnim) {
        $('.book-container .select-time .time-item').each(function(i, value) {
            var $this = $(this);
            $this.removeClass('selected');
            $this.removeClass('disabled');
            $this.find('.checkbox .blockContainer').switchClass('right', 'left', 300);
            if (isNoAnim) {
                $this.find('.checkbox .checkbox-bg').stop().scrollTo('100%', 0);
            } else {
                $this.find('.checkbox .checkbox-bg').stop().scrollTo('100%', 300);
            }
            if ($.inArray(i, selectedDay.availableIndex) < 0) {
                $this.addClass('disabled');
            }
        });
    }

    function displayCalendarError(message) {
        var $error = $('.book-container .error');
        $error.text(message);
    }

    function hiddenError() {
        var $error = $('.book-container .error');
        $error.text('');
    }

    function popupBookConfirm() {
        $('.popup.book-confirm').bPopup({
            modalClose: true,
            opacity: 0.6,
            positionStyle: 'absolute', // 'fixed' or 'absolute',
            fadeSpeed: 'slow', // can be a string ('slow'/'fast') or int
            followSpeed: 'fast'// can be a string ('slow'/'fast') or int
        });
    }
    function popupBookConfirmResult() {
        $('.popup.book-confirm-result').bPopup({
            modalClose: false,
            opacity: 0.6,
            positionStyle: 'absolute', // 'fixed' or 'absolute',
            fadeSpeed: 'slow', // can be a string ('slow'/'fast') or int
            followSpeed: 'fast'// can be a string ('slow'/'fast') or int
        });
    }

    $nojs.before(function(context) {
        var validateOption = {
            errorElement: "span",
            errorClass: "error",
            errorPlacement: function(error, element) {
                var $formGroup = $(element).parent();
                var $error = $("span.error", $formGroup);
                if ($error.length) {
                    $error.text(error.text());
                } else {
                    $formGroup.append(error);
                }
            }
        };
        $("#form-preorder").data(validateOption);
    });
})(jQuery);