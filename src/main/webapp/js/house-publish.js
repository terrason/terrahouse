$(function() {
    var timer;
    var validateCodeInfoFlag = -1;
    var $smsBtn = $('#receive-validate-number');
    var $form = $("#form-publish");

    $form.ajaxForm({
        cache: false,
        beforeSubmit: function(arr, $form, options) {
            return $form.data("validator").form();
        },
        success: function(data, status, xhr, $form) {
            if (data.code === 0) {
                $alertPopup.showAlert("success", null, "房源发布成功", null).one($nojs.EVENT_POPUP_CLOSE, function() {
                    timer.clearTimer();
                    window.location = $form.data("redirect");
                });
            } else {
                $form.data("validator").showErrors({"smscode": data.message});
            }
        },
        error: function(xhr, status, ex, $form) {
            $form.data("validator").showErrors({"smscode": "发布房源失败"});
        }
    });

    var fileuploadOption = {
        dataType: "json",
        acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i, //TODO. not work
        done: function(e, data) {
            // data.result
            // data.textStatus;
            // data.jqXHR;
            var returnedUrl = data.result.message;
            var $currentImageItem = $(this).parent().parent();
            var $newImageItem = $currentImageItem.clone();
            var $ul = $currentImageItem.parent();
            $currentImageItem.find('span.icon-input-file').remove();
            $currentImageItem.find('span.icon-close').show();
            $currentImageItem.css('background-image', "url('" + srx + returnedUrl + "')").css("background-size", "100% 100%");
            var $hidden = $('<input type="hidden" name="snapshots"/>');
            $hidden.val(returnedUrl);
            $form.append($hidden);
            $currentImageItem.data("hidden", $hidden);
            $currentImageItem.find(":file").fileupload("destroy").prop("disabled", true);
            $newImageItem.find(":file").fileupload(fileuploadOption);
            $newImageItem.find('span.icon-close').click(removeLi);
            $newImageItem.appendTo($ul);
        }
    };
    $("[name=file]", $form).fileupload(fileuploadOption);

    function removeLi(e) {
        var $imageItem = $(this).parent().parent();
        $imageItem.data("hidden").remove();
        $imageItem.remove();
    }
    $(".house-publish-container .form-item .images-container ul li span.icon-close").click(removeLi);

    $('div.form-more').hide();
    $('#more-info').click(function() {
        if ($('div.form-more').is(':hidden')) {
            $('#more-info span.text').html("收起");
            $('#more-info .icon-up-down').removeClass('icon-up-down-down');
            $('#more-info .icon-up-down').addClass('icon-up-down-up');
        } else {
            $('#more-info span.text').html("更多信息");
            $('#more-info .icon-up-down').removeClass('icon-up-down-up');
            $('#more-info .icon-up-down').addClass('icon-up-down-down');
        }
        $('div.form-more').toggle('blind', 300);
        return false;
    });

    timer = $.timer(function() {
        if (validateCodeInfoFlag > 0) {
            $smsBtn.text(validateCodeInfoFlag + '秒后重新获取');
            validateCodeInfoFlag = validateCodeInfoFlag - 1;
        } else {
            $smsBtn.text('重新获取验证码');
            timer.stop();
        }
    }, 1000);
    $smsBtn.click(function() {
        if (validateCodeInfoFlag <= 0
                && $form.data("validator").element("#phone-num")
                && $form.data("validator").element("#real-name")) {
            $.post(ctx + "/smscode/" + $("#phone-num").val(), {name: $("#real-name").val()}).done(function(data) {
                if (debug.sms) {
                    $alertPopup.showAlert("success", "模拟短信验证码", data.message);
                }
            });
            validateCodeInfoFlag = 15;
            timer.action();
            timer.play();
        }
    });
});
$nojs.before(function(context) {
    var validateOption = {
        errorElement: "span",
        errorClass: "error",
        errorPlacement: function(error, element) {
            var $formGroup = $(element).parent();
            var $error = $("span.error", $formGroup);
            if ($error.length) {
                $error.text(error.text());
            } else {
                $formGroup.append(error);
            }
        }
    };
    $("#form-publish").data(validateOption);
});