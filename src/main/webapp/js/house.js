$(function() {
    $('.like-container span.icon-like').click(function() {
        var $this = $(this);
        var $count = $this.parent().find('span.like-count');
        var currentLikes = parseInt($count.text());
        var like = !$this.hasClass('liked');
        $.post(ctx + "/house/" + $this.data("house") + "/fork", {like: like}).done(function() {
            if (like) {
                $count.text(currentLikes + 1);
                $this.addClass("liked");
            } else {
                $count.text(currentLikes - 1);
                $this.removeClass("liked");
            }
        });
    });

    $('.carousel').carousel({
        interval: false
    });
    $('.carousel .item').click(function() {
        $('.carousel').carousel('next');

    });

    $('.carousel').on('slide.bs.carousel', function(e) {
        var itemCount = $('.carousel .carousel-inner').children().length;
        var next = ($(e.relatedTarget).index()) % itemCount;
        $('ul.slideshow-thumbnails li.active').removeClass('active');
        var $next = $('ul.slideshow-thumbnails').children().eq(next);
        $next.addClass('active');

        var scrollLeft = $('.thumbnails-viewport').scrollLeft();
        var visableWith = $('.thumbnails-viewport').width();
        var activeWidth = $next.width();
        var activePosition = $next.position().left;

        if (activePosition - 3 < scrollLeft) {
            $('.thumbnails-viewport').stop().scrollTo($next.position().left - 3, 300);
        } else if (activePosition + activeWidth + 3 > scrollLeft + visableWith) {
            $('.thumbnails-viewport').stop().scrollTo(activePosition - visableWith + activeWidth - 3, 300);
        }
    });

    $('.slideshow-btn.slideshow-prev').click(function() {
        $('.carousel').carousel('prev');
    });
    $('.slideshow-btn.slideshow-next').click(function() {
        $('.carousel').carousel('next');
    });
    $('ul.slideshow-thumbnails li').click(function() {
        $('.carousel').carousel($(this).index());
    });

    $('#tabs').tabs();

    $('#tabs .ui-corner-all').not('table.ui-corner-all').removeClass('ui-corner-all');
    $('.ui-corner-top').removeClass('ui-corner-top');
    $('.ui-corner-bottom').removeClass('ui-corner-bottom');

    function loadMoreComments() {
        var $houseSuggestionContainer = $('#tabs-comments .comments');
        var houseId = $houseSuggestionContainer.data("house");
        var currentPage = $houseSuggestionContainer.data("page");
        $.get(ctx + "/house/" + houseId + "/comment", {
            currentPage: currentPage + 1,
            pageSize: 9999
        }).done(function(data) {
            $houseSuggestionContainer.append(data);
            $houseSuggestionContainer.data("page", currentPage + 1);
	    
	    $(".user-name",$houseSuggestionContainer).each(function(){
		var $username=$(this);
		var username=$username.text();
		if(username){
		    if(username.length>7){
			var name=username.substr(0,7)+"...";
			$username.text(name);
		    }
		}
	    });
        });
    }
    loadMoreComments();

    $('#tabs-comments .i-want-comment-container a').click(function() {
        $('#comment-textarea').focus();
        return false;
    });
    $("#comment-form").ajaxForm({
        cache: false,
        beforeSubmit: function(arr, $form, options) {
            return $form.data("validator").form();
        },
        success: function(data, status, xhr, $form) {
            if (data.code === 0) {
                $('.popup.popup-tip').bPopup({
                    modalClose: true,
                    opacity: 0.6,
                    positionStyle: 'absolute', // 'fixed' or 'absolute',
                    fadeSpeed: 'slow', // can be a string ('slow'/'fast') or
                    // int
                    followSpeed: 'fast'
                            // can be a string ('slow'/'fast') or int
                });
            } else {
                $form.data("validator").showErrors({"text": data.message});
            }
        },
        error: function(xhr, status, ex, $form) {
            $form.data("validator").showErrors({"text": "评论失败"});
        }
    });

    $(".bmap-facility").facility();
    $("#map-streat-view").panorama();
});