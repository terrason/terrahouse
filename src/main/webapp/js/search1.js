(function($) {
    $(document).ready(function() {
        var $searchForm = $("#form-search");
        var $searchBtn = $("#submit-location", $searchForm);
        var $minX = $("#minX", $searchForm);
        var $minY = $("#minY", $searchForm);
        var $maxX = $("#maxX", $searchForm);
        var $maxY = $("#maxY", $searchForm);
        var $pageNo = $("#pageNo", $searchForm);
        var $pageSize = $("#pageSize", $searchForm);
        var $pageTotal = $("#pageTotal", $searchForm);
        var $nextPage = $("#nextPage");
        var $orderby = $("#orderby", $searchForm);
        var $needRecord = $("#needRecord", $searchForm);

        var $scroll = $('#right-container');
        var $bmap = $("#map");
        var $houseWidgetsContainer = $('#result-container .items');
        var $loadingPopup = $("#loadingPopup");



        $houseWidgetsContainer.$widgets = function() {
            return  $(".house-item", $houseWidgetsContainer);
        };
        $houseWidgetsContainer.appendHouse = function($widget) {
            var wall = $houseWidgetsContainer.data("widget-freewall");
            $widget.each(function() {
                var $house = $(this);
                $houseWidgetsContainer.append($house);
                $bmap.addHouseWidgets($house);
            });
            wall.fitWidth();
            wall.refresh();
        };
        //查询表单提交动作. ajax 分页请求数据。
        $searchForm.ajaxForm({
            cache: false,
            beforeSubmit: function(arr, $form, options) {
                $loadingPopup.bPopup({
                    modalClose: false,
                    opacity: 0.6,
                    positionStyle: "absolute", // "fixed" or "absolute",
                    fadeSpeed: "slow", // can be a string ("slow"/"fast") or int
                    followSpeed: "fast",
                    onOpen: function() {
                        this.trigger($nojs.EVENT_POPUP_OPEN);
                    },
                    onClose: function() {
                        this.trigger($nojs.EVENT_POPUP_CLOSE);
                    }
                });
            },
            success: function(data, status, xhr, $form) {
                var $container = $($.trim(data));
                var $widget = $(".house-item", $container);
                $houseWidgetsContainer.appendHouse($widget);
                $pageNo.val($container.data("page"));
                $pageTotal.val($container.data("total"));
                $loadingPopup.bPopup().close();
            },
            error: function(xhr, status, ex, $form) {
                $loadingPopup.bPopup().close();
                $alertPopup.showAlert("danger", null, "搜索时出现未知错误");
            }
        });

        //房源排版初始化
        var wall = new freewall($houseWidgetsContainer);
        $houseWidgetsContainer.data("widget-freewall", wall);
        wall.reset({
            selector: '.house-item',
            animate: false,
            cellW: 315,
            cellH: 240,
            onResize: function() {
                wall.refresh();
            }
        });;

        //提交按钮的动作. 停止查询定时器，清空房源列表。
        $searchBtn.click(function() {
            $pageNo.val(1);
            $needRecord.val("true");
            $houseWidgetsContainer.empty();
            $bmap.clearHouseWidgets();
            return true;
        });
        $nextPage.click(function() {
            var pageNo = parseInt($pageNo.val());
            $pageNo.val($.isNumeric(pageNo) ? pageNo + 1 : 1);
            $needRecord.val("false");
            $searchForm.submit();
        });
        //地图初始化. 地图查询动作定义. 
        $bmap.bmap({
            search: function(event, widget) {
                $houseWidgetsContainer.empty();
                $bmap.clearHouseWidgets();
                var bounds = widget.bmap.getBounds();
                $minX.val(bounds.getSouthWest().lng);
                $minY.val(bounds.getSouthWest().lat);
                $maxX.val(bounds.getNorthEast().lng);
                $maxY.val(bounds.getNorthEast().lat);
                $searchForm.submit();
            }
        });

        $bmap.clearHouseWidgets = function() {
            $bmap.bmap("getMap").clearOverlays();
        };
        $bmap.smallHouseWidgets = function() {
            var bmap = $bmap.bmap("getMap");
            var overlays = bmap.getOverlays();
            for (var i = 0; i < overlays.length; i++) {
                var overlay = overlays[i];
                if (overlay.houseId) {
                    overlay.style = "small";
                }
            }
        };
        var idCount = 0;
        $bmap.addHouseWidgets = function($house) {
            var $tip = $house.clone();
            var id = $tip.data("id");
            var carouselId = "map-tip-" + id + "-" + idCount++;
            $(".carousel", $tip).attr("id", carouselId);
            $(".carousel-control", $tip).attr("href", "#" + carouselId);
            $(".carousel", $tip).carousel();
            $tip.addClass("house-item-in-map");

            var point = new BMap.Point($tip.data("x"), $tip.data("y")); // 创建点坐标
            var marker = $.extend(new BMap.Marker(point), {
                houseId: id,
                style: "big",
                status: "default",
                createIcon: function() {
                    var size = this.style === "big" ? new BMap.Size(27, 37) : new BMap.Size(17, 17);
                    var anchor = this.style === "big" ? new BMap.Size(-14, -19) : new BMap.Size(-9, -9);
                    return new BMap.Icon(ctx + "/images/icon-marker-" + this.style + "-" + this.status + ".png", size, {anchor: anchor});
                }
            });
            marker.setIcon(new BMap.Icon(ctx + "/images/icon-marker-big-default.png", new BMap.Size(27, 36)));
            $bmap.bmap("getMap").addOverlay(marker);
            marker.setAnimation(BMAP_ANIMATION_DROP);
            marker.addEventListener("mouseover", function() {
                marker.status = "hover";
                marker.setIcon(marker.createIcon());
            });
            marker.addEventListener("mouseout", function() {
                marker.status = "default";
                marker.setIcon(marker.createIcon());
            });
            marker.addEventListener("click", function() {
                this.openInfoWindow(new BMap.InfoWindow($tip.outerHTML(), {
                    enableMessage: false
                }));
                $("[data-slide]", $tip).click(function() {
                    $(this).parent().carousel($(this).data("slide"));
                });
            });
            $house.hover(function() {
                marker.status = "hover";
                marker.setIcon(marker.createIcon());
            }, function() {
                marker.status = "default";
                marker.setIcon(marker.createIcon());
            });
        };

        $("#price-order").change(function() {
            $orderby.val($(this).data("value"));
            $needRecord.val("false");
            $searchForm.submit();
        });

        $("#hide-filter").click(function() {
            $("#form-search").toggle();
        });
        
        $searchBtn.click();
    });
})(jQuery);