$(function() {
    $("#form-describe").ajaxForm({
        cache: false,
        beforeSubmit: function(arr, $form, options) {
            return $form.data("validator").form();
        },
        success: function(data, status, xhr, $form) {
            if (data.code === 0) {
                $("#i-wanna-say-something-popup").bPopup().close();
                $alertPopup.showAlert("success",null,"您的发言提交成功！");
            } else {
                $form.data("validator").showErrors({"description": data.message});
            }
        },
        error: function(xhr, status, ex, $form) {
            $form.data("validator").showErrors({"description": "评论失败"});
        }
    });
    $(".sticky-container").waypoint("sticky");
    $("#quick-nav").singlePageNav({
        offset: $(".sticky-container").outerHeight(),
        filter: ":not(.external)",
        beforeStart: function() {
            console.log("begin scrolling");
        },
        onComplete: function() {
            console.log("done scrolling");
        }
    });

    $("#descriptions .block-decription-container .full-text-switcher a").click(function() {
        var $this = $(this);
        var $toggle = $this.find("span.icon-less-more-toggle-blue");
        if ($toggle.hasClass("icon-less-more-toggle-blue-more")) {
            $toggle.switchClass("icon-less-more-toggle-blue-more", "icon-less-more-toggle-blue-less");
            $("#descriptions .block-decription-container .content p").each(function(index, element) {
                $(element).show();
            });
            $this.find("span.text").text("隐藏全文");
        } else {
            $toggle.switchClass("icon-less-more-toggle-blue-less", "icon-less-more-toggle-blue-more");
            $("#descriptions .block-decription-container .content p:not(:first)").each(function(index, element) {
                $(element).hide();
            });
            $this.find("span.text").text("显示全文");
        }
        return false;
    }).click();
    $(".bmap-polygon").polygon();

    $("#block-images div.section div").each(function(index, element) {
        $(element).addClass("clearfix");
        var $imgs = $(element).find("img");
        if ($imgs.length === 2) {
            $imgs.each(function(i, img) {
                $(img).css("width", "490px");
                if (i === 0) {
                    $(img).css("float", "left");
                } else if (i === 1) {
                    $(img).css("float", "right");
                }
            });
        }
    });
    $("#block-images img").each(function() {
        var $this = $(this);
        var src = $.trim($this.attr("src"));
        if (src.length > 0) {
            var index=src.indexOf("/",1);
            $this.attr("src", srx + src.substr(index));
        }
    });

    $("#descriptions .add-decription-container #i-wanna-say-something-button").click(function() {
        if (isAuthenticated()) {
            $("form .form-group.input-email").hide();
        } else {
            $("form .form-group.input-email").show();
        }
        $("#i-wanna-say-something-popup").bPopup({
            modalClose: false,
            opacity: 0.6,
            positionStyle: "absolute", // "fixed" or "absolute",
            fadeSpeed: "slow", // can be a string ("slow"/"fast") or
            // int
            followSpeed: 1500
                    // can be a string ("slow"/"fast") or int
        });
    });
    $("#close-describe").click(function() {
        $("#i-wanna-say-something-popup").bPopup().close();
    });
});