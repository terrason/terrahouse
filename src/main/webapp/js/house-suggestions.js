$(function() {
    wall = new freewall(".house-suggestions .items");
    wall.reset({
        selector: '.house-item',
        animate: false,
        cellW: 230,
        cellH: 188,
        onResize: function() {
            wall.refresh();
        }
    });
    wall.fitWidth();
    // for scroll bar appear;
    $(window).trigger("resize");

    loadMoreSuggestion();
});

function loadMoreSuggestion() {
    var $houseSuggestionContainer = $('.house-suggestions .items');
    var currentPage = $houseSuggestionContainer.data("page");
    var pageSize = $houseSuggestionContainer.data("pageSize") || 4;
    var polity = $houseSuggestionContainer.data("polity");
    $.get(ctx + "/house/hothouse", {
        currentPage: currentPage + 1,
        pageSize: pageSize,
        polity:polity
    }).done(function(data) {
        addSuggestions(data);
        $houseSuggestionContainer.data("page", currentPage + 1);
    });
}

function addSuggestions(items) {
    var $itemsContainer = $('.house-suggestions .items');
    $.each(items, function(index, value) {
        var $item = $('.house-suggestions .house-item-template .house-item').clone();
        $item.find('.preview').attr("src", value.preview ? srx + value.preview : ctx + "/images/house-item-pic-default.png");
        $item.find('.name').text(value.name);
        $item.find('.price .number').text(value.price);
        $item.find('.address').text(value.politicalAreaName + "，" + value.communityName);
        $item.find('.area span.number').text(value.area);
        $item.find(".house-anchor").attr("href", ctx + "/house/" + value.id);

        $item.appendTo($itemsContainer);
    });

    wall.refresh();
}
