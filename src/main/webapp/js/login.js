var $alertPopup;
var $signInPopup;
$(document).ready(function() {
    var popupOption = {
        modalClose: true,
        opacity: 0.6,
        positionStyle: "absolute", // "fixed" or "absolute",
        fadeSpeed: "slow", // can be a string ("slow"/"fast") or int
        followSpeed: "fast",
        onOpen: function() {
            this.trigger($nojs.EVENT_POPUP_OPEN);
        },
        onClose: function() {
            $("[data-rule-remote]").removeData("previousValue");
            var $form = this.is("form") ? this : $("form", this);
            var validator = $form.data("validator");
            validator && validator.resetForm();

            this.trigger($nojs.EVENT_POPUP_CLOSE);
        }
    };
    var $navActions = $("#actions");

    var $signInBtn = $("#login", $navActions);
    var $signInArchor = $(".login-archor", $signUpPopup);
    $signInPopup = $("#popup-signin", $navActions);
    var $signInForm = $("#signInForm", $signInPopup);

    var $signUpBtn = $("#register", $navActions);
    var $signUpArchor = $(".register-archor", $signInForm);
    var $signUpPopup = $("#popup-signUp", $navActions);
    var $signUpForm = $("#signUpForm", $signUpPopup);

    var $forgetPasswordArchor = $("#forgetPassword", $signInForm);
    var $forgetPasswordPopup = $("#popup-forget-passowrt", $navActions);
    var $forgetPasswordForm = $("#forgetPasswordForm", $forgetPasswordPopup);

    $alertPopup = $("#alertPopup", $navActions);
    //<editor-fold defaultstate="collapsed" desc="导出弹窗业务">
    $alertPopup.ajaxForm({
        success: function(data, status, xhr, $form) {
            if (data.code === 0) {
                $alertPopup.bPopup().close();
                $alertPopup.showAlert("success", null, "系统已向您的注册邮箱中发送了一封激活邮件，请登录邮件激活账号。");
            } else {
                $alertPopup.showAlert("danger", "发送邮件失败！", data.message);
            }
        }
    });
    $alertPopup.showAlert = function(type, strong, message, archor, close) {
        var $alert = this;
        $alert.empty();
        $alert.addClass("alert-" + type);
        if (strong) {
            $alert.append($("<strong>" + strong + "</strong>"));
        }
        if (message) {
            $alert.append(message);
        }
        if (archor) {
            var $archor = $('<a class="alert-link" href="javascript:;"></a>');
            $archor.text(archor.text);
            if ($.isFunction(archor.click)) {
                $archor.click(function(eventObject) {
                    archor.click.call($archor, eventObject);
                });
            } else if (archor.method === "post") {
                $archor.addClass("action-post").data("post", archor.href);
            } else if (archor.href) {
                $archor.attr("href", archor.href);
            }
            if (archor.data) {
                $.each(archor.data, function(name, value) {
                    $archor.data(name, value);
                });
            }
            $alert.append($archor);
            $archor.wrap('<p style="display:block;margin-top:0.2em;margin-right:1em;text-align:right;"></p>');
        }
        $application($alert);

        $alert.bPopup(popupOption);
        return $alert;
    };
    //</editor-fold>


    //<editor-fold defaultstate="collapsed" desc="导出登陆业务">
    var _loadPrincipal = function() {
        $.ajax(ctx + "/authc/principal", {cache: false}).done(function(data) {
            if (/[\s\n]*<li [\s\S]* <\/li>[\s\n]*/.test(data)) {
                $navActions.find(".menus").prepend(data);
                $application($(".user-info-li", $navActions));
                $(".stranger", $navActions).hide();

                $signInPopup.setAuthenticationStatus("done")
                        .trigger($nojs.EVENT_AUTHENTICATION_DONE, $signInPopup.data("authentication"))
                        .terminateAuthenticationProcess();
            } else {
                $signInPopup.setAuthenticationStatus("fail")
                        .trigger($nojs.EVENT_AUTHENTICATION_FAIL, $signInPopup.data("authentication"))
                        .terminateAuthenticationProcess();
            }
        }).fail(function() {
            $signInPopup.setAuthenticationStatus("fail")
                    .trigger($nojs.EVENT_AUTHENTICATION_FAIL, $signInPopup.data("authentication"))
                    .terminateAuthenticationProcess();
        });
    };
    $signInPopup.startAuthenticationProcess = function() {
        var authentication = {
            id: new Date().getTime(),
            status: "ready"
        };
        this.data("authentication", authentication).trigger($nojs.EVENT_AUTHENTICATION_START, authentication);
        return this;
    };
    $signInPopup.terminateAuthenticationProcess = function() {
        var authentication = this.data("authentication");
        authentication && (authentication.status = "terminated");
        this.removeData("authentication");
        this.trigger($nojs.EVENT_AUTHENTICATION_TERMINATE, authentication);
        return this;
    };
    $signInPopup.setAuthenticationStatus = function(status) {
        var authentication = this.data("authentication");
        authentication && (authentication.status = status);
        return this;
    };
    $signInPopup.setAuthenticationFail = function(error) {
        var authentication = this.data("authentication");
        if (authentication) {
            authentication.status = "fail";
            authentication.error = error;
        }
        return this;
    };
    $signInPopup.setAuthenticationAttribute = function(obj) {
        var authentication = this.data("authentication");
        if (authentication) {
            $.extend(true, authentication, obj);
        }
        return this;
    };
    $signInPopup.isAuthenticationStatus = function(status) {
        var authentication = this.data("authentication");
        return authentication && status === authentication.status;
    };
    $signInPopup.on($nojs.EVENT_POPUP_OPEN, function() {
        $signInPopup.startAuthenticationProcess();
    }).on($nojs.EVENT_POPUP_CLOSE, function() {
        if ($signInPopup.isAuthenticationStatus("fail")) {
            $signInPopup.trigger($nojs.EVENT_AUTHENTICATION_FAIL, $signInPopup.data("authentication"))
                    .terminateAuthenticationProcess();
        } else if ($signInPopup.isAuthenticationStatus("done")) {
            $signInPopup.trigger($nojs.EVENT_AUTHENTICATION_DONE, $signInPopup.data("authentication"))
                    .terminateAuthenticationProcess();
        } else if ($signInPopup.isAuthenticationStatus("ready")) {
            $signInPopup.setAuthenticationStatus("cancel").trigger($nojs.EVENT_AUTHENTICATION_CANCEL, $signInPopup.data("authentication"))
                    .terminateAuthenticationProcess();
        }
    });
    //</editor-fold>

    //----------------------------------------declare complete------------

    $signInBtn.click(function() {
        $signInPopup.bPopup(popupOption);
    });
    $signInForm.ajaxForm({
        cache: false,
        beforeSubmit: function(arr, $form, options) {
            var valid = $form.data("validator").form();
            if (valid === true) {
                $signInPopup.setAuthenticationStatus("process");
                $signInPopup.setAuthenticationAttribute({
                    email: $("input[name=email]", $form).val()
                });
                return true;
            } else {
                return false;
            }
        },
        success: function(data, status, xhr, $form) {
            if (data.code === 0) {
                _loadPrincipal();
                $signInPopup.bPopup().close();
                principalLoadingTimer.play(true);
            } else if (data.code === 412) {
                $signInPopup.setAuthenticationFail("账号未激活");
                $signInPopup.bPopup().close();
                $alertPopup.showAlert("warning", "账号未激活！", "系统已向您的注册邮箱中发送了一封激活邮件，请注意查收并激活账号。", {
                    text: "点此重新发送激活邮件",
                    href: ctx + "/authc/signUp/mail",
                    method: "post",
                    data: {
                        email: $("[name=email]", $form).val()
                    }
                });
                $form.data("validator").showErrors({"email": "账号未激活！"});
            } else {
                $signInPopup.setAuthenticationFail(data.message);
                $form.data("validator").showErrors({"email": data.message});
            }
        },
        error: function(xhr, status, ex, $form) {
            $signInPopup.setAuthenticationFail(ex);
            $form.data("validator").showErrors({"email": "用户名或密码输入错误！"});
        }
    });

    $signUpBtn.click(function() {
        $signUpPopup.bPopup(popupOption);
    });
    $signUpForm.ajaxForm({
        cache: false,
        beforeSubmit: function(arr, $form, options) {
            return $form.data("validator").form();
        },
        success: function(data, status, xhr, $form) {
            if (data.code === 0) {
                $signUpPopup.bPopup().close();
//                注册不发送激活邮件，直接登录。
//                $alertPopup.showAlert("success", "注册成功！", "系统已向您的注册邮箱中发送了一封激活邮件，请登录邮件激活账号。");
                _loadPrincipal();
                principalLoadingTimer.play(true);
            } else {
                $form.data("validator").showErrors({"email": data.message});
            }
        },
        error: function(xhr, status, ex, $form) {
            $form.data("validator").showErrors({"email": "注册失败"});
        }
    });

    $forgetPasswordArchor.click(function() {
        $signInPopup.bPopup().close();
        $forgetPasswordPopup.bPopup(popupOption);
    });
    $forgetPasswordForm.ajaxForm({
        cache: false,
        success: function(data, status, xhr, $form) {
            if (data.code === 0) {
                $forgetPasswordPopup.bPopup().close();
                $alertPopup.showAlert("info", null, "系统已向您的注册邮箱中发送了一封重置密码邮件，请注意查收。");
            } else {
                $form.data("validator").showErrors({"email": data.message});
            }
        },
        error: function(xhr, status, ex, $form) {
            $form.data("validator").showErrors({"email": "邮箱不正确"});
        }
    });

    $signUpArchor.click(function() {
        $signInPopup.bPopup().close();
        $signUpBtn.click();
    });

    $signInArchor.click(function() {
        $signUpPopup.bPopup().close();
        $signInBtn.click();
    });

    var principalLoadingTimer = $.timer(function() {
        if (!isAuthenticated()) {
            _loadPrincipal();
        }
    }, 60000, true);
});

function isAuthenticated() {
    return $("#principal").length > 0;
}
$nojs.before(function(context) {
    $(".authenticated-required").click(function(event) {
        var $authenticatedRequired = $(this);
        if (!isAuthenticated()) {
            event.stopImmediatePropagation();
            $signInPopup.one($nojs.EVENT_AUTHENTICATION_START, function(event, authentication) {
                $authenticatedRequired.data("authenticationId", authentication.id);
            }).one($nojs.EVENT_AUTHENTICATION_DONE, function(event, authentication) {
                if (authentication && authentication.id === $authenticatedRequired.data("authenticationId")) {
                    $authenticatedRequired.click();
                }
            });
            $("#login", context).click();
        }
    });
});

$nojs.ready(function(context) {
    var countries = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url:ctx+'/search/suggest?query=%QUERY',
            filter: function(list) {
                return $.map(list, function(text) {
                    return {name: text};
                });
            }
        }
    });

// kicks off the loading/processing of `local` and `prefetch`
    countries.initialize();

// passing in `null` for the `options` arguments will result in the default
// options being used
    $('.autocomplate-keyword').typeahead(null, {
        name: 'countries',
        displayKey: 'name',
// `ttAdapter` wraps the suggestion engine in an adapter that
// is compatible with the typeahead jQuery plugin
        source: countries.ttAdapter()
    });
});