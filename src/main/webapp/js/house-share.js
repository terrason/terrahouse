$(function() {
    $("#form-share").ajaxForm({
        cache: false,
        beforeSubmit: function(arr, $form, options) {
            return $form.data("validator").form();
        },
        success: function(data, status, xhr, $form) {
            if (data.code === 0) {
                $(".control-label", $form).text("发送成功");
                $("input[name=share-email]", $form).val("").filter(":not(:first)").remove();
            } else {
                $form.data("validator").showErrors({"share-email": data.message});
            }
        },
        error: function(xhr, status, ex, $form) {
            $form.data("validator").showErrors({"share-email": "分享失败"});
        }
    });
    $('.share-container .action a').click(inviteMore);

});
$nojs.before(function(context) {
    var validateOption = {
        highlight: function(element) {
            $(element).parents(".input-container").addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).parents(".input-container").removeClass('has-error');
        },
        errorElement: "label",
        errorClass: "control-label",
        errorPlacement: function(error, element) {
            var $formGroup = $(element).parents(".input-container");
            var $error = $("label.control-label", $formGroup);
            if ($error.length) {
                $error.text(error.text());
            } else {
                $formGroup.prepend(error);
            }
        }
    };
    $("#form-share").data(validateOption);
});
function inviteMore() {
    // var $inputContainer = $('<div/>');
    // $inputContainer.addClass('input-container');
    // $inputContainer.append('<label class="control-label"></label>')
    // $inputContainer
    // .append('<input class="ui-corner-all form-control"
    // name="share-email"type="text">');
    var firstInputContent = $('.input-container-all .input-container').eq(0);
    var $inputContainer = firstInputContent.clone();
    $inputContainer.appendTo($('.input-container-all'));
    $inputContainer.removeClass('has-error');
    $inputContainer.removeClass('has-success');
    $("label",$inputContainer).remove();
    $inputContainer.find('input').val('');
    return false;
}
