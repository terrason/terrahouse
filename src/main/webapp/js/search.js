var wall = null;
var map = null;
var searchedItems = [];
var currentIndex = 0;// 搜索结果很多，当前的是第几页。
var filterHidenByClick = false;
$(function() {
    
	map = new BMap.Map("map"); // 创建Map实例
	var point = new BMap.Point(116.404, 39.915); // 创建点坐标
	map.centerAndZoom(point, 15); // 初始化地图,设置中心点坐标和地图级别。
	map.enableScrollWheelZoom();
	map.addControl(new BMap.NavigationControl()); // 添加默认缩放平移控件
	map.addControl(new BMap.ScaleControl());// 添加默认比例尺控件

	// 创建控件
	var myZoomCtrl = new ZoomControl();
	// 添加到地图当中
	map.addControl(myZoomCtrl);

	var location = $.getUrlParam('location');
	if (!location) {
		location = '';
	}
	$('#location').val(location);

	var priceFrom = $.getUrlParam('price-from');
	var priceTo = $.getUrlParam('price-to');
	if (!priceFrom || priceFrom == '') {
		priceFrom = 0;
	}
	if (priceFrom < 0) {
		priceFrom = 0;
	}
	if (!priceTo || priceTo == '') {
		priceTo = 1000;
	}
	if (priceTo > 1000) {
		priceTo = 1000;
	}
	$('#price-from').text(priceFrom);
	$('#price-to').text(priceTo);
	$('#slider-range-price').slider({
		range : true,
		min : 0,
		max : 1000,
		values : [ priceFrom, priceTo ],
		slide : function(event, ui) {
			$('#price-from').text(ui.values[0]);
			$('#price-to').text(ui.values[1]);
		},
		stop : function(event, ui) {
			search(true);
		}
	});

	$('#slider-range-area').slider({
		range : true,
		min : 0,
		max : 500,
		values : [ 0, 500 ],
		slide : function(event, ui) {
			$('#area-from').text(ui.values[0]);
			$('#area-to').text(ui.values[1]);
		},
		stop : function(event, ui) {
			search(true);
		}
	});

	$('#slider-range-house-age').slider({
		range : true,
		min : 0,
		max : 30,
		values : [ 0, 30 ],
		slide : function(event, ui) {
			$('#house-age-from').text(ui.values[0]);
			$('#house-age-to').text(ui.values[1]);
		},
		stop : function(event, ui) {
			search(true);
		}
	});

	var area = $.getUrlParam('area');
	if (area) {
		$('#area').val(area);
	}
	$('#area-dropdown-menu a').click(function() {
		$('#area').val($(this).text());
		$('#area-dropdown-menu.dropdown-menu').dropdown('toggle')
		return false;
	});

	var nBedroom = $.getUrlParam('n-bedroom');
	if (nBedroom) {
		$('#n-bedroom').val(nBedroom);
	}
	$('#n-bedroom-dropdown-menu a').click(function() {
		$('#n-bedroom').val($(this).text());
		$('#n-bedroom-dropdown-menu.dropdown-menu').dropdown('toggle')
		return false;
	});
	$('#fitting-dropdown-menu a').click(function() {
		$('#fitting').val($(this).text());
		$('#fitting-dropdown-menu.dropdown-menu').dropdown('toggle')
		return false;
	});

	$('#hide-filter').click(function() {
		var $textFix = $(this).find('.text-fix');
		if ($textFix.attr('action') != 'show') {
			hideSearchFilter();
			filterHidenByClick = true;
		} else {
			showSearchFilter();
		}
	});

	$('ul#price-order-options li a')
			.click(
					function() {
						$('button#price-order')
								.html(
										$(this).text()
												+ '<span class="dropdown-icon"></span>');
					});

	wall = new freewall("#result-container .items");
	wall.reset({
		selector : '.house-item',
		animate : false,
		cellW : 315,
		cellH : 240,
		onResize : function() {
			wall.refresh();
		}
	});
	wall.fitWidth();

	// for scroll bar appear;
	$(window).trigger("resize");

	search(true);

	var $loading = $("<div class='loading'><p>Loading more items&hellip;</p></div>");
	var $searchBottom = $('#result-container .bottom');
	var opts = {
		offset : '100%'
	};

	$('#right-container').scroll(bindFilterScroll);

	$('#submit').click(function() {
		search(true);
		return false;
	})
});

function unstuckSearchFilter() {
	if ($('#filter-and-control').hasClass('stuck-all')) {
		$('#filter-and-control').removeClass('stuck-all');
		$('#filter-and-control').removeAttr('style');
		$('.filter-and-control-wrapper').removeAttr('style');
		$('#filter-and-control').prependTo($('.filter-and-control-wrapper'));

	}
}
function hideSearchFilter() {

	unstuckSearchFilter();

	var $options = $('#filter');
	var $textFix = $('#hide-filter .text-fix')
	var $rightContainer = $('#right-container');
	$options.hide();
	$textFix.text('显示搜索条件');
	$textFix.attr('action', 'show');
	$('#icon-hide-show-toggle').switchClass('icon-show', 'icon-hide');

	var $filter = $('#filter');
	var offset = 0;
	if ($filter.is(":visible")) {
		offset = offset + $filter.outerHeight(true);
	}
	var scrollTop = $rightContainer.scrollTop();
	if (scrollTop > offset) {
		$('.control-container-wrapper').css({
			height : $('#control-container').outerHeight(true)
		});
		$rightContainer.addClass('stuck');
		$('#control-container').prependTo($rightContainer);
	}
}
function showSearchFilter() {
	var $options = $('#filter');
	var $textFix = $('#hide-filter .text-fix')
	var $rightContainer = $('#right-container');

	$textFix.text('隐藏搜索条件');
	$textFix.attr('action', 'hide');
	$('#icon-hide-show-toggle').switchClass('icon-hide', 'icon-show');

	$options.show();
	if ($rightContainer.hasClass('stuck')) {
		$rightContainer.removeClass('stuck')
		$('.control-container-wrapper').removeAttr('style');
		$('#control-container').appendTo('.control-container-wrapper');
	}
	$('.filter-and-control-wrapper').css({
		height : $('#filter-and-control').outerHeight(true)
	});
	$('#filter-and-control').addClass('stuck-all');

	$('#filter-and-control').css({
		right : $.scrollbarWidth(),
		width : $rightContainer.width() - $.scrollbarWidth()
	})
	$('#filter-and-control').prependTo($rightContainer);

	filterHidenByClick = false;

	var scrollTop = $('#right-container').scrollTop();
	if (scrollTop == 0) {
		unstuckSearchFilter();
	}

}
var lastResultScrollTop = 0;
function bindResultScroll(event) {
	var scrollTop = $(this).scrollTop();
	if (scrollTop > lastResultScrollTop) {
		// console.log('downscroll code');
	} else {
		// console.log('upscroll code: scrollTop = %s', scrollTop);
	}
	lastResultScrollTop = scrollTop;
}
function bindFilterScroll() {
	var $rightContainer = $('#right-container');

	var $filter = $('#filter');
	var offset = 0;
	if ($filter.is(":visible")) {
		offset = offset + $filter.outerHeight(true);
	}
	var scrollTop = $rightContainer.scrollTop();

	if ($('#filter-and-control').hasClass('stuck-all')) {
		if (scrollTop == 0) {
			unstuckSearchFilter();
		}
	} else {

		if (scrollTop > offset) {
			if ($filter.is(":visible")) {
				$('#hide-filter .text-fix').text('显示搜索条件');
				$('#hide-filter .text-fix').attr('action', 'show');
				$('#icon-hide-show-toggle').switchClass('icon-show',
						'icon-hide');
			}
			$('.control-container-wrapper').css({
				height : $('#control-container').outerHeight(true)
			});
			$rightContainer.addClass('stuck');
			$('#control-container').prependTo($rightContainer);

			$('#control-container').css({
				right : $.scrollbarWidth(),
				width : $rightContainer.width() - $.scrollbarWidth()
			})
		} else {
			$rightContainer.removeClass('stuck')
			$('.control-container-wrapper').removeAttr('style');
			$('#control-container').appendTo('.control-container-wrapper');
			if ($filter.is(':visible')) {
				$('#hide-filter .text-fix').text('隐藏搜索条件');
				$('#hide-filter .text-fix').attr('action', 'hide');
				$('#icon-hide-show-toggle').switchClass('icon-hide',
						'icon-show');
			}
		}
	}

	var childrenHeight = $('#filter-and-result-container').outerHeight(true);
	var visiableHeight = $rightContainer.outerHeight(true);
	console
			.log(
					'scrollTop = %s, childrenHeight = %s, visiableHeight = %s, scrollTop+visiableHeight = %s',
					scrollTop, childrenHeight, visiableHeight, scrollTop
							+ visiableHeight);
	if (scrollTop > 0) {
		if (visiableHeight + scrollTop >= childrenHeight) {
			console.log('search begin!!!')
			$rightContainer.unbind('scroll');
			$.timer(searchAndBindScroll).once(2000);
		}
	}

}

function searchAndBindScroll(){
	search(false);
	$('#right-container').bind('scroll', bindFilterScroll);
}

function search(research) {
	// init suggestions data. load from server.
	var bounds = map.getBounds();
	var sw = bounds.getSouthWest();
	var ne = bounds.getNorthEast();
	var lngSpan = Math.abs(sw.lng - ne.lng);
	var latSpan = Math.abs(ne.lat - sw.lat);

	if (research) {
		searchedItems.length = 0;
		currentIndex = 0;
		map.clearOverlays();
		var $itemsContainer = $('#result-container .items');
		$itemsContainer.empty();
		$('#filter-and-result-container').animate({
			scrollTop : 0
		}, 'slow');
		wall = new freewall("#result-container .items");
		wall.reset({
			selector : '.house-item',
			animate : false,
			cellW : 315,
			cellH : 240,
			onResize : function() {
				wall.refresh();
			}
		});
		wall.fitWidth();
		wall.refresh();
	}

	// searchResult is returned by http request
	var searchResult = {
		total : parseInt(Math.random() * 1000),
		items : []
	}
	// Create test data;
	for (var i = 0; i < 20; i++) {
		var item = {
			imagesBig : [ '../images/house-item-pic-default.png',
					'../images/house-item-pic-default.png',
					'../images/house-item-pic-default.png' ],
			district : '丝庐花语',
			price : '349',
			address : '徐汇，斜土路大木桥',
			area : '163',
			lat : ne.lat - latSpan * (Math.random() * 0.8 + 0.1),
			lng : sw.lng + lngSpan * (Math.random() * 0.8 + 0.1)
		};
		searchResult.items.push(item);
	}
	$.each(searchResult.items, function(index, item) {
		searchedItems.push(item);
	});

	if (research) {
		$('#filter-and-result-container #control-container .result-count span')
				.text(searchResult.total)
	}
	addSearchResults(searchedItems);
}

function addSearchResults(items) {
	var $itemsContainer = $('#result-container .items');
	$.each(items, function(index, value) {
		if (value.marker) {
			value.marker.setIcon(new BMap.Icon(
					"../images/icon-marker-small-default.png", new BMap.Size(
							27, 36)));
			value.isOldData = true;
		} else {
			// here it's new items
			var $item = $('#control-container .house-item').clone();
			var $carouselInner = $item.find('.carousel-inner');
			$carouselInner.empty();
			$.each(value.imagesBig, function(imageIndex, imageUrl) {
				var $div = $('<div/>');
				$div.addClass('item');
				if (imageIndex == 0) {
					$div.addClass('active');
				}
				$("<a href='#'><img src='" + imageUrl + "'></a>")
						.appendTo($div);
				$div.appendTo($carouselInner)
			});
			$item.find('.district').text(value.district);
			$item.find('.price span.number').text(value.price);
			$item.find('.address').text(value.address);
			$item.find('.area span.number').text(value.area);

			$item.appendTo($itemsContainer);

			var point = new BMap.Point(value.lng, value.lat);
			var myIcon = new BMap.Icon("../images/icon-marker-big-default.png",
					new BMap.Size(27, 36));
			var marker = new BMap.Marker(point, {
				icon : myIcon
			});
			map.addOverlay(marker);
			marker.setAnimation(BMAP_ANIMATION_DROP);
			// marker.setAnimation(BMAP_ANIMATION_BOUNCE);

			var $itemSmall = $item.clone();
			$itemSmall.addClass('house-item-in-map');

			var infoWindow = new BMap.InfoWindow($itemSmall.outerHTML(), {
				enableMessage : false
			});
			marker.addEventListener("click", function() {
				this.openInfoWindow(infoWindow);
				$('a[data-slide="prev"]').click(function() {
					$(this).parent().carousel('prev');
				});

				$('a[data-slide="next"]').click(function() {
					$(this).parent().carousel('next');
				});
			});
			value.marker = marker;
		}
	});

	wall.refresh();

	$('.carousel').carousel({
		interval : 0
	})

	$('a[data-slide="prev"]').click(function() {
		$(this).parent().carousel('prev');
	});

	$('a[data-slide="next"]').click(function() {
		$(this).parent().carousel('next');
	});

	$('#result-container .items .house-item').hover(
			function() {
				var index = $(this).index();
				var marker = searchedItems[index].marker;
				if (searchedItems[index].isOldData) {
					marker.setIcon(new BMap.Icon(
							"../images/icon-marker-small-hover.png",
							new BMap.Size(27, 36)));
				} else {
					marker.setIcon(new BMap.Icon(
							"../images/icon-marker-big-hover.png",
							new BMap.Size(27, 36)));
				}
			},
			function() {
				var index = $(this).index();
				var marker = searchedItems[index].marker;
				if (searchedItems[index].isOldData) {
					marker.setIcon(new BMap.Icon(
							"../images/icon-marker-small-default.png",
							new BMap.Size(27, 36)));
				} else {
					marker.setIcon(new BMap.Icon(
							"../images/icon-marker-big-default.png",
							new BMap.Size(27, 36)));
				}

			});
}

// 定义一个控件类,即function
function ZoomControl() {
	// 默认停靠位置和偏移量
	this.defaultAnchor = BMAP_ANCHOR_TOP_LEFT;
	this.defaultOffset = new BMap.Size(60, 15);
}

// 通过JavaScript的prototype属性继承于BMap.Control
ZoomControl.prototype = new BMap.Control();

// 自定义控件必须实现自己的initialize方法,并且将控件的DOM元素返回
ZoomControl.prototype.initialize = function(map) {

	var $button = $("<button id='research-by-map' type='button' class='btn btn-white-blue btn-sm'>按照地图显示区域重新搜索</button>");
	$button.click(function() {
		search(true);
	});
	var button = $button[0];
	map.getContainer().appendChild(button);
	// 将DOM元素返回
	return button;
}