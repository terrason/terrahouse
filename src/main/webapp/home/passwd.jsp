<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jspf/prepare.jspf" %>
<!DOCTYPE html>
<html>
    <head>
        <title><spring:message code="application.title"/></title>
        <%@include file="/WEB-INF/jspf/common.jspf" %>
        <link href="${ctx}/css/reset-password.css" rel="stylesheet"/>
    </head>
    <body>
        <%@include file="/WEB-INF/jspf/navigation.jspf" %>
        <div class="content-width reset-password-container">
            <div class="title">重置密码</div>
            <form id="resetPasswordForm" class="validate" method="post" role="form" action="${ctx}/authc/passwd">
                <div class="form-item">
                    <label for="new-password">新密码</label>
                    <input id="new-password" class="form-control" type="password" name="password" placeholder="新密码"
                               data-rule-required="true"
                               data-msg-required="请输入您的新密码"
                               data-rule-minlength="6"
                               data-msg-minlength="密码位数不得小于六位"/>
                </div>
                <div class="form-item">
                    <label for="reinput-password">确认密码</label>
                    <input id="reinput-password" class="form-control" type="password" name="repassword" placeholder="再输一遍"
                               data-rule-required="true"
                               data-msg-required="请确认您的新密码"
                               data-rule-equalTo="#new-password"
                               data-msg-equalTo="两次输入的密码不一致"/>
                </div>
                <div class="form-item">
                    <input type="hidden" name="email" value="${email}"/>
                    <button type="submit" class="btn btn-primary btn-normal">重置密码</button>
                </div>
            </form>
        </div>
        <%@include file="/WEB-INF/jspf/copyright.jspf" %>
    </body>
</html>
