<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jspf/prepare.jspf" %>
<!DOCTYPE html>

<html>
    <head>
        <title><spring:message code="application.title"/></title>
        <%@include file="/WEB-INF/jspf/common.jspf" %>
        <link href="${ctx}/css/house-item.css" rel="stylesheet"/>
        <link href="${ctx}/css/index.css" rel="stylesheet"/>
        <script type="text/javascript" src="${ctx}/js/menu.js"></script>
        <script type="text/javascript" src="${ctx}/js/index.js"></script>
    </head>
    <body>
        <%@include file="/WEB-INF/jspf/navigation.jspf" %>
        <div id="top-container">
            <div id="carousel-example-captions" class="carousel slide">
                <div class="carousel-inner">
                    <div class="item active">
                        <img src="${ctx}/images/slide-01.jpg" alt="">
                        <div class="carousel-custom-caption">
                            <div class="title">在线预约看房</div>
                            <div class="desc">窝噻的在线预约机制帮您实现线上即可预约看房。</div>
                        </div>
                    </div>
                    <div class="item">
                        <img src="${ctx}/images/slide-02.jpg" alt="">
                        <div class="carousel-custom-caption">
                            <div class="title">100%的真实房源</div>
                            <div class="desc">现实中的房子定如您在窝噻上所见。</div>
                        </div>
                    </div>
                    <div class="item">
                        <img src="${ctx}/images/slide-03.jpg" alt="">
                        <div class="carousel-custom-caption">
                            <div class="title">1%的佣金费用</div>
                            <div class="desc">窝噻的佣金费用远低于行业平均水平。</div>
                        </div>
                    </div>
                    <div class="item">
                        <img src="${ctx}/images/slide-02.jpg" alt="">
                        <div class="carousel-custom-caption">
                            <div class="title">远离骚扰</div>
                            <div class="desc">窝噻还您更纯净的购房环境，帮您远离电话骚扰。</div>
                        </div>
                    </div>
                </div>
                <a class="left carousel-control" href="#carousel-example-captions" data-slide="prev">
                    <img src="${ctx}/images/carousel-control-left.png" alt="left"/>
                </a>
                <a class="right carousel-control" href="#carousel-example-captions" data-slide="next">
                    <img src="${ctx}/images/carousel-control-right.png" alt="right"/>
                </a>
            </div>

            <div id="search-container" class="content-width">
                <form id="search-form" action="${ctx}/search" method="get" role="form" class="custom show-search-options position-left form-inline">
                    <div class="input-wrapper">
                        <input id="search-location" class="form-control autocomplate-keyword" name="keyword" placeholder="您想住哪里？（如:街区、楼盘、轨道交通）"/>
                    </div>
                    <div class="input-wrapper">
                        <a id="search-area-dropdown" href="javascript:;" data-toggle="dropdown" data-value="">
                            <input id="search-area" readonly="readonly" placeholder="区域"/>
                            <input type="hidden" name="polity" value=""/>
                            <span class="dropdown-icon"></span>
                        </a>
                        <ul id="search-area-dropdown-menu" class="dropdown-menu" role="menu" aria-labelledby="area-dropdown">
                            <li><a href="javascript:;" data-value="">所有区域</a></li>
                                <c:forEach items="${polities}" var="cur">
                                <li><a href="javascript:;" data-value="${cur.name}">${cur.name}</a></li>
                                </c:forEach>
                        </ul>
                    </div>
                    <div class="input-wrapper">
                        <a href="javascript:;" data-toggle="dropdown" data-value="">
                            <input id="search-bedroom" readonly="readonly" placeholder="居室"/>
                            <input type="hidden" name="bedroom" value=""/>
                            <span class="dropdown-icon"></span>
                        </a>
                        <ul id="search-bedroom-dropdown-menu" class="dropdown-menu" role="menu">
                            <li><a href="javascript:;" data-value="">所有居室</a></li>
                            <li><a href="javascript:;" data-value="1">一居室</a></li>
                            <li><a href="javascript:;" data-value="2">二居室</a></li>
                            <li><a href="javascript:;" data-value="3">三居室</a></li>
                            <li><a href="javascript:;" data-value="-3">四居室及以上</a></li>
                        </ul>
                    </div>
                    <div class="input-wrapper">
                        <a href="javascript:;" id="search-price-popover-trigger">
                            <span id="search-price">价格</span>
                            <span class="icon-price"></span>
                        </a>
                        <div id="search-price-popover">
                            <input class="numeric form-control" name="priceFrom" maxlength="5"/>
                            至
                            <input class="numeric form-control" name="priceTo" maxlength="5"/> 万
                        </div>
                    </div>
                    <button id="submit-location" type="submit" class="btn btn-primary btn-large btn-contrast">
                        <img class="icon-search" src="${ctx}/images/icon-search.png" alt="research" />
                    </button>
                </form>
                <div class="clear"></div>
            </div>
        </div>

        <div id="how-to" class="clearfix">
            <div class="content-width clearfix" style="overflow: hidden;">
                <div class="title">欢迎来到窝噻网</div>
                <div class="center clearfix">
                    <div class="tab-head current" id="tab-head-buy">如何购买房屋</div><div class="tab-head" id="tab-head-sold">如何出售房屋</div>
                </div>
                <div class="tab-content-container left" style="width: 2000px">
                    <ul id="tab-buy" class="tab-contant">
                        <li class="step-1">
                            <div class="icon-step"></div>
                            <div class="desc-title">搜索住房</div>
                            <div class="desc-content">搜索合适您的房源</div>
                        </li>
                        <li class="step-2">
                            <div class="icon-step"></div>
                            <div class="desc-title">在线预约</div>
                            <div class="desc-content">线上即刻预约看房时间</div>
                        </li>
                        <li class="step-3">
                            <div class="icon-step"></div>
                            <div class="desc-title">上门看房</div>
                            <div class="desc-content">由窝噻网陪您实地看房</div>
                        </li>
                        <li class="step-4">
                            <div class="icon-step"></div>
                            <div class="desc-title">达成交易</div>
                            <div class="desc-content">双方达成交易后成交</div>
                        </li>
                    </ul>
                    <ul id="tab-sold" class="tab-contant">
                        <li class="step-1">
                            <div class="icon-step"></div>
                            <div class="desc-title">发布住房</div>
                            <div class="desc-content">向窝噻提交您的住房</div>
                        </li>
                        <li class="step-2">
                            <div class="icon-step"></div>
                            <div class="desc-title">等待服务</div>
                            <div class="desc-content">窝噻提供拍摄等免费服务</div>
                        </li>
                        <li class="step-3">
                            <div class="icon-step"></div>
                            <div class="desc-title">接待看房</div>
                            <div class="desc-content">接待购房者上门看房</div>
                        </li>
                        <li class="step-4">
                            <div class="icon-step"></div>
                            <div class="desc-title">达成交易</div>
                            <div class="desc-content">双方达成交易后成交</div>
                        </li>
                    </ul>
                </div>
                <div class="clear"></div>
                <div class="more">
                    <a href='/help/buy' target="_blank">了解更多内容</a>
                </div>
            </div>
        </div>
        <div id="blocks">
            <table class="content-width">
                <tr>
                    <td class="item">
                        <a href="${ctx}/block/${blocks[0].id}"><img src="${empty blocks[0].preview ? ctx : srx }${empty blocks[0].preview ? "/images/block-default.jpg" : blocks[0].preview}" alt="${blocks[0].name}"/><div>${blocks[0].name}</div></a>
                    </td>
                    <td class="item">
                        <a href="${ctx}/block/${blocks[1].id}"><img src="${empty blocks[1].preview ? ctx : srx }${empty blocks[1].preview ? "/images/block-default.jpg" : blocks[1].preview}" alt="${blocks[1].name}"/><div>${blocks[1].name}</div></a>
                    </td>
                    <td rowspan="2" class="text">
                        <div class="title">街区</div>
                        <div class="desc">
                            不知道住哪里？窝噻用自己的视角<br />
                            为您诠释这个魅力城市的各个街区。
                        </div>
                        <a href="${ctx}/block" class="btn btn-primary btn-lg btn-block">浏览全部街区</a>
                    </td>
                </tr>
                <tr>
                    <td class="item">
                        <a href="${ctx}/block/${blocks[2].id}"><img src="${empty blocks[2].preview ? ctx : srx }${empty blocks[2].preview ? "/images/block-default.jpg" : blocks[2].preview}" alt="${blocks[2].name}"/><div>${blocks[2].name}</div></a>
                    </td>
                    <td class="item">
                        <a href="${ctx}/block/${blocks[3].id}"><img src="${empty blocks[3].preview ? ctx : srx }${empty blocks[3].preview ? "/images/block-default.jpg" : blocks[3].preview}" alt="${blocks[3].name}"/><div>${blocks[3].name}</div></a>
                    </td>
                </tr>
            </table>
        </div>
        <div id="suggestion" class="content-width">
            <div class="title">窝噻为您推荐</div>
            <div class="desc">我们采用科学的方法精选出高质量的房源，如果您第一眼没看到合适的，不妨点击显示更多推荐。</div>
            <div class="items clearfix" data-page="0"></div>
            <div class="loading">
                <img src="${ctx}/images/loading.gif" alt="loading"/>
            </div>
            <div class="load-more-container">
                <div class='house-item'>
                    <a class="house-anchor" href="javascript:;">
                        <img class="preview" src="${ctx}/images/house-item-pic-default.png" alt="" style="width:238px;height:140px;"/>
                        <table>
                            <tr>
                                <td class='name'>丝庐花语</td>
                                <td class='price'><span class='number'>580</span>万</td>
                            </tr>
                            <tr>
                                <td class='address'>徐汇，斜土路大木桥</td>
                                <td class='area'><span class='number'>125</span>m<sup>2</sup></td>
                            </tr>
                        </table>
                    </a>
                </div>
                <button id="load-more" type="button" class="btn btn-primary btn-lg">显示更多推荐</button>
            </div>
        </div>

        <%@include file="/WEB-INF/jspf/copyright.jspf" %>
    </body>
</html>
