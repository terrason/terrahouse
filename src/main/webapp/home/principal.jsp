<%@ page pageEncoding="UTF-8" %>
<%@include file="/WEB-INF/jspf/prepare.jspf" %>
<c:if test="${not empty principal}">
    <li id="principal" class="user-info-li dropdown">
        <a href="javascript:;" role="button" class="dropdown-toggle" data-toggle="dropdown">
            <img src="${empty principal.avatar ? ctx : srx }${empty principal.avatar ? "/images/user-head-default.jpg" : principal.avatar}" alt="avatar" style="width:22px;height:22px;"/>
            &nbsp;&nbsp;${principal.username} <b class="caret"></b>
            <span class="icon-unread-flag"></span>
        </a>
        <ul class="dropdown-menu" role="menu" aria-labelledby="user-info">
            <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;">账户信息</a></li>
            <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;">更改密码</a></li>
            <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;">我的预约<c:if test="${principal.appointment gt 0}"><span class="bubble-number">${principal.appointment}</span></c:if></a></li>
            <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;">我的出售</a></li>
            <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;">我的信誉值</a></li>
            <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;">我的订阅<c:if test="${principal.subscription gt 0}"><span class="bubble-number">${principal.subscription}</span></c:if></a></li>
            <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;">我的收藏<c:if test="${principal.favorite gt 0}"><span class="bubble-number">${principal.favorite}</span></c:if></a></li>
            <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;">我的评论</a></li>
            <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:;"
                                       class="action-post"
                                       data-post="${ctx}/authc/signOut"
                                       data-javascript-confirm="true">退出</a></li>
        </ul>
    </li>
</c:if>