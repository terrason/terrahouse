<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jspf/prepare.jspf" %>
<!DOCTYPE html>
<html>
    <head>
        <title><spring:message code="application.title"/></title>
        <%@include file="/WEB-INF/jspf/common.jspf" %>
        <link href="${ctx}/css/house-publish.css" rel="stylesheet"/>
        <script src="${ctx}/lib/jquery.iframe-transport.js"></script>
        <script src="${ctx}/lib/jquery.fileupload.js"></script>
        <script src="${ctx}/js/house-publish.js"></script>
    </head>
    <body>
        <%@include file="/WEB-INF/jspf/navigation.jspf" %>
        <form id="form-publish" class="validate" method="post" role="form" action="${ctx}/house/pulish" data-redirect="${ctx}/home">
            <div class="house-publish-container">
                <div class="content-width clearfix">
                    <div class="title">发布您的房源</div>
                    <div class="description">请您尽可能多的填写申请信息，以便窝噻对您提交的房屋有更多的了解，如上传房屋照片、提交房屋描述等信息。窝噻将会对信息提交更完善的房屋进行优先服务。</div>
                    <div class="form-item">
                        <label for="district-name">小区名称</label>
                        <input id="district-name" class="form-control" type="text" name="residenceName"
                               data-rule-required="true"
                               data-rule-maxlength="36"
                               data-msg-required="请填写小区名称"/>
                    </div>
                    <div class="form-item">
                        <label for="district-address">小区地址</label>
                        <input id="district-address" class="form-control" type="text" name="address"
                               data-rule-required="true"
                               data-rule-maxlength="500"
                               data-msg-required="请填写小区地址"/>
                    </div>
                    <div class="form-item">
                        <label for="area">面积</label>
                        <input id="area" class="form-control" type="text" name="area"
                               data-rule-required="true"
                               data-rule-number="true"
                               data-rule-min="1"
                               data-rule-max="99999999"
                               data-msg-required="请填写房屋面积"/>
                        <span class="suffix">m<sup>2</sup></span>
                    </div>
                    <div class="form-item">
                        <label for="n-bedroom">居室</label>
                        <input id="n-bedroom" class="form-control layout" type="text" name="bedroom" value="2"
                               data-rule-require_from_group='[1,".layout"]'
                               data-rule-digits="true"
                               data-rule-min="0"
                               data-rule-max="99"
                               data-msg-require_from_group="请填写居室信息"/><span class="suffix">室</span>
                        <input id="n-parlor" class="form-control layout" type="text" name="parlor" value="1"
                               data-rule-require_from_group='[1,".layout"]'
                               data-rule-digits="true"
                               data-rule-min="0"
                               data-rule-max="99"
                               data-msg-require_from_group="请填写居室信息"/><span class="suffix">厅</span>
                        <input id="n-bathroom" class="form-control layout" type="text" name="bathroom" value="1"
                               data-rule-require_from_group='[1,".layout"]'
                               data-rule-digits="true"
                               data-rule-min="0"
                               data-rule-max="99"
                               data-msg-require_from_group="请填写居室信息"/><span class="suffix">卫</span>
                    </div>
                    <div class="form-item">
                        <a id="more-info" class="dropdown-toggle" href="javascript:;" role="button">
                            <span class="text">更多信息</span><b class="icon-up-down icon-up-down-down"></b>
                        </a>
                    </div>
                    <div class="form-more">
                        <div class="form-item">
                            <label class="rich-label">描述<br />
                                <span>在此区域输入对您生活空间的描述，以便窝噻能更多的了解您的住房。</span></label>
                            <textarea class="form-control" name="introduce" data-rule-maxlength="600"></textarea>
                        </div>
                        <div class="form-item">
                            <label class="rich-label">上传照片<br />
                                <span>文字描述不过瘾，那就干脆再上传几张最能体现您房源特点的照片吧。</span></label>
                            <div class="form-control images-container">
                                <ul class="clearfix">
                                    <li><a href="javascript:;" class="add-pic">
                                            <input name="file" title="支持jpg、jpeg、gif、png格式" type="file" data-url="${ctx}/upfile"/>
                                            <span class="icon-input-file"></span>
                                            <span class="icon-close"></span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="confirm-profile-container">
                <div class="content-width">
                    <div class="title">个人信息确认</div>
                    <div class="description">为确保我们后续为您提供优质的服务，请告知我们您的联系方式。窝噻将在您提交发布申请后的48小时内与您联系，上门提供专业的拍摄服务并确认房屋相关信息。</div>
                    <c:if test="${not empty principal.username}">
			<div class="form-item">
			    <label for="real-name">您的姓名</label>
			    <input id="real-name" class="form-control" type="text" name="userName" value="${principal.username}"
				   data-rule-required="true"
				   data-rule-maxlength="40"
				   data-msg-required="请填写您的姓名"/>
			</div>
		    </c:if>
                    <div class="form-item">
                        <label for="phone-num">手机号码</label>
                        <input id="phone-num" class="form-control" type="text" name="userPhone" value="${principal.mobile}"
                               data-rule-required="true"
                               data-rule-mobile="true"
                               data-msg-required="请填写您的手机号码"
                               data-msg-mobile="请填写格式正确的手机号码"/>
                        <a id="receive-validate-number" href="javascript:;">获取验证码</a>
                    </div>
                    <div class="form-item">
                        <label for="validate-code">验证码</label>
                        <input id="validate-code" class="form-control" type="text" name="smscode"
                               data-rule-required="true"
                               data-rule-rangelength="[6,6]"
                               data-msg-required="请先获取验证码"
                               data-msg-rangelength="请输入6位验证码"/>
                    </div>
                    <div class="form-item">
                        <button type="submit" class="btn btn-primary btn-sm">确认发布</button>
                    </div>
                </div>

            </div>
        </form>
        <%@include file="/WEB-INF/jspf/copyright.jspf" %>
    </body>
</html>
