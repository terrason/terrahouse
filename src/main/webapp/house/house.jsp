<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jspf/prepare.jspf" %>
<!DOCTYPE html>
<html>
    <head>
        <title><spring:message code="application.title"/></title>
        <%@include file="/WEB-INF/jspf/common.jspf" %>
        <link href="${ctx}/css/house.css" rel="stylesheet"/>
        <link href="${ctx}/css/house-slideshow.css" rel="stylesheet"/>
        <link href="${ctx}/css/house-tab.css" rel="stylesheet"/>
        <link href="${ctx}/css/traffic-table.css" rel="stylesheet"/>
        <link href="${ctx}/css/rich-calendar.css" rel="stylesheet"/>
        <link href="${ctx}/css/tax-calculator.css" rel="stylesheet"/>
        <link href="${ctx}/css/house-share.css" rel="stylesheet"/>
        <link href="${ctx}/css/house-comments.css" rel="stylesheet"/>
        <link href="${ctx}/css/house-item.css" rel="stylesheet"/>
        <link href="${ctx}/css/rich-circle.css" rel="stylesheet"/>
        <link href="${ctx}/css/house-suggestions.css" rel="stylesheet"/>
        <script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=sZrTH9UHFf5i9T5KDx122dUB"></script>
        <script type="text/javascript" src="${ctx}/js/house.js"></script>
        <script type="text/javascript" src="${ctx}/js/terrahouse-bmap.js"></script>
        <script type="text/javascript" src="${ctx}/js/tax-calculator.js"></script>
        <script type="text/javascript" src="${ctx}/js/house-share.js"></script>
        <script type="text/javascript" src="${ctx}/js/house-slide-show.js"></script>
        <script type="text/javascript" src="${ctx}/js/rich-circle.js"></script>
        <script type="text/javascript" src="${ctx}/js/house-suggestions.js"></script>
    </head>
    <body>
        <%@include file="/WEB-INF/jspf/navigation.jspf" %>
        <div id="content">
            <div class="content-width-static clearfix">
                <div id="left">
                    <div id="gallery">
                        <div class="gallery-header">
                            <div class="icon-flag icon-sale"></div>
                            <div class="estate-name">${house.name}</div>
                            <div class="clearfix">
                                <div class="estate-address">
                                    ${house.politicalAreaName}，
                                    <a target="_blank" href="${ctx}/block/${house.communityId}">${house.communityName}</a>，
                                    <a target="_blank" href="http://api.map.baidu.com/geocoder?address=${house.address}&output=html">${house.address}</a>
                                </div>
                                <div class="like-container">
                                    <span class="icon-like" data-house="${house.id}"></span> 喜欢（<span class="like-count">${house.forks}</span>）
                                </div>
                            </div>
                        </div>
                        <div id="photos" class="slideshow">
                            <div id="slideshow" class="slideshow">
                                <div class="carousel slide">
                                    <div class="carousel-inner">
                                        <c:forEach items="${house.snapshots}" var="cur" varStatus="status">
                                            <c:set var="layout" value="${house.layouts[status.index]}"/>
                                            <div class="item ${status.first ? 'active':''}">
                                                <img src="${empty cur ? ctx : srx}${empty cur ? "/images/loading.gif":cur}" alt="" style="width:690px;height:405px;"/>
                                                <span class="icon-house-type-pic" data-layout="${empty layout ? "": srx}${layout}"></span>
                                            </div>
                                        </c:forEach>
                                    </div>
                                </div>
                                <div class="thumbnails-container">
                                    <img alt="prev" src="${ctx}/images/icon-left.png" class="slideshow-btn slideshow-prev"/>
                                    <div class="thumbnails-viewport">
                                        <ul class="slideshow-thumbnails clearfix">
                                            <c:forEach items="${house.snapshots}" var="cur" varStatus="status">
                                                <li class="${status.first ? 'active':''}"><img src="${empty cur ? ctx : srx}${empty cur ? "/images/loading.gif":cur}"/></li>
                                             </c:forEach>
                                        </ul>
                                    </div>
                                    <img alt="next" src="${ctx}/images/icon-right.png" class="slideshow-btn slideshow-next"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end of sllide show -->
                    <div class="house-type-pic"><!--this is just a template for display-->
                        <div class="house-type-pic-content clearfix">
                            <img class="picture" src="" border="0" style="width:287px;height:204px;"/>
                            <div class="legend">
                                <div class="pull-left">
                                    <img class="camera-icon" src="${ctx}/images/icon-camera.png"/>
                                    <span class="camera-text">照片拍摄视角及位置</span>
                                </div>
                                <div class="pull-right">
                                    <img class="derection-icon" src="${ctx}/images/icon-derection.png"/>
                                    <span class="derection-text">N</span>
                                </div>
                            </div>
                        </div>
                        <span class="house-type-pic-handler"></span>
                    </div>
                    <div class="icon-house-type-in-out out"></div>
                    <div class="icon-house-type-pic-trigger"></div>
                    <!--房源图片展示结束-->

                    <div id="tabs">
                        <ul>
                            <li><a href="#tab-overview">房屋概况</a></li>
                            <li><a href="#tab-surroundings">周边环境</a></li>
                            <li><a href="#tabs-streat-view">街景地图</a></li>
                            <li><a href="#tabs-comments">评论</a></li>
                        </ul>
                        <div id="tab-overview">
                            <table class="ui-corner-all">
                                <tr class="head">
                                    <td colspan="4" class="title">
                                        <span class="icon-info"></span>基本信息
                                    </td>
                                    <td rowspan="5" class="circles">
                                        <dl>
                                            <dd>
                                                <div class="rich-circle" percent="${house.overview.essential.cycle.location}%" anticlockwise="true">
                                                    <div class="hold hold1">
                                                        <div class="pie pie1"></div>
                                                    </div>
                                                    <div class="hold hold2">
                                                        <div class="pie pie2"></div>
                                                    </div>
                                                    <div class="bg"></div>
                                                    <div class="content">
                                                        <div class="number">${house.overview.essential.cycle.location}</div>
                                                        <div class="category">地段</div>
                                                    </div>
                                                </div>
                                            </dd>
                                            <dd>
                                                <div class="rich-circle" percent="${house.overview.essential.cycle.layout}%" anticlockwise="true">
                                                    <div class="hold hold1">
                                                        <div class="pie pie1"></div>
                                                    </div>
                                                    <div class="hold hold2">
                                                        <div class="pie pie2"></div>
                                                    </div>
                                                    <div class="bg"></div>
                                                    <div class="content">
                                                        <div class="number">${house.overview.essential.cycle.layout}</div>
                                                        <div class="category">房型</div>
                                                    </div>
                                                </div>
                                            </dd>
                                            <dd>
                                                <div class="rich-circle" percent="${house.overview.essential.cycle.decoration}%" anticlockwise="true">
                                                    <div class="hold hold1">
                                                        <div class="pie pie1"></div>
                                                    </div>
                                                    <div class="hold hold2">
                                                        <div class="pie pie2"></div>
                                                    </div>
                                                    <div class="bg"></div>
                                                    <div class="content">
                                                        <div class="number">${house.overview.essential.cycle.decoration}</div>
                                                        <div class="category">装修</div>
                                                    </div>
                                                </div>
                                            </dd>
                                            <dd>
                                                <div class="rich-circle" percent="${house.overview.essential.cycle.surround}%" anticlockwise="true">
                                                    <div class="hold hold1">
                                                        <div class="pie pie1"></div>
                                                    </div>
                                                    <div class="hold hold2">
                                                        <div class="pie pie2"></div>
                                                    </div>
                                                    <div class="bg"></div>
                                                    <div class="content">
                                                        <div class="number">${house.overview.essential.cycle.surround}</div>
                                                        <div class="category">周边</div>
                                                    </div>
                                                </div>
                                            </dd>
                                            <dd>
                                                <div class="rich-circle" percent="${house.overview.essential.cycle.storey}%" anticlockwise="true">
                                                    <div class="hold hold1">
                                                        <div class="pie pie1"></div>
                                                    </div>
                                                    <div class="hold hold2">
                                                        <div class="pie pie2"></div>
                                                    </div>
                                                    <div class="bg"></div>
                                                    <div class="content">
                                                        <div class="number">${house.overview.essential.cycle.storey}</div>
                                                        <div class="category">楼层</div>
                                                    </div>
                                                </div>
                                            </dd>
                                            <dd>
                                                <div class="rich-circle" percent="${house.overview.essential.cycle.age}%" anticlockwise="true">
                                                    <div class="hold hold1">
                                                        <div class="pie pie1"></div>
                                                    </div>
                                                    <div class="hold hold2">
                                                        <div class="pie pie2"></div>
                                                    </div>
                                                    <div class="bg"></div>
                                                    <div class="content">
                                                        <div class="number">${house.overview.essential.cycle.age}</div>
                                                        <div class="category">房龄</div>
                                                    </div>
                                                </div>
                                            </dd>
                                        </dl>
                                    </td>
                                </tr>
                                <tr class="highlight">
                                    <td class="lable">总价：</td>
                                    <td class="value"><fmt:formatNumber value="${house.overview.essential.totalPrice}" type="currency" /></td>
                                    <td class="lable">房型：</td>
                                    <td class="value" width="84px">${house.overview.essential.schema}</td>
                                </tr>
                                <tr>
                                    <td class="lable">单价：</td>
                                    <td class="value"><fmt:formatNumber value="${house.overview.essential.unitPrice}" type="currency" /> / m2</td>
                                    <td class="lable">装修：</td>
                                    <td class="value">${house.overview.essential.decoration}</td>
                                </tr>
                                <tr class="highlight">
                                    <td class="lable">面积：</td>
                                    <td class="value">${house.overview.essential.acreage} m2</td>
                                    <td class="lable">楼层：</td>
                                    <td class="value">${house.overview.essential.storey}</td>
                                </tr>
                                <tr>
                                    <td class="lable">居室：</td>
                                    <td class="value">${house.overview.essential.layout}</td>
                                    <td class="lable">年代：</td>
                                    <td class="value">${house.overview.essential.years}</td>
                                </tr>
                            </table>
                            <div class="introduce">
                                <div class="title">房屋概况</div>
                                ${house.overview.houseIntroduction}
                                <div class="title">小区环境</div>
                                ${house.overview.communityIntroduction}
                            </div>
                        </div>
                        <div id="tab-surroundings">
                            <div class="introduce">
                                <div class="title">周边生活设施</div>
                                <div class="paragraph">${house.surround.livingFacilities}</div>
                                <div class="title">小区交通情况</div>
                                <div class="clearfix">
                                    <div class="paragraph">${house.surround.traffic.overview}</div>
                                    <table class="traffic alternate">
                                        <tbody>
                                            <c:forEach items="${house.surround.traffic.guidepost}" var="cur" varStatus="status">
                                                <tr>
                                                    <td class="commercial-area">${cur.location}</td>
                                                    <td class="bus">
                                                        <span class="icon-bus"></span><span class="time-cost">${cur.bus}</span>min
                                                    </td>
                                                    <td class="texi">
                                                        <span class="icon-texi"></span><span class="time-cost">${cur.taxi}</span>min
                                                    </td>
                                                </tr>
                                            </c:forEach>
                                        </tbody>
                                    </table>
                                </div>
                                <div id="map" class="bmap-facility"
                                     data-x="${house.surround.axis.x}"
                                     data-y="${house.surround.axis.y}"
                                     data-name="房源位置"
                                     data-type="7"></div>
                            </div>
                        </div>
                        <div id="tabs-streat-view">
                            <div id="map-streat-view"
                                 data-x="${house.surround.axis.x}"
                                 data-y="${house.surround.axis.y}"></div>
                        </div>
                        <div id="tabs-comments">
                            <div class="i-want-comment-container">
                                <a href="#comment-textarea">我要评论</a>
                            </div>
                            <ul class="comments" data-page="0" data-house="${house.id}">

                            </ul>
                            <form id="comment-form" class="validate" method="post" action="${ctx}/house/${id}/comment">
                                <input type="hidden" name="houseId" value="${house.id}"/>
                                <textarea id="comment-textarea" name="text" class="form-control"
                                          data-rule-required="true"
                                          data-rule-maxlength="500"></textarea>
                                <div class="action">
                                    <button type="submit" class="btn btn-primary">提交评论</button>
                                    <div class="popup popup-tip ui-corner-all">提交成功待审核</div>
                                </div>
                            </form>
                            <script type="text/javascript">
                                $nojs.before(function(context) {
                                    var validateOption = {
                                        highlight: function(element) {
                                            $(element).parent().addClass('has-error');
                                        },
                                        unhighlight: function(element) {
                                            $(element).parent().removeClass('has-error');
                                        },
                                        errorPlacement: function(error, element) {
                                            //DO NOTHING.
                                        }
                                    };
                                    $("#comment-form").data(validateOption);
                                });
                            </script>
                        </div>
                    </div>
                </div>
                <%@include file="/house/jspf/right.jspf" %>
            </div>
        </div>
        <div class="house-suggestions">
		<div class='suggestion-title content-width'>
			您可能感兴趣的其他房源
		</div>
            <div class="items content-width clearfix" data-page="0"></div>
            <div class="house-item-template">
                <div class="house-item">
                    <a class="house-anchor" href="javascript:;">
                        <img class="preview" src="${ctx}/images/house-item-pic-default.png" alt="" style="width:238px;height:140px;"/>
                        <table>
                            <tr>
                                <td class='name'>丝庐花语</td>
                                <td class='price'><span class='number'>580</span>万</td>
                            </tr>
                            <tr>
                                <td class='address'>徐汇，斜土路大木桥</td>
                                <td class='area'><span class='number'>125</span>m<sup>2</sup></td>
                            </tr>
                        </table>
                    </a>
                </div>
            </div>
        </div>
        <%@include file="/WEB-INF/jspf/copyright.jspf" %>
    </body>
</html>
