<%@page import="java.util.UUID"%>
<%@ page pageEncoding="UTF-8" %>
<%@include file="/WEB-INF/jspf/prepare.jspf" %>
<div data-page="${pager.currentPage}" data-size="${pager.pageSize}" data-total="${pager.totalSize}">
<c:forEach items="${houses}" var="cur">
    <c:set var="uuid" value="<%=UUID.randomUUID()%>"/>
    <div class="house-item" data-id="${cur.id}" data-x="${cur.surround.axis.x}" data-y="${cur.surround.axis.y}">
        <div id="house-widget-${uuid}" class="carousel slide" data-interval="false">
            <div class="carousel-inner">
                <c:forEach items="${cur.snapshots}" var="snapshot" varStatus="status">
                    <div class="item ${status.index eq 0 ? 'active':''}">
                        <a href="${ctx}/house/${cur.id}"><img src="${srx}${snapshot}" alt="" style="width:331.5px;height:197px;"></a>
                    </div>
                </c:forEach>
            </div>
            <!-- Controls -->
            <a class="left carousel-control" href="#house-widget-${uuid}" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
            </a>
            <a class="right carousel-control" href="#house-widget-${uuid}" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
            </a>
        </div>
        <table>
            <tr>
                <td class="name">${cur.name}</td>
                <td class="price"><span class="number">${cur.price}</span>万</td>
            </tr>
            <tr>
                <td class="address">${cur.politicalAreaName}，${cur.communityName}</td>
                <td class="area"><span class="number">${cur.area}</span>m<sup>2</sup></td>
            </tr>
        </table>
    </div>
</c:forEach>
</div>