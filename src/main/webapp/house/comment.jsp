<%@ page pageEncoding="UTF-8" %>
<%@include file="/WEB-INF/jspf/prepare.jspf" %>
<c:forEach items="${comments}" var="cur">
    <li class="user-comment clearfix">
        <div class="pull-left">
            <img src="${ctx}/images/icon-quote.png"/>
        </div>
        <div class="pull-right">
            <img alt="avatar" src="${empty cur.avatar ? ctx : srx }${empty cur.avatar ? "/images/icon-user-hear-big.png" : cur.avatar}"/>
            <div class="user-name">${cur.customerId gt 0 ? cur.customerName:"游客"}</div>
        </div>
        <div class="comment-details-container">
            <div class="text">${cur.text}</div>
            <div class="date"><fmt:formatDate value="${cur.createTime}" pattern="yyyy年M月d日 HH:mm"/></div>
        </div>
    </li>
    <c:forEach items="${cur.reply}" var="reply">
        <li class="admin-comment clearfix">
            <div class="pull-left">
                <img src="${ctx}/images/icon-quote.png"/>
            </div>
            <div class="comment-details-container">
                <div class="text">${reply.text}</div>
                <div class="date">Wellsite <fmt:formatDate value="${reply.createTime}" pattern="yyyy年M月d日 HH:mm"/></div>
            </div>
        </li>
    </c:forEach>
</c:forEach>