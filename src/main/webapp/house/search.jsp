<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jspf/prepare.jspf" %>
<!DOCTYPE html>
<html>
    <head>
        <title><spring:message code="application.title"/></title>
        <%@include file="/WEB-INF/jspf/common.jspf" %>
        <script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=sZrTH9UHFf5i9T5KDx122dUB"></script>
        <link href="${ctx}/css/house-item.css" rel="stylesheet">
        <link href="${ctx}/css/search.css" rel="stylesheet">
        <script src="${ctx}/js/terrahouse-bmap.js"></script>
        <script src="${ctx}/js/search1.js"></script>
    </head>
    <body>
        <%@include file="/WEB-INF/jspf/navigation.jspf" %>
        <div class="menu-shadow"></div>
        <div id="map-container">
            <div id="map"></div>
        </div>
        <div id="right-container">
            <div id="filter-and-result-container">
                <div class="filter-and-control-wrapper">
                    <div id="filter-and-control">
                        <div id="filter" class="clearfix">
                            <form id="form-search" class="form-horizontal" role="form" action="search" method="post">
                                <div class="form-group">
                                    <div class="col-sm-offset-1 col-sm-10">
                                        <button id="submit-location" class="btn btn-primary btn-large btn-contrast" type="submit">
                                            <img class="icon-search" src="${ctx}/images/icon-search.png" />
                                        </button>
                                        <input class="form-control autocomplate-keyword" autocomplete="off" name="keyword" value="${param.keyword}"
                                               placeholder="您想住哪里？（如:街区、楼盘、轨道交通）"/>
                                        <input id="pageNo" type="hidden" name="currentPage" value="0"/>
                                        <input id="pageSize" type="hidden" name="pageSize" value="20"/>
                                        <input id="minX" type="hidden" name="minX"/>
                                        <input id="minY" type="hidden" name="minY"/>
                                        <input id="maxX" type="hidden" name="maxX"/>
                                        <input id="maxY" type="hidden" name="maxY"/>
                                        <input id="orderby" type="hidden" name="orderby"/>
                                        <input id="needRecord" type="hidden" name="needRecord" value="false"/>
                                    </div>
                                </div>
                                <div class="options">
                                    <div class="form-group">
                                        <label class="col-sm-1 control-label with-slider">价格</label>
                                        <div class="col-sm-10">
                                            <input id="search-price-from" type="hidden" name="priceFrom" value="${param.priceFrom}"/>
                                            <input id="search-price-to" type="hidden" name="priceTo" value="${param.priceTo}"/>
                                            <div class="slider" data-range="true" data-min="0" data-max="1000"
                                                 data-display-min="#price-from" data-display-max="#price-to"
                                                 data-val-min="#search-price-from" data-val-max="#search-price-to"></div>
                                            <p class="slider-tip-left">
                                                <span id="price-from">0</span>万
                                            </p>
                                            <p class="slider-tip-right">
                                                <span id="price-to">1000</span>万
                                            </p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-1 control-label with-slider">面积</label>
                                        <div class="col-sm-10">
                                            <input id="search-area-from" type="hidden" name="areaFrom" value="${param.areaFrom}"/>
                                            <input id="search-area-to" type="hidden" name="areaTo" value="${param.areaTo}"/>
                                            <div class="slider" data-range="true" data-min="0" data-max="500"
                                                 data-display-min="#area-from" data-display-max="#area-to"
                                                 data-val-min="#search-area-from" data-val-max="#search-area-to"></div>
                                            <p class="slider-tip-left">
                                                <span id="area-from">0</span>m<sup>2</sup>
                                            </p>
                                            <p class="slider-tip-right">
                                                <span id="area-to">500</span>m<sup>2</sup>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-1 control-label with-slider">房龄</label>
                                        <div class="col-sm-10">
                                            <input id="search-age-from" type="hidden" name="ageFrom" value="${param.ageFrom}"/>
                                            <input id="search-age-to" type="hidden" name="ageTo" value="${param.ageTo}"/>
                                            <div class="slider" data-range="true" data-min="0" data-max="30"
                                                 data-display-min="#house-age-from" data-display-max="#house-age-to"
                                                 data-val-min="#search-age-from" data-val-max="#search-age-to"></div>
                                            <p class="slider-tip-left">
                                                <span id="house-age-from">0</span>年
                                            </p>
                                            <p class="slider-tip-right">
                                                <span id="house-age-to">30</span>年
                                            </p>
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <div class="col-sm-10 col-sm-offset-1 clearfix">
                                            <div class="pull-left select-item">
                                                <a data-toggle="dropdown" href="javascript:;" data-value="${param.polity}">
                                                    <input class="form-control" value="" readonly="readonly" placeholder="所有区域"/>
                                                    <input type="hidden" name="polity"/>
                                                    <span class="dropdown-icon"></span>
                                                </a>
                                                <ul id="area-dropdown-menu" class="dropdown-menu" role="menu" aria-labelledby="area-dropdown">
                                                    <li><a href="javascript:;" data-value="">所有区域</a></li>
                                                        <c:forEach items="${polities}" var="cur">
                                                        <li><a href="javascript:;" data-value="${cur.name}">${cur.name}</a></li>
                                                        </c:forEach>
                                                </ul>
                                            </div>
                                            <div class="pull-right select-item">
                                                <a data-toggle="dropdown" href="javascript:;" data-value="${param.decoration}">
                                                    <input class="form-control" value="" readonly="readonly" placeholder="所有装修"/>
                                                    <input type="hidden" name="decoration"/>
                                                    <span class="dropdown-icon"></span>
                                                </a>
                                                <ul id="fitting-dropdown-menu" class="dropdown-menu" role="menu">
                                                    <li><a href="#" data-value="">所有装修</a></li>
                                                    <li><a href="#" data-value="豪华装修">豪华装修</a></li>
                                                    <li><a href="#" data-value="精装">精装</a></li>
                                                    <li><a href="#" data-value="简单装修">简单装修</a></li>
                                                    <li><a href="#" data-value="一般装修">一般装修</a></li>
                                                    <li><a href="#" data-value="毛坯">毛坯</a></li>
                                                </ul>
                                            </div>
                                            <div class="select-item middle">
                                                <a data-toggle="dropdown" href="javascript:;" data-value="${param.bedroom}">
                                                    <input class="form-control" value="" readonly="readonly" placeholder="所有居室"/>
                                                    <input type="hidden" name="bedroom"/>
                                                    <span class="dropdown-icon"></span>
                                                </a>
                                                <ul id="n-bedroom-dropdown-menu" class="dropdown-menu" role="menu">
                                                    <li><a href="#" data-value="">所有居室</a></li>
                                                    <li><a href="#" data-value="1">一居室</a></li>
                                                    <li><a href="#" data-value="2">二居室</a></li>
                                                    <li><a href="#" data-value="3">三居室</a></li>
                                                    <li><a href="#" data-value="-3">四居室及以上</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-10 col-sm-offset-1 clearfix">
                                            <div class="checkbox select-item pull-left">
                                                <input id="is-over-5-years" name="fiveYearsOlder" type="checkbox" value="true"/>
                                                <label for="is-over-5-years"> <span></span> 满五年</label>
                                            </div>
                                            <div class="checkbox select-item middle">
                                                <input id="is-only-one-4-hourse-owner" name="unique" type="checkbox" value="true"/>
                                                <label for="is-only-one-4-hourse-owner"> <span></span> 房东唯一住房</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="control-container-wrapper">
                            <div id="control-container">
                                <!--  class="stuck" -->
                                <button id="hide-filter" type="button" class="btn btn-default">
                                    <span id="icon-hide-show-toggle" class="icon-show"></span>
                                    <span class="text-fix">隐藏搜索条件</span>
                                </button>
                                <div class="dropdown">
                                    <button id="price-order" type="button" class="btn btn-default" data-toggle="dropdown" data-value="0">
                                        <span class="display">默认排序</span>
                                        <span class="dropdown-icon"></span>
                                    </button>
                                    <ul id="price-order-options" class="dropdown-menu" role="menu" aria-labelledby="price-order">
                                        <li><a href="javascript:;" data-value="0">默认排序</a></li>
                                        <li><a href="javascript:;" data-value="1">价格从高到低</a></li>
                                        <li><a href="javascript:;" data-value="2">价格从低到高</a></li>
                                        <li><a href="javascript:;" data-value="3">交通便捷性</a></li>
                                        <li><a href="javascript:;" data-value="4">房龄</a></li>
                                        <li><a href="javascript:;" data-value="5">收藏次数</a></li>
                                    </ul>
                                </div>
                                <div class="result-count">
                                    共找到<span id="pageTotal">0</span>个房源
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="result-container">
                    <div class="items"></div>
                    <div class="bottom">
                        <button id="nextPage" class="btn btn-primary" type="button">显示更多房源</button>
                    </div>
                </div>
            </div>
            <div id="loadingPopup" class="bottom popup">
                <img src="${ctx}/images/loading.gif">
            </div>
        </div>
    </body>
</html>