$(function() {
	$('ul.about-titles li a').each(function(index, value) {
		var $this = $(value);
		var id = $this.attr('href');
		$(id).hide();
	});
	$($('ul.about-titles li').first().find('a').attr('href')).show();

	$('ul.about-titles li a').click(function() {
		var $this = $(this);
		var $parentLi = $this.parents('li.hilight');
		if ($parentLi.length == 0) {
			var $hilightLi = $('ul.about-titles li.hilight');
			var hilightId = $hilightLi.find('a').attr('href');
			$(hilightId).hide();
			$hilightLi.removeClass('hilight')
			$($this.attr('href')).show();
			$this.parents('li').addClass('hilight');
		}
		return false;
	});
});