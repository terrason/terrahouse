$(function() {
	$('.tabs').tabs({
		activate : onTabActivate
	});

	$('.delete-all button').click(function() {
		popupDeleteConfirm('您是否确定删除所有收藏？', function() {
			$('#tab-favorite-sold .booked-list').remove();
			positionFooter();
		})
	});

	// 我的在售收藏，应该从服务器获取
	var myFavoriteOnSaleList = [ {
		image : '/images/account/booked-item-picture-default.jpg',
		houseId : '231231',
		houseName : '源成春苑1',
		blockAddress : '闵行区，莘庄，沁春路168弄',
		bookedDay : {
			day : '2014-02-06',
			index : 0
		},
		houseStatus : {
			index : 1,
			value : '在售'
		},
	}, {
		image : '/images/account/booked-item-picture-default.jpg',
		houseId : '231231',
		houseName : '源成春苑2',
		blockAddress : '闵行区，莘庄，沁春路1680弄',
		bookedDay : {
			day : '2014-02-25',
			index : 0
		},
		houseStatus : {
			index : 1,
			value : '在售'
		},
	} ]

	$('.tabs ul li a[href="#tab-favorite-on-sale"] span.count').text(
			myFavoriteOnSaleList.length)
	addBookedList($('#tab-favorite-on-sale .booked-list'), myFavoriteOnSaleList);

	// 我的已售收藏，应该从服务器获取
	var myFavoriteSoldList = [ {
		image : '/images/account/booked-item-picture-default.jpg',
		houseId : '231231',
		houseName : '源成春苑3',
		blockAddress : '闵行区，莘庄，沁春路168弄',
		bookedDay : {
			day : '2014-02-06',
			index : 0
		},
		houseStatus : {
			index : 2,
			value : '已售',
			unread : true
		},
	}, {
		image : '/images/account/booked-item-picture-default.jpg',
		houseId : '231231',
		houseName : '源成春苑4',
		blockAddress : '闵行区，莘庄，沁春路1680弄',
		bookedDay : {
			day : '2014-02-25',
			index : 0
		},
		houseStatus : {
			index : 2,
			value : '已售',
			unread : true
		},
	}, {
		image : '/images/account/booked-item-picture-default.jpg',
		houseId : '231231',
		houseName : '源成春苑5',
		blockAddress : '闵行区，莘庄，沁春路1680弄',
		bookedDay : {
			day : '2014-02-25',
			index : 0
		},
		houseStatus : {
			index : 3,
			value : '已售'
		},
	}, {
		image : '/images/account/booked-item-picture-default.jpg',
		houseId : '231231',
		houseName : '源成春苑6',
		blockAddress : '闵行区，莘庄，沁春路1680弄',
		bookedDay : {
			day : '2014-02-25',
			index : 0
		},
		houseStatus : {
			index : 1,
			value : '已售'
		},
	} ]
	$('.tabs ul li a[href="#tab-favorite-sold"] span.count').text(
			myFavoriteSoldList.length);

	var unreadCount = 0;
	$.each(myFavoriteSoldList, function(index, value) {
		if (value.houseStatus.unread) {
			unreadCount = unreadCount + 1;
		}
	});
	if (unreadCount > 0) {
		$('.ui-tabs .ui-tabs-nav li span.bubble-number').text(unreadCount);
	} else {
		$('.ui-tabs .ui-tabs-nav li span.bubble-number').hide();
	}

	addBookedList($('#tab-favorite-sold .booked-list'), myFavoriteSoldList);

});
function addBookedList($container, dataList) {
	var $template = $('#tab-favorite-on-sale .booked-list .booked-item.template');
	$.each(dataList, function(index, value) {
		var $item = $template.clone();
		$item.removeClass('template');
		$item.find('.picture img').attr('src', value.image);
		$item.find('a.block-name').text(value.houseName);
		$item.find('.block-address a').text(value.blockAddress);
		var dayArray = value.bookedDay.day.split('-');
		$item.find('.booked-day').text(
				dayArray[0] + '年' + dayArray[1] + '月' + dayArray[2] + '日');
		$item.find('.house-status a').text(value.houseStatus.value);

		$item.find('.delete-container a').click(function() {
			popupDeleteConfirm('您是否确定删除该条收藏？', function() {
				$item.remove();
				positionFooter();
			});
			return false;
		});

		$item.find('.delete-container a').hover(function() {
			$(this).find('span.icon-delete').addClass('hover');
		}, function() {
			$(this).find('span.icon-delete').removeClass('hover');
		});

		if (value.houseStatus.unread) {
			$item.find('.house-status').addClass('unread')
		}

		$item.appendTo($container);
	});
}

function popupDeleteConfirm(text, onYes) {

	var $confirm = $('.popup.confirm#favorite-delete-confirm');
	$confirm.bPopup({
		modalClose : true,
		opacity : 0.6,
		positionStyle : 'absolute', // 'fixed' or 'absolute',
		fadeSpeed : 'slow', // can be a string ('slow'/'fast') or
		// int
		followSpeed : 'fast'
	// can be a string ('slow'/'fast') or int
	});
	$confirm.find('.text').text(text);
	$('button.yes').unbind('click');
	$('button.no').unbind('click');
	$('button.yes').click(function() {
		onYes();
		$confirm.bPopup().close();

	});
	$('button.no').click(function() {
		$confirm.bPopup().close();
	});
}
function onTabActivate(event, ui) {
	if (ui.newPanel.selector == '#tab-favorite-sold') {
		// my-favorites是左侧“我的收藏”的class
		$('ul.account-menu li.my-favorites span.bubble-number').remove();
		$('.ui-tabs .ui-tabs-nav li span.bubble-number').remove();
	}
}