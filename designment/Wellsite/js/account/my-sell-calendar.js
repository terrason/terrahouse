var timer;
var validateCodeInfoFlag = -1;

var datePicker = null;

// 可预约的日期和时间段数据
var bookDaysSetting = [];
// 买家预约了的日期
var bookedDays = [];

// 显示的时间段
var timeScope = [ "09:00 - 12:00", "12:00 - 18:00", "18:00 - 21:00" ]
// 选择的日期和时间段
var selectedDay = {
	day : null,
	availableIndex : [],// 可选的时间段的索引
	// 待删除
	index : -1,
	// 三个时间段的预定人数
	bookedBuyerCount : [ 0, 0, 0 ]
};

var mySellItemValue;

var today = new Date();
today = new Date(today.getFullYear(), today.getMonth(), today.getDate());
var dayDiffForDays = 1000 * 60 * 60 * 24 * 7;

$(function() {

	// 初始化可预约的日期初始化待开放的日期数据

	datePicker = $(".datepicker").datepicker({
		inline : true,
		showOtherMonths : false,
		dateFormat : 'yy-mm-dd',
		onSelect : function(date, picker) {
			doOnDayClicked(date, picker);
		},
		beforeShowDay : hilightDays
	});
	$(".numeric").numeric()
	var spinner = $("#book-person-limited-value").spinner({
		min : 1,
		max : 20
	});

	$('ul.dropdown-menu#sell-status-menu li a').click(function() {
		$('#sell-status').val($(this).text());
		$('ul.dropdown-menu#sell-status-menu').dropdown('toggle')
		return false;
	});

	$('#my-sell-manage.popup .options button#edit-save').click(bookedEditSave);
	$('#my-sell-manage.popup .options button#edit-close')
			.click(bookedEditClose);
	$('#book-it').click(bookedEditClose);
	$('#my-sell-manage.popup .select-time .time-item').each(
			function(i, element) {
				$this = $(this);
				$this.click({
					index : i,
					element : $this
				}, function(event) {
					selectTimeIndex(event);
				})
			});

	$('#real-name').blur(function() {
		var $realName = $(this);
		if ($realName.val() == '') {
			showPublishErrorMessage($realName, '请填写您的姓名');
		} else {
			clearPublishErrorMessage($realName);
		}
	});

	function validatePhoneNumber() {
		var success = true;
		var num = $('#phone-num').val();
		console.log('num.length = %o, num.substring = %o', num.length, num
				.substring(0, 1));
		if (num.length != 11 || num.substring(0, 1) != '1') {
			showPublishErrorMessage($('#phone-num'), '请填写格式正确的手机号码。');
			success = false;
		} else {
			showPublishErrorMessage($('#phone-num'), '');
		}
		return success;
	}

	$('#phone-num').blur(validatePhoneNumber);

	timer = $.timer(function() {
		if (validateCodeInfoFlag > 0) {
			$('#receive-validate-number').text(validateCodeInfoFlag + '秒后重新获取')
			validateCodeInfoFlag = validateCodeInfoFlag - 1;
		} else {
			$('#receive-validate-number').text('重新获取验证码');
			timer.stop();
		}
	});
	timer.set({
		time : 1000,
		autostart : false
	});
	$('#receive-validate-number').click(function() {
		if (!validatePhoneNumber()) {
			return false;
		}
		if (validateCodeInfoFlag <= 0) {
			validateCodeInfoFlag = 29;
			$(this).text(validateCodeInfoFlag + '秒后重新获取');
			validateCodeInfoFlag = validateCodeInfoFlag - 1;
			timer.play();
		} else {

		}
		return false;
	});

	$('button#book-confirmed-action').click(
			function() {
				var $realName = $('#real-name');
				var $phoneNum = $('#phone-num');
				var $validateCode = $('#validate-code');

				var success = true;

				if ($realName.val() == '') {
					showPublishErrorMessage($realName, '请填写您的姓名');
					success = false;
				} else {
					clearPublishErrorMessage($realName);
				}
				if ($phoneNum.val() == '') {
					showPublishErrorMessage($phoneNum, '请填写格式正确的手机号码。');
					success = false;
				} else {
					clearPublishErrorMessage($phoneNum);
				}
				if ($phoneNum.val().length != 11
						|| $phoneNum.val().substring(0, 1) != '1') {
					showPublishErrorMessage($phoneNum, '请填写您的手机号码');
					success = false;
				} else {
					clearPublishErrorMessage($phoneNum);
				}
				if ($validateCode.val() == '') {
					showPublishErrorMessage($validateCode, '请填写验证码');
					success = false;
				} else {
					if ($validateCode.val().length != 6) {
						showPublishErrorMessage($validateCode, '请输入6位验证码。');
						success = false;
					} else if (validateCodeInfoFlag < 0) {
						showPublishErrorMessage($validateCode, '请先获取验证码。');
						success = false;
					} else if (validateCodeInfoFlag == 0) {
						showPublishErrorMessage($validateCode, '超时，请重新获取验证码。');
						success = false;
					} else {
						clearPublishErrorMessage($validateCode);
					}

				}

				if (success) {
					// 模拟post后
					if ($validateCode.val() != '123456') {
						showPublishErrorMessage($validateCode, '验证码输入有误。');
						success = false;
					} else {
						showPublishErrorMessage($validateCode, '');
					}
				}
				if (success) {
					$('.popup.book-confirm').bPopup().close();
					popupBookConfirmResult();
				}
				return false;
			});

	$('.popup.book-confirm-result #book-confirmed-result-action').click(
			function() {
				$('.popup.popup.book-confirm-result').bPopup().close();
				return true;
			});

	$('input[type="checkbox"]#book-person-limited-label').change(function() {
		var input = $('input#book-person-limited-value');
		if (this.checked) {
			input.removeClass('gray');
		} else {
			input.addClass('gray');
		}
	});
});

// 点击日历某一天的事件
function doOnDayClicked(date, picker) {
	initTimeIndexByDay(date);
}

// 自定义要高亮的日期单元格的class
function hilightDays(date) {

	var diff = date.getTime() - today.getTime();
	if (diff >= 0 && diff < dayDiffForDays) {
		var formatDay = $.format.date(date, "yyyy-MM-dd")

		for (var i = 0; i < bookedDays.length; i++) {
			if (formatDay == bookedDays[i].day) {
				// 房东可设置＋买家已经预约
				return [ true,
						'setting-available-days book-available-days booked-days' ];
			}
		}

		for (var i = 0; i < bookDaysSetting.length; i++) {
			if (formatDay == bookDaysSetting[i].day) {
				// 房东可设置＋买家可预约
				return [ true, 'setting-available-days book-available-days' ];
			}
		}

		return [ true, 'setting-available-days' ];
	}

	return [ false, '' ];
}

// 点击时间段触发的事件
function selectTimeIndex(event) {
	var element = event.data.element;
	if (element.hasClass('disabled')) {
		displayCalendarError('该时段已有人预约，不可变更')
	} else if (!element.hasClass('selected')) {
		checkTime(element, event.data.index);
	} else {
		uncheckTime(element);
	}

	$(".datepicker").datepicker("refresh");
}

function checkTime($element, index) {
	hiddenError();
	$element.addClass('selected');
	$element.find('.checkbox .checkbox-bg').stop().scrollTo('0%', 300);

	$element.find('.checkbox .blockContainer')
			.switchClass('left', 'right', 300);
	$element.find('.booked-info').show();
	$element.find('.booked-info span.number').text(
			selectedDay.bookedBuyerCount[index]);

	var selectedDaySettings = findBookedDaySettingForDay(bookDaysSetting,
			selectedDay.day);
	if (selectedDaySettings == null) {
		bookDaysSetting.push({
			day : selectedDay.day,
			index : [ index ]
		});

		selectedDay.availableIndex = [];
		selectedDay.index = [];

		$(".datepicker").datepicker("refresh");

	}
}

function uncheckTime($element) {
	$element.removeClass('selected');
	$element.find('.checkbox .blockContainer')
			.switchClass('right', 'left', 300);
	$element.find('.checkbox .checkbox-bg').stop().scrollTo('100%', 300);
	$element.find('.booked-info').hide();

	var $selected = $('#my-sell-manage.popup .select-time .time-item.selected');
	if ($selected.size() == 0) {
		removeByDayInArray(bookDaysSetting, selectedDay.day);
		removeByDayInArray(bookedDays, selectedDay.day);
		$(".datepicker").datepicker("refresh");
	}

}

function removeByDayInArray(array, day) {
	var dayIndex = -1;
	$.each(array, function(index, value) {
		if (value.day == day) {
			dayIndex = index;
			return;
		}
	});
	if (dayIndex >= 0) {
		array.splice(dayIndex, 1);
	}
}

function isTimeChecked($element) {
	return $element.hasClass('selected');
}

// 重置时间段样式
function resetTimeIndex(isNoAnim) {
	$('#my-sell-manage.popup .select-time .time-item').each(
			function(i, value) {
				var $this = $(this);
				$this.removeClass('selected');
				$this.removeClass('disabled');
				$this.find('.checkbox .blockContainer').switchClass('right',
						'left', 300);
				if (isNoAnim) {
					$this.find('.checkbox .checkbox-bg').stop().scrollTo(
							'100%', 0);
				} else {
					$this.find('.checkbox .checkbox-bg').stop().scrollTo(
							'100%', 300);
				}
				if (selectedDay.bookedBuyerCount[i] > 0) {
					$this.addClass('disabled');
				}

			});
}

function displayCalendarError(message) {
	var $error = $('#my-sell-manage.popup .error');
	$error.text(message)
}

function hiddenError() {
	var $error = $('#my-sell-manage.popup .error');
	$error.text('')
}
function testDisplayMessage(message) {
	$('#message p').text(message)
	$('#message').css('display', 'block');
}

function testHiddenMessage() {
	$('#message p').text('')
	$('#message').css('display', 'none');
}

function popupBookConfirm() {
	$('.popup.book-confirm').bPopup({
		modalClose : true,
		opacity : 0.6,
		positionStyle : 'absolute', // 'fixed' or 'absolute',
		fadeSpeed : 'slow', // can be a string ('slow'/'fast') or
		// int
		followSpeed : 'fast'
	// can be a string ('slow'/'fast') or int
	});
}
function popupBookConfirmResult() {
	$('.popup.book-confirm-result').bPopup({
		modalClose : false,
		opacity : 0.6,
		positionStyle : 'absolute', // 'fixed' or 'absolute',
		fadeSpeed : 'slow', // can be a string ('slow'/'fast') or
		// int
		followSpeed : 'fast'
	// can be a string ('slow'/'fast') or int
	});
}

function showPublishErrorMessage($input, message) {
	var parent = $input.parent();
	var $error = parent.find('.error');
	if ($error.length == 0) {
		$error = $('<span></span>');
		$error.addClass('error');
		$error.appendTo(parent);
	}
	$error.text(message);
}

function clearPublishErrorMessage($input) {
	showPublishErrorMessage($input, '');
}

function initCalendarByData(item) {
	mySellItemValue = item;
	bookDaysSetting = item.bookDaysSetting;
	bookedDays = item.bookedDays;

	initTimeIndexByDay(new Date());

	$('input#booked-manage-set-price').val(mySellItemValue.price);
	$('input#sell-status').val(mySellItemValue.sellStatus.value);
	$(".datepicker").datepicker("refresh");
}

// 根据选择的某一天初始化日历右侧的时间段的选中情况
function initTimeIndexByDay(day) {
	selectedDay.day = $.format.date(day, "yyyy-MM-dd");

	// TODO
	var selectedDaySettings = findBookedDaySettingForDay(bookDaysSetting,
			selectedDay.day);
	if (selectedDaySettings == null) {
		selectedDay.availableIndex = [];
		selectedDay.index = [];
		selectedDay.bookedBuyerCount = [ 0, 0, 0 ];
	} else {
		selectedDay.availableIndex = selectedDaySettings.index;
		var bookedDayInfo = findBookedDaySettingForDay(bookedDays,
				selectedDay.day);
		if (bookedDayInfo) {
			selectedDay.bookedBuyerCount = bookedDayInfo.bookedBuyerCount;
		} else {
			selectedDay.bookedBuyerCount = [ 0, 0, 0 ];
		}
	}

	resetTimeIndex();

	console.log('bookDaysSetting = %o, bookedDays=%o', bookDaysSetting,
			bookedDays);

	$('#my-sell-manage.popup .select-time .time-item').each(
			function(index, value) {
				var $timeItem = $(value);
				if ($.inArray(index, selectedDay.availableIndex) >= 0) {
					checkTime($timeItem);
					var $bookedInfo = $timeItem.find('.booked-info');
					$bookedInfo.find('span.number').text(
							selectedDay.bookedBuyerCount[index]);
					$bookedInfo.show();
				} else {
					$timeItem.find('.booked-info').hide();
				}
			});

}
function findBookedDaySettingForDay(bookDaysSetting, day) {
	var result = null;
	$.each(bookDaysSetting, function(index, value) {
		if (value.day == day) {
			result = value;
		}
	});
	return result;
}

function findBookedInfoForDay(bookedDays, day) {
	var result = null;
	$.each(bookedDays, function(index, value) {
		if (value.day == day) {
			result = value;
		}
	});
	return result;
}
function bookedEditSave() {
	var $price = $('input#booked-manage-set-price');
	if ($price.val() == '') {
		$('.price-error').show();
		$price.addClass('has-error');
		return false;
	} else {
		$('.price-error').hide();
		$price.removeClass('has-error');
	}

	// 保存
	// 可预约的日期和时间段数据: bookDaysSetting

	// 保存成后关闭
	bookedEditClose();
	if (mySellItemValue.sellStatus.value != $('input#sell-status').val()) {
		popupSaveSuccessAlert('您的已售申请已提交，稍后将与您确认');
	} else {
		popupSaveSuccessAlert('保存成功');
	}

}
function bookedEditClose() {
	// 保存
	$('.popup#my-sell-manage').bPopup().close();
}

function popupSaveSuccessAlert(message) {
	$('.popup.alert#my-sell-manage-save-success-alert').text(message);
	$('.popup.alert#my-sell-manage-save-success-alert').bPopup({
		modalClose : true,
		opacity : 0.6,
		positionStyle : 'absolute', // 'fixed' or 'absolute',
		fadeSpeed : 'slow', // can be a string ('slow'/'fast') or
		// int
		followSpeed : 'fast'
	// can be a string ('slow'/'fast') or int
	});
}