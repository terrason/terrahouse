$(function() {

	$('ul.comments li.user-comment a.delete').click(function() {
		var $comment = $(this).parents('.user-comment');
		popupDeleteConfirm('您是否确定删除该条评论？', function() {
			$comment.nextUntil('.user-comment').remove();
			$comment.remove();
			positionFooter();
		});
		return false;
	});
	
	$('ul.comments li.user-comment a.delete').hover(function() {
		$(this).find('span.icon-delete').addClass('hover');
	}, function() {
		$(this).find('span.icon-delete').removeClass('hover');
	});
	
	$('ul.comments li').each(function(index, value){
		var $this = $(value);
		if($this.hasClass('admin-comment')){
			if(!$this.next().hasClass('admin-comment')){
				$this.addClass('last');
			}
		}
	});

	function popupDeleteConfirm(text, onYes) {
		
		var $confirm = $('.popup.confirm#comment-delete-confirm');
		$confirm.bPopup({
			modalClose : true,
			opacity : 0.6,
			positionStyle : 'absolute', // 'fixed' or 'absolute',
			fadeSpeed : 'slow', // can be a string ('slow'/'fast') or
			// int
			followSpeed : 'fast'
				// can be a string ('slow'/'fast') or int
		});
		$confirm.find('.text').text(text);
		$('button.yes').unbind('click');
		$('button.no').unbind('click');
		$('button.yes').click(function() {
			onYes();
			$confirm.bPopup().close();
		});
		$('button.no').click(function() {
			$confirm.bPopup().close();
		});
	}
});

