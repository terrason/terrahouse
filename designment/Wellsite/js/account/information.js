$(function() {

	$(".numeric").numeric();

	$('#title-dropdown-menu a').click(function() {
		$('#title').val($(this).text());
		$('#title-dropdown-menu.dropdown-menu').dropdown('toggle');
		validateRealName();
		return false;
	});
	$('#year-dropdown-menu a').click(function() {
		$('#year').val($(this).text());
		$('#year-dropdown-menu.dropdown-menu').dropdown('toggle');
		return false;
	});
	$('#month-dropdown-menu a').click(function() {
		$('#month').val($(this).text());
		$('#month-dropdown-menu.dropdown-menu').dropdown('toggle')
		return false;
	});
	$('#day-dropdown-menu a').click(function() {
		$('#day').val($(this).text());
		$('#day-dropdown-menu.dropdown-menu').dropdown('toggle')
		return false;
	});

	$('#real-name').blur(validateRealName);
	$('.form-item #phone-number').blur(validatePhoneNumber);
	$('.form-item #phone-number').click(validateBirthday);
	// $('#year').blur(validateBirthday);
	// $('#month').blur(validateBirthday);
	// $('#day').blur(validateBirthday);

	$('button#save').click(validate);
	$('button#upload-file-action').click(function() {
		$('#input-file').click();
	});

	var jcrop_api;
	$('#input-file').change(function() {
		$('button#save-file').prop('disabled', false);
		$('#head-big').attr('src', '/images/account/temp-head-big.jpg')
		$('#head-preview').attr('src', '/images/account/temp-head-big.jpg')

		$('#head-big').Jcrop({
			onChange : showPreview,
			onSelect : showPreview,
			aspectRatio : 1
		}, function() {
			jcrop_api = this;

			var imageWidth = $('#head-big').width();
			var imageHeight = $('#head-big').height();

			var min = imageWidth > imageHeight ? imageHeight : imageWidth;
			var offset = Math.round(min * 2 / 3);
			var x1 = (imageWidth - offset) / 2;
			var x2 = (imageWidth + offset) / 2;
			var y1 = (imageHeight - offset) / 2;
			var y2 = (imageHeight + offset) / 2;
			jcrop_api.setSelect([ x1, y1, x2, y2 ]);
		});

		function showPreview(coords) {
			var rx = 50 / coords.w;
			var ry = 50 / coords.h;

			var imageWidth = $('#head-big').width();
			var imageHeight = $('#head-big').height();

			$('#head-preview').css({
				width : Math.round(rx * imageWidth) + 'px',
				height : Math.round(ry * imageHeight) + 'px',
				marginLeft : '-' + Math.round(rx * coords.x) + 'px',
				marginTop : '-' + Math.round(ry * coords.y) + 'px'
			});
		}
	})

	$('button#save-file').prop('disabled', true);
	$('button#save-file').click(function() {
		// 当用户点击保存后，左上角小头像和当前头像更新
		popupTip('头像保存成功');
		var headUrlFromServer = '/images/account/temp-head-big.jpg';
		$('.account-nav .user-info .head img').prop('src', headUrlFromServer);
		$('button#save-file').prop('disabled', true);

		// “设置新头像”下面的选框应还原成初始状态，而不应该还停留在可拖动选框的状态。
		jcrop_api.release();
		$("#head-big").prop('src', '/images/account/head-big.jpg')
		$("#head-big").removeAttr('style');
		$('.jcrop-holder').remove();
	});

});

function validate(e) {
	e.preventDefault();
	var success = true;
	if (!validateRealName()) {
		success = false;
	}

	if (!validateBirthday()) {
		success = false;
	}

	if (!validatePhoneNumber()) {
		success = false;
	}

	if (success) {
		popupTip('信息保存成功');

		$('.account-nav .user-info .real-name').text($('#real-name').val());
		$('.account-nav .user-info .title').text($('#title').val());
	}
	return success;
}

function validateRealName() {
	var success = true;
	var $readName = $('#real-name');
	var $title = $('#title');
	if ($readName.val() == '' && $title.val() == '') {
		displayError($readName, '请填入您的姓名及称谓');
		success = false;
	} else if ($readName.val() == '') {
		displayError($readName, '请填入您的姓名');
		success = false;
	} else if ($title.val() == '') {
		displayError($readName, '请选择您的称谓');
		success = false;
	} else {
		clearError($readName);
	}

	return success;
}

function validatePhoneNumber() {
	var success = true;
	var $phoneNumber = $('.form-item #phone-number');
	if ($phoneNumber.val() == '') {
		displayError($phoneNumber, '请填入您的手机号码');
		success = false;
	} else if ($phoneNumber.val().length != 11
			|| $phoneNumber.val().charAt(0) != '1') {
		displayError($phoneNumber, '请填入格式正确的手机号码');
		success = false;
	} else {
		clearError($phoneNumber);
	}

	return success;

}
function validateBirthday() {
	var success = true;
	var $year = $('.form-item #year');
	var $month = $('.form-item #month');
	var $day = $('.form-item #day');
	if ($year.val() == '' || $month.val() == '' || $day.val() == '') {
		displayError($year, '请填入您的出生日期');
		success = false;
	} else {
		clearError($year);
	}
	return success;

}

function popupTip(text) {
	$('.popup.popup-tip').text(text);
	$('.popup.popup-tip').bPopup({
		modalClose : true,
		opacity : 0.6,
		positionStyle : 'absolute', // 'fixed' or 'absolute',
		fadeSpeed : 'slow', // can be a string ('slow'/'fast') or
		// int
		followSpeed : 'fast'
	// can be a string ('slow'/'fast') or int
	});
}