$(function() {
	// 我的有效预约，应该从服务器获取
	var mySellList = [ {
		houseId : '432656757547534275',
		image : '/images/account/booked-item-picture-default.jpg',
		blockId : '231231',
		blockName : '源成春苑',
		blockAddress : '闵行区，莘庄，沁春路168弄',
		publishDay : '2014-02-06',
		houseStatus : {
			index : 1,
			value : '已认证',
			unread : true
		},
		sellStatus : {
			index : 1,
			value : '在售'
		},
		// 总价，单位万
		price : 152,
		// 预约设置
		bookDaysSetting : [ {
			day : "2014-04-26",
			index : [ 0, 2 ]
		}, {
			day : "2014-04-27",
			index : [ 1, 2 ]
		}, {
			day : "2014-04-08",
			index : [ 0, 1 ]
		}, {
			day : "2014-04-29",
			index : [ 0, 1, 2 ]
		} ],
		bookedDays : [ {
			day : "2014-04-26",
			// 三个时间段的预定人数
			bookedBuyerCount : [ 0, 0, 1 ]

		}, {
			day : "2014-04-08",
			// 三个时间段的预定人数
			bookedBuyerCount : [ 0, 0, 0 ]
		}, {
			day : "2014-04-29",
			// 三个时间段的预定人数
			bookedBuyerCount : [ 0, 12, 1 ]
		} ]
	}, {
		houseId : '423656757547534345',
		image : '/images/account/booked-item-picture-default.jpg',
		blockId : '231231',
		blockName : '源成春苑010',
		blockAddress : '闵行区，莘庄，沁春路168弄',
		publishDay : '2014-02-02',
		houseStatus : {
			index : 1,
			value : '已认证',
			unread : true
		},
		sellStatus : {
			index : 1,
			value : '在售'
		},
		// 总价，单位万
		price : 156,
		// 预约设置
		bookDaysSetting : [ {
			day : "2014-04-26",
			index : [ 0, 1, 2 ]
		}, {
			day : "2014-04-27",
			index : [ 1 ]
		}, {
			day : "2014-04-29",
			index : [ 0 ]
		}, {
			day : "2014-05-1",
			index : [ 2 ]
		} ],
		bookedDays : [ {
			day : "2014-04-26",
			// 三个时间段的预定人数
			bookedBuyerCount : [ 10, 12, 1 ]

		}, {
			day : "2014-04-29",
			// 三个时间段的预定人数
			bookedBuyerCount : [ 9, 0, 0 ]
		}, {
			day : "2014-05-1",
			// 三个时间段的预定人数
			bookedBuyerCount : [ 0, 0, 4 ]
		} ]
	}, {
		houseId : '423656757547534345',
		image : '/images/account/booked-item-picture-default.jpg',
		blockId : '231231',
		blockName : '源成春苑010',
		blockAddress : '闵行区，莘庄，沁春路168弄',
		publishDay : '2014-02-02',
		houseStatus : {
			index : 1,
			value : '已认证'
		},
		sellStatus : {
			index : 1,
			value : '在售'
		},
		// 总价，单位万
		price : 156,
		// 预约设置
		bookDaysSetting : [ {
			day : "2014-04-26",
			index : [ 0, 1, 2 ]
		}, {
			day : "2014-04-27",
			index : [ 1 ]
		}, {
			day : "2014-04-29",
			index : [ 0 ]
		}, {
			day : "2014-05-1",
			index : [ 2 ]
		} ],
		bookedDays : [ {
			day : "2014-04-26",
			// 三个时间段的预定人数
			bookedBuyerCount : [ 10, 12, 1 ]

		}, {
			day : "2014-04-29",
			// 三个时间段的预定人数
			bookedBuyerCount : [ 9, 0, 0 ]
		}, {
			day : "2014-05-1",
			// 三个时间段的预定人数
			bookedBuyerCount : [ 0, 0, 4 ]
		} ]
	}, {
		houseId : '432656757547482374263',
		image : '/images/account/booked-item-picture-default.jpg',
		blockId : '231231',
		blockName : '源成春苑2',
		blockAddress : '闵行区，莘庄，沁春路1680弄',
		publishDay : '2014-02-06',
		houseStatus : {
			index : 0,
			value : '未认证'
		},
		sellStatus : {
			index : 0,
			value : '--'
		}
	} ]

	addBookedList($('.booked-list'), mySellList);

});
function addBookedList($container, dataList) {
	var bookedTimeText = [ '9:00-12:00', '12:00-18:00', '18:00-22:00' ];
	var $template = $('.booked-list .booked-item.template');
	$.each(dataList, function(index, value) {
		var $item = $template.clone();
		$item.removeClass('template');
		$item.find('.picture img').attr('src', value.image);
		$item.find('a.block-name').text(value.blockName);
		$item.find('.block-address a').text(value.blockAddress);
		var dayArray = value.publishDay.split('-');
		$item.find('.booked-day').text(
				dayArray[0] + '年' + dayArray[1] + '月' + dayArray[2] + '日');
		$item.find('.bookedId').text(value.houseId);
		$item.find('.house-status a').text(value.houseStatus.value);
		$item.find('.status.book-status a').text(value.sellStatus.value);
		// 2. 这里的“- -”不用做成链接形式。
		if (value.sellStatus.index == 0) {
			$item.find('.status.book-status').text(value.sellStatus.value);
		}
		if (value.houseStatus.index != 0) {
			$item.find('.booked-day-and-time').text(value.price + '万');
			$item.find('.options button.update').click(function() {
				popupManageMySell();
				// 初始化my-booked-calendar.js中的值
				initCalendarByData(value);
			});

			$item.find('.options button.cancel').click(function() {
				popupMySellDeletedAlert(function() {
					$item.remove();
				});
				return false;
			});
		} else {
			$item.find('.booked-day-and-time').text('--');
			// 未认证时“管理该房源”和“删除该房源”两个按钮为灰色不可编辑状态。
			$item.find('.options button').removeClass('btn-primary');
			// $item.find('.options button').prop('disabled', true);
			$item.find('.options button').addClass(
					'fuck-enabled-but-look-like-disabled');
			$item.find('.options button').click(function() {
				$('.popup.alert#my-sell-manage-disabled-alert').bPopup({
					modalClose : true,
					opacity : 0.6,
					positionStyle : 'absolute', // 'fixed' or 'absolute',
					fadeSpeed : 'slow', // can be a string ('slow'/'fast') or
					// int
					followSpeed : 'fast'
				// can be a string ('slow'/'fast') or int
				});
			});
		}

		if (value.houseStatus.unread) {
			$item.find('.house-status').addClass('unread');
		}

		$item.appendTo($container);
	});
}

function popupManageMySell() {

	// 出现之前的初始状态
	$('#booked-manage-set-price').removeClass('has-error');
	$('.price-error').hide();

	$('.popup#my-sell-manage').bPopup({
		modalClose : false,
		opacity : 0.6,
		positionStyle : 'absolute', // 'fixed' or 'absolute',
		fadeSpeed : 'slow', // can be a string ('slow'/'fast') or
		// int
		followSpeed : 'fast'
	// can be a string ('slow'/'fast') or int
	});

}

function popupMySellDeletedAlert(onYes) {

	$('.popup.confirm#my-sell-delete-confirm').bPopup({
		modalClose : true,
		opacity : 0.6,
		positionStyle : 'absolute', // 'fixed' or 'absolute',
		fadeSpeed : 'slow', // can be a string ('slow'/'fast') or
		// int
		followSpeed : 'fast'
	// can be a string ('slow'/'fast') or int
	});
	$('.popup.confirm#my-sell-delete-confirm button.yes').unbind('click');
	$('.popup.confirm#my-sell-delete-confirm button.no').unbind('click');
	$('.popup.confirm#my-sell-delete-confirm button.yes').click(function() {
		onYes();
		$('.popup.confirm#my-sell-delete-confirm').bPopup().close();

	});
	$('.popup.confirm#my-sell-delete-confirm button.no').click(function() {
		$('.popup.confirm#my-sell-delete-confirm').bPopup().close();
	});

}