$(function() {
	$('.account-nav ul.account-menu li').not('.hilight').hover(function() {
		$(this).addClass('hilight');
	}, function() {
		$(this).removeClass('hilight');
	});
	$('.account-nav ul.account-menu li:last-child').addClass('last')
});

function clearError($input, message) {
	displayError($input, '');
}

function displayError($input, message) {
	var parent = $input.parents('.form-item');
	var $error = parent.find('.error');
	if ($error.length == 0) {
		$error = $('<span></span>');
		$error.addClass('error');
		$error.appendTo(parent);
	}
	$error.text(message);
}