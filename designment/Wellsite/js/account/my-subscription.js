$(function() {

	// 订阅的数据，从服务器获取
	var subscriptions = [ {
		area : '闵行区',
		block : '莘庄',
		nBedRoom : '一居室',
		priceFrom : 300,
		priceTo : 800,
		areaFrom : 72,
		areaTo : 120,
		houseAgeFrom : 1,
		houseAgeTo : 7,
		over5Years : true,
		onlyOneForHouseOwner : false,
	}, {
		area : '闵行区',
		block : '莘庄',
		nBedRoom : '二居室',
		priceFrom : 200,
		priceTo : 600,
		areaFrom : 32,
		areaTo : 120,
		houseAgeFrom : 1,
		houseAgeTo : 8,
		over5Years : false,
		onlyOneForHouseOwner : true,
	} ];
	$.each(subscriptions, addSubscriptionItemToUl);

	$('button#new-subscription').click(popupAddSubscription);

	function slideAction(event) {
		var $a = $(this);
		var $subscriptionItem = $a.parents('li.subscription-item');
		var $form = $subscriptionItem.find('.form');
		if ($form.is(':visible')) {
			$form.hide('blind', 300);
			$a.text('编辑');

		} else {
			// 在任何时刻，已有订阅都只能有一个处于展开状态，即点击其他订阅条，当前展开的订阅框自动收起。
			$('.brief a.action').filter(function() {
				return (/收起/).test($(this).text())
			}).click();

			$form.show('blind', 300);
			$a.text('收起');
		}

		return false;
	}

	function slideUpAction(event) {
		var $button = $(this);
		var $subscriptionItem = $button.parents('li.subscription-item');
		var $form = $subscriptionItem.find('.form');
		$form.hide('blind', 300);
		$subscriptionItem.find('.brief a.action').text('编辑')
		return false;
	}
	function deleteAction($a) {
		var $subscriptionItem = $a.parents('li.subscription-item');
		$subscriptionItem.remove();

		// 订阅编号自适应说明。已有订阅的编号从小到大排列，例如当前已有编号为1,2,3，共3条，此时，新增一条订阅后，该新增订阅编号为4，排在最后面。此时用户若删除了订阅2，则后面的订阅编号自动上移，即原来的3号变成2号，4号变成3号。
		$('ul#subscriptions li.subscription-item').each(function(index, value) {
			$(value).find('.brief .name').text('我的订阅' + index);
		});

		return false;
	}
	function optionSelectedAction(event) {
		var $a = $(this);
		var $formItem = $a.parents('.form-item');
		var $input = $formItem.find('input');
		$input.val($a.text());
		$a.parents('ul.dropdown-menu').dropdown('toggle');

		// 四个范畴
		// 目前这四个选项框分别表示了4个范畴的地理信息，
		// 地铁线路是“线”的范畴，用L表示；
		// 站点是“点”的范畴，用d表示；
		// 行政区是“大面”的范畴，用F表示；
		// 街区是“小面”的范畴。用f表示。

		// 大概念和小概念
		// 我们将“线”和“大面”称为大的概念，“点”和“小面”称为小的概念。大的概念和小的概念呈隶属关系，即“点”是隶属于“线”的，“小面”是隶属于“大面”的。

		// 确定地理信息的方式
		// 这四个选框要实现的就是通过“面”、“线”、“点”的结合，来实现对所需地理位置的描述。
		// 所要确定的地理信息通过以下几个方式来实现：
		// 方式1. 单“点”确定；
		// 方式2. 单“线”确定；
		// 方式3. 单“面”确定（可能是大面，也可能是小面）；
		// 方式4. “线”与“面”的结合确定（可能是大面，也可能是小面）。

		// 具体情况说明
		// 情况1：L - d——通过方式1确定地理信息
		// 1. 给L赋值，d变亮，且显示空值。给d赋值，F变灰。√
		// 取消的情况们：
		// L选所有，d变灰，且为空值。×（现状是变灰，且为原值）
		// L切换另一个值，d变亮，且为空值。√
		// d选所有，F变亮。√
		//
		// 情况2：L – F – f——通过方式4确定地理信息
		// 1. 用户给L赋值， d变亮。给F赋值，f变亮。给f赋值，d变灰。√
		// 取消的情况们：
		// F选所有，f变灰，且为空值，d变亮，且为空值。
		// F切换另一个值，f变亮，且为空值，d变亮，且为空值。
		// f选所有，d变亮，且为空值。
		// L选所有，无变化。√
		// L切换另一个值，无变化。√
		//
		// 情况3：L – F – d——通过方式1确定地理信息
		// 1. L赋值，d点亮。F赋值，f点亮。d赋值，F、f变灰，F保留原输入值。√
		// 取消的情况们：
		// L选所有，d变灰，且为空值，F变亮，保持原值，f变亮，且为空值。
		// L切换另一个值，d变亮，且为空值，F变亮，保持原值，f变亮，且为空值。
		// d选所有，F变亮，保持原值，f变亮，且为空值。
		//
		// 情况4：F – L – d——通过方式1确定地理信息
		// 1. 给F赋值，f点亮。给L赋值，d点亮。给d赋值，F、f变灰，F保留原输入值。√
		// 取消的情况们：
		// 同情况3。
		//
		// 情况5：F – f – L——通过方式4确定地理信息
		// 1. 给F赋值，f点亮。给f赋值。给L赋值，d保持灰色。√
		// 取消的情况们：
		// 同情况2。
		//
		// 情况6：F – L – f——通过方式4确定地理信息
		// 1. 给F赋值，f点亮。给L赋值，d点亮。给f赋值，d变灰。√
		// 取消的情况们：
		// 同情况2。
		// 当在输入框的下拉菜单中选择“所有XX”（每一个下拉框都是先有所有XX，然后再是详细选项），该输入框回显成空白。
		if ($a.parent('li').index() == 0) {
			$input.val('');
		}

		var $subway = $a.parents('.form').find('#subway-dropdown');
		var $stop = $a.parents('.form').find('#stop-dropdown');
		var $area = $a.parents('.form').find('#area-dropdown');
		var $block = $a.parents('.form').find('#block-dropdown');
		// 情况1：L - d——通过方式1确定地理信息
		if ($a.parents('ul.dropdown-menu').attr('id') == 'subway-dropdown-menu') {
			if ($a.parent('li').index() == 0) {
				// 取消时：L选所有，d变灰，且为空值。(PS：F亮且空值的情况下，f应该是灰色的)
				// L选所有，d变灰，且为空值，F变亮，保持原值，f变亮，且为空值。
				changeEnableState($stop, false);
				$stop.find('input').val('');
				changeEnableState($area, true);
				if ($area.find('input').val() != '') {
					changeEnableState($block, true);
				} else {
					// 情况2：L选所有，无变化。
					$block.find('input').val('');
				}
			} else {
				// 另一种变更方式是L直接变成为L2，d呈空白值，F重新变亮。也就是说，大的概念选择一个值或变换另一个值后（大概念可不理会同序列小概念值的限制，只受同级别概念值和不同序列小概念值的限制），小的概念回显空白值（无论之前是否选择了某一个值）。
				$stop.find('input').val('');
				changeEnableState($area, true);

				if (!$block.prop('changed')) {
					// 1. 用户给L赋值，如L1，d变亮
					changeEnableState($stop, true);

					// TODO 里面还是外面
					// 情况5：1. 给F赋值，f点亮。给f赋值。给L赋值，d保持灰色。
				}
				// 情况3：L – F –
				// d——通过方式1确定地理信息。取消时：L切换另一个值，d变亮，且为空值，F变亮，保持原值，f变亮，且为空值。
				// changeEnableState($stop, true);
				if ($area.find('input').val() != '') {
					changeEnableState($block, true);
					// $block.find('input').val('');
				}

			}
		}
		if ($a.parents('ul.dropdown-menu').attr('id') == 'stop-dropdown-menu') {
			if ($a.parent('li').index() != 0) {
				// 2. 给d赋值，如d3，F变灰。
				changeEnableState($area, false);
				changeEnableState($block, false);
			} else {
				// 3. d选所有，F变亮，保持原值，f变亮，且为空值。×（f没有变亮）
				changeEnableState($area, true)
				if ($area.find('input').val() != '') {
					changeEnableState($block, true)
				}
			}
		}

		// 情况2：L – F – f——通过方式4确定地理信息
		if ($a.parents('ul.dropdown-menu').attr('id') == 'area-dropdown-menu') {
			$area.prop('area-changed', true);
			if ($a.parent('li').index() != 0) {
				// F切换另一个值，f变亮，且为空值，d变亮，且为空值。
				changeEnableState($block, true);
				if ($subway.find('input').val() != '') {
					$block.find('input').val('');
					changeEnableState($stop, true);
				}
			} else {
				// F选所有，f变灰，且为空值，d变亮，且为空值。
				$block.find('input').val('');
				changeEnableState($block, false);
				if ($subway.find('input').val() != '') {
					changeEnableState($stop, true);
					$stop.find('input').val('');
				}
			}
		}
		if ($a.parents('ul.dropdown-menu').attr('id') == 'block-dropdown-menu') {
			// 标记f选择过了（先选f再选L后d不变亮）
			$block.prop('changed', true);
			if ($a.parent('li').index() != 0) {
				changeEnableState($stop, false);
				// PS：此时d肯定是空白值状态，如果不是空白值，下面的F和f应该是灰色。
				$stop.find('input').val('');
			} else {
				// f选所有，d变亮，且为空值。）
				changeEnableState($stop, true);
				$stop.find('input').val('');
			}
		}

		return false;
	}

	function addSubscriptionItemToUl(index, value) {
		addSubscriptionItem(index, value,
				$('ul.subscription-list#subscriptions'))
	}
	function addSubscriptionItem(index, value, $parent) {
		var $template = $('ul.subscription-list#subscriptions li.subscription-item.template');
		var $item = $template.clone();
		$item.removeClass('template');
		$item.find('.brief').click(function() {
			$(this).find('.action').click();
		});
		$item.find('.brief .name').text('我的订阅' + (index + 1));
		$item.find('.brief .values').html(briefHtmlString(value));
		$item.find('input#area').val(value.area);
		$item.find('input#block').val(value.block);
		$item.find('input#n-bedroom').val(value.nBedRoom);

		$item.find('.slider-range-price').slider({
			range : true,
			min : 0,
			max : 1000,
			values : [ value.priceFrom, value.priceTo ],
			slide : function(event, ui) {
				$item.find('.price-from').text(ui.values[0]);
				$item.find('.price-to').text(ui.values[1]);
			},
			stop : function(event, ui) {

			}
		});
		$item.find('.price-from').text(value.priceFrom);
		$item.find('.price-to').text(value.priceTo);

		$item.find('.slider-range-area').slider({
			range : true,
			min : 0,
			max : 500,
			values : [ value.areaFrom, value.areaTo ],
			slide : function(event, ui) {
				$item.find('.area-from').text(ui.values[0]);
				$item.find('.area-to').text(ui.values[1]);
			},
			stop : function(event, ui) {
			}
		});
		$item.find('.area-from').text(value.areaFrom);
		$item.find('.area-to').text(value.areaTo);

		$item.find('.slider-range-house-age').slider({
			range : true,
			min : 0,
			max : 30,
			values : [ value.houseAgeFrom, value.houseAgeTo ],
			slide : function(event, ui) {
				$item.find('.house-age-from').text(ui.values[0]);
				$item.find('.house-age-to').text(ui.values[1]);
			},
			stop : function(event, ui) {
			}
		});
		$item.find('.house-age-from').text(value.houseAgeFrom);
		$item.find('.house-age-to').text(value.houseAgeTo);

		$item.find('.brief .action').click(slideAction);
		$item.find('.brief .delete').click(function(e) {
			var $a = $(this);
			popupConfirm('您确定要删除该订阅？', function() {
				deleteAction($a);
			});
			return false;
		});
		$item.find('.form .actions button.close-it').click(slideUpAction);
		$item.find('.form .actions button.save-it').click(saveEditSubscription);

		$item.find('ul.dropdown-menu li a').click(optionSelectedAction);
		$item.find('input#over-5-years').prop('checked', value.over5Years);
		$item.find('input#over-5-years').prop('id', 'over-5-years-' + index);
		$item.find("label[for='over-5-years']").prop('for',
				'over-5-years-' + index);
		$item.find('input#only-one-for-house-owner').prop('checked',
				value.onlyOneForHouseOwner);
		$item.find('input#only-one-for-house-owner').prop('id',
				'only-one-for-house-owner-' + index);
		$item.find("label[for='only-one-for-house-owner']").prop('for',
				'only-one-for-house-owner-' + index);

		$item.appendTo($parent);

		// 下图四个输入框的初始状态，2和4为不可选灰色状态。
		changeEnableState($item.find('#stop-dropdown'), false);
		changeEnableState($item.find('#block-dropdown'), false);

	}

	// 上面的灰色条会更新显示订阅的关键字，包括：线路/站点、行政区/街区、居室、装修、价格区间、面积区间、房龄区间、满5年、唯一。
	function briefHtmlString(value) {

		var html = '';

		if (value.stop && value.stop != '') {
			html = html + value.stop + '，';
		} else if (value.subway && value.subway != '') {
			html = html + value.subway + '，';
		}
		if (value.block && value.block != '') {
			html = html + value.block + '，';
		} else if (value.area && value.area != '') {
			html = html + value.area + '，';
		}
		if (value.nBedRoom && value.nBedRoom != '') {
			html = html + value.nBedRoom + '，';
		}
		if (value.fitting && value.fitting != '') {
			html = html + value.fitting + '，';
		}

		html = html + value.priceFrom + '－' + value.priceTo + '万，'
				+ value.areaFrom + '～' + value.areaTo + 'm<sup>2</sup>，'
				+ value.houseAgeFrom + '－' + value.houseAgeTo + '年' + '，';
		if (!value.over5Years) {
		} else {
			html = html + '满5年，';
		}
		if (!value.onlyOneForHouseOwner) {
		} else {
			html = html + '唯一';
		}

		if (html.charAt(html.length - 1) == '，') {
			html = html.substring(0, html.length - 1);
		}
		return html;
	}

	function closeAddSubscriptionPopup() {
		$('.popup.add-subscription').bPopup().close();
	}

	function saveEditSubscription() {
		var $li = $(this).parents('.subscription-item');
		var $form = $li.find('.form');
		var $lable = $form.find('label.only-one-for-house-owner-label');
		var onlyOneForHouseOwnerId = $lable.attr('for');
		var $onlyOneForHouseOwner = $form.find('input#'
				+ onlyOneForHouseOwnerId);
		var $lableOver5Years = $form.find('label.over-5-years-label');
		var over5YearsId = $lableOver5Years.attr('for');
		var $over5YearsId = $form.find('input#' + over5YearsId);
		var value = {
			subway : $form.find('input#subway').val(),
			stop : $form.find('input#stop').val(),
			area : $form.find('input#area').val(),
			block : $form.find('input#block').val(),
			nBedRoom : $form.find('input#n-bedroom').val(),
			priceFrom : $form.find('.price-from').text(),
			priceTo : $form.find('.price-to').text(),
			areaFrom : $form.find('.area-from').text(),
			areaTo : $form.find('.area-to').text(),
			houseAgeFrom : $form.find('.house-age-from').text(),
			houseAgeTo : $form.find('.house-age-to').text(),
			fitting : $form.find('input#fitting').val(),
			over5Years : $over5YearsId.is(':checked'),
			onlyOneForHouseOwner : $onlyOneForHouseOwner.is(':checked')
		}
		$li.find('.brief .values').html(briefHtmlString(value));
		popupAlert('保存成功');
	}
	function saveAddSubscriptionPopup() {
		var $form = $(this).parents('.form');
		var $lable = $form.find('label.only-one-for-house-owner-label');
		var onlyOneForHouseOwnerId = $lable.attr('for');
		var $onlyOneForHouseOwner = $form.find('input#'
				+ onlyOneForHouseOwnerId);
		var $lableOver5Years = $form.find('label.over-5-years-label');
		var over5YearsId = $lableOver5Years.attr('for');
		var $over5YearsId = $form.find('input#' + over5YearsId);
		var value = {
			area : $form.find('input#area').val(),
			block : $form.find('input#block').val(),
			nBedRoom : $form.find('input#n-bedroom').val(),
			priceFrom : $form.find('.price-from').text(),
			priceTo : $form.find('.price-to').text(),
			areaFrom : $form.find('.area-from').text(),
			areaTo : $form.find('.area-to').text(),
			houseAgeFrom : $form.find('.house-age-from').text(),
			houseAgeTo : $form.find('.house-age-to').text(),
			fitting : $form.find('input#fitting').val(),
			over5Years : $over5YearsId.is(':checked'),
			onlyOneForHouseOwner : $onlyOneForHouseOwner.is(':checked')
		}
		var $parent = $('ul.subscription-list#subscriptions');

		addSubscriptionItem($parent.children().length - 1, value, $parent);

		closeAddSubscriptionPopup();

	}
	function popupAddSubscription() {
		$('.popup.add-subscription .form').remove();
		addSubscriptionItem('popup', {
			area : '',
			block : '',
			nBedRoom : '',
			priceFrom : 0,
			priceTo : 1000,
			areaFrom : 0,
			areaTo : 500,
			houseAgeFrom : 0,
			houseAgeTo : 30,
			over5Years : false,
			onlyOneForHouseOwner : false,
		}, $('.popup.add-subscription'))
		$('.popup.add-subscription .form').show();
		$('.popup.add-subscription .form').appendTo('.popup.add-subscription');
		$('.popup.add-subscription li.subscription-item').remove();

		$('.popup.add-subscription').bPopup(
				{
					onOpen : function() {
						var $item = $('.popup.add-subscription')
						$item.find('.slider-range-price').slider({
							range : true,
							min : 0,
							max : 1000,
							slide : function(event, ui) {
								$item.find('.price-from').text(ui.values[0]);
								$item.find('.price-to').text(ui.values[1]);
							},
							stop : function(event, ui) {

							}
						});

						$item.find('.slider-range-area').slider({
							range : true,
							min : 0,
							max : 500,
							slide : function(event, ui) {
								$item.find('.area-from').text(ui.values[0]);
								$item.find('.area-to').text(ui.values[1]);
							},
							stop : function(event, ui) {
							}
						});

						$item.find('.slider-range-house-age').slider(
								{
									range : true,
									min : 0,
									max : 30,
									slide : function(event, ui) {
										$item.find('.house-age-from').text(
												ui.values[0]);
										$item.find('.house-age-to').text(
												ui.values[1]);
									},
									stop : function(event, ui) {
									}
								});

						$item.find('.form .actions button.close-it').unbind(
								'click');
						$item.find('.form .actions button.close-it').click(
								closeAddSubscriptionPopup);
						$item.find('.form .actions button.save-it').unbind(
								'click');
						$item.find('.form .actions button.save-it').click(
								saveAddSubscriptionPopup);
					},
					modalClose : false,
					opacity : 0.6,
					positionStyle : 'absolute', // 'fixed' or 'absolute',
					fadeSpeed : 'slow', // can be a string ('slow'/'fast') or
					// int
					followSpeed : 'fast'
				// can be a string ('slow'/'fast') or int
				});

	}

	function popupConfirm(text, onYes) {

		var $confirm = $('.popup.confirm');
		$confirm.bPopup({
			modalClose : true,
			opacity : 0.6,
			positionStyle : 'absolute', // 'fixed' or 'absolute',
			fadeSpeed : 'slow', // can be a string ('slow'/'fast') or
			// int
			followSpeed : 'fast'
		// can be a string ('slow'/'fast') or int
		});
		$confirm.find('.text').text(text);
		$('button.yes').unbind('click');
		$('button.no').unbind('click');
		$('button.yes').click(function() {
			onYes();
			$confirm.bPopup().close();

		});
		$('button.no').click(function() {
			$confirm.bPopup().close();
		});
	}
	function popupAlert(text) {
		var $alert = $('.popup.alert');
		$alert.text(text);
		$alert.bPopup({
			modalClose : true,
			opacity : 0.6,
			positionStyle : 'absolute', // 'fixed' or 'absolute',
			fadeSpeed : 'slow', // can be a string ('slow'/'fast') or
			// int
			followSpeed : 'fast'
		// can be a string ('slow'/'fast') or int
		});
	}

	function changeEnableState($dropdown, enable) {
		if (enable) {
			$dropdown.attr('data-toggle', 'dropdown');
		} else {
			$dropdown.removeAttr('data-toggle');
		}
		$dropdown.find('input').prop('disabled', !enable);
	}
});
