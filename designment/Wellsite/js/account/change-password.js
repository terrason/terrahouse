$(function() {
	$('#account-form-container #password').blur(validatePassword);
	$('#account-form-container #password-new').blur(validateNewPassword);
	$('#account-form-container #password-confirm').blur(validateConfirmPassword);

	$('#account-form-container button#change-password').click(validate);

	function validate(e) {
		e.preventDefault();
		var success = true;
		if (!validatePassword()) {
			success = false;
		}

		if (!validateNewPassword()) {
			success = false;
		}

		if (!validateConfirmPassword()) {
			success = false;
		}

		if(success){
			$('.popup.popup-tip').bPopup({
				modalClose : true,
				opacity : 0.6,
				positionStyle : 'absolute', // 'fixed' or 'absolute',
				fadeSpeed : 'slow', // can be a string ('slow'/'fast') or
				// int
				followSpeed : 'fast'
					// can be a string ('slow'/'fast') or int
			});
		}
		return success;
	}
	
	function validatePassword() {
		var success = true;
		var $password = $('#account-form-container #password');
		if ($password.val() == '') {
			displayError($password, '请输入您的密码')
			success = false;
		} else if ($password.val().length < 6) {
			displayError($password, '密码位数不得小于六位')
			success = false;
		} else {
			clearError($password)
		}
		return success;
	}
	function validateNewPassword() {
		var success = true;
		var $password = $('#account-form-container #password-new');
		if ($password.val() == '') {
			displayError($password, '请输入您的新密码')
			success = false;
		} else if ($password.val().length < 6) {
			displayError($password, '密码位数不得小于六位')
			success = false;
		} else {
			clearError($password)
		}
		return success;
	}
	function validateConfirmPassword() {
		var success = true;
		var $passwordNew = $('#account-form-container #password-new');
		var $passwordConfirm = $('#account-form-container #password-confirm');
		if ($passwordConfirm.val() == '') {
			displayError($passwordConfirm, '请确认您的新密码')
			success = false;
		} else if ($passwordConfirm.val().length < 6) {
			displayError($passwordConfirm, '密码位数不得小于六位')
			success = false;
		} else if ($passwordConfirm.val() != $passwordNew.val()) {
			displayError($passwordConfirm, '两次输入的密码不一致')
			success = false;
		} else {
			clearError($passwordConfirm)
		}
		return success;
	}

});