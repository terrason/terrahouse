$(function() {
	$('.tabs').tabs({
		activate : onTabActivate
	});

	$('#order-by-dropdown-menu a').click(function() {
		$('#order-by').val($(this).text());
		$('#order-by-dropdown-menu.dropdown-menu').dropdown('toggle')
		return false;
	});

	// 我的有效预约，应该从服务器获取
	var myAvailableBookedList = [ {
		bookId : '432656757547534275',
		image : '/images/account/booked-item-picture-default.jpg',
		blockId : '231231',
		blockName : '源成春苑',
		blockAddress : '闵行区，莘庄，沁春路168弄',
		bookedDay : {
			day : '2014-02-25',
			index : 0
		},
		houseStatus : {
			index : 1,
			value : '在售'
		},
		bookStatus : {
			index : 1,
			value : '可管理'
		},
		// 可预约的日期数据
		availableDays : [ {
			day : "2014-02-21",
			index : [ 0, 2 ]
		}, {
			day : "2014-02-22",
			index : [ 1, 2 ]
		}, {
			day : "2014-02-25",
			index : [ 0, 1 ]
		}, {
			day : "2014-02-27",
			index : [ 0, 1, 2 ]
		}, {
			day : "2014-02-02",
			index : [ 0, 2 ]
		}, {
			day : "2014-02-06",
			index : [ 0, 1, 2 ]
		} ]
	}, {
		bookId : '432656757547482374263',
		image : '/images/account/booked-item-picture-default.jpg',
		blockId : '231231',
		blockName : '源成春苑2',
		blockAddress : '闵行区，莘庄，沁春路1680弄',
		bookedDay : {
			day : '2014-02-06',
			index : 0
		},
		houseStatus : {
			index : 1,
			value : '在售'
		},
		bookStatus : {
			index : 2,
			value : '已生效'
		},
		// 可预约的日期数据
		availableDays : [ {
			day : "2014-02-21",
			index : [ 0, 2 ]
		}, {
			day : "2014-02-22",
			index : [ 1, 2 ]
		}, {
			day : "2014-02-25",
			index : [ 0, 1 ]
		}, {
			day : "2014-02-27",
			index : [ 0, 1, 2 ]
		}, {
			day : "2014-02-02",
			index : [ 0, 2 ]
		}, {
			day : "2014-02-06",
			index : [ 0, 1, 2 ]
		} ]
	} ]

	$('.tabs ul li a[href="#tab-available"] span.count').text(
			myAvailableBookedList.length)
	addBookedList($('#tab-available .booked-list'), myAvailableBookedList);

	// 我的已失效预约，应该从服务器获取
	var myUnavailableBookedList = [ {
		bookId : '432656757547534275',
		image : '/images/account/booked-item-picture-default.jpg',
		blockId : '231231',
		blockName : '源成春苑3',
		blockAddress : '闵行区，莘庄，沁春路168弄',
		bookedDay : {
			day : '2014-02-26',
			index : 0
		},
		houseStatus : {
			index : 2,
			value : '已删除'
		},
		bookStatus : {
			index : 5,
			value : '已失效',
			unavailableAndUnread : true
		},
		// 可预约的日期数据
		availableDays : [ {
			day : "2014-02-21",
			index : [ 0, 2 ]
		}, {
			day : "2014-02-22",
			index : [ 1, 2 ]
		}, {
			day : "2014-02-25",
			index : [ 0, 1 ]
		}, {
			day : "2014-02-27",
			index : [ 0, 1, 2 ]
		}, {
			day : "2014-02-02",
			index : [ 0, 2 ]
		}, {
			day : "2014-02-06",
			index : [ 0, 1, 2 ]
		} ]
	}, {
		bookId : '432656757547482374263',
		image : '/images/account/booked-item-picture-default.jpg',
		blockId : '231231',
		blockName : '源成春苑4',
		blockAddress : '闵行区，莘庄，沁春路1680弄',
		bookedDay : {
			day : '2014-02-25',
			index : 0
		},
		houseStatus : {
			index : 2,
			value : '已售'
		},
		bookStatus : {
			index : 5,
			value : '已失效',
			unavailableAndUnread : true
		},
		// 可预约的日期数据
		availableDays : [ {
			day : "2014-02-21",
			index : [ 0, 2 ]
		}, {
			day : "2014-02-22",
			index : [ 1, 2 ]
		}, {
			day : "2014-02-25",
			index : [ 0, 1 ]
		}, {
			day : "2014-02-27",
			index : [ 0, 1, 2 ]
		}, {
			day : "2014-02-02",
			index : [ 0, 2 ]
		}, {
			day : "2014-02-06",
			index : [ 0, 1, 2 ]
		} ]
	}, {
		bookId : '432656757547482374263',
		image : '/images/account/booked-item-picture-default.jpg',
		blockId : '231231',
		blockName : '源成春苑5',
		blockAddress : '闵行区，莘庄，沁春路1680弄',
		bookedDay : {
			day : '2014-02-25',
			index : 0
		},
		houseStatus : {
			index : 3,
			value : '已删除',
			unavailableAndUnread : true
		},
		bookStatus : {
			index : 3,
			value : '违约'
		},
		// 可预约的日期数据
		availableDays : [ {
			day : "2014-02-21",
			index : [ 0, 2 ]
		}, {
			day : "2014-02-22",
			index : [ 1, 2 ]
		}, {
			day : "2014-02-25",
			index : [ 0, 1 ]
		}, {
			day : "2014-02-27",
			index : [ 0, 1, 2 ]
		}, {
			day : "2014-02-02",
			index : [ 0, 2 ]
		}, {
			day : "2014-02-06",
			index : [ 0, 1, 2 ]
		} ]
	}, {
		bookId : '432656757547482374263',
		image : '/images/account/booked-item-picture-default.jpg',
		blockId : '231231',
		blockName : '源成春苑5',
		blockAddress : '闵行区，莘庄，沁春路1680弄',
		bookedDay : {
			day : '2014-02-25',
			index : 0
		},
		houseStatus : {
			index : 1,
			value : '在售'
		},
		bookStatus : {
			index : 4,
			value : '已看过',
			unavailableAndUnread : true
		},
		// 可预约的日期数据
		availableDays : [ {
			day : "2014-02-21",
			index : [ 0, 2 ]
		}, {
			day : "2014-02-22",
			index : [ 1, 2 ]
		}, {
			day : "2014-02-25",
			index : [ 0, 1 ]
		}, {
			day : "2014-02-27",
			index : [ 0, 1, 2 ]
		}, {
			day : "2014-02-02",
			index : [ 0, 2 ]
		}, {
			day : "2014-02-06",
			index : [ 0, 1, 2 ]
		} ]
	}, {
		bookId : '432656757547482374263',
		image : '/images/account/booked-item-picture-default.jpg',
		blockId : '231231',
		blockName : '源成春苑6',
		blockAddress : '闵行区，莘庄，沁春路1680弄',
		bookedDay : {
			day : '2014-02-25',
			index : 0
		},
		houseStatus : {
			index : 1,
			value : '在售'
		},
		bookStatus : {
			index : 6,
			value : '作废'
		},
		// 可预约的日期数据
		availableDays : [ {
			day : "2014-02-21",
			index : [ 0, 2 ]
		}, {
			day : "2014-02-22",
			index : [ 1, 2 ]
		}, {
			day : "2014-02-25",
			index : [ 0, 1 ]
		}, {
			day : "2014-02-27",
			index : [ 0, 1, 2 ]
		}, {
			day : "2014-02-02",
			index : [ 0, 2 ]
		}, {
			day : "2014-02-06",
			index : [ 0, 1, 2 ]
		} ]
	} ]
	$('.tabs ul li a[href="#tab-unavailable"] span.count').text(
			myUnavailableBookedList.length);

	// 预约失效的原因，一是由于预约状态的变更，即从“可管理”和“已生效”，变更成“已看过”、“违约”、“已失效”、“作废”；二是由于房屋状态的变更，从“在售”，变更成“已售”、“已删除”。
	// 转到已失效但是还是未读状态的个数，即Tab头上的气泡红点数字
	var unreadDisabledCount = 0;
	$.each(myUnavailableBookedList, function(index, value) {
		if (value.houseStatus.unavailableAndUnread
				|| value.bookStatus.unavailableAndUnread) {
			unreadDisabledCount = unreadDisabledCount + 1;
		}
	});
	if (unreadDisabledCount > 0) {
		$('.ui-tabs .ui-tabs-nav li span.bubble-number').text(
				unreadDisabledCount);
	} else {
		$('.ui-tabs .ui-tabs-nav li span.bubble-number').hide();
	}
	addBookedList($('#tab-unavailable .booked-list'), myUnavailableBookedList);

	$('#tab-unavailable input[type="checkbox"]#onSale').change(function() {
		if (this.checked) {
			console.log('从服务器加载在售的房源并显示');
		} else {
			console.log('从服务器加载已售的房源并显示');
		}
	})
});
function addBookedList($container, dataList) {
	var bookedTimeText = [ '9:00-12:00', '12:00-18:00', '18:00-22:00' ];
	var $template = $('#tab-available .booked-list .booked-item.template');
	$.each(dataList, function(index, value) {
		var $item = $template.clone();
		$item.removeClass('template');
		$item.find('.picture img').attr('src', value.image);
		$item.find('a.block-name').text(value.blockName);
		$item.find('.block-address a').text(value.blockAddress);
		var dayArray = value.bookedDay.day.split('-');
		$item.find('.booked-day').text(
				dayArray[0] + '年' + dayArray[1] + '月' + dayArray[2] + '日');
		$item.find('.house-status a').text(value.houseStatus.value);
		$item.find('.bookedId').text(value.bookId);
		$item.find('.book-status a').text(value.bookStatus.value);
		$item.find('.booked-day-and-time').text(
				value.bookedDay.day + ' '
						+ bookedTimeText[value.bookedDay.index]);

		// 当预约状态为“可管理”时，“更改预约时间”和“取消预约”按钮高亮显示，表示可以点击。点击“更改预约时间”，弹出预约弹窗。
		if (value.bookStatus.index == 1) {
			$item.find('.options button.update').click(function() {
				popupEdit();
				// 初始化my-booked-calendar.js中的值
				initCalendarByData(value);
			});
			$item.find('.options button.cancel').click(function() {
				popupCancelConfirm(function() {
					$item.remove();
				});
				return false;
			});

		}
		// 已生效状态。
		else if (value.bookStatus.index == 2) {
			// 当预约状态为已生效时，当预约状态为已生效时，“更改预约时间”和“取消预约”按钮都变成灰色。
			$item.find('.options button').removeClass('btn-primary');
			$item.find('.options button').addClass(
					'fuck-enabled-but-look-like-disabled');
			// 当预约状态为已生效时，用户点击“更改预约时间”或“取消预约”，弹出弹窗提示，点击他处提示弹窗消失。
			$item.find('.options button').click(popupCannotEditAlert);
		}
		// 当预约状态为其他状态（已看过、违约、已失效、作废）时，“更改预约时间”和“取消预约”两个按钮都不可点击。
		else {
			if (value.bookStatus.unavailableAndUnread) {
				$item.find('.book-status').addClass('unread')
			}
			$item.find('.options button').removeClass('btn-primary');
			$item.find('.options button').prop('disabled', true);
		}

		// 删除是指房东在自己的管理页面里将该房源删除的状态，该状态下，用户点击房屋名称，弹出弹窗，提示“很抱歉，该房源已被房东删除！”
		if (value.houseStatus.index == 3) {
			if (value.houseStatus.unavailableAndUnread) {
				$item.find('.house-status').addClass('unread')
			}
			$item.find('.house-status').addClass('unread')
			$item.find('a.block-name').click(function() {
				popupHouseDeletedAlert();
				return false;
			});
		}

		$item.appendTo($container);
	});
}

function popupEdit() {

	$('.popup#booked-edit').bPopup({
		modalClose : false,
		opacity : 0.6,
		positionStyle : 'absolute', // 'fixed' or 'absolute',
		fadeSpeed : 'slow', // can be a string ('slow'/'fast') or
		// int
		followSpeed : 'fast'
	// can be a string ('slow'/'fast') or int
	});
}
function popupCannotEditAlert() {

	$('.popup.alert#booked-cannot-edit-alert').bPopup({
		modalClose : true,
		opacity : 0.6,
		positionStyle : 'absolute', // 'fixed' or 'absolute',
		fadeSpeed : 'slow', // can be a string ('slow'/'fast') or
		// int
		followSpeed : 'fast'
	// can be a string ('slow'/'fast') or int
	});
}
function popupHouseDeletedAlert() {

	$('.popup.alert#booked-house-deleted-alert').bPopup({
		modalClose : true,
		opacity : 0.6,
		positionStyle : 'absolute', // 'fixed' or 'absolute',
		fadeSpeed : 'slow', // can be a string ('slow'/'fast') or
		// int
		followSpeed : 'fast'
	// can be a string ('slow'/'fast') or int
	});
}
function popupCancelConfirm(onYes) {

	$('.popup.confirm#booked-cancel-confirm').bPopup({
		modalClose : true,
		opacity : 0.6,
		positionStyle : 'absolute', // 'fixed' or 'absolute',
		fadeSpeed : 'slow', // can be a string ('slow'/'fast') or
		// int
		followSpeed : 'fast'
	// can be a string ('slow'/'fast') or int
	});
	$('.popup.confirm#booked-cancel-confirm button.yes').unbind('click');
	$('.popup.confirm#booked-cancel-confirm button.no').unbind('click');
	$('.popup.confirm#booked-cancel-confirm button.yes').click(function() {
		onYes();
		$('.popup.confirm#booked-cancel-confirm').bPopup().close();

	});
	$('.popup.confirm#booked-cancel-confirm button.no').click(function() {
		$('.popup.confirm#booked-cancel-confirm').bPopup().close();
	});

}
function onTabActivate(event, ui) {
	if (ui.newPanel.selector == '#tab-unavailable') {
		// 用户点击失效预约标签页后，小红点消失。
		$('ul.account-menu li.my-booked span.bubble-number').remove();
		$('.ui-tabs .ui-tabs-nav li span.bubble-number').remove();
	}
}