$(function() {
	$('.tabs').tabs();
	$('.rating-select').barrating('show', {
		showSelectedRating : false,
		readonly : true
	});

	$('.ratingbar-value').each(function(index, value) {
		var $this = $(value);
		var $prev = $this.prev();
		var $select = $prev.find('select');
		$this.text($select.val());
	});

	$('.fresh-table .item .order-id a').click(
			function() {
				var bookedTimeText = [ '9:00-12:00', '12:00-18:00',
						'18:00-22:00' ];
				var bookedDetails = {
					bookId : '432656757547534275',
					image : '/images/account/booked-item-picture-default.jpg',
					blockId : '231231',
					blockName : '源成春苑3',
					blockAddress : '闵行区，莘庄，沁春路168弄',
					bookedDay : {
						day : '2014-02-06',
						index : 0
					},
					houseStatus : {
						index : 2,
						value : '在售'
					},
					bookStatus : {
						index : 5,
						value : '违约'
					},
					// 可预约的日期数据
					availableDays : [ {
						day : "2014-02-21",
						index : [ 0, 2 ]
					}, {
						day : "2014-02-22",
						index : [ 1, 2 ]
					}, {
						day : "2014-02-25",
						index : [ 0, 1 ]
					}, {
						day : "2014-02-27",
						index : [ 0, 1, 2 ]
					}, {
						day : "2014-02-02",
						index : [ 0, 2 ]
					}, {
						day : "2014-02-06",
						index : [ 0, 1, 2 ]
					} ]
				}

				var $item = $('.popup.booked-item');
				var value = bookedDetails;
				$item.find('.picture img').attr('src', value.image);
				$item.find('a.block-name').text(value.blockName);
				$item.find('.block-address a').text(value.blockAddress);
				var dayArray = value.bookedDay.day.split('-');
				$item.find('.booked-day').text(
						dayArray[0] + '年' + dayArray[1] + '月' + dayArray[2]
								+ '日');
				$item.find('.house-status a').text(value.houseStatus.value);
				$item.find('.bookedId').text(value.bookId);
				$item.find('.book-status a').text(value.bookStatus.value);
				$item.find('.booked-day-and-time').text(
						value.bookedDay.day + ' '
								+ bookedTimeText[value.bookedDay.index]);

				$('.popup.booked-item').bPopup({
					modalClose : true,
					opacity : 0.6,
					positionStyle : 'absolute', // 'fixed' or 'absolute',
					fadeSpeed : 'slow', // can be a string ('slow'/'fast') or
					// int
					followSpeed : 'fast'
				// can be a string ('slow'/'fast') or int
				});
				return false;
			});
});