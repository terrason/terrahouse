$(function() {
	$('.share-container .action a').click(inviteMore);
	$('.input-container-all input').blur(validateInviteEmailOnBlur);
	$('.share-container .action button').click(
			function() {
				var validated = true;
				var allBlank = true;
				$('.input-container-all input').each(function(index, value) {
					var $input = $(value);
					if ($input.val() != '') {
						allBlank = false;
						if (!validateInviteEmail($input)) {
							validated = false;
						}
					}
				});
				if (!allBlank) {
					if (validated) {
						// post and success
						var isFirstValidate = true;
						$('.share-container .input-container').each(
								function(index, value) {
									var $this = $(value);
									if ($this.find('input').val() != '') {
										if (!isLogin) {
											popupLogin();
										} else {
											if (isFirstValidate) {
//												$this.addClass('has-success');
												$this.find('label')
														.text('发送成功');
												$this.find('input').val('');
												isFirstValidate = false;
											} else {
												$this.remove();
											}
										}
									} else {
										$this.remove();
									}
								});
					}
				} else {
					$('.input-container-all input').each(
							function(index, value) {
								var $input = $(value);
								validateInviteEmail($input);
							});
				}
			});
});

function inviteMore() {
	// var $inputContainer = $('<div/>');
	// $inputContainer.addClass('input-container');
	// $inputContainer.append('<label class="control-label"></label>')
	// $inputContainer
	// .append('<input class="ui-corner-all form-control"
	// name="share-email"type="text">');
	var firstInputContent = $('.input-container-all .input-container').eq(0);
	var $inputContainer = firstInputContent.clone();
	$inputContainer.appendTo($('.input-container-all'));
	$inputContainer.find('input').blur(validateInviteEmailOnBlur);
	$inputContainer.removeClass('has-error');
	$inputContainer.removeClass('has-success');
	$inputContainer.find('label').text('');
	$inputContainer.find('input').val('');
	return false;
}

function validateInviteEmailOnBlur(event) {
	var $input = $(this);
	if (!$input.val() == '') {
		validateInviteEmail($input);
	}
}

function validateInviteEmail($input) {
	var $label = $input.parent().prev();
	var $parent = $input.parent().parent();
	if (validateEmail($input.val())) {
		$parent.removeClass('has-error');
		$label.text('');
		return true;
	} else {
		$parent.addClass('has-error');
		$parent.removeClass('has-success');
		if ($input.val() == '') {
			$label.text('请输入一个邮箱地址');
		} else {
			$label.text('请输入格式正确的邮件地址');
		}
		return false;
	}
}
