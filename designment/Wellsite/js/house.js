var map = null;
$(function() {
	$('.like-container span.icon-like').click(function() {
		var $this = $(this);
		var $count = $this.parent().find('span.like-count');
		if (!$this.hasClass('liked')) {
			$count.text(parseInt($count.text()) + 1);
			$this.addClass('liked');
		} else {
			$count.text(parseInt($count.text()) - 1);
			$this.removeClass('liked');
		}
	});

	$('.carousel').carousel({
		interval : false
	})
	$('.carousel .item').click(function() {
		$('.carousel').carousel('next');

	});

	$('.carousel')
			.on(
					'slide.bs.carousel',
					function(e) {
						console.log('e.relatedTarget = %o, e.direction = %o',
								e.relatedTarget, e.direction);
						var itemCount = $('.carousel .carousel-inner')
								.children().length;
						var next = ($(e.relatedTarget).index()) % itemCount;
						$('ul.slideshow-thumbnails li.active').removeClass(
								'active');
						var $next = $('ul.slideshow-thumbnails').children().eq(
								next);
						$next.addClass('active');
						console.log('$next.position().left = %s', $next
								.position().left);

						var scrollLeft = $('.thumbnails-viewport').scrollLeft();
						var visableWith = $('.thumbnails-viewport').width();
						var activeWidth = $next.width();
						var activePosition = $next.position().left;

						if (activePosition - 3 < scrollLeft) {
							$('.thumbnails-viewport').stop().scrollTo(
									$next.position().left - 3, 300);
						} else if (activePosition + activeWidth + 3 > scrollLeft
								+ visableWith) {
							$('.thumbnails-viewport').stop().scrollTo(
									activePosition - visableWith + activeWidth
											- 3, 300);
						}

					});

	$('.slideshow-btn.slideshow-prev').click(function() {
		$('.carousel').carousel('prev');
	});
	$('.slideshow-btn.slideshow-next').click(function() {
		$('.carousel').carousel('next');
	});
	$('ul.slideshow-thumbnails li').click(function() {
		$('.carousel').carousel($(this).index());
	});

	$('#tabs').tabs();

	$('#tabs .ui-corner-all').not('table.ui-corner-all').removeClass(
			'ui-corner-all');
	$('.ui-corner-top').removeClass('ui-corner-top');
	$('.ui-corner-bottom').removeClass('ui-corner-bottom');

	$('#tabs-comments .i-want-comment-container a').click(function() {
		$('#comment-textarea').focus();
		return false;
	});
	initMap();
	initMapStreatView();

	$('#tabs-comments #comment-textarea').blur(function() {
		$(this).parent().removeClass('has-error');
	});

	$('#tabs-comments .action button').click(function() {
		var $textarea = $('#tabs-comments #comment-textarea');
		if ($textarea.val() == '') {
			$textarea.parent().addClass('has-error');
		} else {
			$textarea.parent().removeClass('has-error');
			$('.popup.popup-tip').bPopup({
				modalClose : true,
				opacity : 0.6,
				positionStyle : 'absolute', // 'fixed' or 'absolute',
				fadeSpeed : 'slow', // can be a string ('slow'/'fast') or
				// int
				followSpeed : 'fast'
			// can be a string ('slow'/'fast') or int
			});
		}
	});

});

function initMap() {
	// 周边环境下面地图增加一个房源图标(坐标为"121.387835, 31.102757”)，图标类型是7
	var facilityHouseSource = {
		lat : 31.102757,
		lng : 121.387835,
		type : 7,
		name : '房源位置'
	};
	facilities.push(facilityHouseSource);

	map = new BMap.Map("map"); // 创建Map实例
	var point = new BMap.Point(facilityHouseSource.lng, facilityHouseSource.lat); // 创建点坐标
	map.centerAndZoom(point, 15); // 初始化地图,设置中心点坐标和地图级别。
	map.enableScrollWheelZoom();
	map.addControl(new BMap.NavigationControl()); // 添加默认缩放平移控件
	map.addControl(new BMap.ScaleControl());// 添加默认比例尺控件

	addFacilitiesBitIcons();
	// addFacilitiesSmallIcons();

	map.addEventListener("resize", function() {
		map.centerAndZoom(point, 15); // 重新设置中心点坐标和地图级别。
	});

	// 周边环境下面的设施地图加这样一个功能，当比例尺从1公里缩小至2公里时，设施的图
	// 标不再用原来的大图标，而将各类设施统一用小蓝点表示。这样做的目的是，当用户将比例
	// 尺缩小的很小时，所有图标堆在一块，使用的体验特别的差。
	// PS：当变成小蓝点时，原本的点击即显示该设施名称的功能不变，保留。
	map.addEventListener("zoomend", function() {
		map.clearOverlays();
		var zoom = map.getZoom();
		if (zoom <= 13) {
			addFacilitiesSmallIcons();
		} else {
			addFacilitiesBitIcons();
		}
	});

	function addFacilitiesBitIcons() {
		$.each(facilities, function(index, value) {
			var point = new BMap.Point(value.lng, value.lat);
			var myIcon = new BMap.Icon(iconFacilities[value.type].normal,
					new BMap.Size(43, 57));
			var marker = new BMap.Marker(point, {
				icon : myIcon,
				offset : new BMap.Size(-22, -29)
			});
			marker.tag = value;
			map.addOverlay(marker);

			marker.addEventListener('mouseover',
					function(type, target, point, pixel) {
						this.setIcon(new BMap.Icon(
								iconFacilities[this.tag.type].hover,
								new BMap.Size(43, 57)));

						if (!marker.nameOverlay) {
							var nameOverlay = new FacilityNameOverlay(marker
									.getPosition(), value.name, new BMap.Size(
									-22, -52));
							marker.nameOverlay = nameOverlay;
							map.addOverlay(marker.nameOverlay);
						} else {
							marker.nameOverlay.show();
						}
					})
			marker.addEventListener('mouseout', function(type, target, point,
					pixel) {
				this.setIcon(new BMap.Icon(
						iconFacilities[this.tag.type].normal, new BMap.Size(43,
								57)));

				if (marker.nameOverlay && marker.nameOverlay.isVisible()) {
					marker.nameOverlay.hide();
				}
			});
		});
	}
	function addFacilitiesSmallIcons() {
		var dotNormal = 'images/icon-marker-small-hover.png';
		var dotHover = 'images/icon-marker-small-default.png';
		var dotWidht = 17;
		var dotHeigt = 17;
		$.each(facilities, function(index, value) {
			var point = new BMap.Point(value.lng, value.lat);
			var myIcon = new BMap.Icon(dotNormal, new BMap.Size(dotWidht,
					dotHeigt));
			var marker = new BMap.Marker(point, {
				icon : myIcon,
				offset : new BMap.Size(-dotWidht / 2, -dotHeigt / 2)
			});
			marker.tag = value;
			map.addOverlay(marker);

			marker.addEventListener('mouseover', function(type, target, point,
					pixel) {
				this.setIcon(new BMap.Icon(dotHover, new BMap.Size(dotWidht,
						dotHeigt)));

				if (!marker.nameOverlay) {
					var nameOverlay = new FacilityNameOverlay(marker
							.getPosition(), value.name, new BMap.Size(
							-dotWidht / 2, -dotHeigt / 2));
					marker.nameOverlay = nameOverlay;
					map.addOverlay(marker.nameOverlay);
				} else {
					marker.nameOverlay.show();
				}

				console.log('map.getCenter() = %o', map.getCenter());

			})
			marker.addEventListener('mouseout', function(type, target, point,
					pixel) {
				this.setIcon(new BMap.Icon(dotNormal, new BMap.Size(dotWidht,
						dotHeigt)));

				if (marker.nameOverlay && marker.nameOverlay.isVisible()) {
					marker.nameOverlay.hide();
				}
			});
		});

	}

}
function initMapStreatView() {
	var mapStreatView = new BMap.Map("map-streat-view"); // 创建Map实例
	var point = new BMap.Point(121.387835, 31.102757); // 创建点坐标
	mapStreatView.centerAndZoom(point, 14); // 初始化地图,设置中心点坐标和地图级别。
	mapStreatView.addTileLayer(new BMap.PanoramaCoverageLayer());// 添加街景

	var panorama = new BMap.Panorama('map-streat-view');
	panorama.setPosition(new BMap.Point(121.387835, 31.102757)); // 根据经纬度坐标展示全景图
	panorama.setPov({
		heading : -40,
		pitch : 6
	});
	// mapStreatView.addControl(new BMap.OverviewMapControl()); //添加默认缩略地图控件
	mapStreatView.addControl(new BMap.OverviewMapControl({
		isOpen : true,
		anchor : BMAP_ANCHOR_TOP_RIGHT
	})); // 右上角，打开
}