$(function() {
	wall = new freewall(".house-suggestions .items");
	wall.reset({
		selector : '.house-item',
		animate : false,
		cellW : 230,
		cellH : 188,
		onResize : function() {
			wall.refresh();
		}
	});
	wall.fitWidth();
	// for scroll bar appear;
	$(window).trigger("resize");
	
	loadSuggestions();
});

function loadSuggestions() {
	// init suggestions data. load from server.
	var suggestionItems = [];
	for (var i = 0; i < 4; i++) {
		var item = {
			district : '丝庐花语',
			price : '349',
			address : '徐汇，斜土路大木桥',
			area : '125'
		};
		suggestionItems.push(item);
	}
	addSuggestions(suggestionItems);
}

function addSuggestions(items) {
	$itemsContainer = $('.house-suggestions .items');
	console.log('$itemsContainer = %o', $itemsContainer);
	$.each(items, function(index, value) {
		var $item = $('.house-suggestions .house-item-template .house-item').clone();
		$item.find('.district').text(value.district);
		$item.find('.price .number').text(value.price);
		$item.find('.address').text(value.address);
		$item.find('.area span.number').text(value.area);

		$item.appendTo($itemsContainer);
	});

	wall.refresh();
}