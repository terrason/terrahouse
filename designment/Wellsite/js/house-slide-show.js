var isHouseTypeTriggerHover = false;
var isHouseTypeHover = false;
var $timerHideHouseType;
var timerId;
$(function() {
	$('#slideshow .thumbnails-container img.slideshow-prev').hover(function() {
		$(this).attr('src', 'images/icon-left-hilight.png');
	}, function() {
		$(this).attr('src', 'images/icon-left.png');
	});
	$('#slideshow .thumbnails-container img.slideshow-next').hover(function() {
		$(this).attr('src', 'images/icon-right-hilight.png');
	}, function() {
		$(this).attr('src', 'images/icon-right.png');
	});

	$timerHideHouseType = $.timer(function() {
		$('#left div.house-type-pic').hide('slide');
	});

	$('#left .icon-house-type-pic-trigger').mouseenter(
			function() {
				clearTimeout(timerId);
				var $houseType = $('#left div.house-type-pic');
				var imageUrl = $('.carousel .item.active .icon-house-type-pic')
						.attr('data');
				$houseType.find('img.picture').attr('src', imageUrl)
				$houseType.show('slide');
				// $('#left div.icon-house-type-in-out').switchClass('out',
				// 'in');
				$('#left div.icon-house-type-in-out').hide();
			}).mouseleave(function() {
		timerId = setTimeout(hideHouseType, 200);
	});
	$('#left div.house-type-pic').mouseenter(function() {
		clearTimeout(timerId);
	}).mouseleave(function() {
		timerId = setTimeout(hideHouseType, 200);
	});

	$('.carousel').on('slid.bs.carousel', function() {
		changeTriggerState();
	});

	changeTriggerState();

	function changeTriggerState() {
		var $trigger = $('#left .icon-house-type-pic-trigger');
		var $typePic = $('.carousel .item.active .icon-house-type-pic');
		if ($typePic.attr('data')) {
			$trigger.show();
		} else {
			$trigger.hide();
		}
	}

});

function hideHouseType() {
	$('#left div.house-type-pic').hide('slide');
	// $('#left div.icon-house-type-in-out').switchClass('in', 'out');
	$('#left div.icon-house-type-in-out').show();
}
