var timer;
var validateCodeInfoFlag = -1;

var datePicker = null;

// 可预约的日期和时间段数据
var availableDays = null;

// 显示的时间段
var timeScope = [ "09:00 - 12:00", "12:00 - 18:00", "18:00 - 21:00" ]
// 待开放的日期
var tobeAvailableDays = [];
// 选择的日期和时间段
var selectedDay = {
	day : null,
	availableIndex : [],// 可选的时间段的索引
	index : -1
// 选择了的时间段的索引
};

$(function() {

	// 初始化可预约的日期数据
	availableDays = [ {
		day : "2013-11-21",
		index : [ 0, 2 ]
	}, {
		day : "2013-11-22",
		index : [ 1, 2 ]
	}, {
		day : "2013-11-25",
		index : [ 0, 1 ]
	}, {
		day : "2013-11-27",
		index : [ 0, 1, 2 ]
	}, {
		day : "2013-12-02",
		index : [ 0, 2 ]
	}, {
		day : "2014-01-05",
		index : [ 1, 2 ]
	} ];
	// 初始化可预约的日期初始化待开放的日期数据
	// tobeAvailableDays = [ "2013-11-28", "2013-11-29", "2013-11-30" ];

	datePicker = $(".datepicker").datepicker({
		inline : true,
		showOtherMonths : false,
		dateFormat : "yy-mm-dd",
		onSelect : function(date, picker) {
			doOnDayClicked(date, picker);
		},
		beforeShowDay : hilightDays
	});
	$(".datepicker").datepicker("setDate", "2013-11-12");
	$('#book-it').click(bookIt);
	$('.book-container .select-time .time-item').each(function(i, element) {
		$this = $(this);
		$this.click({
			index : i,
			element : $this
		}, function(event) {
			selectTimeIndex(event);
		})
	});

	$('#real-name').blur(function() {
		var $realName = $(this);
		if ($realName.val() == '') {
			showPublishErrorMessage($realName, '请填写您的姓名');
		} else {
			clearPublishErrorMessage($realName);
		}
	});

	function validatePhoneNumber() {
		var success = true;
		var num = $('#phone-num').val();
		console.log('num.length = %o, num.substring = %o', num.length, num
				.substring(0, 1));
		if (num.length != 11 || num.substring(0, 1) != '1') {
			showPublishErrorMessage($('#phone-num'), '请填写格式正确的手机号码。');
			success = false;
		} else {
			showPublishErrorMessage($('#phone-num'), '');
		}
		return success;
	}

	$('#phone-num').blur(validatePhoneNumber);

	timer = $.timer(function() {
		if (validateCodeInfoFlag > 0) {
			$('#receive-validate-number').text(validateCodeInfoFlag + '秒后重新获取')
			validateCodeInfoFlag = validateCodeInfoFlag - 1;
		} else {
			$('#receive-validate-number').text('重新获取验证码');
			timer.stop();
		}
	});
	timer.set({
		time : 1000,
		autostart : false
	});
	$('#receive-validate-number').click(function() {
		if (!validatePhoneNumber()) {
			return false;
		}
		if (validateCodeInfoFlag <= 0) {
			validateCodeInfoFlag = 29;
			$(this).text(validateCodeInfoFlag + '秒后重新获取');
			validateCodeInfoFlag = validateCodeInfoFlag - 1;
			timer.play();
		} else {

		}
		return false;
	});

	$('button#book-confirmed-action').click(
			function() {
				var $realName = $('#real-name');
				var $phoneNum = $('#phone-num');
				var $validateCode = $('#validate-code');

				var success = true;

				if ($realName.val() == '') {
					showPublishErrorMessage($realName, '请填写您的姓名');
					success = false;
				} else {
					clearPublishErrorMessage($realName);
				}
				if ($phoneNum.val() == '') {
					showPublishErrorMessage($phoneNum, '请填写格式正确的手机号码。');
					success = false;
				} else {
					clearPublishErrorMessage($phoneNum);
				}
				if ($phoneNum.val().length != 11
						|| $phoneNum.val().substring(0, 1) != '1') {
					showPublishErrorMessage($phoneNum, '请填写您的手机号码');
					success = false;
				} else {
					clearPublishErrorMessage($phoneNum);
				}
				if ($validateCode.val() == '') {
					showPublishErrorMessage($validateCode, '请填写验证码');
					success = false;
				} else {
					if ($validateCode.val().length != 6) {
						showPublishErrorMessage($validateCode, '请输入6位验证码。');
						success = false;
					} else if (validateCodeInfoFlag < 0) {
						showPublishErrorMessage($validateCode, '请先获取验证码。');
						success = false;
					} else if (validateCodeInfoFlag == 0) {
						showPublishErrorMessage($validateCode, '超时，请重新获取验证码。');
						success = false;
					} else {
						clearPublishErrorMessage($validateCode);
					}

				}

				if (success) {
					// 模拟post后
					if ($validateCode.val() != '123456') {
						showPublishErrorMessage($validateCode, '验证码输入有误。');
						success = false;
					} else {
						showPublishErrorMessage($validateCode, '');
					}
				}
				if (success) {
					$('.popup.book-confirm').bPopup().close();
					popupBookConfirmResult();
				}
				return false;
			});

	$('.popup.book-confirm-result #book-confirmed-result-action').click(
			function() {
				$('.popup.popup.book-confirm-result').bPopup().close();
				return true;
			});
});

// 点击日历某一天的事件
function doOnDayClicked(date, picker) {
	$.each(availableDays, function(i, element) {
		if (element.day == date) {

			hiddenError();
			selectedDay.index = -1;

			if (selectedDay.day != date) {
				selectedDay.day = date;
				selectedDay.availableIndex = element.index;

				$('.book-container .select-time').show('blind', 300);
				resetTimeIndex();

			} else {
				selectedDay.day = null;
				selectedDay.availableIndex = [];

				$('.book-container .select-time').hide('blind', 300);
				resetTimeIndex(true);

			}

			testHiddenMessage();
		}
	})

}

// 自定义要高亮的日期单元格的class
function hilightDays(date) {

	var formatDay = $.format.date(date, "yyyy-MM-dd")

	if (formatDay == selectedDay.day) {
		return [ true, 'selected-days' ];
	}

	for (var i = 0; i < tobeAvailableDays.length; i++) {
		if (formatDay == tobeAvailableDays[i]) {
			return [ true, 'tobe-available-days' ];
		}
	}
	for (var i = 0; i < availableDays.length; i++) {
		if (formatDay == availableDays[i].day) {
			return [ true, 'available-days' ];
		}
	}
	return [ true, '' ];
}

// 点击预定后触发的事件。提交数据在本方法写。
function bookIt() {
	if (!selectedDay.day) {
		displayCalendarError('请选择日期和具体时段！');
	} else if (selectedDay.index < 0) {
		displayCalendarError('请选择具体时段！');
	} else {
		hiddenError();
		// testDisplayMessage('预约：' + selectedDay.day + ' '
		// + timeScope[selectedDay.index]);

		var date = new Date(selectedDay.day);
		var formatDay = $.format.date(date, "yyyy年MM月dd日，ddd");
		$('.popup.book-confirm form .step .text.right .content').text(
				formatDay + ' ' + timeScope[selectedDay.index]);
		var dayPre = date.setDate(date.getDate() - 1);
		var formatDayPre = $.format.date(dayPre, "yyyy年MM月dd日ddd");
		$('.popup.book-confirm-result .description span.day-pre').text(
				formatDayPre);
		$('.popup.book-confirm form input[type="text"]').each(
				function(index, value) {
					clearPublishErrorMessage($(value));
				});
		popupBookConfirm();
	}
}

// 点击时间段触发的事件
function selectTimeIndex(event) {
	var element = event.data.element;
	if (element.hasClass('disabled')) {
		displayCalendarError('抱歉，此时间段不可预约！')
	} else if (!element.hasClass('selected')) {
		hiddenError();
		resetTimeIndex();
		element.addClass('selected');
		selectedDay.index = event.data.index;
		element.find('.checkbox .checkbox-bg').stop().scrollTo('0%', 300);

		element.find('.checkbox .blockContainer').switchClass('left', 'right',
				300);
		testHiddenMessage();
	} else {
		selectedDay.index = -1;
		element.removeClass('selected');
		element.find('.checkbox .blockContainer').switchClass('right', 'left',
				300);
		element.find('.checkbox .checkbox-bg').stop().scrollTo('100%', 300);
	}
}

// 重置时间段样式
function resetTimeIndex(isNoAnim) {
	$('.book-container .select-time .time-item').each(
			function(i, value) {
				var $this = $(this);
				$this.removeClass('selected');
				$this.removeClass('disabled');
				$this.find('.checkbox .blockContainer').switchClass('right',
						'left', 300);
				if (isNoAnim) {
					$this.find('.checkbox .checkbox-bg').stop().scrollTo(
							'100%', 0);
				} else {
					$this.find('.checkbox .checkbox-bg').stop().scrollTo(
							'100%', 300);
				}
				if ($.inArray(i, selectedDay.availableIndex) < 0) {
					$this.addClass('disabled');
				}

			});
}

function displayCalendarError(message) {
	var $error = $('.book-container .error');
	$error.text(message)
}

function hiddenError() {
	var $error = $('.book-container .error');
	$error.text('')
}
function testDisplayMessage(message) {
	$('#message p').text(message)
	$('#message').css('display', 'block');
}

function testHiddenMessage() {
	$('#message p').text('')
	$('#message').css('display', 'none');
}

function popupBookConfirm() {
	$('.popup.book-confirm').bPopup({
		modalClose : true,
		opacity : 0.6,
		positionStyle : 'absolute', // 'fixed' or 'absolute',
		fadeSpeed : 'slow', // can be a string ('slow'/'fast') or
		// int
		followSpeed : 'fast'
	// can be a string ('slow'/'fast') or int
	});
}
function popupBookConfirmResult() {
	$('.popup.book-confirm-result').bPopup({
		modalClose : false,
		opacity : 0.6,
		positionStyle : 'absolute', // 'fixed' or 'absolute',
		fadeSpeed : 'slow', // can be a string ('slow'/'fast') or
		// int
		followSpeed : 'fast'
	// can be a string ('slow'/'fast') or int
	});
}

function showPublishErrorMessage($input, message) {
	var parent = $input.parent();
	var $error = parent.find('.error');
	if ($error.length == 0) {
		$error = $('<span></span>');
		$error.addClass('error');
		$error.appendTo(parent);
	}
	$error.text(message);
}

function clearPublishErrorMessage($input) {
	showPublishErrorMessage($input, '');
}