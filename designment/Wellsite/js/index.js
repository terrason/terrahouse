var wall = null;
var timerCounter = 0;
$(function() {
	$('.carousel').carousel({
		interval : 8000
	})
	$('.carousel .carousel-control').hover(function() {
		$('.carousel .carousel-control').addClass('hilight')
	}, function() {
		$('.carousel .carousel-control').removeClass('hilight')
	});
	$('input, textarea').placeholder();

	$('#area-dropdown-menu a').click(function() {
		$('#area').val($(this).text());
	});
	$('#n-bedroom-dropdown-menu a').click(function() {
		$('#n-bedroom').val($(this).text());
	});

	$(document)
			.mouseup(
					function(e) {
						var container = $('#price-popover');
						var containerTrigger = $('#price-popover-trigger');

						if ((!container.is(e.target) && container.has(e.target).length === 0)
								&& (!containerTrigger.is(e.target) && containerTrigger
										.has(e.target).length === 0)) {
							container.hide();
							refreshPriseArea();
						}
					});

	$('#price-popover-trigger').click(function() {
		$('#price-popover').toggle();
		refreshPriseArea();
	});

	$(".numeric").numeric();

	$('#how-to .tab-head#tab-head-sold')
			.click(
					function() {
						if (!$(this).hasClass('current')) {
							timer.stop();
							$("#how-to ul.tab-contant#tab-sold").each(
									function(i, element) {
										$lis = $(this).find('li');
										$lis.removeClass('hovered');
										$(this).children().eq(0).addClass(
												'hovered');
										timerCounter = 1;
										timer.play();
									});
							$('#how-to .tab-head#tab-head-buy').toggleClass(
									'current');
							$('#how-to .tab-head#tab-head-sold').toggleClass(
									'current');
							$('#how-to .tab-content-container').switchClass(
									'left', 'right', 500);
							$('.more a').attr('href', '/help/sell');
						}
					});
	$('#how-to .tab-head#tab-head-buy')
			.click(
					function() {
						if (!$(this).hasClass('current')) {
							timer.stop();
							$("#how-to ul.tab-contant#tab-buy").each(
									function(i, element) {
										$lis = $(this).find('li');
										$lis.removeClass('hovered');
										$(this).children().eq(0).addClass(
												'hovered');
										timerCounter = 1;
										timer.play();
									});
							$('#how-to .tab-head#tab-head-buy').toggleClass(
									'current');
							$('#how-to .tab-head#tab-head-sold').toggleClass(
									'current');
							$('#how-to .tab-content-container').switchClass(
									'right', 'left', 500);
							$('.more a').attr('href', '/help/buy');
						}
					});

	wall = new freewall("#suggestion .items");
	wall.reset({
		selector : '.house-item',
		animate : false,
		cellW : 230,
		cellH : 188,
		onResize : function() {
			wall.refresh();
		}
	});
	wall.fitWidth();
	// for scroll bar appear;
	$(window).trigger("resize");

	loadMoreSuggestion();
	$('#suggestion .loading img').hide();
	// $('#load-more').click(loadMoreSuggestion);
	$('#load-more').click(function() {
		$('#suggestion .loading img').show();
		$.timer(loadMoreSuggestion).once(2000);
	});

	var timer = $.timer(function() {
		$("#how-to ul.tab-contant").each(function(i, element) {
			$lis = $(this).find('li');
			$lis.removeClass('hovered');
			$(this).children().eq(timerCounter % 4).addClass('hovered');
		});
		timerCounter = timerCounter + 1;
	});

	timer.set({
		time : 2500,
		autostart : true
	});

	var searchValue = '';
	var searchChangeTimer = $.timer(function() {
		var currentValue = $('#location').val();
		if (currentValue != searchValue) {
			keywordsChanged(searchValue, currentValue);
			searchValue = currentValue;
		}
	});
	searchChangeTimer.set({
		time : 500,
		autostart : false
	});
	$('#location').focus(function() {
		searchChangeTimer.play();
		if ($('#location').val() != '') {
			$('.search-suggestions').show();
		}
	});
	$('#location').focusout(function() {
		var isHovered = $('ul.search-suggestions').find('li:hover').length;
		if (!isHovered) {
			$('.search-suggestions').hide();
			searchChangeTimer.pause();
		}
	});
	function keywordsChanged(oldValue, newValue) {
		$('.search-suggestions').show();
		console.log('oldValue = %s, newValue = %s', oldValue, newValue);
	}

	$('ul.search-suggestions li').click(function() {
		searchChangeTimer.pause();
		$('.search-suggestions').hide();
		$('#location').val($(this).text());
	})

});

function addSuggestions(items) {
	$itemsContainer = $('#suggestion div.items');
	$.each(items, function(index, value) {
		var $item = $('.load-more-container .house-item').clone();
		$item.find('.district').text(value.district);
		$item.find('.price .number').text(value.price);
		$item.find('.address').text(value.address);
		$item.find('.area span.number').text(value.area);

		$item.appendTo($itemsContainer);
	});

	wall.refresh();
}

function loadMoreSuggestion() {
	// init suggestions data. load from server.
	var suggestionItems = [];
	for (var i = 0; i < 8; i++) {
		var item = {
			district : '丝庐花语',
			price : '349',
			address : '徐汇，斜土路大木桥',
			area : '125'
		};
		suggestionItems.push(item);
	}
	addSuggestions(suggestionItems);
	$('#suggestion .loading img').hide();
}

function refreshPriseArea() {
	var from = $('#price-from').val();
	var to = $('#price-to').val();
	if (from != '' && to != '') {
		$('#price').html(
				from + '&nbsp;至&nbsp;' + to
						+ '万&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');
	} else if (from == '' && to != '') {
		$('#price').html(to + '万以下&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');
	} else if (from != '' && to == '') {
		$('#price').html(from + '万以上&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');
	} else {
		$('#price').html('价格');

	}
}