var taxCalculateInitValue = {
	total : 438,
	area : 107,
	isOnlyOne : false,
	isOver5Years : true,
	region : 0,
	priceForLandlord : 30
}
$(function() {
	$(".numeric").numeric();

	$('ul.dropdown-menu#is-only-one-dropdown-menu li').click(function() {
		$('#is-only-one').val($(this).text());
		$('#is-only-one-dropdown.dropdown-toggle').dropdown('toggle');
		disableResetButton();
		return false;
	});
	$('ul.dropdown-menu#is-over-5-years-dropdown-menu li').click(function() {
		$('#is-over-5-years').val($(this).text());
		$('#is-over-5-years-dropdown.dropdown-toggle').dropdown('toggle');
		disableResetButton();

		return false;
	});
	$('ul.dropdown-menu#region-dropdown-menu li').click(function() {
		$('#region').val($(this).text());
		$('#region-dropdown.dropdown-toggle').dropdown('toggle');
		disableResetButton();

		return false;
	});
	$('ul.dropdown-menu#is-first-property-for-buyer-dropdown-menu li').click(
			function() {
				$('#is-first-property-for-buyer').val($(this).text());
				$('#is-first-property-for-buyer-dropdown.dropdown-toggle')
						.dropdown('toggle');
				$('.tax-calculator .content .input-data .item.error span')
						.hide();
				disableResetButton();
				return false;
			});

	// “显示初始值”应该的状态是一开始处于灰色状态不可点击状态，在不改动任何初始值的情况下，鼠标放上去应该还是灰色（现在的情况是鼠标放上去就变橙色）；当用户改动上方任意一初始值的时候，此按钮变成橙色可点击状态，若点击还原到初始值后再度变为灰色的不可点击状态。

	$(".item input").change(disableResetButton);
	$(".item input").blur(function(){
		var $inputContainer = $(this).parents('div.item');
		$inputContainer.removeClass('has-error');
	});

	function disableResetButton() {
		var $reset = $('.tax-calculator #reset')
		$reset.addClass('btn-orange');
		$reset.prop('disabled', false);
	}

	$('.popup.book-confirm .checkbox input[type="checkbox"]').change(function(){
		console.log('this.checked = %o', this.checked);
		if(this.checked){
			$('button#book-confirmed-action').addClass('btn-primary');
			$('button#book-confirmed-action').removeAttr('disabled');
		} else {
			$('button#book-confirmed-action').removeClass('btn-primary');
			$('button#book-confirmed-action').attr('disabled', 'disabled');
		}
	});
	$('button#calulate')
			.click(
					function() {
						var success = true;
						$('.item input').each(function(index, value) {
							if (!validateInput($(this))) {
								success = false;
							}
						});

						var $errorInfo = $('.tax-calculator .content .input-data .item.error span');
						if (success) {
							$errorInfo.hide();
							$('.tax-calculator .content-container').stop()
									.scrollTo('100%', 300);
						} else {
							$errorInfo.show();
						}

					});
	$('img#tax-calculator-back').click(function() {
		$('.tax-calculator .content-container').stop().scrollTo('0%', 300);
	});

	$('button#reset').click(initTaxCalculateValue);

	initTaxCalculateValue();

	function validateInput($input, notAddErrorClass) {
		var $inputContainer = $input.parents('div.item');

		if ($input.val() == '') {
			if (!notAddErrorClass) {
				$inputContainer.addClass('has-error');
			}
			return false;

		} else {
			$inputContainer.removeClass('has-error');
			return true;
		}
	}

	$('.item input')
			.blur(
					function() {
						var success = validateInput($(this), true);
						var $errorInfo = $('.tax-calculator .content .input-data .item.error span');
						if (success) {
							$errorInfo.hide();
						} else {
							// $errorInfo.show();
						}
					});

});

function initTaxCalculateValue() {
	$('.input-data input#total').val(taxCalculateInitValue.total);
	$('.input-data input#area').val(taxCalculateInitValue.area);

	$('.input-data input#is-only-one').val(
			$('ul#is-only-one-dropdown-menu li').eq(
					taxCalculateInitValue.isOnlyOne ? 0 : 1).text());
	$('.input-data input#is-over-5-years').val(
			$('ul#is-over-5-years-dropdown-menu li').eq(
					taxCalculateInitValue.isOver5Years ? 0 : 1).text());
	$('.input-data input#region').val(
			$('ul#region-dropdown-menu li').eq(taxCalculateInitValue.region)
					.text());
	$('.input-data input#price-for-landlord').val(
			taxCalculateInitValue.priceForLandlord);

	var $reset = $('.tax-calculator #reset');
	$reset.removeClass('btn-orange');
	$reset.prop('disabled', true);
}