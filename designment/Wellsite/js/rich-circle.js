$(function() {
	var $richCircle = $('.rich-circle');
	$richCircle.each(function(index, value) {
		richCircle($(this));

	});
});
function richCircle($richCircle) {

	var $this = $richCircle;
	var anticlockwise = $this.attr('anticlockwise');
	var percent = parseFloat($this.attr('percent')) / 100;
	 if (anticlockwise == 'true') {
	 percent = 1 - percent;
	 }
	var $pie1 = $this.find('.hold.hold1 .pie.pie1');
	var $pie2 = $this.find('.hold.hold2 .pie.pie2');

	if (percent <= 0.5) {
		$pie1.css('-o-transform', 'rotate(' + (180 * percent * 2) + 'deg)');
		$pie1.css('-moz-transform', 'rotate(' + (180 * percent * 2) + 'deg)');
		$pie1
				.css('-webkit-transform', 'rotate(' + (180 * percent * 2)
						+ 'deg)');
		$pie2.css('-o-transform', 'rotate(' + (0) + 'deg)');
		$pie2.css('-moz-transform', 'rotate(' + (0) + 'deg)');
		$pie2.css('-webkit-transform', 'rotate(' + (0) + 'deg)');
	} else {
		$pie1.css('-o-transform', 'rotate(' + (180) + 'deg)');
		$pie1.css('-moz-transform', 'rotate(' + (180) + 'deg)');
		$pie1.css('-webkit-transform', 'rotate(' + (180) + 'deg)');
		$pie2.css('-o-transform', 'rotate(' + (180 * (percent - 0.5) * 2)
				+ 'deg)');
		$pie2.css('-moz-transform', 'rotate(' + (180 * (percent - 0.5) * 2)
				+ 'deg)');
		$pie2.css('-webkit-transform', 'rotate(' + (180 * (percent - 0.5) * 2)
				+ 'deg)');

	}

}