var isLogin = false;
$(function() {
	$('#login').click(function() {
		popupLogin();
		return false;
	});

	$('#login-action').click(function() {
		var $email = $('.popup.login #email');
		var $password = $('.popup.login #password');
		if ($email.val() == '') {
			displayError($email, '请输入您的电子邮箱')
		} else if ($password.val() == '') {
			displayError($password, '请输入您的登陆密码')
		} else if (!validateEmail($email.val())) {
			displayError($email, '请输入格式正确的电子邮箱')
		} else if ($password.val().length < 6) {
			displayError($password, '密码位数不得小于六位')
		} else {
			$('.popup.login').bPopup().close();
			onLoggedIn();
		}
		return false;
	});
	$('.popup.login #email').blur(function() {
		var $email = $(this);
		if ($email.val() == '') {
			displayError($email, '请输入您的电子邮箱')
		} else if (!validateEmail($email.val())) {
			displayError($email, '请输入格式正确的电子邮箱')
		} else {
			// 从服务器检测是否注册，没注册就提示错误信息：“该邮箱尚未注册”
			clearTip($email);
		}
	});
	$('.popup.login #password').blur(function() {
		var $password = $(this);
		if ($password.val() == '') {
			displayError($password, '请输入您的登陆密码')
		} else if ($password.val().length < 6) {
			displayError($password, '密码位数不得小于六位')
		} else {
			// 从服务器检测登录密码，密码不对就提示错误信息：“您输入的登录密码不正确”
			clearTip($password);
		}
	});

	$('.popup.login .register-archor').click(function() {
		$('.popup.login').bPopup().close();
		popupRegister();
		return false;
	})
	$('.popup.login .forget-password-archor').click(function() {
		$('.popup.login').bPopup().close();
		popupRestPassword();
		return false;
	})

	$('#register').click(function() {
		popupRegister();
		return false;
	});

	$('#register-action').click(function() {
		var $email = $('.popup.register #email');
		var $password = $('.popup.register #password');
		var $repassword = $('.popup.register #repassword');
		if ($email.val() == '') {
			displayError($email, '请输入您的注册邮箱')
		} else if ($password.val() == '') {
			displayError($password, '请输入您的登陆密码')
		} else if ($repassword.val() == '') {
			displayError($repassword, '请确认您的登录密码')
		} else if (!validateEmail($email.val())) {
			displayError($email, '请输入格式正确的电子邮箱')
		} else if ($password.val().length < 6) {
			displayError($password, '密码位数不得小于六位')
		} else if ($password.val() != $repassword.val()) {
			displayError($repassword, '两次输入的密码不一致')
		} else {
			$('.popup.register').bPopup().close();
		}
		return false;
	});

	$('.popup.register #email').blur(function() {
		var $email = $(this);
		if ($email.val() == '') {
			displayError($email, '请输入您的注册邮箱')
		} else if (!validateEmail($email.val())) {
			displayError($email, '请输入格式正确的电子邮箱')
		} else {
			// 从服务器检测是否注册，注册的提示错误信息：“该邮箱已注册，请直接登录”
			clearTip($email);
		}
	});
	$('.popup.register #password').focus(function() {
		displayInfo($('.popup.register #password'), '密码位数不得少于6位');
	});
	$('.popup.register #password').blur(function() {
		var $password = $(this);
		if ($password.val() == '') {
			displayError($password, '请输入您的登陆密码')
		} else if ($password.val().length < 6) {
			displayError($password, '密码位数不得小于六位')
		} else {
			clearTip($password);
		}
	});
	$('.popup.register #repassword').blur(function() {
		var $password = $('.popup.register #password');
		var $repassword = $(this);
		if ($repassword.val() == '') {
			displayError($repassword, '请确认您的登录密码')
		} else if ($password.val() != $repassword.val()) {
			displayError($repassword, '两次输入的密码不一致')
		} else {
			clearTip($repassword);
		}
	});
	$('.popup.register .login-archor').click(function() {
		$('.popup.register').bPopup().close();
		popupLogin();
		return false;
	});
	$('#email-action').click(function() {
		var $email = $('.popup.reset-password #email');
		if ($email.val() == '') {
			displayError($email, '请输入您的注册邮箱')
		} else if (!validateEmail($email.val())) {
			displayError($email, '请输入格式正确的电子邮箱')
		} else {
			$('.popup.reset-password').bPopup().close();
		}
		return false;
	});
	$('.popup.reset-password #email').blur(function() {
		var $email = $(this);
		if ($email.val() == '') {
			displayError($email, '请输入您的注册邮箱')
		} else if (!validateEmail($email.val())) {
			displayError($email, '请输入格式正确的电子邮箱')
		} else {
			// 从服务器检测是否注册，没注册就提示错误信息：“该邮箱尚未注册”
			clearTip($email);
		}
		return false;
	});

	initMenuLoginStatus();
});

function popupLogin() {
	$('.popup.login').bPopup({
		modalClose : true,
		opacity : 0.6,
		positionStyle : 'absolute', // 'fixed' or 'absolute',
		fadeSpeed : 'slow', // can be a string ('slow'/'fast') or
		// int
		followSpeed : 'fast'
	// can be a string ('slow'/'fast') or int
	});
}
function popupRegister() {
	$('.popup.register').bPopup({
		modalClose : true,
		opacity : 0.6,
		positionStyle : 'absolute', // 'fixed' or 'absolute',
		fadeSpeed : 'slow', // can be a string ('slow'/'fast') or
		// int
		followSpeed : 'fast'
	// can be a string ('slow'/'fast') or int
	});
}
function popupRestPassword() {
	$('.popup.reset-password').bPopup({
		modalClose : true,
		opacity : 0.6,
		positionStyle : 'absolute', // 'fixed' or 'absolute',
		fadeSpeed : 'slow', // can be a string ('slow'/'fast') or
		// int
		followSpeed : 'fast'
	// can be a string ('slow'/'fast') or int
	});
}

function initMenuLoginStatus() {
	$('#user-info').parent().hide();
	$('#login').parent().show();
	$('#register').parent().show();
}

function onLoggedIn() {
	$('#user-info').parent().show();
	$('#login').parent().hide();
	$('#register').parent().hide();
	isLogin = true;
}

function findTipByElement($input) {
	var $tip = $input.parent().prev();
	if ($tip.length == 0 || !$tip.hasClass('tip')) {
		$tip = $('<div/>');
		$input.parent().before($tip);
	}
	return $tip;
}
function displayError($input, error) {
	// $input.closest('form').find('.tip').text('');
	var $tip = findTipByElement($input);
	$tip.removeClass();
	$tip.addClass('tip');
	$tip.addClass('error');
	$tip.text(error);
}
function displayInfo($input, info) {
	var $tip = findTipByElement($input);
	$tip.removeClass();
	$tip.addClass('tip');
	$tip.addClass('info');
	$tip.text(info);
}
function clearTip($input) {
	var $tip = findTipByElement($input);
	$tip.removeClass();
	$tip.addClass('tip');
	$tip.text('');
}

function validateEmail(email, nullable) {
	var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	if (nullable) {
		return (emailReg.test(email))
	} else {
		return (email.length > 0 && emailReg.test(email))
	}
}