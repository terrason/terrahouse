$(function() {
	$('.reset-password-container button[type="submit"]').click(function() {
		var $password = $('.reset-password-container #new-password');
		var $repassword = $('.reset-password-container #reinput');
		if ($password.val() == '') {
			displayError($password, '请输入您的新密码')
		} else if ($repassword.val() == '') {
			displayError($repassword, '请确认您的新密码')
		} else if ($password.val().length < 6) {
			displayError($password, '密码位数不得小于六位')
		} else if ($password.val() != $repassword.val()) {
			displayError($repassword, '两次输入的密码不一致')
		} else {
			return true;
		}
		return false;
	});

	$('.reset-password-container #new-password').focus(function() {
		var $password = $(this);
		displayInfo($password, '密码位数不得少于6位');
	});
	$('.reset-password-container #new-password').blur(function() {
		var $password = $(this);
		if ($password.val() == '') {
			displayError($password, '请输入您的新密码')
		} else if ($password.val().length < 6) {
			displayError($password, '密码位数不得小于六位')
		} else {
			clearTip($password);
		}
	});
	$('.reset-password-container #reinput').blur(function() {
		var $repassword = $(this);
		var $password = $('.reset-password-container #new-password');
		if ($repassword.val() == '') {
			displayError($repassword, '请确认您的新密码')
		} else if ($password.val() != $repassword.val()) {
			displayError($repassword, '两次输入的密码不一致')
		} else {
			clearTip($repassword);
		}
	});
});