var iconFacilities = [ {
	normal : 'images/icon-block-facility-park-default.png',
	hover : 'images/icon-block-facility-park-hover.png'
}, {
	normal : 'images/icon-block-facility-gas-station-default.png',
	hover : 'images/icon-block-facility-gas-station-hover.png'
}, {
	normal : 'images/icon-block-facility-hospital-default.png',
	hover : 'images/icon-block-facility-hospital-hover.png'
}, {
	normal : 'images/icon-block-facility-mall-default.png',
	hover : 'images/icon-block-facility-mall-hover.png'
}, {
	normal : 'images/icon-block-facility-super-market-default.png',
	hover : 'images/icon-block-facility-super-market-hover.png'
}, {
	normal : 'images/icon-block-facility-library-default.png',
	hover : 'images/icon-block-facility-library-hover.png'
}, {
	normal : 'images/icon-block-facility-car-park-default.png',
	hover : 'images/icon-block-facility-car-park-hover.png'
}, {
	normal : 'images/icon-marker-red.png',
	hover : 'images/icon-marker-red.png'
} ];
var facilities = [ {
	lat : 31.136075,
	lng : 121.385733,
	type : 0,
	name : '黎安公园'
}, {
	lat : 31.110749,
	lng : 121.393256,
	type : 0,
	name : '莘城中央公园'
}, {
	lat : 31.108036,
	lng : 121.379785,
	type : 0,
	name : '莘庄公园'
}, {
	lat : 31.114228,
	lng : 121.384662,
	type : 4,
	name : '乐购莘东店'
}, {
	lat : 31.124739,
	lng : 121.378563,
	type : 4,
	name : '乐购莘庄店'
}, {
	lat : 31.105741,
	lng : 121.370599,
	type : 4,
	name : '乐购莘松店'
}, {
	lat : 31.137783,
	lng : 121.37959,
	type : 4,
	name : '大润发港澳店'
}, {
	lat : 31.112527,
	lng : 121.394011,
	type : 4,
	name : '家乐福莘庄店'
}, {
	lat : 31.104156,
	lng : 121.394691,
	type : 4,
	name : '乐天玛特春申店'
}, {
	lat : 31.096542,
	lng : 121.397758,
	type : 4,
	name : '农工商银都路店'
}, {
	lat : 31.112821,
	lng : 121.381788,
	type : 4,
	name : '世纪联华莘庄店'
}, {
	lat : 31.107905,
	lng : 121.378644,
	type : 4,
	name : '世纪联华莘松路店'
}, {
	lat : 31.113748,
	lng : 121.382731,
	type : 2,
	name : '闵行区中心医院'
}, {
	lat : 31.112967,
	lng : 121.392978,
	type : 6,
	name : '百安居莘庄店'
}, {
	lat : 31.112156,
	lng : 121.393122,
	type : 3,
	name : '仲盛世界商场'
}, {
	lat : 31.11405,
	lng : 121.385666,
	type : 3,
	name : '莘庄凯德龙之梦'
}, {
	lat : 31.111321,
	lng : 121.395611,
	type : 5,
	name : '闵行区图书馆'
} ];