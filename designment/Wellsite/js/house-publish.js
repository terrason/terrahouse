var timer;
var validateCodeInfoFlag = -1;
$(function() {
	$(".house-publish-container .form-item .images-container ul li input:file")
			.change(onFileChange);
	$(
			".house-publish-container .form-item .images-container ul li span.icon-close")
			.click(removeLi);

	$('div.form-more').hide();
	$('#more-info').click(function() {

		if ($('div.form-more').is(':hidden')) {
			$('#more-info span.text').html("收起")
			$('#more-info .icon-up-down').removeClass('icon-up-down-down')
			$('#more-info .icon-up-down').addClass('icon-up-down-up')
		} else {
			$('#more-info span.text').html("更多信息")
			$('#more-info .icon-up-down').removeClass('icon-up-down-up')
			$('#more-info .icon-up-down').addClass('icon-up-down-down')
		}
		$('div.form-more').toggle('blind', 300);

		return false;
	});

	function validatePhoneNumber() {
		var success = true;
		var num = $('#phone-num').val();
		console.log('num.length = %o, num.substring = %o', num.length, num
				.substring(0, 1));
		if (num.length != 11 || num.substring(0, 1) != '1') {
			showPublishErrorMessage($('#phone-num'), '请填写格式正确的手机号码。');
			success = false;
		} else {
			showPublishErrorMessage($('#phone-num'), '');
		}
		return success;
	}

	$('#phone-num').blur(validatePhoneNumber);

	timer = $.timer(function() {
		if (validateCodeInfoFlag > 0) {
			$('#receive-validate-number').text(validateCodeInfoFlag + '秒后重新获取')
			validateCodeInfoFlag = validateCodeInfoFlag - 1;
		} else {
			$('#receive-validate-number').text('重新获取验证码');
			timer.stop();
		}
	});
	timer.set({
		time : 1000,
		autostart : false
	});
	$('#receive-validate-number').click(function() {
		if (!validatePhoneNumber()) {
			return false;
		}
		if (validateCodeInfoFlag <= 0) {
			validateCodeInfoFlag = 9;
			$(this).text(validateCodeInfoFlag + '秒后重新获取');
			validateCodeInfoFlag = validateCodeInfoFlag - 1;
			timer.play();
		} else {

		}
		return false;
	});
	$('#district-name').blur(function() {
		var $districtName = $(this);
		if ($districtName.val() == '') {
			showPublishErrorMessage($districtName, '请填写小区名称');
		} else {
			clearPublishErrorMessage($districtName);
		}
	});
	$('#district-address').blur(function() {
		var $districtAddress = $(this);
		if ($districtAddress.val() == '') {
			showPublishErrorMessage($districtAddress, '请填写小区地址');
		} else {
			clearPublishErrorMessage($districtAddress);
		}
	});
	$('#area').blur(function() {
		var $area = $(this);
		if ($area.val() == '') {
			showPublishErrorMessage($area, '请填写房屋面积');
		} else {
			clearPublishErrorMessage($area);
		}
	});
	$('#n-bedroom').blur(validateRooms);
	$('#n-parlor').blur(validateRooms);
	$('#n-bathroom').blur(validateRooms);
	$('#real-name').blur(function() {
		var $realName = $(this);
		if ($realName.val() == '') {
			showPublishErrorMessage($realName, '请填写您的姓名');
		} else {
			clearPublishErrorMessage($realName);
		}
	});
	$('#validate-code').blur(function() {
		var $validateCode = $(this);
		if ($validateCode.val() == '') {
			showPublishErrorMessage($validateCode, '请填写验证码');
		} else {
			if ($validateCode.val().length != 6) {
				showPublishErrorMessage($validateCode, '请输入6位验证码。');
			} else if (validateCodeInfoFlag < 0) {
				showPublishErrorMessage($validateCode, '请先获取验证码。');
			} else if (validateCodeInfoFlag == 0) {
				showPublishErrorMessage($validateCode, '超时，请重新获取验证码。');
			} else {
				clearPublishErrorMessage($validateCode);
			}

		}
	});
	$('form#form-publish').submit(
			function() {
				var $districtName = $('#district-name');
				var $districtAddress = $('#district-address');
				var $area = $('#area');
				var $nBedroom = $('#n-bedroom');
				var $nParlor = $('#n-parlor');
				var $nBathroom = $('#n-bathroom');
				var $realName = $('#real-name');
				var $phoneNum = $('#phone-num');
				var $validateCode = $('#validate-code');

				var success = true;
				if ($districtName.val() == '') {
					showPublishErrorMessage($districtName, '请填写小区名称');
					success = false;
				} else {
					clearPublishErrorMessage($districtName);
				}
				if ($districtAddress.val() == '') {
					showPublishErrorMessage($districtAddress, '请填写小区地址');
					success = false;
				} else {
					clearPublishErrorMessage($districtAddress);
				}
				if ($area.val() == '') {
					showPublishErrorMessage($area, '请填写房屋面积');
					success = false;
				} else {
					clearPublishErrorMessage($area);
				}
				if ($nBedroom.val() == '' || $nParlor.val() == ''
						|| $nBathroom.val() == '') {
					showPublishErrorMessage($nBathroom, '请填写居室信息');
					success = false;
				} else {
					clearPublishErrorMessage($nBathroom);
				}
				if ($realName.val() == '') {
					showPublishErrorMessage($realName, '请填写您的姓名');
					success = false;
				} else {
					clearPublishErrorMessage($realName);
				}
				if ($phoneNum.val() == '') {
					showPublishErrorMessage($phoneNum, '请填写格式正确的手机号码。');
					success = false;
				} else {
					clearPublishErrorMessage($phoneNum);
				}
				if ($phoneNum.val().length != 11
						|| $phoneNum.val().substring(0, 1) != '1') {
					showPublishErrorMessage($phoneNum, '请填写您的手机号码');
					success = false;
				} else {
					clearPublishErrorMessage($phoneNum);
				}
				if ($validateCode.val() == '') {
					showPublishErrorMessage($validateCode, '请填写验证码');
					success = false;
				} else {
					if ($validateCode.val().length != 6) {
						showPublishErrorMessage($validateCode, '请输入6位验证码。');
						success = false;
					} else if (validateCodeInfoFlag < 0) {
						showPublishErrorMessage($validateCode, '请先获取验证码。');
						success = false;
					} else if (validateCodeInfoFlag == 0) {
						showPublishErrorMessage($validateCode, '超时，请重新获取验证码。');
						success = false;
					} else {
						clearPublishErrorMessage($validateCode);
					}

				}

				if (success) {
					// 模拟post后
					if ($validateCode.val() != '123456') {
						showPublishErrorMessage($validateCode, '验证码输入有误。');
						success = false;
					} else {
						showPublishErrorMessage($validateCode, '');
					}
					return success;
				}
				return success;

			});
});

function validateRooms() {
	var $input = $(this);
	if ($input.val() == '') {
		showPublishErrorMessage($input, '请填写居室信息');
	} else {
		var $nBedroom = $('#n-bedroom');
		var $nParlor = $('#n-parlor');
		var $nBathroom = $('#n-bathroom');
		if ($nBedroom.val() == '' || $nParlor.val() == ''
				|| $nBathroom.val() == '') {
			// not clear errors.
		} else {
			clearPublishErrorMessage($nBathroom);
		}
	}
}
function showPublishErrorMessage($input, message) {
	var parent = $input.parent();
	var $error = parent.find('.error');
	if ($error.length == 0) {
		$error = $('<span></span>');
		$error.addClass('error');
		$error.appendTo(parent);
	}
	$error.text(message);
}

function clearPublishErrorMessage($input) {
	showPublishErrorMessage($input, '');
}
function onFileChange(e) {
	var fileName = $(this).val();
	console.log('fileName = %o', fileName);
	if (fileName == '') {
		$thisLi.css('background', '');
	} else {
		// upload file and its url returned;
		var returnedUrl = 'http://img1.yooyo.com/uploadfile/hotel/88574fe0118ea8b9faae7e96c6394fbc/147463/thumb_184_114_9_66948636-44d4-47b1-8a6a-7eb8c95b606f.jpg';
		var $thisLi = $(this).parent().parent();
		var $nextLi = $thisLi.clone();
		var $ul = $thisLi.parent();
		$thisLi.find('span.icon-input-file').remove();
		$thisLi.find('span.icon-close').show();
		$thisLi.css('background', "url('" + returnedUrl + "')");
		$nextLi.find('input:file').change(onFileChange);
		$nextLi.find('span.icon-close').change(removeLi);
		$nextLi.appendTo($ul);
	}
}

function removeLi(e) {
	$(this).parent().parent().remove();
}