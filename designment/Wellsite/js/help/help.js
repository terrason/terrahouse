$(function() {
	$('.help-nav ul.help-menu li').not('.hilight').hover(function() {
		$(this).addClass('hilight');
	}, function() {
		$(this).removeClass('hilight');
	});

	$('a.term').each(function(index, value) {
		var $this = $(value);
		$this.prop('href', '/help/terms#' + $(this).text());
	});
});