$(function() {
	$('#action-ask').click(function() {
		var text = $('#text-area-ask').val();
		if (text == '') {
			$('.error').text('内容不能为空');
			$('.error').show();
		} else {
			$('.error').text('');
			$('.error').hide();
			popupAlert('您的反馈我们已收到，感谢您宝贵的建议！')
		}
	});

	function popupAlert(text) {
		var $alert = $('.popup.alert');
		$alert.text(text);
		$alert.bPopup({
			modalClose : true,
			opacity : 0.6,
			positionStyle : 'fixed', // 'fixed' or 'absolute',
			fadeSpeed : 'slow', // can be a string ('slow'/'fast') or
			// int
			followSpeed : 'fast'
		// can be a string ('slow'/'fast') or int
		});
	}
});