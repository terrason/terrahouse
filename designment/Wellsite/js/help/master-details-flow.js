$(function() {

	$(
			'#help-details-container .master-details-flow .details .template div.title')
			.each(function() {
				var $li = $('<li>');
				$("<a href='#'></a>").text($(this).text()).appendTo($li);
				$li.appendTo('.master-details-flow .master ul');
			});
	$('.master-details-flow .master a').click(function() {
		var id = $(this).text();
		var $content = $('.master-details-flow .details .content');
		$content.html('');
		$content.html($('#' + id).html());
		$('#help-details-container').stop().scrollTo('100%', 300);

		displayLi3Text(id);

		return false;
	});
	$('.master-details-flow .details .back a').click(function() {
		$('#help-details-container').stop().scrollTo('0%', 300);

		var $ul = $('#help-top-nav ul');
		var $li2 = $ul.find('li:eq(1)');
		$li2.find('span').remove();
		var $li3 = $ul.find('li:eq(2)');
		$li3.remove();

		return false;
	});

	// 根据url中的id显示对应的详情
	var hash = window.location.hash;
	if (hash && hash != '') {
		console.log('hash=%o', hash);
		var $content = $('.master-details-flow .details .content');
		$content.html('');
		$content.html($(hash).html());
		$('#help-details-container').stop().scrollTo('100%', 0);

		displayLi3Text(hash.substring(1));
	}

	function displayLi3Text(text) {
		var $ul = $('#help-top-nav ul');
		var $li2 = $ul.find('li:eq(1)');
		if ($li2.find('span.triangle-right').length == 0) {
			var $span = $('<span>');
			$span.addClass('triangle-right');
			$span.appendTo($li2);
		}
		var $li3 = $ul.find('li:eq(2)');
		if ($li3.length == 0) {
			$li3 = $('<li>');
			$li3.appendTo($ul)
		}
		$li3.text(text);
	}
});