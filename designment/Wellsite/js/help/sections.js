$(function() {
	$('.section-more-action a').click(function() {
		var $this =$(this); 
		var $section = $this.parents('.section');
		var $moreContainer = $section.find('.section-more-container');
		var $iconUpDown = $section.find('.icon-up-down');
		if ($moreContainer.is(':hidden')) {
			$section.find('.section-more-action a span').html("收起");
			$iconUpDown.removeClass('icon-up-down-down')
			$iconUpDown.addClass('icon-up-down-up')
		} else {
			$section.find('.section-more-action a span').html("了解更多")
			$iconUpDown.removeClass('icon-up-down-up')
			$iconUpDown.addClass('icon-up-down-down')
		}
		$section.find('.section-more-container').toggle('blind', 300);

		return false;
	});
	
});