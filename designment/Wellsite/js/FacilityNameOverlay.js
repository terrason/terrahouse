function FacilityNameOverlay(point, text, offset) {
	this._point = point;
	this._text = text;
	this._offset = offset;
}
FacilityNameOverlay.prototype = new BMap.Overlay();
FacilityNameOverlay.prototype.initialize = function(map) {
	this._map = map;
	var div = this._div = document.createElement("div");
	div.style.position = "absolute";
	div.style.zIndex = BMap.Overlay.getZIndex(this._point.lat);
	div.style.padding = "0px";
	var span = this._span = document.createElement("div");
	span.style.padding = "4px";
	span.style.color = "#6e6e6e";
	span.style.backgroundColor = "#ffffff";
	span.style.border = "1px solid #ffffff";
	span.style.whiteSpace = "nowrap";
	span.style.MozUserSelect = "none";
	span.style.fontSize = "12px";
	span.style.borderRadius = "4px";
	div.appendChild(span);
	span.appendChild(document.createTextNode(this._text));
	var that = this;

	var arrow = this._arrow = document.createElement("div");
	arrow.style.width = "18px";
	arrow.style.height = "18px";
	arrow.style.borderTop = "9px solid #ffffff";
	arrow.style.borderLeft = "9px solid transparent";
	arrow.style.borderRight = "9px solid transparent";
	arrow.style.margin = "0px auto";
	div.appendChild(arrow);

	map.getPanes().labelPane.appendChild(div);

	return div;
}
FacilityNameOverlay.prototype.draw = function() {
	var map = this._map;
	var pixel = map.pointToOverlayPixel(this._point);
	var left = pixel.x - $(this._div).width() / 2;
	var top = pixel.y - 40;
	if(this._offset){
		left  = left + this._offset.width;
		top  = top + this._offset.height;
	}
	this._div.style.left = left+'px';
	this._div.style.top = top+'px';
}