var map = null;
var iconFacilities = [ {
	normal : 'images/icon-block-facility-park-default.png',
	hover : 'images/icon-block-facility-park-hover.png'
}, {
	normal : 'images/icon-block-facility-gas-station-default.png',
	hover : 'images/icon-block-facility-gas-station-hover.png'
}, {
	normal : 'images/icon-block-facility-hospital-default.png',
	hover : 'images/icon-block-facility-hospital-hover.png'
}, {
	normal : 'images/icon-block-facility-mall-default.png',
	hover : 'images/icon-block-facility-mall-hover.png'
}, {
	normal : 'images/icon-block-facility-super-market-default.png',
	hover : 'images/icon-block-facility-super-market-hover.png'
}, {
	normal : 'images/icon-block-facility-library-default.png',
	hover : 'images/icon-block-facility-library-hover.png'
}, {
	normal : 'images/icon-block-facility-car-park-default.png',
	hover : 'images/icon-block-facility-car-park-hover.png'
} ];

$(function() {

	$('.sticky-container').waypoint('sticky');
	$('#quick-nav').singlePageNav({
		offset : $('.sticky-container').outerHeight(),
		filter : ':not(.external)',
		beforeStart : function() {
			console.log('begin scrolling');
		},
		onComplete : function() {
			console.log('done scrolling');
		}
	});

	$('#descriptions .block-decription-container .content p').each(
			function(index, element) {
				if (index != 0) {
					$(element).hide();
				}
			});

	$('#descriptions .block-decription-container .full-text-switcher a').click(
			function() {
				var $this = $(this);
				var $toggle = $this.find('span.icon-less-more-toggle-blue');
				if ($toggle.hasClass('icon-less-more-toggle-blue-more')) {
					$toggle.switchClass('icon-less-more-toggle-blue-more',
							'icon-less-more-toggle-blue-less');
					$('#descriptions .block-decription-container .content p')
							.each(function(index, element) {
								$(element).show();
							});
					$this.find('span.text').text('隐藏全文');
				} else {
					$toggle.switchClass('icon-less-more-toggle-blue-less',
							'icon-less-more-toggle-blue-more');
					$('#descriptions .block-decription-container .content p')
							.each(function(index, element) {
								if (index != 0) {
									$(element).hide();
								}
							});
					$this.find('span.text').text('显示全文');
				}
				return false;
			});

	map = new BMap.Map("map"); // 创建Map实例
	var point = new BMap.Point(121.381205, 31.116648); // 创建点坐标
	map.centerAndZoom(point, 14); // 初始化地图,设置中心点坐标和地图级别。
	map.enableScrollWheelZoom();
	map.addControl(new BMap.NavigationControl()); // 添加默认缩放平移控件
	map.addControl(new BMap.ScaleControl());// 添加默认比例尺控件

	// var bdary = new BMap.Boundary();
	// bdary.get("上海闵行区", function(rs) { // 获取行政区域
	// // map.clearOverlays(); // 清除地图覆盖物
	// var count = rs.boundaries.length; // 行政区域的点有多少个
	// for (var i = 0; i < count; i++) {
	// var ply = new BMap.Polygon(rs.boundaries[i], {
	// strokeWeight : 2,
	// strokeColor : "#227dda",
	// strokeOpacity : 1,
	// fillColor : '#227dda',
	// fillOpacity : 0.2
	// }); // 建立多边形覆盖物
	// map.addOverlay(ply); // 添加覆盖物
	// map.setViewport(ply.getPath()); // 调整视野
	// map.setZoom(14); // 临时测试
	// }
	// });

	var testBoundaries = '121.384458, 31.142556; 121.378996, 31.141444; 121.370893, 31.138384; 121.36174, 31.134041; 121.365423, 31.130571; 121.367201, 31.12755; 121.370004, 31.120888; 121.362566, 31.118616; 121.354445, 31.114257; 121.35838, 31.103775; 121.375124, 31.108939; 121.379311, 31.099323; 121.383784, 31.090734; 121.396064, 31.095527; 121.407966, 31.100405; 121.40405, 31.109928; 121.394941, 31.12384; 121.384458, 31.142556';
	var ply = new BMap.Polygon(testBoundaries, {
		strokeWeight : 3,
		strokeColor : "#227dda",
		strokeOpacity : 1,
		fillColor : '#227dda',
		fillOpacity : 0.2
	}); // 建立多边形覆盖物
	map.addOverlay(ply); // 添加覆盖物
	// map.setViewport(ply.getPath()); // 调整视野
	console.log('map.getCenter() = %o', map.getCenter());

	var facilities = [ {
		lat : 31.136075,
		lng : 121.385733,
		type : 0,
		name : '黎安公园'
	}, {
		lat : 31.110749,
		lng : 121.393256,
		type : 0,
		name : '莘城中央公园'
	}, {
		lat : 31.108036,
		lng : 121.379785,
		type : 0,
		name : '莘庄公园'
	}, {
		lat : 31.114228,
		lng : 121.384662,
		type : 4,
		name : '乐购莘东店'
	}, {
		lat : 31.124739,
		lng : 121.378563,
		type : 4,
		name : '乐购莘庄店'
	}, {
		lat : 31.105741,
		lng : 121.370599,
		type : 4,
		name : '乐购莘松店'
	}, {
		lat : 31.137783,
		lng : 121.37959,
		type : 4,
		name : '大润发港澳店'
	}, {
		lat : 31.112527,
		lng : 121.394011,
		type : 4,
		name : '家乐福莘庄店'
	}, {
		lat : 31.104156,
		lng : 121.394691,
		type : 4,
		name : '乐天玛特春申店'
	}, {
		lat : 31.096542,
		lng : 121.397758,
		type : 4,
		name : '农工商银都路店'
	}, {
		lat : 31.112821,
		lng : 121.381788,
		type : 4,
		name : '世纪联华莘庄店'
	}, {
		lat : 31.107905,
		lng : 121.378644,
		type : 4,
		name : '世纪联华莘松路店'
	}, {
		lat : 31.113748,
		lng : 121.382731,
		type : 2,
		name : '闵行区中心医院'
	}, {
		lat : 31.112967,
		lng : 121.392978,
		type : 6,
		name : '百安居莘庄店'
	}, {
		lat : 31.112156,
		lng : 121.393122,
		type : 3,
		name : '仲盛世界商场'
	}, {
		lat : 31.11405,
		lng : 121.385666,
		type : 3,
		name : '莘庄凯德龙之梦'
	}, {
		lat : 31.111321,
		lng : 121.395611,
		type : 5,
		name : '闵行区图书馆'
	} ];

	// 设施地图加这样一个功能，当比例尺从1公里缩小至2公里时，设施的图
	// 标不再用原来的大图标，而将各类设施统一用小蓝点表示。这样做的目的是，当用户将比例
	// 尺缩小的很小时，所有图标堆在一块，使用的体验特别的差。
	// PS：当变成小蓝点时，原本的点击即显示该设施名称的功能不变，保留。
	map.addEventListener("zoomend", function() {
		map.clearOverlays();
		var zoom = map.getZoom();
		if (zoom <= 13) {
			addFacilitiesSmallIcons();
		} else {
			addFacilitiesBigIcons();
		}
	});
	addFacilitiesBigIcons();

	$('#block-images div.section div').each(function(index, element) {
		$(element).addClass('clearfix');
		var $imgs = $(element).find('img');
		if ($imgs.length == 2) {
			$imgs.each(function(i, img) {
				$(img).css('width', '490px');
				if (i == 0) {
					$(img).css('float', 'left')
				} else if (i == 1) {
					$(img).css('float', 'right')
				}
			});
		}
	});

	$('#descriptions .add-decription-container #i-wanna-say-something-button')
			.click(function() {
				if (Math.random() < 0.5) {
					$('form .form-group.input-email').hide();
				} else {
					$('form .form-group.input-email').show();
				}
				$('#i-wanna-say-something-popup').bPopup({
					modalClose : false,
					opacity : 0.6,
					positionStyle : 'absolute', // 'fixed' or 'absolute',
					fadeSpeed : 'slow', // can be a string ('slow'/'fast') or
					// int
					followSpeed : 1500
				// can be a string ('slow'/'fast') or int
				});

			});
	$('form button#close').click(function() {
		$('#i-wanna-say-something-popup').bPopup().close();
	});
	$('form button#submit').click(function() {
		$('form .email-blank').hide();
		$('form .email-error').hide();
		var $textarea = $('textarea.say-content');
		$textarea.parent().removeClass('has-error');

		var validate = true;
		var $email = $('form .form-group.input-email input');
		if ($('form .form-group.input-email').is(":visible")) {
			if ($email.val() == '') {
				$('form .email-blank').show();
				validate = false;
			} else if (!validateEmail($email.val())) {
				$('form .email-error').show();
				validate = false;
			}
		}

		if ($textarea.val() == '') {
			$textarea.parent().addClass('has-error');
		} else if (validate) {
			$('#i-wanna-say-something-popup').bPopup().close();
			popupAlert('信息已提交成功')
		}

	});

	function popupAlert(text) {
		var $alert = $('.popup.alert');
		$alert.text(text);
		$alert.bPopup({
			modalClose : true,
			opacity : 0.6,
			positionStyle : 'absolute', // 'fixed' or 'absolute',
			fadeSpeed : 'slow', // can be a string ('slow'/'fast') or
			// int
			followSpeed : 'fast'
		// can be a string ('slow'/'fast') or int
		});
	}

	function addFacilitiesBigIcons() {
		$.each(facilities, function(index, value) {
			var point = new BMap.Point(value.lng, value.lat);
			var myIcon = new BMap.Icon(iconFacilities[value.type].normal,
					new BMap.Size(43, 57));
			var marker = new BMap.Marker(point, {
				icon : myIcon,
				offset : new BMap.Size(-22, -29)
			});
			marker.tag = value;
			map.addOverlay(marker);

			marker.addEventListener('mouseover',
					function(type, target, point, pixel) {
						this.setIcon(new BMap.Icon(
								iconFacilities[this.tag.type].hover,
								new BMap.Size(43, 57)));

						if (!marker.nameOverlay) {
							var nameOverlay = new FacilityNameOverlay(marker
									.getPosition(), value.name, new BMap.Size(
									-22, -52));
							marker.nameOverlay = nameOverlay;
							map.addOverlay(marker.nameOverlay);
						} else {
							marker.nameOverlay.show();
						}
					})
			marker.addEventListener('mouseout', function(type, target, point,
					pixel) {
				this.setIcon(new BMap.Icon(
						iconFacilities[this.tag.type].normal, new BMap.Size(43,
								57)));

				if (marker.nameOverlay && marker.nameOverlay.isVisible()) {
					marker.nameOverlay.hide();
				}
			});
		});
	}

	function addFacilitiesSmallIcons() {
		var dotNormal = 'images/icon-marker-small-hover.png';
		var dotHover = 'images/icon-marker-small-default.png';
		var dotWidht = 17;
		var dotHeigt = 17;
		$.each(facilities, function(index, value) {
			var point = new BMap.Point(value.lng, value.lat);
			var myIcon = new BMap.Icon(dotNormal, new BMap.Size(dotWidht,
					dotHeigt));
			var marker = new BMap.Marker(point, {
				icon : myIcon,
				offset : new BMap.Size(-dotWidht / 2, -dotHeigt / 2)
			});
			marker.tag = value;
			map.addOverlay(marker);

			marker.addEventListener('mouseover', function(type, target, point,
					pixel) {
				this.setIcon(new BMap.Icon(dotHover, new BMap.Size(dotWidht,
						dotHeigt)));

				if (!marker.nameOverlay) {
					var nameOverlay = new FacilityNameOverlay(marker
							.getPosition(), value.name, new BMap.Size(
							-dotWidht / 2, -dotHeigt / 2));
					marker.nameOverlay = nameOverlay;
					map.addOverlay(marker.nameOverlay);
				} else {
					marker.nameOverlay.show();
				}

			})
			marker.addEventListener('mouseout', function(type, target, point,
					pixel) {
				this.setIcon(new BMap.Icon(dotNormal, new BMap.Size(dotWidht,
						dotHeigt)));

				if (marker.nameOverlay && marker.nameOverlay.isVisible()) {
					marker.nameOverlay.hide();
				}
			});
		});

	}
});