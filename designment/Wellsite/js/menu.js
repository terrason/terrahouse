$(function() {
	$('#navigation #actions>ul>li>ul>li>a').click(function() {
		$(this).find('span.bubble-number').remove();
		var $userInfoLi = $('li.user-info-li');
		var count = $userInfoLi.find('span.bubble-number').length;
		if (count == 0) {
			$userInfoLi.find('span.icon-unread-flag').hide();
		}
	});

	positionFooter();

	$(window).scroll(positionFooter).resize(positionFooter);

	window.setInterval(positionFooter, 200);

});
function positionFooter() {
	var footerHeight = 0, footerTop = 0, $footer = $("#footer");
	footerHeight = $footer.height();

	var bodyHeight = $(document.body).height();
	var windowHeight = $(window).height();
	if ($footer.css('position') == 'absolute') {
		bodyHeight = bodyHeight + footerHeight;
	}
	if ((bodyHeight) < windowHeight) {
		$footer.css({
			position : "absolute",
			width : '100%',
			bottom : '0px'
		})
	} else {
		$footer.css({
			position : "static"
		})
	}

}
