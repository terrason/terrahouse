/*
SQLyog Ultimate v11.24 (32 bit)
MySQL - 5.0.18-nt : Database - wns
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`wns` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `wns`;

/*Table structure for table `area` */

DROP TABLE IF EXISTS `area`;

CREATE TABLE `area` (
  `ID` decimal(16,0) NOT NULL,
  `AREA_NAME` varchar(36) default NULL,
  `AREA_DESC` varchar(128) default NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `area` */

insert  into `area`(`ID`,`AREA_NAME`,`AREA_DESC`) values ('22263807545','闵行区','ac');

/*Table structure for table `axis` */

DROP TABLE IF EXISTS `axis`;

CREATE TABLE `axis` (
  `ID` decimal(16,0) NOT NULL,
  `S_XY` varchar(100) default NULL,
  `S_NAME` varchar(100) default NULL,
  `S_TYPE` varchar(100) default NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `axis` */

insert  into `axis`(`ID`,`S_XY`,`S_NAME`,`S_TYPE`) values ('1','(121.385733,31.136075)','黎安公园','0'),('2','(121.393256,31.110749)','莘城中央公园','0'),('3','(121.379785,31.108036)','莘庄公园','0'),('4','(121.382731,31.113748)','闵行区中心医院','2'),('5','(121.393122,31.112156)','仲盛世界商场','3'),('6','(121.385666,31.11405)','莘庄凯德龙之梦','3'),('7','(121.384662,31.114228)','乐购莘东店','4'),('8','(121.378563,31.124739)','乐购莘庄店','4'),('9','(121.370599,31.105741)','乐购莘松店','4'),('10','(121.37959,31.137783)','大润发港澳店','4'),('11','(121.394011,31.112527)','家乐福莘庄店','4'),('12','(121.394691,31.104156)','乐天玛特春申店','4');

/*Table structure for table `calendar` */

DROP TABLE IF EXISTS `calendar`;

CREATE TABLE `calendar` (
  `ID` decimal(16,0) NOT NULL,
  `task` varchar(38) default NULL,
  `builddate` varchar(38) default NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `calendar` */

insert  into `calendar`(`ID`,`task`,`builddate`) values ('24722170108','gmmm','2013-11-1'),('36210648004','3333','2013-11-2'),('62075431051','mmmm','2013-11-1'),('65502808770','4444444','2013-11-1'),('94826949942','444444444','2013-11-11');

/*Table structure for table `community` */

DROP TABLE IF EXISTS `community`;

CREATE TABLE `community` (
  `ID` decimal(16,0) NOT NULL,
  `AREA_ID` decimal(16,0) default NULL,
  `COMMUNITY_NAME` varchar(128) default NULL,
  `COMMUNITY_DESC` blob,
  `COMMUNTIY_DL_INFO` blob,
  `COMMUNTIY_XY` varchar(128) default NULL,
  `COMMUNTIY_BJXY` varchar(1024) default NULL,
  `COMMUNTIY_GDS` varchar(128) default NULL,
  `DT_RMGC` varchar(128) default NULL,
  `GJ_RMGC` varchar(128) default NULL,
  `DT_LJZ` varchar(128) default NULL,
  `GJ_LJZ` varchar(128) default NULL,
  `DT_XJH` varchar(128) default NULL,
  `GJ_XJH` varchar(128) default NULL,
  `DT_JAS` varchar(128) default NULL,
  `GJ_JAS` varchar(128) default NULL,
  `DT_WJC` varchar(128) default NULL,
  `GJ_WJC` varchar(128) default NULL,
  `COMMUNITY_COMMENT` blob,
  `IS_INVODE` varchar(2) default NULL,
  `PICTURE` varchar(128) default NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `community` */

insert  into `community`(`ID`,`AREA_ID`,`COMMUNITY_NAME`,`COMMUNITY_DESC`,`COMMUNTIY_DL_INFO`,`COMMUNTIY_XY`,`COMMUNTIY_BJXY`,`COMMUNTIY_GDS`,`DT_RMGC`,`GJ_RMGC`,`DT_LJZ`,`GJ_LJZ`,`DT_XJH`,`GJ_XJH`,`DT_JAS`,`GJ_JAS`,`DT_WJC`,`GJ_WJC`,`COMMUNITY_COMMENT`,`IS_INVODE`,`PICTURE`) values ('17582581954','22263807545','FFF','GGGGGGG<span style=\"background-color:#E53333;\">GGGGGGGGGGGGGGGG</span>GGGGGGGG','HHHHHHHHHHHH<strong><u>HHHHHHHHHHH</u></strong>HHHH','(33,444)','(444,555);(5555,444)','3,4,5','43','45','65','79','45','34','67','76','76','89','hhhhhh<span style=\"background-color:#FF9900;\">hhhhjjjjjjjjjjjjjjjjjjjjjjjjjj</span>jjjjjjjjjjjjjjj','是','files/upload/2014/04/30/5a3a7d0a-b054-47fa-8a5c-48ff66fbc766.jpg');

/*Table structure for table `community_pinglun` */

DROP TABLE IF EXISTS `community_pinglun`;

CREATE TABLE `community_pinglun` (
  `ID` decimal(16,0) default NULL,
  `COMMUNITY` decimal(16,0) default NULL,
  `comment` varchar(1000) default NULL,
  `USER_id` decimal(16,0) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `community_pinglun` */

insert  into `community_pinglun`(`ID`,`COMMUNITY`,`comment`,`USER_id`) values ('111','17582581954','aaaaaaa','-514876807');

/*Table structure for table `csince_record` */

DROP TABLE IF EXISTS `csince_record`;

CREATE TABLE `csince_record` (
  `ID` decimal(16,0) default NULL,
  `U_ID` decimal(16,0) default NULL,
  `CUSTOM_ORDER_ID` decimal(16,0) default NULL,
  `SURP_VALUE` decimal(16,0) default NULL,
  `LAND_ID` decimal(16,0) default NULL,
  `RTIME` datetime default NULL,
  `COMMENT` varchar(128) default NULL,
  `IS_READ` varchar(36) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `csince_record` */

insert  into `csince_record`(`ID`,`U_ID`,`CUSTOM_ORDER_ID`,`SURP_VALUE`,`LAND_ID`,`RTIME`,`COMMENT`,`IS_READ`) values ('90376457470','187300','1111','3','29994232235','2013-11-12 10:12:29','首次注册奖励信誉值3分','否');

/*Table structure for table `custom_order` */

DROP TABLE IF EXISTS `custom_order`;

CREATE TABLE `custom_order` (
  `ID` decimal(16,0) NOT NULL,
  `U_ID` decimal(16,0) default NULL,
  `custom_name` varchar(36) default NULL,
  `custom_phone` varchar(36) default NULL,
  `H_ID` decimal(16,0) default NULL,
  `H_DATE` date default NULL,
  `H_TIME` varchar(36) default NULL COMMENT '0：上午 1：下午 2：晚上',
  `STATE` varchar(1) default NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `custom_order` */

insert  into `custom_order`(`ID`,`U_ID`,`custom_name`,`custom_phone`,`H_ID`,`H_DATE`,`H_TIME`,`STATE`) values ('-382494349','899650522','house108@163.com','18154227651','-1316131119','2014-04-30','0','0');

/*Table structure for table `fsince_record` */

DROP TABLE IF EXISTS `fsince_record`;

CREATE TABLE `fsince_record` (
  `ID` decimal(16,0) NOT NULL,
  `U_ID` decimal(16,0) default NULL,
  `SURP_VALUE` decimal(16,0) default NULL,
  `LOST_TIME` varchar(36) default NULL,
  `CUSTOM_ORDER_IDS` varchar(128) default NULL,
  `RTIME` datetime default NULL,
  `COMMENT` varchar(128) default NULL,
  `IS_READ` varchar(36) default NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `fsince_record` */

insert  into `fsince_record`(`ID`,`U_ID`,`SURP_VALUE`,`LOST_TIME`,`CUSTOM_ORDER_IDS`,`RTIME`,`COMMENT`,`IS_READ`) values ('545045548','29994232235','-1','2011-11-1 上午','187300,29994232235','2013-11-12 10:40:18','2011-11-1 上午爽约 扣除信誉值1分','否');

/*Table structure for table `house_info` */

DROP TABLE IF EXISTS `house_info`;

CREATE TABLE `house_info` (
  `ID` decimal(16,0) default NULL,
  `user_ID` decimal(16,0) default NULL,
  `h_user_name` varchar(100) default NULL,
  `h_user_phone` varchar(100) default NULL,
  `H_NAME` varchar(60) default NULL,
  `H_CODE` varchar(60) default NULL,
  `H_PLOAT` varchar(36) default NULL,
  `H_ADDRESS` varchar(500) default NULL,
  `H_COMMUNITY` varchar(60) default NULL,
  `H_COMMUNITY_ID` decimal(16,0) default NULL,
  `H_AREA` varchar(60) default NULL,
  `H_AREA_ID` decimal(16,0) default NULL,
  `H_MJ` varchar(60) default NULL,
  `H_SH` decimal(2,0) default NULL,
  `H_TH` decimal(2,0) default NULL,
  `H_WH` decimal(2,0) default NULL,
  `H_INSID_DETAIL` varchar(600) default NULL,
  `H_LIKES` decimal(10,0) default NULL,
  `H_GM_MONEY` varchar(20) default NULL,
  `H_TOTAL_MONEY` varchar(20) default NULL,
  `H_TYPE` varchar(60) default NULL,
  `H_PRICE` varchar(20) default NULL,
  `H_DECORATE` varchar(600) default NULL,
  `H_FLOOR` varchar(60) default NULL,
  `H_AGE` varchar(60) default NULL,
  `H_LOCATION_VALUE` varchar(5) default NULL,
  `H_TYPE_VALUE` varchar(5) default NULL,
  `H_DECORATE_VALUE` varchar(5) default NULL,
  `H_AREA_VALUE` varchar(5) default NULL,
  `H_AGE_VALUE` varchar(5) default NULL,
  `H_FLOOR_VALUE` varchar(5) default NULL,
  `H_TOTAL_VALUE` varchar(5) default NULL,
  `SEAT_XY` varchar(100) default NULL,
  `DTS` varchar(600) default NULL,
  `GJS` varchar(600) default NULL,
  `IN_WEIYI` varchar(36) default NULL,
  `IN_SEAT` varchar(36) default NULL,
  `IS_RATE` varchar(36) default NULL,
  `TO_RMGC_GJ` decimal(3,0) default NULL,
  `TO_RMGC_DS` decimal(3,0) default NULL,
  `TO_LJZ_GJ` decimal(3,0) default NULL,
  `TO_LJZ_DS` decimal(3,0) default NULL,
  `TO_XJH_GJ` decimal(3,0) default NULL,
  `TO_XJH_DS` decimal(3,0) default NULL,
  `TO_JAS_GJ` decimal(3,0) default NULL,
  `TO_JAS_DS` decimal(3,0) default NULL,
  `TO_WJC_GJ` decimal(3,0) default NULL,
  `TO_WJC_DS` decimal(3,0) default NULL,
  `H_DETAIL` blob,
  `COMM_DETAIL` blob,
  `ARROUND_DETAIL` blob,
  `H_TRAFFIC` blob,
  `H_HXT` varchar(160) default NULL,
  `H_PIC` varchar(160) default NULL,
  `H_PIC1` varchar(160) default NULL,
  `H_PIC2` varchar(160) default NULL,
  `H_PIC3` varchar(160) default NULL,
  `H_PIC4` varchar(160) default NULL,
  `H_PIC5` varchar(160) default NULL,
  `H_PIC6` varchar(160) default NULL,
  `H_PIC7` varchar(160) default NULL,
  `H_PIC8` varchar(160) default NULL,
  `H_PIC9` varchar(160) default NULL,
  `H_PIC10` varchar(160) default NULL,
  `H_PIC11` varchar(160) default NULL,
  `H_PIC12` varchar(160) default NULL,
  `H_PIC13` varchar(160) default NULL,
  `H_PIC14` varchar(160) default NULL,
  `H_PIC15` varchar(160) default NULL,
  `H_PIC16` varchar(160) default NULL,
  `H_PIC17` varchar(160) default NULL,
  `H_PIC18` varchar(160) default NULL,
  `H_PIC19` varchar(160) default NULL,
  `H_STATE` decimal(1,0) default NULL,
  `H_SALE_STATE` decimal(1,0) default NULL,
  `H_DESC` decimal(10,0) default NULL,
  `H_CREATE_TIME` date default NULL,
  `H_TJ` decimal(1,0) default NULL,
  `h_hxt1` varchar(160) default NULL,
  `h_hxt2` varchar(160) default NULL,
  `h_hxt3` varchar(160) default NULL,
  `h_hxt4` varchar(160) default NULL,
  `h_hxt5` varchar(160) default NULL,
  `h_hxt6` varchar(160) default NULL,
  `h_hxt7` varchar(160) default NULL,
  `h_hxt8` varchar(160) default NULL,
  `h_hxt9` varchar(160) default NULL,
  `h_hxt10` varchar(160) default NULL,
  `h_hxt11` varchar(160) default NULL,
  `h_hxt12` varchar(160) default NULL,
  `h_hxt13` varchar(160) default NULL,
  `h_hxt14` varchar(160) default NULL,
  `h_hxt15` varchar(160) default NULL,
  `h_hxt16` varchar(160) default NULL,
  `h_hxt17` varchar(160) default NULL,
  `h_hxt18` varchar(160) default NULL,
  `h_hxt19` varchar(160) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `house_info` */

insert  into `house_info`(`ID`,`user_ID`,`h_user_name`,`h_user_phone`,`H_NAME`,`H_CODE`,`H_PLOAT`,`H_ADDRESS`,`H_COMMUNITY`,`H_COMMUNITY_ID`,`H_AREA`,`H_AREA_ID`,`H_MJ`,`H_SH`,`H_TH`,`H_WH`,`H_INSID_DETAIL`,`H_LIKES`,`H_GM_MONEY`,`H_TOTAL_MONEY`,`H_TYPE`,`H_PRICE`,`H_DECORATE`,`H_FLOOR`,`H_AGE`,`H_LOCATION_VALUE`,`H_TYPE_VALUE`,`H_DECORATE_VALUE`,`H_AREA_VALUE`,`H_AGE_VALUE`,`H_FLOOR_VALUE`,`H_TOTAL_VALUE`,`SEAT_XY`,`DTS`,`GJS`,`IN_WEIYI`,`IN_SEAT`,`IS_RATE`,`TO_RMGC_GJ`,`TO_RMGC_DS`,`TO_LJZ_GJ`,`TO_LJZ_DS`,`TO_XJH_GJ`,`TO_XJH_DS`,`TO_JAS_GJ`,`TO_JAS_DS`,`TO_WJC_GJ`,`TO_WJC_DS`,`H_DETAIL`,`COMM_DETAIL`,`ARROUND_DETAIL`,`H_TRAFFIC`,`H_HXT`,`H_PIC`,`H_PIC1`,`H_PIC2`,`H_PIC3`,`H_PIC4`,`H_PIC5`,`H_PIC6`,`H_PIC7`,`H_PIC8`,`H_PIC9`,`H_PIC10`,`H_PIC11`,`H_PIC12`,`H_PIC13`,`H_PIC14`,`H_PIC15`,`H_PIC16`,`H_PIC17`,`H_PIC18`,`H_PIC19`,`H_STATE`,`H_SALE_STATE`,`H_DESC`,`H_CREATE_TIME`,`H_TJ`,`h_hxt1`,`h_hxt2`,`h_hxt3`,`h_hxt4`,`h_hxt5`,`h_hxt6`,`h_hxt7`,`h_hxt8`,`h_hxt9`,`h_hxt10`,`h_hxt11`,`h_hxt12`,`h_hxt13`,`h_hxt14`,`h_hxt15`,`h_hxt16`,`h_hxt17`,`h_hxt18`,`h_hxt19`) values ('-645101526','899650522','house108@163.com','18154227651','好房子0',NULL,'好房子','安徽省合肥市','莘庄',NULL,'闵行',NULL,'89','2','2','1','回复哈市地方',NULL,'67','89',NULL,NULL,'简单装修',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'未知','未知','未知',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'GGGGG<span style=\"background-color:#4C33E5;\">GGGGGGGGGGGGGGG</span>GGGGG',NULL,'files/upload/2014/04/06/b4707313-022e-4c3a-8280-90fa27341bbc.jpg','files/upload/2014/04/06/d6cca0a0-f767-42de-9610-743dc72cc1d7.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1','0','0','2014-04-30','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('968602131','899650522','house108@163.com','18154227651','hhh',NULL,'广告歌','闵行区安徽广告歌','安徽',NULL,'闵行区',NULL,'56','1','1','1','',NULL,'55','89',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'(121.425024,31.093538)',NULL,NULL,'未知','未知','未知',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'files/upload/2014/04/09/831ab0f4-8e62-44a7-bafb-853b24950769.jpg','files/upload/2014/04/09/a7e43dca-1f05-4323-91c6-1c9fdeb26923.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1','0','0','2014-04-09','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('845235327','899650522','house108@163.com',NULL,'嘎嘎嘎',NULL,'fff','fddsf','莘庄',NULL,'闵行区',NULL,'67','2','1','1','gadg飞洒发打发',NULL,'56','78',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'120',NULL,NULL,NULL,'未知','未知','未知',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'files/upload/2014/03/16/6b5612a8-e63a-47e0-8360-fa3c63314224.jpg','files/upload/2014/04/06/45f94811-db80-4847-9278-5635dbf3da87.jpg','files/upload/2014/03/16/c490693f-9d7f-4671-b26c-5bb7bedae72d.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1','0','0','2014-04-06','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('-1316131119','899650522','house108@163.com','18154227651','好房子','hfz','哈啊哈','安徽省合肥市哈啊哈','合肥市',NULL,'安徽省',NULL,'78','2','1','1','打发第三方',NULL,'55','63',NULL,NULL,NULL,NULL,NULL,'0','67','54','55','67','78','100','(117.282699,31.866942)',NULL,NULL,'未知','未知','未知','56','56','43','55','33','22','11','55','66','77','<div class=\"paragraph\" style=\"font-size:14px;color:#707070;font-family:\'Microsoft YaHei\', ΢���ź�, \'Microsoft JhengHei\', ����ϸ��, STHeiti, MingLiu, ����;background-color:#FFFFFF;\">\r\n ����ס�����������������Ǳ�׼���ϱ�ͨ���ͣ�����������λ���ϱߣ������ʹ���λ�ڱ��ߣ�ϴ�ּ�λ��������֮�䡣����ס���ϱ�ͨ͸��ÿ�䷿�䶼��ȫ����ơ����ݽ������81ƽ�ף���Ϊû�е��ݺ���������Ĺ�̯��������ݵĵ÷��ʺܸߡ���Ȼ��2¥�����ɹ�á������Ǽ�װ�ޣ�������һ��װ��Ҳ��Щ��ͷ�������Ļ�����������ʩ��ȫ����ֱ����ס��\r\n</div>\r\n<div class=\"paragraph\" style=\"font-size:14px;color:#707070;font-family:\'Microsoft YaHei\', ΢���ź�, \'Microsoft JhengHei\', ����ϸ��, STHeiti, MingLiu, ����;background-color:#FFFFFF;\">\r\n ���������ڶ������ʵ�Ʈ���������õ�ʱ�������������Ȳ衢���������졣���������ų�����Ҳʮ�ַ��㡣ǰ��������̨Ϊϴ�º���ɹ�ṩ�˸���Ŀռ䡣\r\n</div>','<div class=\"paragraph\" style=\"font-size:14px;color:#707070;font-family:\'Microsoft YaHei\', ΢���ź�, \'Microsoft JhengHei\', ����ϸ��, STHeiti, MingLiu, ����;background-color:#FFFFFF;\">\r\n  С������ݷׯ������Ȧ����֮�κ���ʢ�̳ǣ�������1.5��������ܵ���Ȧ������ľ�������ı������С������ݷׯ��Сѧ����������ҽԺ��ΪС���ľ����ṩ�����õ�ѧ����ҽ��������С�����Ű��׸���������ʩȱ��������·����վ�Աߵ��������������С���Ĵ��ͳ��У�����Լ10����ʱ�䡣С�����ͷǳ����㣬�ڰ빫��֮��Ļ��ɹ�·�Ͼ���2������վ��\r\n</div>\r\n<div class=\"paragraph\" style=\"font-size:14px;color:#707070;font-family:\'Microsoft YaHei\', ΢���ź�, \'Microsoft JhengHei\', ����ϸ��, STHeiti, MingLiu, ����;background-color:#FFFFFF;\">\r\n С�����ų�����һ�����򳡺�һЩ�������ģ�ϲ�������������˿�����ĩ���������򣬻��ֹǡ����������߲��г�������ƽ�յĲɹ���\r\n</div>','<div class=\"paragraph\" style=\"font-size:14px;color:#707070;font-family:\'Microsoft YaHei\', ΢���ź�, \'Microsoft JhengHei\', ����ϸ��, STHeiti, MingLiu, ����;background-color:#FFFFFF;\">\r\n С������ݷׯ������Ȧ����֮�κ���ʢ�̳ǣ�������1.5��������ܵ���Ȧ������ľ�������ı������С������ݷׯ��Сѧ����������ҽԺ��ΪС���ľ����ṩ�����õ�ѧ����ҽ��������С�����Ű��׸���������ʩȱ��������·����վ�Աߵ��������������С���Ĵ��ͳ��У�����Լ10����ʱ�䡣С�����ͷǳ����㣬�ڰ빫��֮��Ļ��ɹ�·�Ͼ���2������վ��\r\n</div>\r\n<div class=\"paragraph\" style=\"font-size:14px;color:#707070;font-family:\'Microsoft YaHei\', ΢���ź�, \'Microsoft JhengHei\', ����ϸ��, STHeiti, MingLiu, ����;background-color:#FFFFFF;\">\r\n С�����ų�����һ�����򳡺�һЩ�������ģ�ϲ�������������˿�����ĩ���������򣬻��ֹǡ����������߲��г�������ƽ�յĲɹ���\r\n</div>','<span style=\"color:#707070;font-family:\'Microsoft YaHei\', ΢���ź�, \'Microsoft JhengHei\', ����ϸ��, STHeiti, MingLiu, ����;font-size:14px;line-height:24px;background-color:#FFFFFF;\">����7��8���Ӽ��ɵ��ﴺ��·����վ��5���ߣ���Ϊ��ƽʱ�ĳ����ṩ�˱�ݵĹ����ͨ��С�����ſ���ͨ��ݷׯ��708·������������ݷׯ��֮�κ�ݷׯ����վ�������ٶ��߼����ӣ���������·����վ��λ���ж���ͨ����һ�ͺ�����Ŧ�Ĺ�����·��&nbsp;</span><br />\r\n<span style=\"color:#707070;font-family:\'Microsoft YaHei\', ΢���ź�, \'Microsoft JhengHei\', ����ϸ��, STHeiti, MingLiu, ����;font-size:14px;line-height:24px;background-color:#FFFFFF;\">С�������Ĺ�·����Ҳʮ�ַ�����ɹ�·���ӻ��ɸ߼ܣ�ֱͨ��һ���С��ڻ��߼�·�����Ҫȥ��Χ����������Ҳ�ɴӴ���·ֱ����S20�⻷�߼�·��&nbsp;</span><br />\r\n<span style=\"color:#707070;font-family:\'Microsoft YaHei\', ΢���ź�, \'Microsoft JhengHei\', ����ϸ��, STHeiti, MingLiu, ����;font-size:14px;line-height:24px;background-color:#FFFFFF;\">������˵��С���Ľ�ͨ������⻷�������Χ�ڻ�����ǳ������ġ�</span>','files/upload/2014/04/09/d3dd0a06-a658-4158-9d3b-d6a2ea1234aa.jpg','/files/upload/2014/3/16/a3ad705f-f007-4e98-b1c4-8639e0818dda/2012425029251159810225.jpg','/files/upload/2014/3/16/7cb6a185-f320-4318-b490-8df241dd8169/2012425029251159810225.jpg','files/upload/2014/04/09/8d35d38e-b747-4622-81b8-a8416252308e.jpg',NULL,NULL,NULL,NULL,NULL,'files/upload/2014/04/06/d58c3cb6-50c3-45ba-a05d-127b5731412d.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1','0','0','2014-04-09','0','files/upload/2014/04/09/decb9dc2-5df8-4fdd-ac67-3649f16d1865.jpg','files/upload/2014/04/09/28fd68b1-f9b2-474f-864e-44d8409da9ab.jpg',NULL,NULL,NULL,NULL,NULL,'files/upload/2014/04/06/bc49d7fb-7ecd-4016-8bfd-fb3f0e0b6ab2.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1190113074','899650522','house108@163.com','18154227651','不行说','bxs','YYYjj','安徽省合肥市YYYjj','合肥市',NULL,'安徽省',NULL,'67','2','1','1','我不想说',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'(117.282699,31.866942)',NULL,NULL,'未知','未知','未知',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'files/upload/2014/04/09/a64327ad-2751-48e3-9ae6-978767e333b5.jpg','/files/upload/2014/4/9/block-default.jpg-2b8e8667-454e-45e8-9245-3059d7aea9d9/block-default.jpg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1','0','0','2014-04-09','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `meet_cale` */

DROP TABLE IF EXISTS `meet_cale`;

CREATE TABLE `meet_cale` (
  `ID` decimal(16,0) default NULL,
  `H_ID` decimal(16,0) default NULL,
  `DAYS` date default NULL,
  `M_PERSON` decimal(1,0) default NULL,
  `A_PERSON` decimal(1,0) default NULL,
  `N_PERSON` decimal(1,0) default NULL,
  `STATE` decimal(2,0) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `meet_cale` */

insert  into `meet_cale`(`ID`,`H_ID`,`DAYS`,`M_PERSON`,`A_PERSON`,`N_PERSON`,`STATE`) values ('17861885128','34936499349','2014-02-27','3','3','0','0'),('63082550711','34936499349','2014-02-26','2','2','2','0'),('75670298166','-1316131119','2014-04-30','3','2','2','0');

/*Table structure for table `mod_popedom` */

DROP TABLE IF EXISTS `mod_popedom`;

CREATE TABLE `mod_popedom` (
  `ID` decimal(16,0) NOT NULL COMMENT 'ID',
  `MOD_ID` decimal(16,0) NOT NULL COMMENT '模块ID',
  `USER_ID` decimal(16,0) default NULL COMMENT '用户ID',
  `ROLE_ID` decimal(16,0) default NULL COMMENT '角色ID',
  `PPDM_CODE` decimal(4,0) NOT NULL COMMENT '权限代码',
  PRIMARY KEY  (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `mod_popedom` */

insert  into `mod_popedom`(`ID`,`MOD_ID`,`USER_ID`,`ROLE_ID`,`PPDM_CODE`) values ('10340','10',NULL,'10003','0'),('10341','1010',NULL,'10003','0'),('10637','80',NULL,'10000','0'),('10638','8030',NULL,'10000','0'),('10639','8010',NULL,'10000','0'),('10640','8020',NULL,'10000','0'),('10641','8040',NULL,'10000','0'),('10642','8050',NULL,'10000','0'),('10643','20',NULL,'10000','0'),('10644','2010',NULL,'10000','0'),('10645','2030',NULL,'10000','0'),('10646','2020',NULL,'10000','0'),('10647','90',NULL,'10000','0'),('10648','9020',NULL,'10000','0'),('10649','9010',NULL,'10000','0'),('10650','10',NULL,'10000','0'),('10651','1010',NULL,'10000','0'),('10652','1020',NULL,'10000','0'),('10653','1040',NULL,'10000','0'),('10654','1030',NULL,'10000','0'),('10655','30',NULL,'10000','0'),('10656','3010',NULL,'10000','0'),('10657','3020',NULL,'10000','0'),('10658','100',NULL,'10000','0'),('10659','10010',NULL,'10000','0'),('10660','10020',NULL,'10000','0'),('10661','10030',NULL,'10000','0'),('40300','80',NULL,'165200','0'),('79900','8010',NULL,'165200','0'),('154100','90',NULL,'165200','0'),('208000','9020',NULL,'165200','0'),('278800','8020',NULL,'165200','0'),('934100','9010',NULL,'165200','0'),('286643207','7010',NULL,'10001','0'),('1397337580','9020',NULL,'10001','0'),('2361343896','7050',NULL,'10001','0'),('23097238339','6060',NULL,'10001','0'),('25097558019','6040',NULL,'10001','0'),('29708268614','9010',NULL,'10001','0'),('31675891608','6070',NULL,'10001','0'),('32120953344','7020',NULL,'10001','0'),('32572202042','7060',NULL,'10001','0'),('34130184943','6020',NULL,'10001','0'),('36152922582','60',NULL,'10001','0'),('37798898982','6080',NULL,'10001','0'),('38510582911','8020',NULL,'10001','0'),('40193520710','6063',NULL,'10001','0'),('48517789923','6011',NULL,'10001','0'),('49048002855','90',NULL,'10001','0'),('55776981596','80',NULL,'10001','0'),('56437421922','70',NULL,'10001','0'),('59744840171','7040',NULL,'10001','0'),('81168304925','8010',NULL,'10001','0'),('93966823159','6230',NULL,'10001','0');

/*Table structure for table `my_collect` */

DROP TABLE IF EXISTS `my_collect`;

CREATE TABLE `my_collect` (
  `ID` decimal(16,0) NOT NULL,
  `U_ID` decimal(16,0) default NULL,
  `H_ID` decimal(16,0) default NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `my_collect` */

insert  into `my_collect`(`ID`,`U_ID`,`H_ID`) values ('39626951553','187300','34936499349');

/*Table structure for table `my_read` */

DROP TABLE IF EXISTS `my_read`;

CREATE TABLE `my_read` (
  `ID` decimal(16,0) NOT NULL,
  `U_ID` decimal(16,0) default NULL,
  `AREA` varchar(36) default NULL,
  `PLOAT` varchar(36) default NULL,
  `KEY_TITLE` varchar(36) default NULL,
  `PRICE_LOW` decimal(16,0) default NULL,
  `PRICE_UP` decimal(16,0) default NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `my_read` */

insert  into `my_read`(`ID`,`U_ID`,`AREA`,`PLOAT`,`KEY_TITLE`,`PRICE_LOW`,`PRICE_UP`) values ('49743205635','187300','闵行区','而且 请我','而','23','100');

/*Table structure for table `pinglun` */

DROP TABLE IF EXISTS `pinglun`;

CREATE TABLE `pinglun` (
  `ID` decimal(16,0) NOT NULL,
  `H_ID` decimal(16,0) default NULL,
  `FROM_ID` decimal(16,0) default NULL,
  `TO_ID` decimal(16,0) default NULL,
  `U_NAME` varchar(36) default NULL,
  `COMMENT` varchar(516) default NULL,
  `PTIME` datetime default NULL,
  `STATE` varchar(36) default NULL,
  `IS_READ` varchar(36) default NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `pinglun` */

insert  into `pinglun`(`ID`,`H_ID`,`FROM_ID`,`TO_ID`,`U_NAME`,`COMMENT`,`PTIME`,`STATE`,`IS_READ`) values ('138941632','-1316131119','899650522','0','house108@163.com','好房子','2014-04-10 00:19:50','0','1'),('26769509843','-1316131119','1','138941632','Website','ha方法','2014-04-10 00:00:40','0','1'),('40390332360','-1316131119','1','138941632','Website','xx学校','2014-04-10 00:19:50','0','1');

/*Table structure for table `recommend` */

DROP TABLE IF EXISTS `recommend`;

CREATE TABLE `recommend` (
  `ID` decimal(16,0) NOT NULL,
  `U_ID` decimal(16,0) default NULL,
  `AREA_NAME` varchar(36) default NULL,
  `PRICE_LOWER` decimal(16,0) default NULL,
  `PRICE_UPER` decimal(16,0) default NULL,
  `JTIME` datetime default NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='记录用户近一个月的搜索条件（价格，行政区';

/*Data for the table `recommend` */

insert  into `recommend`(`ID`,`U_ID`,`AREA_NAME`,`PRICE_LOWER`,`PRICE_UPER`,`JTIME`) values ('51939632233','29994232235','33','33','44','2014-02-27 18:37:43');

/*Table structure for table `role_info` */

DROP TABLE IF EXISTS `role_info`;

CREATE TABLE `role_info` (
  `ID` decimal(16,0) NOT NULL,
  `ROLE_NAME` varchar(60) default NULL,
  `ROLE_DESC` varchar(250) default NULL,
  `ORDER_VALUE` decimal(4,0) default NULL,
  `IS_LOCK` decimal(1,0) default NULL,
  `IS_DEL` decimal(1,0) default NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `role_info` */

insert  into `role_info`(`ID`,`ROLE_NAME`,`ROLE_DESC`,`ORDER_VALUE`,`IS_LOCK`,`IS_DEL`) values ('10001','超级管理员','super manager','0',NULL,'0');

/*Table structure for table `role_user` */

DROP TABLE IF EXISTS `role_user`;

CREATE TABLE `role_user` (
  `ID` decimal(16,0) NOT NULL COMMENT 'ID',
  `ROLE_ID` decimal(16,0) NOT NULL COMMENT '角色ID',
  `USER_ID` decimal(16,0) NOT NULL COMMENT '用户ID',
  PRIMARY KEY  (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `role_user` */

insert  into `role_user`(`ID`,`ROLE_ID`,`USER_ID`) values ('86793044663','10001','1');

/*Table structure for table `sys_module` */

DROP TABLE IF EXISTS `sys_module`;

CREATE TABLE `sys_module` (
  `MOD_ID` decimal(16,0) NOT NULL,
  `PAR_ID` decimal(16,0) default '0',
  `MOD_NAME` varchar(40) default NULL,
  `MOD_DESC` varchar(250) default NULL,
  `MOD_URL` varchar(250) default NULL,
  `PPDM_CODE` decimal(4,0) default NULL,
  `ORDER_VALUE` decimal(4,0) default NULL,
  `IS_PUBLIC` decimal(1,0) default NULL,
  `IS_LOCK` decimal(1,0) default NULL,
  `IS_DEL` decimal(1,0) default NULL,
  `MOD_USER_TYPE` decimal(1,0) default NULL,
  PRIMARY KEY  (`MOD_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sys_module` */

insert  into `sys_module`(`MOD_ID`,`PAR_ID`,`MOD_NAME`,`MOD_DESC`,`MOD_URL`,`PPDM_CODE`,`ORDER_VALUE`,`IS_PUBLIC`,`IS_LOCK`,`IS_DEL`,`MOD_USER_TYPE`) values ('0','60',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('60','1','业务管理','business',NULL,NULL,'60',NULL,NULL,'0',NULL),('70','1','基础数据','baseInfo',NULL,NULL,NULL,NULL,NULL,'0',NULL),('80','1','系统管理','system',NULL,NULL,'80',NULL,NULL,'0',NULL),('90','1','账户管理','myInfo',NULL,NULL,'90',NULL,NULL,'0',NULL),('6011','60','房源信息','HOUSE_INFO','HouseInfo.do',NULL,'6011',NULL,NULL,'0',NULL),('6020','60','评论管理','pinglun','Pinglun.do',NULL,'6020',NULL,NULL,'0',NULL),('6040','60','订阅管理','MyRead','MyRead.do',NULL,'6040',NULL,NULL,'0',NULL),('6050','60','日程控件','Calendar','Calendar.do',NULL,'6050',NULL,NULL,'1',NULL),('6060','60','房东预约','MeetCale','MeetCale.do',NULL,'6060',NULL,NULL,'0',NULL),('6063','60','客户预约','CustomOrder','CustomOrder.do',NULL,'6055',NULL,NULL,'0',NULL),('6070','60','房客信誉值','CsinceRecord','CsinceRecord.do',NULL,'6060',NULL,NULL,'0',NULL),('6080','60','房东信誉值','FsinceRecord','FsinceRecord.do',NULL,'6080',NULL,NULL,'0',NULL),('6230','60','搜藏管理','MyCollect','MyCollect.do',NULL,'6030',NULL,NULL,'0',NULL),('7010','70','行政区域','area','Area.do',NULL,'7010',NULL,NULL,'0',NULL),('7020','70','用户搜索记录','recommend','Recommend.do',NULL,'7020',NULL,NULL,'0',NULL),('7030','70','智能推荐查询','recommend','Recommend.do?method=init',NULL,'7030',NULL,NULL,'1',NULL),('7040','70','邮件营销','Email','Email.do',NULL,'7040',NULL,NULL,'0',NULL),('7050','70','短信营销','Message','Message.do',NULL,'7050',NULL,NULL,'0',NULL),('7060','70','设施管理','Axis','Axis.do',NULL,'7060',NULL,NULL,'0',NULL),('8010','80','用户管理','user','UserInfo.do',NULL,'8010',NULL,NULL,'0',NULL),('8020','80','角色管理','role','RoleInfo.do',NULL,'8020',NULL,NULL,'0',NULL),('9010','90','个人信息','person','Profile.do',NULL,NULL,NULL,NULL,'0',NULL),('9020','90','密码管理','password','Password.do',NULL,NULL,NULL,NULL,'0',NULL);

/*Table structure for table `user_info` */

DROP TABLE IF EXISTS `user_info`;

CREATE TABLE `user_info` (
  `ID` decimal(16,0) NOT NULL COMMENT 'ID',
  `USER_NAME` varchar(60) NOT NULL COMMENT '登录名',
  `PASSWORDS` varchar(60) NOT NULL default '' COMMENT '使用算法加密',
  `SEX` decimal(1,0) default '0' COMMENT '1.男,2.女,9不详',
  `BIRTHDAY` date default NULL COMMENT '生日',
  `User_TYPE` decimal(1,0) default NULL COMMENT '1.用户 2 管理员',
  `MOBILE` varchar(60) default NULL COMMENT '手机',
  `ADDR` varchar(255) default NULL COMMENT '住址',
  `EMAIL` varchar(60) default NULL COMMENT '电子信箱',
  `type` varchar(30) default NULL COMMENT '管理员,用户',
  `IS_INVODE` decimal(1,0) default NULL COMMENT '是否订阅UNISITE',
  `C_CION` decimal(16,0) default NULL COMMENT '用户信誉值',
  `F_CION` decimal(16,0) default NULL COMMENT '房东信誉值',
  `C_JF` decimal(16,0) default NULL COMMENT '用户积分',
  `F_JF` decimal(16,0) default NULL COMMENT '房东积分',
  `U_PIC` varchar(128) default NULL COMMENT '图像',
  `STATE` decimal(1,0) default NULL COMMENT '状态 0.可用,1,不可用',
  `IS_REMEMBER` decimal(1,0) default NULL COMMENT '0:是 1：否',
  `email_union` varchar(60) default NULL COMMENT 'EMAIL小写唯一标识',
  PRIMARY KEY  (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `user_info` */

insert  into `user_info`(`ID`,`USER_NAME`,`PASSWORDS`,`SEX`,`BIRTHDAY`,`User_TYPE`,`MOBILE`,`ADDR`,`EMAIL`,`type`,`IS_INVODE`,`C_CION`,`F_CION`,`C_JF`,`F_JF`,`U_PIC`,`STATE`,`IS_REMEMBER`,`email_union`) values ('-514876807','house1081@163.com','F9B151DFFC343A03144CFE880AC4710A','0',NULL,NULL,NULL,NULL,'house1081@163.com','用户','0','3','5',NULL,NULL,NULL,'1',NULL,'house1081@163.com'),('-57609457','Jone','F9B151DFFC343A03144CFE880AC4710A','0',NULL,NULL,'18154227651',NULL,'505185679@qq.com','用户','0','3','5',NULL,NULL,NULL,'0',NULL,'505185679@qq.com'),('1','admin','81DC9BDB52D04DC20036DBD8313ED055','1','2012-07-04',NULL,'13434343232','SDFSDF','213123@123.COM','管理员','1','0','0','0','0','files/upload/2014/02/26/3e6e2cdc-e659-4c6e-be20-19b9259bf834.jpg','0','1','213123@123.com'),('899650522','house108@163.com','F9B151DFFC343A03144CFE880AC4710A','0',NULL,NULL,'18154227651',NULL,'house108@163.com','用户','0','3','5',NULL,NULL,NULL,'0',NULL,'house108@163.com');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
