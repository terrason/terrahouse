update house_info set
h_pic='images/house-temp/01.jpg',h_hxt='images/house-temp/01-type.jpg'
,h_pic1='images/house-temp/02.jpg',h_hxt1='images/house-temp/02-type.jpg'
,h_pic2='images/house-temp/03.jpg',h_hxt2='images/house-temp/03-type.jpg'
,h_pic3='images/house-temp/04.jpg',h_hxt3='images/house-temp/04-type.jpg'
,h_pic4='images/house-temp/05.jpg',h_hxt4='images/house-temp/05-type.jpg'
,h_pic5='images/house-temp/06.jpg',h_hxt5='images/house-temp/06-type.jpg'
,h_pic6='images/house-temp/07.jpg',h_hxt6='images/house-temp/07-type.jpg'
,h_pic7='images/house-temp/08.jpg',h_hxt7='images/house-temp/08-type.jpg'
,h_pic8='images/house-temp/09.jpg',h_hxt8='images/house-temp/09-type.jpg'
,h_pic9='images/house-temp/10.jpg',h_hxt9='images/house-temp/10-type.jpg'
,h_pic10='images/house-temp/11.jpg'
,h_pic11='images/house-temp/12.jpg'
,h_detail='<div class="paragraph">该套住房两室两厅，房型是标准的南北通户型，客厅和主卧位于南边，餐厅和次卧位于北边，洗手间位于两卧室之间。整套住房南北通透，每间房间都是全明设计。房屋建筑面积81平米，因为没有电梯和其他多余的公摊面积，房屋的得房率很高。虽然是2楼，但采光好。房屋是简单装修，距离上一次装修也有些年头，保养的还不错，房屋设施齐全，可直接入住。</div><div class="paragraph">两间卧室内都有舒适的飘窗，天气好的时候可以坐在上面喝茶、和朋友聊天。餐厅紧挨着厨房，也十分方便。前后两个阳台为洗衣和晾晒提供了更多的空间。 </div>'
,comm_detail='<div class="paragraph">小区建于2003年，整体呈狭长型，正门入口朝南，向北共有17幢楼房。小区西侧有一条小河，有时会看到白鹭飞过，河边柳树成荫，还有错落的长椅，闲来无事时在河边锻炼休憩都是不错的选择。小区非常安静，绿化覆盖率很高，再加上周边清脆的鸟鸣声，会有种生活在乡村田园里的感觉。小区无地下停车位，所有车辆均停于地面，规划的比较好，无乱停现象，但也因此使得楼房之间的过道（因被停车位占据而）显得较为拥挤。</div> '
,arround_detail='<div class="paragraph">小区距离莘庄两大商圈（龙之梦和仲盛商城）都不过1.5公里，可享受到商圈给这里的居民带来的便利生活。小区近邻莘庄镇小学和闵行中心医院，为小区的居民提供了良好的学区和医疗条件。小区正门百米附近生活设施缺乏，春申路地铁站旁边的乐天玛特是最靠近小区的大型超市，步行约10分钟时间。小区加油非常方便，在半公里之外的沪闵公路上就有2座加油站。</div><div class="paragraph">小区后门出口有一个篮球场和一些健身器材，喜欢体育锻炼的人可以周末在这里打打球，活动活动胫骨。附近还有蔬菜市场，方便平日的采购。 </div>'
,h_traffic='<div class="paragraph">步行7、8分钟即可到达春申路地铁站（5号线），为您平时的出行提供了便捷的轨道交通。小区正门口有通往莘庄的708路公交，连接了莘庄龙之梦和莘庄地铁站。出门再多走几分钟，靠近春申路地铁站的位置有多条通往徐家汇和虹桥枢纽的公交线路。</div><div class="paragraph">小区附近的公路网络也十分发达。沪闵公路连接沪闵高架，直通徐家汇和中、内环高架路。如果要去外围的其他区域，也可从春申路直接上S20外环高架路。</div><div class="paragraph">总体来说，小区的交通情况在外环外这个范围内还是算非常便利的。 </div>'
;
drop table if exists axis;
CREATE TABLE `axis` (
  `ID` int(16) NOT NULL AUTO_INCREMENT,
  `S_XY` varchar(100) DEFAULT NULL,
  `S_NAME` varchar(100) DEFAULT NULL,
  `S_TYPE` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
insert into axis(S_XY,S_NAME,S_TYPE) values('(121.385733,31.136075)','黎安公园','0');
insert into axis(S_XY,S_NAME,S_TYPE) values('(121.393256,31.110749)','莘城中央公园','0');
insert into axis(S_XY,S_NAME,S_TYPE) values('(121.379785,31.108036)','莘庄公园','0');
insert into axis(S_XY,S_NAME,S_TYPE) values('(121.382731,31.113748)','闵行区中心医院','2');
insert into axis(S_XY,S_NAME,S_TYPE) values('(121.393122,31.112156)','仲盛世界商场','3');
insert into axis(S_XY,S_NAME,S_TYPE) values('(121.385666,31.11405)','莘庄凯德龙之梦','3');
insert into axis(S_XY,S_NAME,S_TYPE) values('(121.384662,31.114228)','乐购莘东店','4');
insert into axis(S_XY,S_NAME,S_TYPE) values('(121.378563,31.124739)','乐购莘庄店','4');
insert into axis(S_XY,S_NAME,S_TYPE) values('(121.370599,31.105741)','乐购莘松店','4');
insert into axis(S_XY,S_NAME,S_TYPE) values('(121.37959,31.137783)','大润发港澳店','4');
insert into axis(S_XY,S_NAME,S_TYPE) values('(121.394011,31.112527)','家乐福莘庄店','4');
insert into axis(S_XY,S_NAME,S_TYPE) values('(121.394691,31.104156)','乐天玛特春申店','4');
insert into axis(S_XY,S_NAME,S_TYPE) values('(121.397758,31.096542)','农工商银都路店','4');
insert into axis(S_XY,S_NAME,S_TYPE) values('(121.381788,31.112821)','世纪联华莘庄店','4');
insert into axis(S_XY,S_NAME,S_TYPE) values('(121.378644,31.107905)','世纪联华莘松路店','4');
insert into axis(S_XY,S_NAME,S_TYPE) values('(121.395611,31.111321)','闵行区图书馆','5');
insert into axis(S_XY,S_NAME,S_TYPE) values('(121.392978,31.112967)','百安居莘庄店','6');

truncate table community;
truncate table area;
insert into area(id,area_name)values(99,'其 他');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(80,7,'动物园','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(79,7,'北新泾','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(78,7,'仙霞新村','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(77,7,'天山路','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(76,7,'新华路','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(75,7,'古北','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(74,7,'中山公园','/images/block-cover.jpg');
insert into area(id,area_name)values(7,'长 宁');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(73,6,'彭浦','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(72,6,'宝山路','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(71,6,'不夜城','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(70,6,'芷江西路','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(69,6,'西藏北路','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(68,6,'大宁','/images/block-cover.jpg');
insert into area(id,area_name)values(6,'闸 北');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(67,5,'万里城','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(66,5,'宜川','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(65,5,'长风','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(64,5,'梅川路','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(63,5,'甘泉路','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(62,5,'石泉路','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(61,5,'桃浦','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(60,5,'长征','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(59,5,'武宁路','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(58,5,'真如','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(57,5,'曹杨','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(56,5,'长寿路','/images/block-cover.jpg');
insert into area(id,area_name)values(5,'普 陀');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(55,4,'马桥','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(54,4,'江川','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(53,4,'吴泾','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(52,4,'华漕','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(51,4,'老闵行','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(50,4,'梅陇','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(49,4,'龙柏','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(48,4,'古美','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(47,4,'虹桥','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(46,4,'春申','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(45,4,'七宝','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(44,4,'莘庄','/images/block-cover.jpg');
insert into area(id,area_name)values(4,'闵 行');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(43,3,'中原','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(42,3,'鞍山','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(41,3,'平凉路','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(40,3,'控江路','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(39,3,'黄兴公园','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(38,3,'新江湾城','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(37,3,'五角场','/images/block-cover.jpg');
insert into area(id,area_name)values(3,'杨 浦');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(36,2,'北外滩','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(35,2,'广中路','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(34,2,'提篮桥','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(33,2,'临平路','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(32,2,'鲁迅公园','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(31,2,'四川北路','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(30,2,'江湾','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(29,2,'凉城','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(28,2,'曲阳','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(27,2,'大柏树','/images/block-cover.jpg');
insert into area(id,area_name)values(2,'虹 口');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(26,1,'临港新城','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(25,1,'行头','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(24,1,'惠南','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(23,1,'曹路','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(22,1,'金桥','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(21,1,'世博','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(20,1,'新场','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(19,1,'周康','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(18,1,'川沙','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(17,1,'外高桥','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(16,1,'唐镇','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(15,1,'周浦','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(14,1,'北蔡','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(13,1,'周家渡','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(12,1,'张江','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(11,1,'三林','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(10,1,'上南地区','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(9,1,'碧云','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(8,1,'八佰伴','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(7,1,'塘桥','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(6,1,'洋泾','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(5,1,'潍坊','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(4,1,'源深','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(3,1,'世纪公园','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(2,1,'联洋','/images/block-cover.jpg');
insert into community(id,AREA_ID,COMMUNITY_NAME,PICTURE)values(1,1,'陆家嘴','/images/block-cover.jpg');
insert into area(id,area_name)values(1,'浦 东');

update community set
COMMUNITY_DESC='
				<p>
					莘庄位于上海市中心城区的西南部，隶属于闵行区，是闵行区政府所在地。这里也聚集了如区图书馆、上海城市剧院、区博物馆、区中心医院等重要设施。无论从交通条件、商业水平、居住环境均可称为闵行区最重要的街区之一。
				</p>
				<p style="display: block;">
					区域内的轨道交通和高速公路造就了莘庄极为发达的立体化交通网络。轨道交通1号线和5号线交汇于此，其中1号线直接连接了徐家汇、人民广场等市区核心商业腹地，5号线向西南方向连接上海交大、华师大等沪上知名学府，以及闵行开发区等重要工业基地。上海之最的莘庄立交亦连接了向北向东的外环高架路，以及直通徐家汇的沪闵高架路，沪昆高速及沪金高速更是将莘庄的触角向外延伸至宁波、杭州等江浙地区。便利的交通带来了人口的迁入和区域的发展，也使得莘庄站成了“臭名昭著”的最挤站点之一。每天早上7:30到8:30的高峰时段，车站不得不采取限流措施来保障乘客顺利进站。
				</p>
				<p style="display: block;">
					通过近年来不断的招商引资，莘庄形成了以分别坐落在莘庄地铁站南北广场的仲盛世界商场和凯德龙之梦为主体的商业圈，带来了极为优越的餐饮购物休闲娱乐的一体化生活环境，成为了该区域乃至整个闵行区最为核心的商业地带。南北广场生活设施的集中化让生活在莘庄站附近的人们享受到了完善的生活圈，无论是购物、休闲、餐饮、就医，都非常方便，使得周边的楼盘成为炙手可热的抢手货。
				</p>
				<p style="display: block;">
					莘庄以环境优美闻名，不仅有以梅花享誉沪上的五星级公园——莘庄公园，还有莘城中央公园、黎安公园以及众多大型绿地，整个街区绿化覆盖率达到40%。另外，分布在各处的乐购、家乐福、大润发、世纪联华、乐天玛特等超大型购物超市，为街区内的居民带来极为便利的日常生活采购条件。
				</p>
			',
COMMUNTIY_DL_INFO='<p>
					莘庄街区北靠顾戴路，东边以外环高架路为界，南临银都路，西至中春路。与其临近地街区有<a href="#">七宝</a>、<a href="#">梅陇</a>、<a href="#">古美</a>、<a href="#">颛桥</a>，西靠松江区
				</p>',
COMMUNTIY_XY='(121.391623,31.116581)',
COMMUNTIY_BJXY='(121.384458, 31.142556);(121.378996, 31.141444);(121.370893, 31.138384);(121.36174, 31.134041);(121.365423, 31.130571);(121.367201, 31.12755);(121.370004, 31.120888);(121.362566, 31.118616);(121.354445, 31.114257);(121.35838, 31.103775);(121.375124, 31.108939);(121.379311, 31.099323);(121.383784, 31.090734);(121.396064, 31.095527);(121.407966, 31.100405);(121.40405, 31.109928);(121.394941, 31.12384);(121.384458, 31.142556)',
COMMUNTIY_GDS='4,3,12',
DT_RMGC='60',
GJ_RMGC='35',
DT_LJZ='70',
GJ_LJZ='42',
DT_XJH='40',
GJ_XJH='27',
DT_JAS='60',
GJ_JAS='35',
DT_WJC='90',
GJ_WJC='60',
COMMUNITY_COMMENT='
			<div class="section">
				<p>以仲盛世界商城以及凯德龙之梦为主体的核心商业圈，为居民提供了极为优越的餐饮、购物、休闲、娱乐一体化热点场所。</p>
				<div class="clearfix">
					<img alt="" src="/cns/images/block-temp/block-img-01.jpg">
				</div>
				<div class="clearfix">
					<img alt="" src="/cns/images/block-temp/block-img-02.jpg">
				</div>
				<div class="clearfix">
					<img alt="" src="/cns/images/block-temp/block-img-03.jpg">
				</div>
			</div>
			<div class="section">
				<p>在这里不用去到市中心也可以享受到让您流连忘返的繁华景致，无论是老者、儿童，还是您，都可以在街区内的众多场所享受美妙的午后时光。</p>
				<div class="clearfix">
					<img alt="" src="/cns/images/block-temp/block-img-11.jpg">
				</div>
				<div class="clearfix">
					<img style="width: 490px; float: left;" alt="" src="/cns/images/block-temp/block-img-12.jpg"> <img style="width: 490px; float: right;" alt="" src="images/block-temp/block-img-13.jpg">
				</div>
				<div class="clearfix">
					<img alt="" src="/cns/images/block-temp/block-img-14.jpg">
				</div>
				<div class="clearfix">
					<img style="width: 490px; float: left;" alt="" src="/cns/images/block-temp/block-img-15.jpg"> <img style="width: 490px; float: right;" alt="" src="images/block-temp/block-img-16.jpg">
				</div>
				<div class="clearfix">
					<img style="width: 490px; float: left;" alt="" src="/cns/images/block-temp/block-img-17.jpg"> <img style="width: 490px; float: right;" alt="" src="images/block-temp/block-img-18.jpg">
				</div>
			</div>
			<div class="section">
				<p>上海城市剧院，闵行图书馆，闵行博物馆、运动场等众多设施可以让您在街区内享受丰富的精神文化生活。</p>
				<div class="clearfix">
					<img alt="" src="/cns/images/block-temp/block-img-21.jpg">
				</div>
				<div class="clearfix">
					<img style="width: 490px; float: left;" alt="" src="/cns/images/block-temp/block-img-22.jpg"> <img style="width: 490px; float: right;" alt="" src="images/block-temp/block-img-23.jpg">
				</div>
				<div class="clearfix">
					<img alt="" src="/cns/images/block-temp/block-img-24.jpg">
				</div>
				<div class="clearfix">
					<img style="width: 490px; float: left;" alt="" src="/cns/images/block-temp/block-img-25.jpg"> <img style="width: 490px; float: right;" alt="" src="images/block-temp/block-img-26.jpg">
				</div>
			</div>
			<div class="section">
				<p>随处可见的公园及绿地为您提供了休闲散步的良好场所。</p>
				<div class="clearfix">
					<img style="width: 490px; float: left;" alt="" src="/cns/images/block-temp/block-img-31.jpg"> <img style="width: 490px; float: right;" alt="" src="images/block-temp/block-img-32.jpg">
				</div>
				<div class="clearfix">
					<img alt="" src="/cns/images/block-temp/block-img-33.jpg">
				</div>
				<div class="clearfix">
					<img style="width: 490px; float: left;" alt="" src="/cns/images/block-temp/block-img-34.jpg"> <img style="width: 490px; float: right;" alt="" src="images/block-temp/block-img-35.jpg">
				</div>
				<div class="clearfix">
					<img alt="" src="/cns/images/block-temp/block-img-36.jpg">
				</div>
				<div class="clearfix">
					<img style="width: 490px; float: left;" alt="" src="/cns/images/block-temp/block-img-37.jpg"> <img style="width: 490px; float: right;" alt="" src="images/block-temp/block-img-38.jpg">
				</div>
			</div>
		';

show tables;
select * from community;

DROP TABLE IF EXISTS `community_pinglun`;
CREATE TABLE `community_pinglun` (
    `ID` decimal(16,0) default NULL,
    `COMMUNITY` decimal(16,0) default NULL,
    `comment` varchar(1000) default NULL,
    `USER_id` decimal(16,0) default NULL,
    `EMAIL` varchar(100) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
